<?php
/*
	Template Name: Template - Home Page
*/

get_header(); 
$introBg = wp_get_attachment_image_src((get_field('intro_img', 'options')),'full');
$introTagline = get_field('intro_tagline', 'options');

//Home Setting
$vlogo = wp_get_attachment_image_src((get_field('about_agent_verified', 'options')),'full');
$headerTitle = get_field('about_header_title', 'options');
$agentContent = get_field('about_description', 'options');
$contactLabel = get_field('about_contact_label', 'options');
$contactNumber = get_field('about_contact_number', 'options');
$emailAddress = get_field('about_agent_email', 'options');
$logoEmailAdd = get_field('about_email_img', 'options');
?>




<body class="intro-done is-ready">
<div id="container" class="app-started">
    <!-- Menu -->
    <?php get_template_part('menu'); ?>
    
    <!--Content Page-->
    <?php get_template_part('section/home/home_topsimpleheader'); ?>
    <?php get_template_part('section/home/home_topclientheader'); ?>
    <?php get_template_part('section/home/home_mainsection'); ?>
    <?php get_template_part('section/home/home_buysellinvest'); ?>
    <?php //get_template_part('section/home/home_featuredlisting'); ?>
    <?php get_template_part('section/home/home_recentblog'); ?>
    <?php get_template_part('section/bottom_mail'); ?>
</div>
<a id='backTop'>Back To Top</a>
</body>
</html>
