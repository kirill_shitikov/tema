<?php
/*
	Template Name: Template - About Me
*/
get_header(); 
?>
<body class="intro-done is-ready">
<div id="container" class="app-started">
    <!-- Menu -->
    <?php get_template_part('menu'); ?>
    
    <!--Content Page-->
    <?php get_template_part('section/topsimpleheader'); ?>
    <?php get_template_part('section/aboutme/aboutme'); ?>
    <?php get_template_part('section/aboutme/aboutme_testimonial'); ?>
    <?php get_template_part('section/aboutme/aboutme_soldlisting'); ?>
    <?php get_template_part('section/aboutme/aboutme_skillset'); ?>
    <?php get_template_part('section/bottom_mail'); ?>
</div>
<a id='backTop'>Back To Top</a>


</body>
</html>
