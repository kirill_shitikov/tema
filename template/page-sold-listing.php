<?php
/*
	Template Name: Page - Sold Listing
*/
get_header(); 

wp_reset_postdata(); 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$soldlist =  array(
	//'showposts'		  => 1,
	'posts_per_page'  => 1,
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'paged'			  => $paged
);
?>
    <?php
	   query_posts($soldlist);
	   
		 if (have_posts())  {
			while (have_posts()) : the_post();
				$content = get_the_content();
				//$custname = get_post_custom_values('custest_custname');
	?>                                               		
          <div class="entry-single"><?=$content;?></div>
     <?php 
				endwhile; 
				}
				// Reset Query
wp_reset_query();
			?>
    <?php //endwhile; endif; //} ?>
