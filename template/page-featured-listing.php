<?php
/*
	Template Name: Page - Featured Listing
*/
get_header(); 

wp_reset_postdata(); 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$soldlist =  array(
	'posts_per_page'  => 1,
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'paged'			  => $paged
);

$feaShortCode = get_field('homefea_shortcode', 'options');
?>
    <?php
	   query_posts($soldlist);
	   
		 if (have_posts())  {
			while (have_posts()) : the_post();
				$content = get_the_content();
	?>                                               		
          <div class="entry-single featuredFrame noFeat"><?=$content;?></div>
     <?php 
				endwhile; 
				}
	wp_reset_query();
			?>

<div class="homefeature hideFeatureListing">
	<?=$feaShortCode;?>
</div>