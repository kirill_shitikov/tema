<?php
/*
	Template Name: Template - Communities
*/
get_header(); 
?>
<body class="intro-done sidebar-open is-ready">
<div id="container" class="app-started">
    <!-- Menu -->
    <?php get_template_part('menu'); ?>
    
    <!--Content Page-->
    <?php get_template_part('section/topsimpleheader'); ?>
    <?php get_template_part('section/community/communities'); ?>
    <?php //get_template_part('section/bottom_mail'); ?>
</div>
</body>
</html>
