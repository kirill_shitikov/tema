<?php
/*
	Template Name: Template - Properties Search
*/
get_header(); 
?>
<body class="intro-done is-ready">
<div id="container" class="app-started">
    <!-- Menu -->
    <?php get_template_part('menu'); ?>
    
    <!--Content Page-->
    <?php get_template_part('section/topsimpleheader'); ?>
    <?php get_template_part('section/properties_search/properties_search'); ?>
    <?php get_template_part('section/bottom_mail'); ?>
</div>
<a id='backTop'>Back To Top</a>
</body>
</html>
