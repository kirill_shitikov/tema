<?php
get_header(); 

$introBg = wp_get_attachment_image_src((get_field('intro_img', 'options')),'full');
$introTagline = get_field('intro_tagline', 'options');

?>
<body><div id="bgscroll"></div>
<div id="container">
<?php get_template_part('menu'); ?>
<div id="main-view" class="content-page mainviewScroll">
	
    <div class="view enable-smooth view-visible">
        <article class="view-article content-view">
            
            <section class="intro-holder"> <!--Section Intro Start-->
            	<?php //get_template_part('home-about'); ?>
                
                <div class="intro-text-right vertical-center" data-postname="28">
                    <div class="intro-text-right-inner vertical-el">
                        <div class="row"></div>
                    </div>
                </div>
                
               <!--INTRO START-->
                <div class="intro">
                   <!-- <div class="cursor" style="transform: matrix(1, 0, 0, 1, 1853, 1);"></div><img src="< ?php bloginfo('template_directory'); ?>/images/scrolldown.png">-->
                   <div class="intro-scroll scroll" id="intro-scroll" style="visibility: hidden;"></div>
                    <div class="intro-background">
                        <div class="intro-border horizontal top"></div>
                        <div class="intro-border vertical right"></div>
                        <div class="intro-border horizontal bottom"></div>
                        <div class="intro-border vertical left"></div>
                        <div class="el" style="background-image: url(<?=$introBg[0];?>);"></div>
                        
                    </div>
                    <div class="intro-text"  id="intro-text-bottom" style="visibility: hidden;">
                         <h2  style = " background-color: rgba(0, 0, 0, 0.5);
                            padding-bottom: 4px;
                            padding-top: 4px;"><?=$introTagline;?></h2>
                    </div>
                     
                </div>
                <!--INTRO END-->
            </section> <!--Section Intro End-->
        </article>
    </div>
</div>
<!--<div id="overlay" class="overlay"></div>-->
</div>
<?php
$agentName = get_field('agent_name', 'options');
//$chars = explode(" ",$agentName);

?>
<div id="preloader" class="loader">
	<div class="logo">
    	<?php
        $chars = str_split($agentName);
        //var_dump($chars);
        $all='';
		foreach($chars as $char){
			$all .=  '<span class="els">'.trim($char).'&nbsp;&nbsp;</span>';
		}
        echo $all;
		?>
	</div>
</div>

<script type="text/javascript" data-main="<?=get_template_directory_uri()."/assets/preload/main-build"?>" src="<?=get_template_directory_uri()."/assets/preload/require.js"?>"></script>