<?php 

add_action('wp_ajax_um_lightbox', 'um_lightbox');
add_action('wp_ajax_nopriv_um_lightbox', 'um_lightbox');


function um_lightbox(){
	
	$post_ID = $_POST['post_id'];
	
	$slider = get_field('slider',$post_ID);
	?> 
		<div class="um_ex_lightbox um_lightbox">
		<div class="um_helper">
			<div class="um_middle">
	
				<div class="container">
				
					<section class="lightSliderContainer hasDec mainBgColor">
						
						<div class="roomsWideSlider royalSlider rsDefault">
							<?php if($slider): ?>
								<?php foreach($slider as $slide): ?>	
								<div class="sliderItem">
									<?php $type = $slide['type']; ?>
									<?php if($type == 'image'):?>
										<?php $image = wp_get_attachment_image_src($slide['image'],'full');  ?>
										<img class="rsImg img-responsive" src="<?php echo $image[0]; ?>" data-rsTmb="<?php echo $image[0]; ?>"  />
									<?php else: ?>
											<?php $videoType = $slide['video_type']; ?>
											<?php if($videoType == 'selfhost'): ?>
												
												<?php $video_poster = wp_get_attachment_image_src($slide['video_poster'],'full'); ?>
													<video preload="none"  poster="<?php  echo $video_poster[0];?>" width="480" height="270">
															<source type="video/mp4" src="<?php echo $slide['video']; ?>">
													</video>
													<div class="rsPlayBtn selfPlay">
															<div class="rsPlayBtnIcon"></div>
													</div>
													
												
											<?php elseif($videoType == 'youtube'): ?>
														<?php 
															$video_id = $slide['video_url']; 
															parse_str( parse_url( $video_id, PHP_URL_QUERY ), $youTubeID );
														?>	
														<?php 
															$poster = "http://img.youtube.com/vi/". $youTubeID['v']."/hqdefault.jpg";
															if($slide['video_poster']){ 
																
																$poster = wp_get_attachment_image_src($slide['video_poster'],'full'); 
																$poster = $poster[0];
															}
														?>
															
													<img class="rsImg img-responsive" src="<?php echo $poster; ?>" data-rsVideo="<?php echo $video_id; ?>" />
														
											<?php else: ?>
											
													<?php
														$video_id = $slide['video_url']; 
														preg_match('~^http://(?:www\.)?vimeo\.com/(?:clip:)?(\d+)~', $video_id , $vimeoID);
														$link = 'http://vimeo.com/api/v2/video/'.$vimeoID[1].'.php';
														$link = wp_remote_get($link);
														$link = wp_remote_retrieve_body($link);
														$html_returned = unserialize($link);
														$thumb_url = $html_returned[0]['thumbnail_large'];
												
														$poster = $thumb_url;
														if($slide['video_poster']){ 
															
															$poster = wp_get_attachment_image_src($slide['video_poster'],'full'); 
															$poster = $poster[0];
														}
														?>
													
													<img class="rsImg img-responsive" src="<?php echo $poster; ?>" data-rsVideo="<?php echo $video_id; ?>" />
													
											<?php endif; ?>		
									<?php endif; ?>
								</div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
						
						<div class="closeLightbox">
							<div class="triangle"><i class="brandColor fa fa-plus"></i></div>
						</div>
					</section>
											
				</div>
	
			</div>
		</div>	
	<?php
	die();
}

 ?>