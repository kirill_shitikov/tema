<?php
class um_instagram_widget extends WP_Widget{
	function um_instagram_widget() 
	{
		parent::WP_Widget(false, $name = 'Umbrella > Instagram');
	}
	
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters('widget_title', $instance['title']);	
		$user_id	= $instance['user_id'];
		$access_token = $instance['access_token'];
		$count_images = $instance['images'];
		echo $before_widget;
		?>
		
				<h4><?php echo $title;?></h4>
				<div class="instagramContent" id="insta"></div>
		
					<script>
				    jQuery(document).ready(function($){
						jQuery(function() {						
							$.ajax({
								type: "GET",
								dataType: "jsonp",
								cache: false,
								url: "https://api.instagram.com/v1/users/<?php echo $user_id;?>/media/recent/?access_token=<?php echo $access_token;?>",
								success: function(data) {
                                    var count = "<?php echo $count_images; ?>";
                                    if(data.data.length >= count){
                                        count = count;
                                    }
                                    else{
                                        count = data.data.length;
                                    }

									for (var i = 0; i < count; i++) {
                                        $("#insta").append("<div class='instagram-placeholder'><a target='_blank' href='" + data.data[i].link +"'><img class='instagram-image' src='" + data.data[i].images.low_resolution.url +"' /></a></div>");
                                    }
													
								}
							});
						});
					});
					</script>	
		<?php
		 echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);	
		$instance['user_id'] = strip_tags($new_instance['user_id']);	
		$instance['access_token'] = strip_tags($new_instance['access_token']);		
		$instance['images'] = strip_tags($new_instance['images']);	
			
		return $instance;
	}
	
	function form($instance)
	{
		$title = isset($instance['title']) ? esc_attr($instance['title']) : "";
		$user_id = isset($instance['user_id']) ? esc_attr($instance['user_id']) : "";
		$access_token = isset($instance['access_token']) ? esc_attr($instance['access_token']) : "";
		$images = isset($instance['images']) ? esc_attr($instance['images']) : 8;
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title',"um_lang"); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('user_id'); ?>"><?php _e('User ID',"um_lang"); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('user_id'); ?>" name="<?php echo $this->get_field_name('user_id'); ?>" type="text" value="<?php echo $user_id; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('access_token'); ?>"><?php _e('Access Token',"um_lang"); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('access_token'); ?>" name="<?php echo $this->get_field_name('access_token'); ?>" type="text" value="<?php echo $access_token; ?>" />
		</p>
		
		
		<p>
			<label for="<?php echo $this->get_field_id('images'); ?>"><?php _e('Show Images',"um_lang"); ?></label>
			<input min="1" class="widefat" id="<?php echo $this->get_field_id('images'); ?>" name="<?php echo $this->get_field_name('images'); ?>" type="number" value="<?php echo $images; ?>" />
		</p>
				
		<?php
	}
}

function register_um_instagram_widget() {			
	register_widget('um_instagram_widget');			
}
add_action('widgets_init', 'register_um_instagram_widget');
?>