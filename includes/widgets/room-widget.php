<?php
	class um_room_widget extends WP_Widget{
		
		function um_room_widget() 
		{
			parent::WP_Widget(false, $name = 'Umbrella > Rooms');
		}	
		
		function widget($args, $instance)
		{
			extract( $args );
			$title = apply_filters('widget_title', $instance['title']);	
			$select =  $instance['select'];
			$args = array();
			
			if($select == 'all'){
				$args = array(
						'post_type' => 'room_post',
						'posts_per_page'  => -1
				);	
			}
			else{
				
				$values = explode("~", $select);
				$args = array(
					'post_type' => 'room_post',
					'posts_per_page'  => -1,
	                'tax_query' => array(
	                    array(
	                        'taxonomy' => $values[0] ,
	                        'field' => 'slug',
	                        'terms' => $values[1]
	                    )
	                )
				);
			}
			
			echo $before_widget;
			?>
			
			<h4><?php echo $title; ?></h4>
			
			<div class="widgetWideContainer">
				<div class="roomsWidgetSlider owl-carousel owl-theme">
				
				
        			<?php $the_Query = new WP_Query($args);
							while ($the_Query->have_posts()):
								$the_Query->the_post(); ?>
								
								<article class="roomsBox mainBgColor">
		                            <div class="boxContent">
		
										<?php if(has_post_thumbnail()): ?>
		                               		<a href="<?php the_permalink(); ?>">
		                               		<?php the_post_thumbnail('blogMasonry'); ?>	
	                               			</a>
		                            	<?php endif; ?>
		                            </div>
		                            <footer>
		                                <div class="roomTitle um_left">
		                                    <a href="<?php the_permalink(); ?>"><h5 class="title titleUp"><?php the_title(); ?></h5></a>
		                                    <h6><?php the_field('price_label'); ?></h6>
		                                </div>
		                                <div class="roomIcons um_right">
		                                    <div class="boxIcons list-unstyled list-inline text-right">
		                                        <a class="iconHolder brandHover text-center um_expand_lightbox" data-postID="<?php echo get_the_ID(); ?>" href="#"><i class="fa fa-expand"></i></a>
	                                             <?php 
													$redirectClass = 'um_book';
													if(get_field('redirect_to') != "booking"){
														$redirectClass = '';	
													}
												 ?>
                                        		<a class="iconHolder brandHover text-center <?php echo $redirectClass; ?>" data-postID="<?php echo get_the_ID(); ?>" href="<?php the_field('shop_link'); ?>"><i class="fa fa-bookmark-o"></i></a>
	                                                     
		                                    </div>
		                                </div>
		                            </footer>
		                        </article>
					<?php endwhile; ?>
					
				</div>
			</div>
					
			<?php
			 echo $after_widget;
		}
		
		function update($new_instance, $old_instance)
		{
			$instance = $old_instance;
			
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['select'] = strip_tags($new_instance['select']);
			return $instance;
		}
		
		function form($instance)
		{
			$title = isset($instance['title']) ? esc_attr($instance['title']) : "";
			
			$select = isset($instance['select']) ? esc_attr($instance['select']) : "all";
			
			
			$taxonomies = get_object_taxonomies('room_post');
			?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title',"um_lang"); ?></label>
					<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
				</p>
				
				<select id="<?php echo $this->get_field_id('select'); ?>" name="<?php echo $this->get_field_name('select'); ?>">
	                <option <?php echo  $select == 'all' ? ' selected="selected"' : '' ?> value="all"><?php _e('All','um_lang'); ?></option>
	                
	                
	                <?php foreach($taxonomies as $tax): ?>
	                  <optgroup label="<?php echo preg_replace('/_/',' ', preg_replace('/um_/','',$tax));?>">
	                  		<?php $terms = get_terms($tax); ?>
		                  	<?php foreach($terms as $term): ?>
		                    	<option <?php echo  $select == $tax.'~'.$term->slug ? ' selected="selected"' : '' ?> value="<?php echo $tax .'~'.$term->slug;?>"><?php echo $term->name; ?></option>
		                	<?php endforeach;?>
	                 </optgroup>
	                <?php endforeach;?>
            	</select>
            	
			<?php
		}
	}
	
function register_um_room() {			
	register_widget('um_room_widget');			
}
add_action('widgets_init', 'register_um_room');
?>