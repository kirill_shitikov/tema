<?php
	
	class um_contact_widget extends WP_Widget{
		
		function um_contact_widget() 
		{
			parent::WP_Widget(false, $name = 'Umbrella > Contact');
		}	
		
		function widget($args, $instance)
		{
			
			extract( $args );
			$title = apply_filters('widget_title', $instance['title']);	
			$phone	= $instance['phone'];
			$email	= $instance['email'];
			$location	= $instance['location'];
			
			echo $before_widget;
			?>
			
				<h4><?php echo $title;?></h4>
				
				<table class="um_contactWidget">
					<tr>
						<address>
							<td><div class="contactIcon"><i class="fa fa-phone"></i></div></td>
							<td><div class="contactInfo"><?php echo $phone; ?></div></td>
						</address>
					</tr>
					<tr>
						<address>
							<td><div class="contactIcon"><i class="fa fa-envelope"></i></div></td>
							<td><div class="contactInfo"><?php echo $email; ?></div></td>
						</address>
					</tr>
					<tr>
						<address>
							<td><div class="contactIcon"><i class="fa fa-location-arrow"></i></div></td>
							<td><div class="contactInfo"><?php echo $location; ?></div></td>
						</address>
					</tr>
				</table>
			
			
			<?php
			 echo $after_widget;
		}
		
		function update($new_instance, $old_instance)
		{
			$instance = $old_instance;
			
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['phone'] = strip_tags($new_instance['phone']);
			$instance['email'] = strip_tags($new_instance['email']);
			$instance['location'] = strip_tags($new_instance['location']);
			return $instance;
		}
		
		function form($instance)
		{
			$title = isset($instance['title']) ? esc_attr($instance['title']) : "";
			$phone = isset($instance['phone']) ? esc_attr($instance['phone']) : "";
			$email = isset($instance['email']) ? esc_attr($instance['email']) : "";
			$location = isset($instance['location']) ? esc_attr($instance['location']) : "";
			
			?>
			
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title',"um_lang"); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</p>
			
			<p>
				<label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone Number:',"um_lang"); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo $phone; ?>" />
			</p>
			
				<p>
				<label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email',"um_lang"); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" />
			</p>
			
				<p>
				<label for="<?php echo $this->get_field_id('location'); ?>"><?php _e('Location',"um_lang"); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('location'); ?>" name="<?php echo $this->get_field_name('location'); ?>" type="text" value="<?php echo $location; ?>" />
			</p>
			
			<?php
		}
		
		
	}
	
function register_um_contact() {			
	register_widget('um_contact_widget');			
}
add_action('widgets_init', 'register_um_contact');
?>