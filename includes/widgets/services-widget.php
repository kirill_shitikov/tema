<?php
	class um_service_widget extends WP_Widget{
		
		function um_service_widget() 
		{
			parent::WP_Widget(false, $name = 'Umbrella > Services');
		}	
		
		function widget($args, $instance)
		{
			
			extract( $args );
			$title = apply_filters('widget_title', $instance['title']);	
			$number = $instance['number'];
			
			if($number == ""){
				$number = 4;
			}
			
			echo $before_widget;
			?>
	

                <h4><?php echo $title; ?></h4>

                <div class="serviceContent">
                	<ul class="list-unstyled">
                    	<?php $args = array(	
										'post_type' => 	'service_post',
										'orderby'	=>	'menu_order date',
										'order' 	=> 	'DESC',
										'posts_per_page' => $number		
									);
									
						$the_Query = new WP_Query($args);
						while ($the_Query->have_posts()):
							$the_Query->the_post();?>
							
               				 <li>
               				 	<a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span>
               				 
                   				 	<?php if(get_field('service_thumbnail') == "image"): ?>
                   				 		<?php $img = wp_get_attachment_image_src(get_field('image') ,'service_icon')?>
										<img width="104" height="104" src="<?php echo $img[0]; ?>"/>
                   				 	<?php else: ?>
                   				 		<i class="brandColor fa <?php the_field('icon'); ?> fa-2x"></i> 
                   				 	<?php endif; ?>
               				 	</a>
               				 </li>
            			<?php endwhile; ?>
                    </ul>
                </div>

			
			<?php
			 echo $after_widget;
		}
		
		function update($new_instance, $old_instance)
		{
			$instance = $old_instance;
			
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['number'] = strip_tags($new_instance['number']);
			return $instance;
		}
		
		function form($instance)
		{
			$title = isset($instance['title']) ? esc_attr($instance['title']) : "";
			$number = isset($instance['number']) ? esc_attr($instance['number']) : 4;
			?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title',"um_lang"); ?></label>
					<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('How meny services to display',"um_lang"); ?></label>
					<input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" min="0" name="<?php echo $this->get_field_name('number'); ?>" type="number" value="<?php echo $number; ?>" />
				</p>
			<?php
		}
		
		
	}
	
function register_um_service() {			
	register_widget('um_service_widget');			
}
add_action('widgets_init', 'register_um_service');
?>