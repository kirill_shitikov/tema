<?php
class umbrella_twitterfeed_widget extends WP_Widget{
	function umbrella_twitterfeed_widget() 
	{
		parent::WP_Widget(false, $name = 'Umbrella > Twitterfeed');
	}
	
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters('widget_title', $instance['title']);
		$twitter_search = $instance['twitter_search'];
        //$twitter_url = $instance['twitter_url'];
        $consumer_key = $instance['consumer_key'];
        $consumer_secret = $instance['consumer_secret'];
        $user_token = $instance['user_token'];
        $user_secret = $instance['user_secret'];

        if(!isset($twitter_url)){
            $twitter_url = '';
        }
        /*Twitter oAuth Request*/
     
     	if (!class_exists('tmhOAuth')) {
     	
	   		function my_streaming_callback($data, $length, $metrics) {}

		
	        require 'oAuth/tmhOAuth.php';
	        require 'oAuth/tmhUtilities.php';
		}
		    $tmhOAuth = new tmhOAuth(array(
	          'consumer_key'    => $consumer_key,
	          'consumer_secret' => $consumer_secret,
	          'user_token'      => $user_token,
	          'user_secret'     => $user_secret,
	        ));
       
	   
		
        $method = "https://api.twitter.com/1.1/statuses/user_timeline.json";
        $params = array(
	        'screen_name' => $twitter_search
        );
        $tmhOAuth->request('GET', $method, $params, 'my_streaming_callback');
        $tweets = json_decode($tmhOAuth->response['response'],true);
        if($tweets):
        echo $before_widget;
		?>
		
		<h4><?php echo $title; ?></h4>
		
        <div class="widgetTweetWrapper">
            <div class="twitter_feed tweets_widget f_widget col_footer_02 col_460 footer_col " twitter_search="<?php echo $twitter_search; ?>">

                <?php foreach($tweets as $tw): ?>
	                 <?php if(isset($tw['text'])): ?>	
		                <div class="tweet">
		                   
		                   <?php $newStr = preg_replace("/([\w]+:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/i","<a target=\"_blank\" href=\"$1\">$1</A>",$tw["text"]); ?>
		                    <p class="um_tweet"><i class="fa fa-twitter fa-2x"></i><strong>@<?php echo $tw["user"]["screen_name"]; ?> </strong><?php echo $newStr; ?> </p>
		                </div>
	                <?php endif; ?>
                <?php endforeach; ?>

            </div><!--Close Twitter Feed-->

        </div>
        <script>
            jQuery(document).ready(function(){
                jQuery("div.twitter_feed").twitterfeed(0);
            });
        </script>
		<?php
        echo $after_widget;
        endif;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['twitter_search'] = strip_tags($new_instance['twitter_search']);
        $instance['consumer_key'] = strip_tags($new_instance['consumer_key']);
        $instance['consumer_secret'] = strip_tags($new_instance['consumer_secret']);
        $instance['user_token'] = strip_tags($new_instance['user_token']);
        $instance['user_secret'] = strip_tags($new_instance['user_secret']);
		return $instance;
	}
	
	function form($instance)
	{
		$title = isset($instance['title']) ? esc_attr($instance['title']) : "";
		$twitter_search = isset($instance['twitter_search']) ? esc_attr($instance['twitter_search']) : "";
        $consumer_key = isset($instance['consumer_key']) ? esc_attr($instance['consumer_key']) : "";
        $consumer_secret = isset($instance['consumer_secret']) ? esc_attr($instance['consumer_secret']) : "";
        $user_token = isset($instance['user_token']) ? esc_attr($instance['user_token']) : "";
        $user_secret = isset($instance['user_secret']) ? esc_attr($instance['user_secret']) : "";
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title',"um_lang"); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('twitter_search'); ?>"><?php _e('Twitter Screen Name (Without @)',"um_lang"); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('twitter_search'); ?>" name="<?php echo $this->get_field_name('twitter_search'); ?>" type="text" value="<?php echo $twitter_search; ?>" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('consumer_key'); ?>"><?php _e('Consumer Key',"um_lang"); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('consumer_key'); ?>" name="<?php echo $this->get_field_name('consumer_key'); ?>" type="text" value="<?php echo $consumer_key; ?>" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('consumer_secret'); ?>"><?php _e('Consumer Secret',"um_lang"); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('consumer_secret'); ?>" name="<?php echo $this->get_field_name('consumer_secret'); ?>" type="text" value="<?php echo $consumer_secret; ?>" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('user_token'); ?>"><?php _e('User Token',"um_lang"); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('user_token'); ?>" name="<?php echo $this->get_field_name('user_token'); ?>" type="text" value="<?php echo $user_token; ?>" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('user_secret'); ?>"><?php _e('User Secret',"um_lang"); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('user_secret'); ?>" name="<?php echo $this->get_field_name('user_secret'); ?>" type="text" value="<?php echo $user_secret; ?>" />
		</p>
		<?php
	}
}

function register_umbrella_twitterfeed_widget() {			
	register_widget('umbrella_twitterfeed_widget');			
}
add_action('widgets_init', 'register_umbrella_twitterfeed_widget');
?>
