<?php
	
	class um_location_widget extends WP_Widget{
		
		function um_location_widget() 
		{
			parent::WP_Widget(false, $name = 'Umbrella > Location Map');
		}	
		
		function widget($args, $instance)
		{
			
			extract( $args );
			$title = apply_filters('widget_title', $instance['title']);	
			$address = $instance['address'];
			$zoom = $instance['zoom'];
			$addresLabel = $instance['addresLabel'];
			echo $before_widget;
			?>
                <h4><?php echo $title; ?></h4>
	      		
	      		<div class="widgetWideContainer">
	      			<div id="um_widgetMap" class="mapContent"></div>
	      		</div>
	      		
                <footer>
                    <address>
                        <h5 class="title titleUp"><?php _e('Address','um_lang'); ?></h5>
                        <p><?php echo $addresLabel; ?></p>
                    </address>
                </footer>
				
			</article>	
				
				
			<script>
				jQuery(document).ready(function($){
						
					function codeAddress() {
						var geocoder, map;
					    geocoder = new google.maps.Geocoder();
					    geocoder.geocode({
					        'address': "<?php echo $address; ?>"
					    }, function(results, status) {
					        if (status == google.maps.GeocoderStatus.OK) {
					            var myOptions = {
					                zoom: <?php echo $zoom; ?>,
					                center: results[0].geometry.location,
					                mapTypeId: google.maps.MapTypeId.ROADMAP
					            }
					            map = new google.maps.Map(document.getElementById("um_widgetMap"), myOptions);
					
					            var marker = new google.maps.Marker({
					                map: map,
					                position: results[0].geometry.location
					            });
					        }
					    });
					}
					codeAddress(); 
				});
				
			</script>	

			<?php
			 echo $after_widget;
		}
		
		function update($new_instance, $old_instance)
		{
			$instance = $old_instance;
			
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['address'] = strip_tags($new_instance['address']);
			$instance['zoom'] = strip_tags($new_instance['zoom']);
			$instance['addresLabel'] = strip_tags($new_instance['addresLabel']);
			
			return $instance;
		}
		
		function form($instance)
		{
			$title = isset($instance['title']) ? esc_attr($instance['title']) : "";
			$address = isset($instance['address']) ? esc_attr($instance['address']) : "";
			$zoom = isset($instance['zoom']) ? esc_attr($instance['zoom']) : 15;
			$addresLabel = isset($instance['addresLabel']) ? esc_attr($instance['addresLabel']) : "";
			?>
			
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title',"um_lang"); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Geo Location (Cordinates)',"um_lang"); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" type="text" value="<?php echo $address; ?>" />
			</p>
			
			<p>
				<label for="<?php echo $this->get_field_id('zoom'); ?>"><?php _e('Zoom Level',"um_lang"); ?></label>
				<input min="0" max="19" class="widefat" id="<?php echo $this->get_field_id('zoom'); ?>" name="<?php echo $this->get_field_name('zoom'); ?>" type="number" value="<?php echo $zoom; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('addresLabel'); ?>"><?php _e('Address Label',"um_lang"); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('addresLabel'); ?>" name="<?php echo $this->get_field_name('addresLabel'); ?>" type="text" value="<?php echo $addresLabel; ?>" />
			</p>
			<?php
		}
		
		
	}
	
function register_um_location() {			
	register_widget('um_location_widget');			
}
add_action('widgets_init', 'register_um_location');
?>