<?php
/*Ajax Filter*/

add_action('wp_ajax_um_filter', 'um_filter');
add_action('wp_ajax_nopriv_um_filter', 'um_filter');


function um_filter(){
	
	$filterData = $_POST['um_cat'];
	$post_ID = $_POST['post_id'];
	$tax_query = array('relation' => 'AND');
	$tax = '';
	$terms = array();

	 
	foreach($filterData as $data){

		foreach($data as $cat)
		{	
			if($cat['selected'] != "um_all")
			{
				$tax = $cat['tax'];
				array_push($terms, $cat['selected']);		
	  		}
		}			
		if($tax != '')
		{
		 	array_push($tax_query,array(
                        'taxonomy' => $tax,
                        'terms' => $terms,
                        'field' => 'slug',
                        'include_children' => false,
                        'operator' => 'IN'
                        )
	            );
		}
	}
		
 ?>
  	<section class="articles">
  		<?php
  		
		  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;  
			
			$args = array(
						'post_type' => 'room_post',
						'orderby'	=>	'menu_order date',
						'order' 	=> 	'DESC',
						'paged'     => $paged,
						'tax_query' => $tax_query
						
					);
					
					$the_Query = new WP_Query($args);
					while ($the_Query->have_posts()):
						$the_Query->the_post();	
		 ?>	
		
			 <div class="col-md-4 um_in animated">
                <article class="theBox roomsBox mainBgColor">
                    <div class="boxContent">

						<?php if(has_post_thumbnail()): ?>
                       		<a href="<?php the_permalink(); ?>">
                       		<?php the_post_thumbnail('blogMasonry'); ?>	
                   			</a>
                    	<?php endif; ?>
                    </div>
                    <footer>
                        <div class="roomTitle um_left">
                            <a href="<?php the_permalink(); ?>"><h5 class="title titleUp"><?php the_title(); ?></h5></a>
                            <h6><?php the_field('price_label'); ?></h6>
                        </div>
                        <div class="roomIcons um_right">
                            <div class="boxIcons list-unstyled list-inline text-right">
                                <a class="iconHolder brandHover text-center um_expand_lightbox" href="#"><i class="fa fa-expand"></i></a>
                                 <?php 
									$redirectClass = 'um_book';
									if(get_field('redirect_to', $post_ID) != "booking"){
										$redirectClass = '';	
									}
								 ?>
                            	<a class="iconHolder brandHover text-center <?php echo $redirectClass; ?>" data-postID="<?php echo $post_ID; ?>" href="<?php the_field('shop_link',$post_ID); ?>"><i class="fa fa-bookmark-o"></i></a>
                                    
                                
                            </div>
                        </div>
                    </footer>
                </article>
            </div>
		 
		 <?php endwhile; ?>			 
  	</section>
  	
  		<script>
			 pageRooms = parseInt("<?php echo $paged; ?>");
    		 lastPageRooms = parseInt('<?php echo $the_Query->max_num_pages; ?>');
	</script>	
 <?php
 die();
}
/*Ajax Filter END*/
?>
