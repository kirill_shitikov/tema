<?php

if(function_exists("register_field_group"))
{
	
//THEME OPTION SETTING - START HERE	
//Theme option - HEADER
register_field_group(array (
	'id' => 'agent_header',
	'title' => 'Agent Header',
	'fields' => array (
		//TAB Agent Setting
		array(
			'key' => 'field_000000agentTab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Agent Setting',
		),	
			
			
			array (
				'key' => 'field_0000001agenthead',
				'label' => 'Agent Logo',
				'name' => 'agent_logo',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000002agenthead',
				'label' => 'Agent Image',
				'name' => 'agent_image',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000003agenthead',
				'label' => 'Agent Name',
				'name' => 'agent_name',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			
		//TAB Intro Setting
		array(
			'key' => 'field_000000introTab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Intro Setting',
		),	
			array (
				'key' => 'field_0000001introset01',
				'label' => 'Intro Image',
				'name' => 'intro_img',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000001introset02',
				'label' => 'Intro Tag Line',
				'name' => 'intro_tagline',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			
			
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-theme-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));

//Theme option - Home & Bios
register_field_group(array (
	'id' => 'home_setting',
	'title' => 'Home Setting',
	'fields' => array (
		//TAB Agent Setting
		array(
			'key' => 'field_0000001hometab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Home Setting',
		),				
			
			array (
			'key' => 'field_0000001hometab01',
			'label' => 'Header Title Label',
			'name' => 'about_header_title',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
			array (
				'key' => 'field_0000001hometab02',
				'label' => 'Agent Description',
				'name' => 'about_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			
			
		//TAB Bios Setting
		array(
			'key' => 'field_0000001biostab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Bios Setting',
		),	
			array (
				'key' => 'field_0000001biostab0divider',
				'label' => 'Divider Title',
				'name' => 'bio_divider_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000001biostab01',
				'label' => 'Header Title Label',
				'name' => 'bio_header_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000001biostab02',
				'label' => 'Bio Description',
				'name' => 'bio_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			
			array (
				'key' => 'field_0000001biostab08',
				'label' => 'Vimeo Video URL ID<br><i><font style="font-weight: normal; color: #ff0000;">https://player.vimeo.com/video/131506757 <br> https://youtu.be/uU5YPIvJ24Y</font></i>',
				'name' => 'bio_vimeo_url',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000001biostab09',
				'label' => 'Youtube Video URL ID<br><i><font style="font-weight: normal; color: #ff0000;">https://player.vimeo.com/video/131506757 <br> https://youtu.be/uU5YPIvJ24Y</font></i>',
				'name' => 'bio_youtube_url',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000001biostab10',
				'label' => 'Upload Image',
				'name' => 'bio_image_url',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000001biostab11',
					'label' => 'Select Display Bio Video or Image<br><i><font style="font-weight: normal; color: #ff0000;">Please choose which display do you want to display in bio section (Vimeo, youtube or image).</font></i>',
					'name' => 'bio_display_imgvideo',
					'type' => 'radio',
					'choices' => array (
						'1' => 'Vimeo Video',
						'2' => 'Youtube Video',
						'3' => 'Image',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => '2',
					'layout' => 'horizontal',
			),
			
			
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-home-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));

//Theme Option - Buying & Selling Settings
register_field_group(array (
	'id' => 'buysell_setting',
	'title' => 'Buying &amp; Selling Setting',
	'fields' => array (
	
		//TAB 1
		array(
			'key' => 'field_0000001sellbuytab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Homepage',
		),	
			
			array (
				'key' => 'field_0000001sellbuytab1',
				'label' => 'Divider Title',
				'name' => 'buysell_divider_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000001sellbuytab2',
				'label' => 'Home Page Buy &amp; Selling Description',
				'name' => 'buysell_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_0000001sellbuytab3',
				'label' => 'Buy Button Label',
				'name' => 'buy_label',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000001sellbuytab4',
				'label' => 'Buy Button URL',
				'name' => 'buy_url',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000001sellbuytab5',
				'label' => 'Sell Button Label',
				'name' => 'sell_label',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000001sellbuytab6',
				'label' => 'Sell Button URL',
				'name' => 'sell_url',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			
		//TAB 2
		array(
			'key' => 'field_0000002sellbuytab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Buying Page',
		),
		
			array (
				'key' => 'field_0000002sellbuytab1',
				'label' => 'Header Title',
				'name' => 'buy_header_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000002sellbuytab2',
				'label' => 'Header Short Description',
				'name' => 'buy_short_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_0000002sellbuytab3',
				'label' => 'Right Description',
				'name' => 'buy_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_0000002sellbuytab4',
				'label' => 'Full Description',
				'name' => 'buy_full_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			
		//TAB 3
		array(
			'key' => 'field_0000003sellbuytab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Selling Page',
		),
			array (
				'key' => 'field_0000003sellbuytab1',
				'label' => 'Header Title',
				'name' => 'sell_header_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000003sellbuytab2',
				'label' => 'Header Short Description',
				'name' => 'sell_short_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_0000003sellbuytab3',
				'label' => 'Right Description',
				'name' => 'sell_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_0000003sellbuytab4',
				'label' => 'Full Description',
				'name' => 'sell_full_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			
		//TAB 4
		array(
			'key' => 'field_0000004sellbuytab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Investing Page',
		),
			array (
				'key' => 'field_0000004sellbuytab1',
				'label' => 'Header Title',
				'name' => 'invest_header_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000004sellbuytab2',
				'label' => 'Header Short Description',
				'name' => 'invest_short_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_0000004sellbuytab3',
				'label' => 'Full Description',
				'name' => 'invest_full_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
	
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-buying-sell-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));

//Theme Option - Featured Settings
register_field_group(array (
	'id' => 'featured_setting',
	'title' => 'Featured Setting',
	'fields' => array (
		array (
			'key' => 'field_000000001feadivider',
			'label' => 'Divider Title',
			'name' => 'fea_divider_title',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_000000001fea',
			'label' => 'Header Title Label',
			'name' => 'fea_header_title',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_000000002fea',
			'label' => 'See More Button Label',
			'name' => 'fea_btn_label',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_000000002fea01',
			'label' => 'See More Button URL',
			'name' => 'fea_btn_url',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_000000003fea',
			'label' => 'Properties Listings',
			'name' => 'fea_repeater',
			'type' => 'repeater',
			'sub_fields' => array (
				array (
					'key' => 'field_000000003fea00',
						'label' => 'Display in Front Page<br><i><font style="font-weight: normal; color: #ff0000;">If Yes, will display in front page.</font></i>',
						'name' => 'fea_display_frontpage',
						'type' => 'radio',
						'choices' => array (
							'1' => 'Yes',
							'2' => 'No'
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => '2',
						'layout' => 'horizontal',
				),		
				
				array (
					'key' => 'field_000000003fea01',
					'label' => 'Upload Thumbnailss',
					'name' => 'fea_image_url',
					'type' => 'image',
					'column_width' => '',
					'save_format' => 'id',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_000000003fea02',
					'label' => 'Vimeo Video URL ID<br><i><font style="font-weight: normal; color: #ff0000;">https://player.vimeo.com/video/131506757 <br> https://youtu.be/uU5YPIvJ24Y</font></i>',
					'name' => 'fea_vimeo_url',
					'type' => 'text',
					'column_width' => '',
					'save_format' => 'id',
				),
				array (
					'key' => 'field_000000003fea03',
					'label' => 'Youtube Video URL ID<br><i><font style="font-weight: normal; color: #ff0000;">https://player.vimeo.com/video/131506757 <br> https://youtu.be/uU5YPIvJ24Y</font></i>',
					'name' => 'fea_youtube_url',
					'type' => 'text',
					'column_width' => '',
					'save_format' => 'id',
				),	
				array (
					'key' => 'field_000000003fea04',
						'label' => 'Select Display Video or Image in Feature Listings Section<br><i><font style="font-weight: normal; color: #ff0000;">Please choose which display do you want to display in featured listings section (Vimeo, youtube or image).</font></i>',
						'name' => 'fea_display_imgvideo',
						'type' => 'radio',
						'choices' => array (
							'1' => 'Vimeo Video',
							'2' => 'Youtube Video',
							'3' => 'Image',
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => '3',
						'layout' => 'horizontal',
				),			
				array (
					'key' => 'field_000000003fea05',
					'label' => 'Listing Title',
					'name' => 'fea_listing_title',
					'type' => 'text',
					'column_width' => '',
					'save_format' => 'id',
				),
				array (
					'key' => 'field_000000003fea06',
					'label' => 'Properties Short Description',
					'name' => 'fea_short_description',
					'type' => 'wysiwyg',
					'column_width' => '',
					'save_format' => 'id',
					'toolbar' => 'full',
					'media_upload' => 'yes',
				),
				array (
					'key' => 'field_000000003fea07',
					'label' => 'Property Website URL',
					'name' => 'fea_property_url',
					'type' => 'text',
					'column_width' => '',
					'save_format' => 'id',
				),
									
			),
			'row_min' => 1,
			'row_limit' => '',
			'layout' => 'row',
			'button_label' => 'Add Properties',
		),
		
	),
	'row_min' => 1,
	'row_limit' => '1',
	'layout' => 'row',
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-featured-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));


//Theme Option - Blog Settings
register_field_group(array (
	'id' => 'blog_setting',
	'title' => 'Blog Setting',
	'fields' => array (
		array (
			'key' => 'field_000000001blog00',
			'label' => 'Divider Title',
			'name' => 'blog_divider_title',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_000000001blog01',
			'label' => 'Blog Header Title',
			'name' => 'blog_header_title',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		)
	),
	'row_min' => 1,
	'row_limit' => '1',
	'layout' => 'row',
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-blog-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));

//Theme Option - Quick Search Settings
register_field_group(array (
	'id' => 'quicksearch_setting',
	'title' => 'Quick Search Setting',
	'fields' => array (
		array (
			'key' => 'field_000000001qsdivider',
			'label' => 'Divider Title',
			'name' => 'qs_divider_title',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_000000001qs00',
			'label' => 'Title',
			'name' => 'qs_title',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_000000001qs',
			'label' => 'Searching Data Description',
			'name' => 'qs_main_description',
			'type' => 'wysiwyg',
			'column_width' => '',
			'save_format' => 'id',
			'toolbar' => 'full',
			'media_upload' => 'yes',
		)	
	),
	'row_min' => 1,
	'row_limit' => '1',
	'layout' => 'row',
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-quick-search-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));

//Theme Option - Featured Settings
register_field_group(array (
	'id' => 'search_setting',
	'title' => 'Search Property Setting',
	'fields' => array (
		array (
			'key' => 'field_000000001sp',
			'label' => 'Searching Data Description',
			'name' => 'sp_main_description',
			'type' => 'wysiwyg',
			'column_width' => '',
			'save_format' => 'id',
			'toolbar' => 'full',
			'media_upload' => 'yes',
		),
		array (
			'key' => 'field_000000002sp',
			'label' => 'Content Description',
			'name' => 'sp_content_description',
			'type' => 'wysiwyg',
			'column_width' => '',
			'save_format' => 'id',
			'toolbar' => 'full',
			'media_upload' => 'yes',
		),
		
		
	),
	'row_min' => 1,
	'row_limit' => '1',
	'layout' => 'row',
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-searching-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));





//Theme Option - Testimonial Settings
register_field_group(array (
	'id' => 'testimonial_setting',
	'title' => 'Testimonial Setting',
	'fields' => array (
	
		//TAB 1
		array(
			'key' => 'field_000000001testimonialtab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Custom Setting',
		),		
			array (
				'key' => 'field_000000001testimonialtab_1',
				'label' => 'Divider Title',
				'name' => 'testimonial_divider_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000001testimonialtab_2',
				'label' => 'Description Font Color<br><i><font style="font-weight: normal; color: #ff0000;">Eg. #ffffff. Leave blank if default css color.</font></i>',
				'name' => 'tes_desc_font_color',
				'type' => 'text',
				'column_width' => '',
				'class' => 'colorSetWidth',
				'maxlength' => 7,
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000001testimonialtab_3',
				'label' => 'Left Border Color<br><i><font style="font-weight: normal; color: #ff0000;">Default is Red color.</font></i>',
				'name' => 'tes_leftborder_color',
				'type' => 'text',
				'column_width' => '',
				'class' => 'colorSetWidth',
				'maxlength' => 7,
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000001testimonialtab_4',
				'label' => 'Background Color<br><i><font style="font-weight: normal; color: #ff0000;">Default is Grey color.</font></i>',
				'name' => 'tes_blockquotebg_color',
				'type' => 'text',
				'column_width' => '',
				'class' => 'colorSetWidth',
				'maxlength' => 7,
				'save_format' => 'id',
			),
			/*array (
				'key' => 'field_000000001testimonialtab_3',
				'label' => 'Testimonial Description Font Size<br><i><font style="font-weight: normal; color: #ff0000;">Eg. 12</font></i>',
				'name' => 'tes_desc_font_size',
				'type' => 'text',
				'column_width' => '',
				'class' => 'fontSizeWidth',
				'maxlength' => 2,
				'save_format' => 'id',
			),*/
	),
	'row_min' => 1,
	'row_limit' => '1',
	'layout' => 'row',
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-testimonial-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));

//Theme Option - Email Form Settings
register_field_group(array (
	'id' => 'bottom_email',
	'title' => 'Bottom Email Setting',
	'fields' => array (
		/*array (
			'key' => 'field_0000001mail',
			'label' => 'Arrow Image',
			'name' => 'mail_arrow',
			'type' => 'image',
			'column_width' => '',
			'save_format' => 'id',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),*/
		array (
			'key' => 'field_0000002mail',
			'label' => 'Email Title',
			'name' => 'mail_title',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),			
		/*array (
			'key' => 'field_0000003mail',
			'label' => 'Email Form Title',
			'name' => 'mail_form_title',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),*/
		array (
			'key' => 'field_0000004mail',
			'label' => 'Company Name',
			'name' => 'mail_form_company_name',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_0000005mail',
			'label' => 'Contact Number',
			'name' => 'mail_form_contact',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_0000006mail',
			'label' => 'Email Address',
			'name' => 'mail_form_email',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_0000007mail',
			'label' => 'Company Adress',
			'name' => 'mail_form_company_address',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-email-form-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));
	
//Theme Option - Communities Start
register_field_group(array (
	'id' => 'front-communities',
	'title' => 'Communities Setting',
	'fields' => array (
		//TAB Front Community Setting
		/*array(
			'key' => 'field_0000001fcomTab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Front Setting',
		),
			array (
					'key' => 'field_0000001fcomdivider',
					'label' => 'Divider Title',
					'name' => 'fcom_divider_title',
					'type' => 'text',
					'column_width' => '',
					'save_format' => 'id',
				),
			array (
					'key' => 'field_0000001fcom',
					'label' => 'Main Title<br><font style="font-weight: normal; color: #ff0000;"><i>This is compulsory *</i></font>',
					'name' => 'fcom_main_title',
					'type' => 'text',
					'column_width' => '',
					'save_format' => 'id',
				),
			array (
				'key' => 'field_0000002fcom',
				'name' => 'fcom_repeater',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_0000002fcom01',
						'label' => 'Button Background Image<br><font style="font-weight: normal; color: #ff0000;"><i>This is compulsory *</i></font>',
						'name' => 'fcom_btn_bg',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_0000002fcom02',
						'label' => 'Button Label<br><font style="font-weight: normal; color: #ff0000;"><i>This is compulsory *</i></font>',
						'name' => 'fcom_btn_label',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_0000002fcom03',
						'label' => 'Button Url',
						'name' => 'fcom_btn_url',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
				),
				'row_min' => 1,
				'row_limit' => '',
				'layout' => 'row',
			),*/
			
		//TAB 2
		array(
			'key' => 'field_0000002comtab',
			'name' => '',
			'type' => 'tab',
			'label' => 'Neighborhoods',
		),
			array (
				'key' => 'field_0000002comtab1',
				'label' => 'Website URL<br><font style="font-weight: normal; color: #ff0000;"><i>Please fill in the neighborhood website url. Eg. http://www.circlevisions.com</i></font>',
				'name' => 'neighbor_url',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
		
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-community-setting',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0
));	

//Them Option - Communities End

//THEME OPTION SETTING - END HERE



//START TEMPLATE CUSTOMIZATION





//CUSTOM POST TYPE START
/*Communities Custom Post Start*/
register_field_group(array (
	'id' => 'cus_community',
	'title' => 'Communities Settings',
	'fields' => array (
		array (
			'key' => 'field_0000001cuscom',
			'label' => 'Community Name',
			'name' => 'cuscom_name',
			'type' => 'text',
			'column_width' => '',
			'save_format' => 'id',
		),
		array (
			'key' => 'field_0000002cuscom01',
			'label' => 'Properties Image',
			'name' => 'cuscom_properties_img',
			'type' => 'image',
			'column_width' => '',
			'save_format' => 'id',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => 'field_0000002cuscom',
			'label' => 'Safest Cities Logo<br><i><font style="font-weight: normal; color: #ff0000;">Please add if you have any logo</font></i>',
			'name' => 'cuscom_safe_logo',
			'type' => 'image',
			'column_width' => '',
			'save_format' => 'id',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => 'field_0000003cuscom',
			'label' => 'Description',
			'name' => 'cuscom_description',
			'type' => 'wysiwyg',
			'column_width' => '',
			'save_format' => 'id',
			'toolbar' => 'full',
			'media_upload' => 'no',
		),
		/*array (
			'key' => 'field_0000004cuscom',
				'label' => 'Display Mortgage Calculator',
				'name' => 'cuscom_mortgage_cal',
				'type' => 'radio',
				'choices' => array (
					'1' => 'Yes',
					'2' => 'No',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '1',
				'layout' => 'horizontal',
		),*/
		array (
			'key' => 'field_0000004cuscom01',
				'label' => 'Display Property Search',
				'name' => 'cuscom_property_search',
				'type' => 'radio',
				'choices' => array (
					'1' => 'Yes',
					'2' => 'No',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '1',
				'layout' => 'horizontal',
		),
		array (
			'key' => 'field_0000005cuscom',
			'label' => 'Map Location',
			'name' => 'cuscom_map_repeater',
			'type' => 'repeater',
			'sub_fields' => array (
				array (
					'key' => 'field_0000005cuscom01',
					'label' => 'Latitude',
					'name' => 'cuscom_latitude',
					'type' => 'text',
					'class' => 'cuscom-latitude',
					'column_width' => '',
					'save_format' => 'id',
				),
				array (
					'key' => 'field_0000005cuscom02',
					'label' => 'Longitude',
					'name' => 'cuscom_longitude',
					'type' => 'text',
					'class' => 'cuscom-longitude',
					'column_width' => '',
					'save_format' => 'id',
				),
									
			),
			'row_min' => 1,
			'row_limit' => '1',
			'layout' => 'row',
			'button_label' => 'Add Row',
		),
		
	),	
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'communities',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));


register_field_group(array (
	'id' => 'cus_community_listing',
	'title' => 'Listing List',
	'fields' => array (
		array (
			'key' => 'field_0000001cuscomlist',
			'label' => 'New Listings',
			'name' => 'cuscomlist_repeater',
			'type' => 'repeater',
			'sub_fields' => array (
				array (
					'key' => 'field_0000001cuscomlist01',
					'label' => 'Chart Image',
					'name' => 'cuscomlist_chart',
					'type' => 'image',
					'column_width' => '',
					'save_format' => 'id',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_0000001cuscomlist02',
					'label' => 'Main Title',
					'name' => 'cuscomlist_main_title',
					'type' => 'text',
					'column_width' => '',
					'save_format' => 'id',
				),
				array (
					'key' => 'field_000001cuscomlist03',
					'label' => 'Sub Title',
					'name' => 'cuscomlist_sub_title',
					'type' => 'text',
					'column_width' => '',
					'save_format' => 'id',
				),
									
			),
			'row_min' => 0,
			'row_limit' => '',
			'layout' => 'row',
			'button_label' => 'Add New Listing',
		),
		
	),	
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'communities',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));

register_field_group(array (
	'id' => 'cus_community_whatin',
	'title' => 'Interesting Park Location',
	'fields' => array (
		array (
			'key' => 'field_0000001cuscomwhatin',
			'label' => 'What Is In The Town..??',
			'name' => 'cuswhat_repeater',
			'type' => 'repeater',
			'sub_fields' => array (
				array (
					'key' => 'field_0000001cuscomwhatin01',
					'label' => 'Interesting Location In Town',
					'name' => 'cuswhat_interest_img',
					'type' => 'image',
					'column_width' => '',
					'save_format' => 'id',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_0000001cuscomwhatin02',
					'label' => 'Location Name',
					'name' => 'cuswhat_interest_name',
					'type' => 'text',
					'column_width' => '',
					'save_format' => 'id',
				),
				array (
					'key' => 'field_0000001cuscomwhatin03',
					'label' => 'Location Description',
					'name' => 'cuswhat_interest_desc',
					'type' => 'textarea',
					'column_width' => '',
					'save_format' => 'id',
				),
									
			),
			'row_min' => 0,
			'row_limit' => '',
			'layout' => 'row',
			'button_label' => 'Add New Interesting Location',
		),
		
	),	
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'communities',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 1,
));

/*Communities Custom Post Start*/


/*Testimonial Custom Post*/
register_field_group(array (
	'id' => 'tes_setting',
	'title' => 'Settings',
	'fields' => array (
		//TAB 1
		array(
			'key' => 'field_0000001testab1',
			'name' => '',
			'type' => 'tab',
			'label' => 'Testimonial Image/Video',
		),
			array (
					'key' => 'field_0000001testab1_1',
					'name' => 'tes_img_user',
					'type' => 'image',
					'column_width' => '',
					'save_format' => 'id',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
			
			array (
					'key' => 'field_0000001testab1_2',
					'label' => 'Testimonial Youtube Video ID<br><i><font style="font-weight: normal; color: #ff0000;">Please fill in youtube video ID only. If no video, please leave it blank.</font></i>',
					'name' => 'tes_youtube_id',
					'type' => 'text',
					'class' => 'shortVideoUrl',
					'column_width' => '',
					'save_format' => 'id',
				),
			array (
					'key' => 'field_0000001testab1_3',
					'label' => 'Testimonial Vimeo Video ID<br><i><font style="font-weight: normal; color: #ff0000;">Please fill in vimeo video ID only. If no video, please leave it blank.</font></i>',
					'name' => 'tes_vimeo_id',
					'type' => 'text',
					'class' => 'shortVideoUrl',
					'column_width' => '',
					'save_format' => 'id',
				),
			
			array (
				'key' => 'field_0000001testab1_4',
					'label' => 'Display Image or Video<br><i><font style="font-weight: normal; color: #ff0000;">Please select which you want to display in testimonial section. DEFAULT: Image</font></i>',
					'name' => 'tes_img_video',
					'type' => 'radio',
					'choices' => array (
						'1' => 'Display Testimonial Image',
						'2' => 'Display Testimonial Youtube Video',
						'3' => 'Display Testimonial Vimeo Video',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => '1',
			),
	
		//TAB 2
		/*array(
			'key' => 'field_0000001testab2',
			'name' => '',
			'type' => 'tab',
			'label' => 'Custom Settings',
		),*/
			
		
	),	
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'testimonials',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'normal',
		'layout' => 'default',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));

//CUSTOM POST TYPE END

















































//Theme option - Students saying	
	register_field_group(array (
		'id' => 'students_saying',
		'title' => 'Students saying',
		'fields' => array (
			array (
				'key' => 'field_000000000034',
				'label' => 'Title Block',
				'name' => 'stud_say_title_block',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000008',
				'name' => 'students_saying',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000009',
						'label' => 'Recall',
						'name' => 'recall',
						'type' => 'textarea',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000010',
						'label' => 'Author',
						'name' => 'author',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
										
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-students-saying',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
	
//Theme option - Subscribe
	register_field_group(array (
		'id' => 'subscribe',
		'title' => 'Subscribe',
		'fields' => array (
			array (
				'key' => 'field_000000000051',
				'name' => 'subscribe',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000052',
						'label' => 'Title Block',
						'name' => 'subscribe_title',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000152',
						'label' => 'SubTitle Block',
						'name' => 'subscribe_subtitle',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000053',
						'label' => 'Description Block',
						'name' => 'subscribe_description',
						'type' => 'textarea',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_0000000000531',
						'label' => 'After send text',
						'name' => 'after_send_text_subscribe',
						'type' => 'text',
						'column_width' => '',
					),
				),
				'row_min' => 1,
				'row_limit' => '1',
				'layout' => 'row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-subscribe',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));	
	
	
//Theme option - Footer
	register_field_group(array (
		'id' => 'footer',
		'title' => 'Footer',
		'fields' => array (
			array(
				'key' => 'field_000000000025',
				'name' => '',
				'type' => 'tab',
				'label' => 'Logo Footer',
			),
			array (
				'key' => 'field_000000000011',
				'name' => 'logo_footer',
				'type' => 'repeater',
				'label' => 'Logo Footer',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000012',
						'name' => 'image_logo_footer',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
										
				),
				'row_min' => 1,
				'row_limit' => '1',
				'layout' => 'horizontal',
			),
			array(
				'key' => 'field_000000000026',
				'name' => '',
				'type' => 'tab',
				'label' => 'Footer Blocks',
			),
			array (
				'key' => 'field_000000000013',
				'label' => 'Footer Blocks',
				'name' => 'footer_blocks',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000014',
						'label' => 'Title Block',
						'name' => 'title_block_footer',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000015',
						'label' => 'Item Block',
						'name' => 'item_block_footer',
						'type' => 'repeater',
						'sub_fields' => array (
							array (
								'key' => 'field_000000000016',
								'label' => 'Title Item',
								'name' => 'title_item_footer',
								'type' => 'text',
								'column_width' => '',
								'save_format' => 'id',
							),
							array (
								'key' => 'field_000000000017',
								'label' => 'Link Item',
								'name' => 'link_item_footer',
								'type' => 'text',
								'column_width' => '',
								'save_format' => 'id',
							),
						),
					),
										
				),
				'row_min' => 0,
				'row_limit' => '3',
				'layout' => 'row',
			),
			array(
				'key' => 'field_000000000027',
				'name' => '',
				'type' => 'tab',
				'label' => 'Footer Block Last',
			),
			array (
				'key' => 'field_000000000018',
				'label' => 'Footer Block Last',
				'name' => 'footer_block_last',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000019',
						'label' => 'Title Block',
						'name' => 'title_block_last_footer',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000020',
						'label' => 'Address',
						'name' => 'address_block_last_footer',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000021',
						'label' => 'Phone Title',
						'name' => 'phone_title_block_last_footer',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000022',
						'label' => 'Phone',
						'name' => 'phone_block_last_footer',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000023',
						'label' => 'Email Title',
						'name' => 'email_title_block_last_footer',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000024',
						'label' => 'Email',
						'name' => 'email_block_last_footer',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
				),
				'row_min' => 1,
				'row_limit' => '1',
				'layout' => 'row',
			),
			array(
				'key' => 'field_000000000028',
				'name' => '',
				'type' => 'tab',
				'label' => 'Footer Copyright',
			),
			array (
				'key' => 'field_000000000029',
				'label' => 'Copyright Text',
				'name' => 'copyright_text',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array(
				'key' => 'field_000000000030',
				'name' => '',
				'type' => 'tab',
				'label' => 'Footer Social Block',
			),
			array (
				'key' => 'field_000000000031',
				'label' => 'Footer Social Block',
				'name' => 'footer_social_last',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000032',
						'label' => 'Icon',
						'name' => 'social_icon_footer',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_0000000000321',
						'label' => 'Icon Hover',
						'name' => 'social_icon_hover_footer',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_000000000033',
						'label' => 'Link',
						'name' => 'social_link_footer',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
				),
				'row_min' => 0,
				'layout' => 'row',
			),	
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-footer',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
	
//Type post - Trainers	
	register_field_group(array (
		'id' => 'trainer',
		'title' => 'Bios Information',
		'fields' => array (
			array (
				'key' => 'field_000000000003',
				'name' => 'trainer',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000004',
						'label' => 'Photo',
						'name' => 'photo',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_000000000005',
						'label' => 'Name',
						'name' => 'name',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000006',
						'label' => 'Position',
						'name' => 'office',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000007in',
						'label' => 'LinkedIn Account',
						'name' => 'bios_linkedin',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000008twitter',
						'label' => 'Twitter Account',
						'name' => 'bios_twitter',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					)
					
					
				),
				'row_min' => 1,
				'row_limit' => '1',
				'layout' => 'row',
			),
			array (
				'key' => 'field_0000000007bios',
					'label' => 'Team Categories<br><i><font style="font-weight: normal; color: #ff0000;">Please select related category.</font></i>',
					'name' => 'bios_team_categories',
					'type' => 'checkbox',
					'choices' => array (
						'1' => 'Our Core Team',
						'2' => 'Our Consultants',
						'3' => 'Our Trainers',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					/*'default_value' => '1',*/
			),
			array (
				'key' => 'field_0000000008bios',
				'label' => 'Core Team Ranking Order<br><i><font style="font-weight: normal; color: #ff0000;">Use this to reorder base on ranking in core team section. eg: 1,2...etc (If you start with no 1, next bios in this category must 2,3,4... etc)</font></i>',
				'name' => 'bios_coreteam_ranking',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
				'maxlength' => 3,
				'class' => 'biosranking'
			),
			array (
				'key' => 'field_0000000009bios',
				'label' => 'Consultants Ranking Order<br><i><font style="font-weight: normal; color: #ff0000;">Use this to reorder base on ranking in consultant team section. eg: 1,2...etc (If you start with no 1, next bios in this category must 2,3,4... etc)</font></i>',
				'name' => 'bios_consultant_ranking',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
				'maxlength' => 3,
				'class' => 'biosranking'
			),
			array (
				'key' => 'field_00000000010bios',
				'label' => 'Trainers Ranking Order<br><i><font style="font-weight: normal; color: #ff0000;">Use this to reorder base on ranking in trainer team section. eg: 1,2...etc (If you start with no 1, next bios in this category must 2,3,4... etc)</font></i>',
				'name' => 'bios_trainer_ranking',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
				'maxlength' => 3,
				'class' => 'biosranking'
			),
			
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trainers_post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
	
//Type post - SeeAsInAction	
	register_field_group(array(
		'id' => 'see_as_in_action_post_setting',
		'title' => 'Settings',
		'fields' => array (
		
			array (
				'key' => 'field_000000000200122',
				'name' => 'see_as_in_action_post_setting',
				'type' => 'repeater',
				'sub_fields' => array ( 
				array (
					'key' => 'field_00000000070911',
						'label' => 'Display in front page',
						'name' => 'seeasinactiondisplay',
						'type' => 'radio',
						'choices' => array (
							'1' => 'Yes',
							'2' => 'No',
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => '2',
						'layout' => 'horizontal',
				),
				
			),
			'row_min' => 1,
				'row_limit' => '1',
				'layout' => 'row',
				),
		),
	
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'see-as-in-action',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			
		),
		'menu_order' => 0,
	
	
	));


	register_field_group(array (
		'id' => 'see_as_in_action_post',
		'title' => 'See As In Action',
		'fields' => array (
			array (
				'key' => 'field_0000000002001',
				'name' => 'see_as_in_action_post',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_0000000002011',
						'label' => 'Image',
						'name' => 'image_post',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_0000000002021',
						'label' => 'Title',
						'name' => 'title_post',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_0000000002031',
						'label' => 'City',
						'name' => 'city_post',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_00000000020341sees',
						'label' => 'Event Date',
						'name' => 'see_us_event_date',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
				),
				'row_min' => 1,
				'row_limit' => '1',
				'layout' => 'row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'see-as-in-action',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

//Template - Full Schedule
	register_field_group(array (
		'id' => 'full_schedule',
		'title' => 'Full Schedule',
		'fields' => array (
			array (
				'key' => 'field_000000000065',
				'label' => 'Title',
				'name' => 'title_full_schedule',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000066',
				'label' => 'Description',
				'name' => 'description_full_schedule',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
		),
		'row_min' => 1,
		'row_limit' => '1',
		'layout' => 'row',
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-full-schedule.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
	
//Template - Agile Coaching & Consulting
	register_field_group(array (
		'id' => 'agile_coaching_consulting',
		'title' => 'Agile Coaching & Consulting',
		'fields' => array (
			array(
				'key' => 'field_000000000067',
				'name' => '',
				'type' => 'tab',
				'label' => 'Banner part',
			),
			array (
				'key' => 'field_00000000001acc01',
				'label' => 'Header Image<br><i><font style="font-weight: normal; color: #ff0000;">Image Size: 300px (w) x 300px (h)</font></i>',
				'name' => 'header_img_agile_coaching_consulting',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000000000681',
				'label' => 'Background',
				'name' => 'background_agile_coaching_consulting',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000000000682',
				'label' => 'Background Mobile<br><i><font style="font-weight: normal; color: #ff0000;">Banner Size: 670px (w) x 350px (h)</font></i>',
				'name' => 'background_mobile_agile_coaching_consulting',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_000000000068',
				'label' => 'Title',
				'name' => 'title_agile_coaching_consulting',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000069',
				'label' => 'Description',
				'name' => 'description_agile_coaching_consulting',
				'type' => 'textarea',
				'column_width' => '',
				'save_format' => 'id',
			),
			array(
				'key' => 'field_000000000070',
				'name' => '',
				'type' => 'tab',
				'label' => 'Content part',
			),
			array (
				'key' => 'field_000000000071',
				'label' => 'Blocks',
				'name' => 'blocks_agile_coaching_consulting',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000072',
						'label' => 'Title',
						'name' => 'title_blocks_agile_coaching_consulting',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000073',
						'label' => 'Top Description',
						'name' => 'top_desc_blocks_agile_coaching_consulting',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000074',
						'label' => 'Bottom Description',
						'name' => 'bottom_desc_blocks_agile_coaching_consulting',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_000000000075',
						'label' => 'Block',
						'name' => 'block_agile_coaching_consulting',
						'type' => 'repeater',
						'sub_fields' => array (
							array (
								'key' => 'field_000000000076',
								'label' => 'Title',
								'name' => 'title_block_agile_coaching_consulting',
								'type' => 'text',
								'column_width' => '',
								'save_format' => 'id',
							),
							array (
								'key' => 'field_0000000000761',
								'label' => 'Color',
								'name' => 'color_block_agile_coaching_consulting',
								'type' => 'color_picker',
							),
							array (
								'key' => 'field_0000000000762',
								'label' => 'Link',
								'name' => 'link_block_agile_coaching_consulting',
								'type' => 'text',
								'column_width' => '',
								'save_format' => 'id',
							),
							array (
								'key' => 'field_000000000077',
								'label' => 'Image',
								'name' => 'image_block_agile_coaching_consulting',
								'type' => 'image',
								'column_width' => '',
								'save_format' => 'id',
							),
						),
						'row_min' => 1,
						'layout' => 'row',
					),
				),
				'row_min' => 1,
				'layout' => 'row',
			),
		),
		'row_min' => 1,
		'row_limit' => '1',
		'layout' => 'row',
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-agile-coaching-consulting.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
	
//Template - Agile Coaching & Consulting Items
	register_field_group(array (
		'id' => 'agile_coaching_consulting_item',
		'title' => 'Agile Coaching & Consulting Item',
		'fields' => array (
			array(
				'key' => 'field_000000000300',
				'name' => '',
				'type' => 'tab',
				'label' => 'Banner part',
			),
			array (
				'key' => 'field_0000000000301',
				'label' => 'Background',
				'name' => 'background_agile_coaching_consulting_item',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000000000302',
				'label' => 'Background Mobile<br><i><font style="font-weight: normal; color: #ff0000;">Banner Size: 670px (w) x 350px (h)</font></i>',
				'name' => 'background_mobile_agile_coaching_consulting_item',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_00000000001acci01',
				'label' => 'Header Badge Image<br><i><font style="font-weight: normal; color: #ff0000;">Image Size: 150px (w) x 150px (h)</font></i>',
				'name' => 'header_badgeimg_agile',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_000000000303',
				'label' => 'Title',
				'name' => 'title_agile_coaching_consulting',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000304',
				'label' => 'Description',
				'name' => 'description_agile_coaching_consulting_item',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_000000000313',
				'label' => 'Title Button',
				'name' => 'title_button_agile_coaching_consulting_item',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array(
				'key' => 'field_000000000314',
				'name' => '',
				'type' => 'tab',
				'label' => 'Content part',
			),
			array (
				'key' => 'field_0000000000305',
				'label' => 'Image',
				'name' => 'image_cp_agile_coaching_consulting_item',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000000000306',
				'label' => 'Title',
				'name' => 'title_cp_agile_coaching_consulting_item',
				'type' => 'text',
			),
			array (
				'key' => 'field_000000000307',
				'label' => 'Text Blocks',
				'name' => 'text_blocks_agile_coaching_consulting_item',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_0000000000308',
						'label' => 'Title',
						'name' => 'title_tb_agile_coaching_consulting_item',
						'type' => 'text',
					),
					array (
						'key' => 'field_0000000000309',
						'label' => 'Description',
						'name' => 'description_tb_agile_coaching_consulting_item',
						'type' => 'wysiwyg',
						'column_width' => '',
						'save_format' => 'id',
						'toolbar' => 'full',
						'media_upload' => 'no',
					),
				),
				'row_min' => 1,
				'layout' => 'row',
			),
		),
		'row_min' => 1,
		'row_limit' => '1',
		'layout' => 'row',
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-agile-coaching-consulting-items.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));


//Template - Organization & Executive Orientation
	register_field_group(array (
		'id' => 'orientation_page',
		'title' => 'Page Settings',
		'fields' => array (
			//TAB 1
			array(
				'key' => 'field_0000000001orient',
				'name' => '',
				'type' => 'tab',
				'label' => 'Banner Setting',
			),
			array (
				'key' => 'field_0000000001orient01',
				'label' => 'Background',
				'name' => 'orientation_bg',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000000001orient02',
				'label' => 'Background Mobile<br><i><font style="font-weight: normal; color: #ff0000;">Banner Size: 670px (w) x 350px (h)</font></i>',
				'name' => 'orientation_mobile_bg',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_0000000001orient03',
				'label' => 'Header Title',
				'name' => 'orientation_header_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000000001orient04',
				'label' => 'Short Description',
				'name' => 'orientation_description',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_0000000001orient05',
				'label' => 'Title Button',
				'name' => 'orientation_btn_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			
			//TAB 2
			array(
				'key' => 'field_0000000002orient',
				'name' => '',
				'type' => 'tab',
				'label' => 'Left Section',
			),
			array (
				'key' => 'field_0000000002orient01',
				'label' => 'Course Overview Title',
				'name' => 'orientation_course_overview_title',
				'type' => 'text',
			),
			array (
				'key' => 'field_0000000002orient02',
				'label' => 'Course Overview Description',
				'name' => 'orientation_course_overview_desc',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_0000000002orient03',
				'label' => 'Course Agenda Title',
				'name' => 'orientation_course_agenda_title',
				'type' => 'text',
			),
			array (
				'key' => 'field_0000000002orient04',
				'label' => 'Course Agenda Description',
				'name' => 'orientation_course_agenda_desc',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			
			//TAB 3
			array(
				'key' => 'field_0000000003orient',
				'name' => '',
				'type' => 'tab',
				'label' => 'Right Section',
			),
			array (
				'key' => 'field_0000000003orient01',
				'label' => 'Main Title',
				'name' => 'orientation_right_main_title',
				'type' => 'text',
			),
			/*array (
				'key' => 'field_0000000003orient02',
				'label' => 'Sub Title',
				'name' => 'orientation_right_sub_title',
				'type' => 'text',
			),*/
			array (
				'key' => 'field_0000000003orient03',
				'label' => 'Description',
				'name' => 'orientation_right_desc',
				'type' => 'wysiwyg',
				'column_width' => '',
				'save_format' => 'id',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
		),
		'row_min' => 1,
		'row_limit' => '1',
		'layout' => 'row',
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-organization-executive-orientation.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));	
	
//Template - Our Company
	register_field_group(array (
		'id' => 'our_company',
		'title' => 'Our Company',
		'fields' => array (
		    
			//TAB 1
			array(
				'key' => 'field_000000000078',
				'name' => '',
				'type' => 'tab',
				'label' => 'Banner part',
			),
			array (
				'key' => 'field_000000000079',
				'label' => 'Title',
				'name' => 'title_our_company',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000080',
				'label' => 'Description',
				'name' => 'description_our_company',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_0000000000801',
				'label' => 'Background',
				'name' => 'background_our_company',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000000000802',
				'label' => 'Background Mobile<br><i><font style="font-weight: normal; color: #ff0000;">Banner Size: 670px (w) x 350px (h)</font></i>',
				'name' => 'background_mobile_our_company',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000081',
				'label' => 'Title social block',
				'name' => 'title_social_block_our_company',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000082',
				'label' => 'Icons',
				'name' => 'icons_social_block_our_company',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000083',
						'label' => 'Image',
						'name' => 'icon_social_block_our_company',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_0000000000831',
						'label' => 'Link',
						'name' => 'icon_link_social_block_our_company',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
				),
				'row_min' => 1,
				'layout' => 'row',
			),
			//TAB 2
			array(
				'key' => 'field_00000000001media01',
				'name' => '',
				'type' => 'tab',
				'label' => 'Media Collections',
			),
			array (
				'key' => 'field_00000000001media02',
				'label' => 'Title',
				'name' => 'media_title',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_00000000001media03',
				'label' => 'Description',
				'name' => 'media_description',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_00000000001media04',
				'label' => 'Media',
				'name' => 'media_collection',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_00000000001media041',
						'label' => 'Media Thumbnails<br><i><font style="font-weight: normal; color: #ff0000;">.jpg, .gif, .png (250px X 250px)</font></i>',
						'name' => 'media_thumbnails',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_00000000001media042',
						'label' => 'Media Title<br><i><font style="font-weight: normal; color: #ff0000;">Short Title Only</font></i>',
						'name' => 'media_imgvideo_title',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_00000000001media043',
						'label' => 'Media Full URL<br><i><font style="font-weight: normal; color: #ff0000;">https://player.vimeo.com/video/131506757 <br> https://youtu.be/uU5YPIvJ24Y</font></i>',
						'name' => 'media_imgvideo_url',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_00000000001media044',
						'label' => 'Full Image Media<br><i><font style="font-weight: normal; color: #ff0000;">Only for image gallery if there is no video available</font></i>',
						'name' => 'media_fullimg',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_00000000001media045',
						'label' => 'Categories<br><i><font style="font-weight: normal; color: #ff0000;">Will show video or image only</font></i>',
						'name' => 'media_category',
						'type' => 'radio',
						'choices' => array (
							'1' => 'Video Category',
							'0' => 'Image Category',
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => '1',
						'layout' => 'horizontal',
					),
					
				),
				'row_min' => 1,
				'layout' => 'row',
			),
			
			//TAB 3
			array(
				'key' => 'field_000000000084',
				'name' => '',
				'type' => 'tab',
				'label' => 'Our Clients',
			),
			array (
				'key' => 'field_000000000085',
				'label' => 'Title',
				'name' => 'title_our_clients',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000086',
				'label' => 'Description',
				'name' => 'description_our_clients',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000087',
				'label' => 'Clients',
				'name' => 'clients_our_clients',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_000000000088',
						'label' => 'Image',
						'name' => 'client_our_clients',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
					),
				),
				'row_min' => 1,
				'layout' => 'row',
			),
			array(
				'key' => 'field_000000000089',
				'name' => '',
				'type' => 'tab',
				'label' => 'Our Team',
			),
			array (
				'key' => 'field_000000000090',
				'label' => 'Title',
				'name' => 'title_our_team',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array(
				'key' => 'field_000000000090conTab',
				'name' => '',
				'type' => 'tab',
				'label' => 'Our Consultant',
			),
			array (
				'key' => 'field_00000000001cons',
				'label' => 'Title',
				'name' => 'title_consultant_team',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			
			array(
				'key' => 'field_000000000091',
				'name' => '',
				'type' => 'tab',
				'label' => 'Our Philanthropy',
			),
			array (
				'key' => 'field_000000000092',
				'label' => 'Title',
				'name' => 'title_our_philanthropy',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000093',
				'label' => 'Description',
				'name' => 'description_our_philanthropy',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array(
				'key' => 'field_000000000094',
				'name' => '',
				'type' => 'tab',
				'label' => 'Join us',
			),
			array (
				'key' => 'field_000000000095',
				'label' => 'Title',
				'name' => 'title_join_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000096',
				'label' => 'Description',
				'name' => 'description_join_us',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'basic',
				'media_upload' => 'no',
			),
		),
		'row_min' => 1,
		'row_limit' => '1',
		'layout' => 'row',
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-our-company.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));	
	
	
//Page - Template Training
	register_field_group(array (
		'id' => 'training_page',
		'title' => 'Training Page',
		'fields' => array (
			array(
				'key' => 'field_000000000105',
				'name' => '',
				'type' => 'tab',
				'label' => 'Banner part',
			),
			array (
				'key' => 'field_000000000106',
				'label' => 'Image',
				'name' => 'image_training_page',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000107',
				'label' => 'Title',
				'name' => 'title_training_page',
				'type' => 'text',
			),
			array (
				'key' => 'field_000000000108',
				'label' => 'Right Block',
				'name' => 'right_block_training_page',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_000000000109',
				'label' => 'Iframe video',
				'name' => 'video_training_page',
				'type' => 'textarea',
			),
			array(
				'key' => 'field_000000000111',
				'name' => '',
				'type' => 'tab',
				'label' => 'Left box',
			),
			array (
				'key' => 'field_000000000113',
				'label' => 'Title',
				'name' => 'title_left_box',
				'type' => 'text',
			),
			array (
				'key' => 'field_000000000114',
				'label' => 'Description',
				'name' => 'description_left_box',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array(
				'key' => 'field_000000000112',
				'name' => '',
				'type' => 'tab',
				'label' => 'Right box',
			),
		),
		'row_min' => 1,
		'row_limit' => '1',
		'layout' => 'row',
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-training.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));	
	
	
//Template - Agile Training
	register_field_group(array (
		'id' => 'agile_training',
		'title' => 'Agile Training',
		'fields' => array (
			array (
				'key' => 'field_53c67ac4ab24e11011',
				'label' => 'Background',
				'name' => 'repeater_bg_agile_training',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_53c67ac4ab24e11021',
						'label' => 'Background',
						'name' => 'bg_agile_training',
						'type' => 'image',
						'column_width' => '',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_53c67ac4ab24e110211',
						'label' => 'Background Mobile<br><i><font style="font-weight: normal; color: #ff0000;">Banner Size: 670px (w) x 350px (h)</font></i>',
						'name' => 'bg_agile_training_mobile',
						'type' => 'image',
						'column_width' => '',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				),
				'row_min' => 1,
				'row_limit' => '1',
				'layout' => 'row',
			),
			array (
				'key' => 'field_53c67ac4ab24e1103',
				'label' => 'Items Block',
				'name' => 'items_block',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_53c67ac4ab24e11041',
						'label' => 'Items Title',
						'name' => 'items_title_agile_training',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_53c67ac4ab24e110411',
						'label' => 'Items SubTitle',
						'name' => 'items_subtitle_agile_training',
						'type' => 'text',
						'column_width' => '',
						'save_format' => 'id',
					),
					array (
						'key' => 'field_53c67ac4ab24e110421',
						'label' => 'Items',
						'name' => 'items_agile_training',
						'type' => 'repeater',
						'sub_fields' => array (
							array (
								'key' => 'field_53c67ac4ab24e11051',
								'label' => 'Item',
								'name' => 'items_agile_training',
								'type' => 'text',
							),
						),
						'row_min' => 1,
						'row_limit' => '3',
						'layout' => 'row',
					),
										
				),
				'row_min' => 1,
				'row_limit' => '1',
				'layout' => 'row',
			),
		),	
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-agile-training.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));


//Template - Contact Us
	register_field_group(array (
		'id' => 'contact_us',
		'title' => 'Contact Us',
		'fields' => array (
			array(
				'key' => 'field_000000000200',
				'name' => '',
				'type' => 'tab',
				'label' => 'Map',
			),
			/*array (
				'key' => 'field_000000000201',
				'label' => 'Map Iframe',
				'name' => 'map_iframe_contact_us',
				'type' => 'textarea',
				'column_width' => '',
				'save_format' => 'id',
				'instructions' => 'Site map: maps.google.com',
			),*/
			array (
				'key' => 'field_0000000000203',
				'label' => 'Question',
				'name' => 'question_contact_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_0000000000204',
				'label' => 'Phone Title',
				'name' => 'phone_title_contact_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000205',
				'label' => 'Phone',
				'name' => 'phone_contact_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000206',
				'label' => 'Mail Title',
				'name' => 'mail_title_contact_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000207',
				'label' => 'Mail',
				'name' => 'mail_contact_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000208',
				'label' => 'Address',
				'name' => 'address_contact_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array(
				'key' => 'field_000000000209',
				'name' => '',
				'type' => 'tab',
				'label' => 'Form',
			),
			array (
				'key' => 'field_000000000210',
				'label' => 'Image',
				'name' => 'image_form_contact_us',
				'type' => 'image',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000211',
				'label' => 'Title Form',
				'name' => 'title_form_contact_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000212',
				'label' => 'Description Form',
				'name' => 'description_form_contact_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
			array (
				'key' => 'field_000000000213',
				'label' => 'After submit text',
				'name' => 'after_form_contact_us',
				'type' => 'text',
				'column_width' => '',
				'save_format' => 'id',
			),
		),
		'row_min' => 1,
		'row_limit' => '1',
		'layout' => 'row',
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-contact-us.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));	
	
	

	
//Type post - blog Image
	/*register_field_group(array (
		'id' => 'blog_postimg',
		'title' => 'Image',
		'fields' => array (
			array (
				'key' => 'field_0000000005001e',
				'label' => 'Thumbnail Blog Image',
				'name' => 'blog_post_image',
				'type' => 'image',
				'column_width' => '',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'save_format' => 'id',
			)
		),	
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'blog',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));*/
	
//Type post - blog 	Featured Post
	register_field_group(array (
		'id' => 'blog_postfeature',
		'title' => 'Featured Post',
		'fields' => array (
			array (
				'key' => 'field_000000000blogfeature',
					'label' => '<i><font style="font-weight: normal; color: #ff0000;">Display blog in featured.</font></i>',
					'name' => 'blog_featured',
					'type' => 'checkbox',
					'choices' => array (
						'1' => 'Display Blog in Featured Post',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					/*'default_value' => '1',*/
			),
		),	
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'blog',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 1,
	));


//Type post - blog Recents Post
	register_field_group(array (
		'id' => 'blog_postrecent',
		'title' => 'Recents Post',
		'fields' => array (
			array (
				'key' => 'field_000000000blogrecent',
					'label' => '<i><font style="font-weight: normal; color: #ff0000;">Display blog in recents post.</font></i>',
					'name' => 'blog_recent',
					'type' => 'checkbox',
					'choices' => array (
						'1' => 'Display Blog in Recent Post',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					/*'default_value' => '1',*/
			),
		),	
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'blog',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 2,
	));
/*
Front Page Setup*/



}//End of Custom


?>