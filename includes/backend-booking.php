<?php 
	
	
   if(isset($_POST["um_Delete"]) && $_POST["um_Delete"])
    {
        $curentID = $_POST["um_Delete"];
        $wpdb->get_results('delete from um_bookingTable where id ='.$curentID.';');

    }
	
	
	add_action( 'after_setup_theme', 'createBookingTable' );
	add_action( 'admin_menu', 'bookingPage' );	
	
	function createBookingTable()
	{
    	global $wpdb;
    	$table_name = 'um_bookingTable';   
    	if($wpdb->get_var("show tables like '".$table_name."'" ) != $table_name) 
    	{
	      	 $sql = "CREATE TABLE $table_name (
	              		id bigint(20) NOT NULL AUTO_INCREMENT,
	              		booking_post_id bigint(20) NOT NULL,
	              		fields longtext NOT NULL,              
	              		UNIQUE KEY id (id)
	            	);";
	            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	        dbDelta( $sql );
    	}
	}
	
	function bookingPage() 
	{
	    add_menu_page('Booking', 'Booking', 'manage_options', 'all-bookings', 'booking_page' );
	}
	
	function booking_page()
	{
  			
	  	global $wpdb; 
	    $myRows = $wpdb->get_results
	    (
	        "select ".$wpdb->prefix."posts.post_title,um_bookingTable.id ,um_bookingTable.fields
	        from ".$wpdb->prefix."posts, um_bookingTable 
	        where ".$wpdb->prefix."posts.ID = um_bookingTable.booking_post_id"
	    );
		 ?>
		 
		 
		 <div class="bookingInformation">
		 	<h2><?php _e('Booking information','um_lang'); ?></h2>
		 	
		 	<ul class="um_outterList">
	    		<?php foreach($myRows as $row):?>
					<li>
				
						
							<div class="roomImage">
					             <b><?php echo $row->id;?></b>
				             	     
				            	<p class="um_room_title"><?php echo $row->post_title;?></p>
				            	<?php $thisPost = get_page_by_title( $row->post_title , OBJECT, 'room_post' ); ?>
				            	<?php echo get_the_post_thumbnail( $thisPost->ID, 'thumbnail' ); ?>
				            </div>
				           
             				<ul class="um_innerList">			
				             	<?php 
			             			 $fields = json_decode($row->fields);
									 $fields = objectToArray($fields);
									 $innerUlClosed = false;
									 foreach($fields as $field):
        							 	foreach($field as $data):
				             	 	
				             	 ?>
			             	 			<?php if($data): ?>
				             	 			<?php if($data['value']): ?>	
				             	 			
				             	 				
					             	 			<?php if($data['type'] == 'email'):?>
													<li>
														<p><b><i class="fa <?php echo $data['icon']; ?>"></i><?php echo $data['label'];?></b></p>
														<p><a href="mailto:<?php echo $data['value'];?>"><?php echo $data['value']; ?></a></p> 
													</li>
													
												<?php elseif($data['type'] == 'area'): ?>					
													</ul>
												 		
												 		<div class="adminExtraMessage">
										                   <p><b><i class="fa <?php echo $data['icon']; ?>"></i><?php echo $data['label']; ?></b></p>
										                   <p><?php echo $data['value']; ?></p>
										                </div>
									              	<ul class="um_innerList">
						
												<?php elseif($data['type'] == 'um_arival' || $data['type'] == 'um_departure'): ?>
					             	 				
					             	 				<li>
						             	 				<div class="um_dates">
					             	 						<p><b><i class="fa <?php echo $data['icon']; ?>"></i><?php echo $data['label'];?></b></p>
				             	 							<p><?php echo $data['value']; ?></p> 
					             	 					</div>
					             	 				</li>
					             	 				
					             	 			<?php elseif($data['type'] == 'check'): ?>
										             	 				
					             	 				<?php
					             	 						$val = ""; 
					             	 						if($data['value']){
					             	 							foreach($data['value'] as $checkVal){
				             	 									if($checkVal != 'null'){
				             	 											$val .= $checkVal.' ';
																	}
					             	 							 }
					             	 						}
				             	 					?>
					             	 				<?php if($val): ?>
					             	 					<li>
					             	 						<p><b><i class="fa <?php echo $data['icon']; ?>"></i><?php echo $data['label'];?></b></p>
					             	 						<p><?php echo $val;?></p>
					             	 					</li>
				             	 					<?php endif; ?>
										             	 				 
					             	 			<?php elseif($data['type'] == 'price'): ?>
					             	 					</ul><!-- Inner list END -->
						             	 					<div class="um_price">
						             	 						<p><b><i class="fa <?php echo $data['icon']; ?>"></i><?php _e('Price', 'um_lang'); ?></b></p>
							             	 					<p class="thePrice"><?php echo $data['currency'].$data['value']; ?></p>
							             	 				</div>	
				             	 				<?php else: ?>
				             	 					<li>
					             	 					<p><b><i class="fa <?php echo $data['icon']; ?>"></i><?php echo $data['label'];?></b></p>
					             	 					<p><?php echo $data['value']; ?></p> 
					             					</li>			
					             				<?php endif; ?>
									             	
									             	<?php 
									             		//  Check if Inner list (ul) is closed
									             		if($data['type'] == 'price'){
															$innerUlClosed = true;
									             		}
									             		else{
															$innerUlClosed = false;
									             		} ?>						
									             									
				             				<?php endif; ?>
		             					<?php endif; ?>
					             								            			
		             			<?php endforeach; ?>
		             		<?php endforeach; ?>
			            
			            	<?php
			            		//  Check if Inner list (ul) is closed 
			            		if(!$innerUlClosed): ?>
			            		</ul>
		            		<?php endif; ?>
 
			             	 	   <form method="post">
                                    	<input type="hidden" name="um_Delete" value="<?php echo $row->id;?>" >
                                  		<a href="" onclick="jQuery(this).closest('form').submit(); return false;">Delete</a>
                                	</form>
				         	        
			        </li>     				    			
    			<?php endforeach; ?>	
		 	</ul>
		 </div>		 
	<?php		 	   
	}
	
	 function objectToArray($d) {
		if (is_object($d)) {
			$d = get_object_vars($d);
		}
 
		if (is_array($d)) {
			return array_map(__FUNCTION__, $d);
		}
		else {
			return $d;
		}
	}
	
?>