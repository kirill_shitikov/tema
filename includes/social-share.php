<?php function addSocialShareButtons($socialBox) {
        global $post;
        setup_postdata($post);

        if(!$socialBox){
             $socialBox = array();
        }
    ?>
    <?php if(in_array("facebook",$socialBox)):?>
		<li><a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" class="um_socialButton animated" id="facebook"><i class="fa fa-facebook-square"></i>Facebook</a></li>
    <?php endif;?>
    
	<?php if(in_array('twitter',$socialBox)):?>
		<li><a href="http://twitter.com/home?status=<?php the_title(); ?>+<?php the_permalink(); ?>" target="_blank" class="um_socialButton animated" id="twitter"><i class="fa fa-twitter-square"></i>Twitter</a></li>
	<?php endif;?>
    
	<?php if(in_array('google',$socialBox)):?>
		<li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" class="um_socialButton animated" id="google"><i class="fa fa-google-plus-square"></i>Google+</a></li>
    <?php endif;?>
    
	<?php if(in_array('linkedIn',$socialBox)):?>
		<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" class="um_socialButton animated" id="linkedin"><i class="fa fa-linkedin-square"></i>Linkdin</a></li>
    <?php endif;?>
    
	<?php if(in_array('stumble',$socialBox)):?>
		<li><a href="http://www.stumbleupon.com/submit?url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" class="um_socialButton animated" id="stumble"><i class="fa fa-stumbleupon"></i>Stumble Upon</a></li>
	<?php endif;?>
	
	<?php if(in_array('pinit',$socialBox)):?>
		<li><a href="http://pinterest.com/pin/create/link/?url=<?php the_permalink(); ?>" target="_blank" class="um_socialButton animated" id="pinit"><i class="fa fa-pinterest-square"></i>Pinterest</a></li>
	<?php endif;?>

<?php } ?>