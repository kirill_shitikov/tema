<?php function doBranding(){
 	
	/*Logo Text*/
		if(get_field('logo_type','options') == "text"):
			$logo_color = get_field('logo_color','options');
			$logo_size = get_field('logo_size','options');
			$logo_font = get_field('logo_font','options'); ?>
			
			<style>
				.logo {
					color: <?php echo $logo_color; ?> !important;
					font-size: <?php echo $logo_size; ?>px !important;
					font-family: <?php echo $logo_font['family']; ?> !important;
				}
			</style>
	
		<?php endif;
	/*Logo Text END*/
	

	
	if(!get_field('default','options')):	
	   
			/*Custom Colors*/	
		
			$brandColor 			= get_field('brand_color','options') 			== 	'' 	? "#e2b55d" : get_field('brand_color','options');
			$pri_complementColor 	= get_field('pri_complement_color','options') 	== 	'' 	? "#ffffff" : get_field('pri_complement_color','options');
			$sec_complementColor 	= get_field('sec_complement_color','options') 	== 	'' 	? "#e4e4e4" : get_field('sec_complement_color','options');
			$defaultBgColor 		= get_field('body_color','options') 			== 	'' 	? "#e4e4e4" : get_field('body_color','options');
			$pageBgColor 			= get_field('body_color'); 
			$check_default 			= get_field('default_body_color');
			$rgbBrand 				= sscanf($brandColor, "#%02x%02x%02x");
			$sec_complementRgb 		= sscanf($sec_complementColor, "#%02x%02x%02x");
			if(is_woocommerce_active()){	
				if(is_shop()){	
					$page_id = woocommerce_get_page_id('shop');
					$pageBgColor = get_field('body_color',$page_id); 
				}
			}
			
		
			if($check_default || ($pageBgColor == "")){
				$pageBgColor = $defaultBgColor;
			}
			?>
			
			<style>					
				/* Sec Complement styles */
					.btn.btn-default.um_book:hover
					{
						color:<?php echo $sec_complementColor; ?> !important;
					}
					.sub-menu, .menuPrice, 
					input, select, textarea, button a.btn, 
					.bookingForm .bootstrap-select,
					.woocommerce #payment div.payment_box, 
					.woocommerce-page #payment div.payment_box, 
					.um_table tr:nth-child(even), 
					.social,
					input, select, textarea, button a.btn,
					.bookingForm .bootstrap-select, 
					.formInput .btn-default, 
					.formInput fieldset, 
					.dropdown-menu>li>a:hover, .dropdown-menu>li>a:focus, 
					.preHeader .cart_content .widget.widget_shopping_cart, 
					.bookingForm input
					{	
						background-color: <?php echo $sec_complementColor; ?> !important;
					}
					
					.woocommerce #payment div.payment_box:after, 
					.woocommerce-page #payment div.payment_box:after 
					{
						border-color:<?php echo $sec_complementColor; ?> !important;
					}	
					.total .bookTotal, .um_table tfoot .bookTotal
					{	
						background: rgba(<?php echo $sec_complementRgb[0].','.$sec_complementRgb[1].','.$sec_complementRgb[2]; ?>,0.7) !important; 
					}
					
				/* Pri Complement styles */
					.mainBgColor,
					.woocommerce-page div.product, 
					.singleComments .inputFields input, 
					.singleComments .inputFields textarea, 
					.rsDefault.roomsSlider, 
					.rsDefault.roomsSlider .rsOverflow, 
					.rsDefault.roomsSlider .rsSlide, 
					.rsDefault.roomsSlider .rsVideoFrameHolder, 
					.rsDefault.roomsSlider .rsThumbs, 
					.closeBtn, 
					.closeLightbox, 
					.homeBoxes .widget, 
					.roomsHomeSlider .owl-nav, 
					.bgImg .pagination, 
					.roomsHomeSlider .owl-buttons, 
					.um_table tr:nth-child(odd), 
					.um_table th, 
					.um_cusotmArrows .rsArrowIcn, .rsDefault .rsArrowIcn, 
					.woocommerce span.onsale, .woocommerce-page span.onsale, 
					.woocommerce .widget_price_filter .ui-slider .ui-slider-handle, 
					.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle, 
					.woocommerce-page form input, .woocommerce-page form textarea, 
					.woocommerce-page button a.btn, .woocommerce-page .bootstrap-select, 
					.woocommerce-page .chosen-container-single .chosen-single, 
					.chosen-container .chosen-drop, 
					.woocommerce-page select, 
					.woocommerce .woocommerce-error, 
					.woocommerce .woocommerce-info, 
					.woocommerce .woocommerce-message, 
					.woocommerce-page .woocommerce-error, 
					.woocommerce-page .woocommerce-info, 
					.woocommerce-page .woocommerce-message, 
					.dropdown-menu, .searchFilter select,
					.panel, 
					.woocommerce #content div.product, 
					.woocommerce-tabs ul.tabs li.active, 
					.woocommerce div.product .woocommerce-tabs ul.tabs li.active, 
					.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active, 
					.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active,
					.woocommerce #payment, .woocommerce-page #payment,
					.woocommerce #content div.product .woocommerce-tabs ul.tabs li,
					.woocommerce div.product .woocommerce-tabs ul.tabs li,
					.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li,
					.woocommerce-page div.product .woocommerce-tabs ul.tabs li, 
					.btn.btn-default.um_book:hover
					{
						background-color: <?php echo $pri_complementColor; ?> !important;
					}
					.btn.btn-default.um_book,
					.btn-brand, 
					.commentBody li .commentText .comment_name, 
					.commentBody li .commentText .comment_name a, 
					.comment-respond .logged-in-as a, 
					.navbar-toggle, 
					.navbar-default .navbar-toggle:hover,
					.navbar-default .navbar-toggle:focus, 
					.um_cusotmArrows .rsArrowIcn:hover,
					.rsDefault .rsArrowIcn:hover, 
					.singleRoomPage .bookingInfo, 
					.theBox.menuBox:hover .menuPrice p, 
					.socialLinks li a:hover, 
					.rsDefault .rsArrowDisabled .rsArrowIcn, 
					.attrBox:hover footer h5, 
					.menuContent .menuItem .menuImg .icon, 
					.submitBtn, 
					.blogItem header i,  
					.triangle > i, 
					.triangle:hover > i,
					.owl-prev:hover i, .owl-next:hover i, 
					.bgImg .woocommerce-breadcrumb,
					.bgImg .woocommerce-breadcrumb a, 
					.bgImg .woocommerce-result-count, 
					.bgImg .woocommerce-ordering select, 
					.fourOfour .btn.white:hover, 
					#wp-calendar tbody #today,  
					.woocommerce .star-rating:before, 
					.woocommerce-page .star-rating:before, 
					.woocommerce .star-rating span:before, 
					.woocommerce-page .star-rating span:before, 
					.woocommerce .woocommerce-error:before, 
					.woocommerce .woocommerce-info:before, 
					.woocommerce .woocommerce-message:before, 
					.woocommerce-page .woocommerce-error:before, 
					.woocommerce-page .woocommerce-info:before, 
					.woocommerce-page .woocommerce-message:before, 
					.rsDefault, .rsDefault .rsOverflow, 
					.rsDefault .rsVideoFrameHolder, 
					.rsDefault .rsThumbs, 
					.mainColor,
					.um_table th
					{
						color: <?php echo $pri_complementColor; ?> !important;
					}
					.btn-default, 
					.rsDefault .rsArrowDisabled .rsArrowIcn, 
					.um_cusotmArrows .rsArrow.rsArrowDisabled .rsArrowIcn, 
					.menuPrice  
					{
						border-color: <?php echo $pri_complementColor; ?> !important;
					}
					
					.woocommerce .woocommerce-error, 
					.woocommerce .woocommerce-info, 
					.woocommerce .woocommerce-message, 
					.woocommerce-page .woocommerce-error, 
					.woocommerce-page .woocommerce-info, 
					.woocommerce-page .woocommerce-message 
					{
						text-shadow: 0 1px 0 <?php echo $pri_complementColor; ?> !important;
					}
					
				/* BG Color Styles */
					body
					{
						background: <?php echo $pageBgColor; ?> !important;
					}	
					
					::selection 
					{
						background: <?php echo $brandColor; ?> !important;
					}
						
					::-moz-selection 
					{
						background: <?php echo $brandColor; ?> !important;
					}
					
				/* Brand Color Styles */
					.brandColor, 
					.um_cusotmArrows .rsArrowIcn,
					.rsDefault .rsArrowIcn, 
					.nav.navbar-nav li a:hover, 
					.orderByIsotope li a:hover, 
					.blogItem:hover .title, 
					.pagination .page-numbers.current, 
					.owl-prev i, 
					.owl-next i, 
					.roomsHomeSlider .owl-controls .owl-buttons > div:hover i.fa, 
					.staffLinks .boxIcons a:hover, 
					.staffBox a:hover .title, 
					.roomsBox:hover .title, 
					.menuContent .menuItem:hover .menuDetails .title, 
					.headerImg .um_error_contnet, 
					.fourOfour .btn.white, 
					.btn.default,
					.btn.white:hover,
					.woocommerce span.onsale, 
					.woocommerce-page span.onsale, 
					.woocommerce #content table.cart td.actions .coupon .button, 
					.woocommerce table.cart td.actions .coupon .button, 
					.woocommerce-page #content table.cart td.actions .coupon .button, 
					.woocommerce-page table.cart td.actions .coupon .button, 
					.woocommerce #content div.product .woocommerce-tabs ul.tabs li.active, 
					.woocommerce div.product .woocommerce-tabs ul.tabs li.active, 
					.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active, 
					.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active, 
					.woocommerce table.shop_table th, 
					.woocommerce-page table.shop_table th, 
					.woocommerce #content table.cart a.remove:hover, 
					.woocommerce table.cart a.remove:hover, 
					.woocommerce-page #content table.cart a.remove:hover, 
					.woocommerce-page table.cart a.remove:hover, 
					div.products .product:hover h3,
					.woocommerce #content input.button.alt, 
					.woocommerce #respond input#submit.alt, 
					.woocommerce a.button.alt, 
					.woocommerce button.button.alt, 
					.woocommerce input.button.alt, 
					.woocommerce-page #content input.button.alt, 
					.woocommerce-page #respond input#submit.alt, 
					.woocommerce-page a.button.alt, 
					.woocommerce-page button.button.alt, 
					.woocommerce-page input.button.alt, 
					.um_cusotmArrows .rsArrowIcn, .rsDefault .rsArrowIcn, 
					.woocommerce.single-product .star-rating:before, 
					.woocommerce-page.single-product .star-rating:before, 
					.woocommerce.single-product .star-rating span:before, 
					.woocommerce-page.single-product .star-rating span:before, 
					.boxIcons a.iconHolder:hover
					{
						color : <?php echo $brandColor; ?> !important;
					}
								
					.brandBgColor, 
					.btn-brand, 
					.um_cusotmArrows .rsArrowIcn:hover,
					.rsDefault .rsArrowIcn:hover, 
					.theBox.menuBox:hover .menuPrice, 
					.socialLinks li a:hover, 
					.attrBox:hover footer, 
					.theBox.attrBox .boxContent, 
					.rsDefault .rsThumb.rsNavSelected, 
					.triangle, 
					.um_booking_lightbox .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar, 
					.owl-prev:hover, .owl-next:hover, 
					span.cart_totlWrapper, 
					.roomsBox .boxContent, 
					.fourOfour .btn.white:hover,
					.btn.default:hover,
					#wp-calendar tbody #today, 
					#wp-calendar tbody td:not(.pad):hover, 
					.widget input[type=submit], 
					.instagram-placeholder > a, 
					.widget .roomsGallery .roomItem .hoverBtn,
					.woocommerce .widget_price_filter .ui-slider .ui-slider-range, 
					.woocommerce-page .widget_price_filter .ui-slider .ui-slider-range, 
					.woocommerce #content input.button.alt:hover, 
					.woocommerce #respond input#submit.alt:hover, 
					.woocommerce a.button.alt:hover, 
					.woocommerce button.button.alt:hover, 
					.woocommerce input.button.alt:hover, 
					.woocommerce-page #content input.button.alt:hover, 
					.woocommerce-page #respond input#submit.alt:hover, 
					.woocommerce-page a.button.alt:hover, 
					.woocommerce-page button.button.alt:hover, 
					.woocommerce-page input.button.alt:hover, 
					.woocommerce #content input.button.added:before, 
					.woocommerce #respond input#submit.added:before, 
					.woocommerce a.button.added:before, 
					.woocommerce button.button.added:before, 
					.woocommerce input.button.added:before, 
					.woocommerce-page #content input.button.added:before, 
					.woocommerce-page #respond input#submit.added:before, 
					.woocommerce-page a.button.added:before, 
					.woocommerce-page button.button.added:before, 
					.woocommerce-page input.button.added:before, 
					.woocommerce .woocommerce-message:before, 
					.woocommerce-page .woocommerce-message:before, 
					.chosen-container .chosen-results li.highlighted,
					.um_table .brandBgColor, 
					.wpcf7 input[type="submit"]
					{
						background-color: <?php echo $brandColor; ?> !important;
					}
					
					.socialLinks li a:hover, 
					.services .servicePostWrapper:hover, 
					.fourOfour .btn.white, 
					a.btn.btn-default.titleUp.um_book:hover,
					.btn.default,
					.woocommerce #content table.cart td.actions .coupon .button, 
					.woocommerce table.cart td.actions .coupon .button, 
					.woocommerce-page #content table.cart td.actions .coupon .button, 
					.woocommerce-page table.cart td.actions .coupon .button,
					.woocommerce #content input.button.alt, 
					.woocommerce #respond input#submit.alt, 
					.woocommerce a.button.alt, 
					.woocommerce button.button.alt, 
					.woocommerce input.button.alt, 
					.woocommerce-page #content input.button.alt, 
					.woocommerce-page #respond input#submit.alt, 
					.woocommerce-page a.button.alt, 
					.woocommerce-page button.button.alt, 
					.woocommerce-page input.button.alt, 
					.sub-menu, 
					.blogPage .blogItem:hover .itemContainer,
					.blog2Page .blogItem:hover, 
					.pagination .page-numbers.current, 
					.owl-prev, .owl-next, 
					.roomsHomeSlider .owl-controls .owl-buttons .owl-prev:hover,
					.roomsHomeSlider .owl-controls .owl-buttons .owl-next:hover, 
					.preHeader .cart_content .widget.widget_shopping_cart, 
					.woocommerce #content div.product .woocommerce-tabs ul.tabs li.active, 
					.woocommerce div.product .woocommerce-tabs ul.tabs li.active, 
					.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active, 
					.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active, 
					.preHeader .cart_content .widget.widget_shopping_cart:before, 
					.woocommerce .woocommerce-message, 
					.woocommerce-page .woocommerce-message, 
					.um_loading, 
					.sticky
					{
						border-color:<?php echo $brandColor; ?> !important;
					}
			
					.mainBrandOverlay, 
					div.products .product:hover .um_relative .productOverlay 
					{
						background: rgba(<?php echo $rgbBrand[0].','.$rgbBrand[1].','.$rgbBrand[2]; ?>,0.85) !important; 
					}
			</style>
			
			<?php
		
			/*Custom Colors END*/
			
		
			/*Web Font*/
		
				$webFont = get_field('website_font','options');
				$fontColor = get_field('font_color','options');
						
				if($webFont):
					?>
					
						<style>
							 body{
								font-family: <?php echo $webFont['family']; ?> !important;
							} 
						</style>
					
					<?php
				endif;
					
				if($fontColor):
					if($fontColor != "dark"):
						?>
							<style>
								body
								{
									color: #e0e0e0 !important;
								}
								a:hover, a:focus 
								{
									color:#acacac !important;
								}
								
								.woocommerce #content input.button:hover, 
								.woocommerce #respond input#submit:hover, 
								.woocommerce a.button:hover, 
								.woocommerce button.button:hover, 
								.woocommerce input.button:hover, 
								.woocommerce-page #content input.button:hover, 
								.woocommerce-page #respond input#submit:hover, 
								.woocommerce-page a.button:hover, 
								.woocommerce-page button.button:hover, 
								.woocommerce-page input.button:hover, 
								div.products .product .button.product_type_simple.add_to_cart_button:hover
								{
									background:#e0e0e0 !important;
									color:#acacac !important;
								}
								
								.woocommerce #content input.button, 
								.woocommerce #respond input#submit, 
								.woocommerce a.button, 
								.woocommerce button.button, 
								.woocommerce input.button, 
								.woocommerce-page #content input.button, 
								.woocommerce-page #respond input#submit, 
								.woocommerce-page a.button, 
								.woocommerce-page button.button, 
								.woocommerce-page input.button, 
								div.products .product .button.product_type_simple.add_to_cart_button
								{
									border-color: #e0e0e0 !important;
								}
								
								body, 
								.menuContent .menuItem, 
								.blog2Page .blogItem, 
								.innerContent.singleBlogContent, 
								.um_table, 
								.um_table tr td, 
								.searchFilter .selectWrapper:not(:last-child), 
								.contactInfo h5, 
								.tags, 
								.tags > a, 
								.singleComments .inputFields input, 
								.singleComments .inputFields textarea, 
								.singleComments p.form-submit #submit, 
								.pagination .page-numbers, 
								.sidebar.sidebar_footer, 
								.chosen-container-single .chosen-single, 
								.chosen-container-active.chosen-with-drop .chosen-single, 
								.chosen-container .chosen-drop, 
								.chosen-container-single .chosen-search input[type=text], 
								.billButtons .btn.grey, 
								footer#siteFooter, 
								.theBox.menuBox ul li, 
								.roomsPage:not(.bgImg) .roomsBox footer, 
								.boxIcons a.iconHolder, 
								.searchFilter, 
								.product_list_widget li, 
								.woocommerce.widget_shopping_cart .total, 
								.woocommerce.widget_shopping_cart .buttons, 
								.cart_content .woocommerce.widget_shopping_cart .buttons, 
								.woocommerce.widget_shopping_cart .buttons a:first-child, 
								.woocommerce #content input.button, 
								.woocommerce #respond input#submit, 
								.woocommerce a.button, 
								.woocommerce button.button, 
								.woocommerce input.button, 
								.woocommerce-page #content input.button, 
								.woocommerce-page #respond input#submit, 
								.woocommerce-page a.button, 
								.woocommerce-page button.button, 
								.woocommerce-page input.button, 
								div.products .product .button.product_type_simple.add_to_cart_button, 
								.woocommerce-page .quantity input.qty, 
								.woocommerce #content .quantity .minus, 
								.woocommerce #content .quantity .plus, 
								.woocommerce .quantity .minus, 
								.woocommerce .quantity .plus, 
								.woocommerce-page #content .quantity .minus, 
								.woocommerce-page #content .quantity .plus, 
								.woocommerce-page .quantity .minus, 
								.woocommerce-page .quantity .plus, 
								.woocommerce #content div.product .woocommerce-tabs ul.tabs li, 
								.woocommerce div.product .woocommerce-tabs ul.tabs li, 
								.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li, 
								.woocommerce-page div.product .woocommerce-tabs ul.tabs li, 
								.woocommerce #content div.product .woocommerce-tabs ul.tabs li.active, 
								.woocommerce div.product .woocommerce-tabs ul.tabs li.active, 
								.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active, 
								.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active, 
								.woocommerce #reviews #comments .comment_container, 
								.shopPage .comment-form p input[type="text"], 
								.shopPage .comment-form p.comment-form-rating, 
								.shopPage .comment-form p #comment, 
								.woocommerce table.shop_table td, 
								.woocommerce-page table.shop_table td, 
								.woocommerce #content table.cart td.actions .coupon .input-text, 
								.woocommerce table.cart td.actions .coupon .input-text, 
								.woocommerce-page #content table.cart td.actions .coupon .input-text, 
								.woocommerce-page table.cart td.actions .coupon .input-text, 
								.shipping-calculator-form input, 
								.shipping-calculator-form select, 
								.woocommerce .woocommerce-error, 
								.woocommerce .woocommerce-info, 
								.woocommerce .woocommerce-message, 
								.woocommerce-page .woocommerce-error, 
								.woocommerce-page .woocommerce-info, 
								.woocommerce-page .woocommerce-message, 
								.woocommerce #payment, 
								.woocommerce-page #payment, 
								.woocommerce-page input, 
								.woocommerce-page textarea, 
								.woocommerce-page button a.btn, 
								.woocommerce-page .bootstrap-select, 
								.woocommerce #payment ul.payment_methods, 
								.woocommerce-page #payment ul.payment_methods, 
								.woocommerce .order_details, .woocommerce-page .order_details, 
								.woocommerce #content div.product .woocommerce-tabs ul.tabs:before, 
								.woocommerce div.product .woocommerce-tabs ul.tabs:before, 
								.woocommerce-page #content div.product .woocommerce-tabs ul.tabs:before, 
								.woocommerce-page div.product .woocommerce-tabs ul.tabs:before
								{
									border-color:rgba(255,255,255,0.1) !important;
								}
								
								
							</style>
						<?php				
					endif;
				endif;		
				
			/*Web Font END*/
		
	endif;	
 }




function setBgImg ($global = false){
		
		$imgURL = "";	
		$featuredImageType = "";
		$opacity = 1;
		$color = '#000000';

		 if(!is_woocommerce_active()){
			if($global){
				if(get_field('background_image','options')){
		
					$overlayPercantage = get_field('overlay_percentage','options');
					$opacity = 1 - ($overlayPercantage / 100);
					$color = get_field('overlay_color','options');
		
					$featuredImageType = get_field('background_image_type','options');	
					$imgURL = wp_get_attachment_url(get_field('background_image','options'));
				}
			}
			else{
					
				if(has_post_thumbnail()){
					
					$overlayPercantage = get_field('overlay_percentage');
					$opacity = 1 - ($overlayPercantage / 100);
					$color = get_field('overlay_color');
						
					$featuredImageType = get_field('background_image_type');
					 global $post;
					 $imgURL = wp_get_attachment_url(get_post_thumbnail_id($post->ID));	
				}
			}	
		 }
		 else{
				if(!is_shop()){
					
					if($global){						
						if(get_field('background_image','options')){
							
							$overlayPercantage = get_field('overlay_percentage','options');
							$opacity = 1 - ($overlayPercantage / 100);
							$color = get_field('overlay_color','options');
							
							$featuredImageType = get_field('background_image_type','options');	
							$imgURL = wp_get_attachment_url(get_field('background_image','options'));
						}
					}
					else{
						if(has_post_thumbnail()){
								
							$overlayPercantage = get_field('overlay_percentage');
							$opacity = 1 - ($overlayPercantage / 100);
							$color = get_field('overlay_color');	
									
							$featuredImageType = get_field('background_image_type');
							 global $post;
							 $imgURL = wp_get_attachment_url(get_post_thumbnail_id($post->ID));	
						}
					}
				}
				else{
					$page_id = woocommerce_get_page_id('shop');
					if(get_post_thumbnail_id($page_id)){
						
							$overlayPercantage = get_field('overlay_percentage',$page_id);
							$opacity = 1 - ($overlayPercantage / 100);
							$color = get_field('overlay_color',$page_id);
							
							$featuredImageType = get_field('background_image_type',$page_id);
							 $imgURL = wp_get_attachment_url(get_post_thumbnail_id($page_id));	
						}
				}
		 }
		if($imgURL != ""):	
			if($featuredImageType == 'bgImg'): ?>
			
				<style>
					.bgImg .pageImage .fullBg{
						background-image:url("<?php echo $imgURL; ?>");
						opacity: <?php echo $opacity; ?>; 
					}
					.bgImg .pageImage {
						bottom:0;
						background: <?php echo $color; ?>;
					}
				</style>
				
			<?php elseif($featuredImageType == 'headerImg'): ?>	
				<style>
					.headerImg .um_parallax{
						background-image:url("<?php echo $imgURL; ?>"); 
						opacity: <?php echo $opacity; ?>; 
					}
					.headerImg .pageImage{
						background: <?php echo $color; ?>;
					}
					
				</style>
				
			<?php endif;
			endif;			
	}
 ?>