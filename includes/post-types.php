<?php 
add_action( 'init', 'register_posts' );


/*Custom Post Type*/
function register_posts() {
	//Custom Blog Post
	register_post_type( 'blog',	
		array(
			'labels' => array(
				'add_new_item' => __('Add New Blog Post'),
				'edit_item' => __('Edit Blog Post'),
				'name' => __( "Blog","um_lang"),
				'singular_name' => __( "blog" ,"um_lang")

			),
			'public' => true,
			'has_archive' => true,
			'menu_icon'   => 'dashicons-welcome-write-blog',
			'rewrite' => array('slug' => "blog", 'with_front' => TRUE),
			'supports' => array('title','editor','thumbnail','page-attributes','comments'),
		)
	);
	
	
	//Custom Community Post
	/*register_post_type( 'communities',	
		array(
			'labels' => array(
				'add_new_item' => __('Add New Community'),
				'edit_item' => __('Edit Community'),
				'name' => __( "Community","um_lang"),
				'singular_name' => __( "communities" ,"um_lang")

			),
			'public' => true,
			'has_archive' => true,
			'menu_icon'   => 'dashicons-admin-home',
			'rewrite' => array('slug' => "communities", 'with_front' => TRUE),
			'supports' => array('title')
		)
	);*/
	/*register_taxonomy('blog_tag', 'blog',
		array( 
			'hierarchical' => false, 
			'label' => __('Tags',"um_lang"),
			'show_ui' => true,
			'query_var' => true,
			'show_admin_column' => true,
			'singular_label' => __('tag',"um_lang")
		) 
	);*/


    /*Testimonial Custom Post*/
	register_post_type( 'testimonials',	
		array(
			'labels' => array(
				'add_new_item' => __('Add New Testimonial'),
				'edit_item' => __('Edit Testimonial'),
				'name' => __( "Testimonial","um_lang"),
				'singular_name' => __( "testimonials" ,"um_lang")

			),
			'public' => true,
			'has_archive' => true,
			'menu_icon'   => 'dashicons-groups',
			'rewrite' => array('slug' => "testimonials", 'with_front' => TRUE),
			'supports' => array('title','editor'),
		)
	);
    
	

}

?>