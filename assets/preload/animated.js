window.Modernizr = function(e, t, n) {
        function r(e) {
            m.cssText = e
        }

        function i(e, t) {
            return r(b.join(e + ";") + (t || ""))
        }

        function s(e, t) {
            return typeof e === t
        }

        function o(e, t) {
            return !!~("" + e).indexOf(t)
        }

        function u(e, t) {
            for (var r in e) {
                var i = e[r];
                if (!o(i, "-") && m[i] !== n) return t == "pfx" ? i : !0
            }
            return !1
        }

        function a(e, t, r) {
            for (var i in e) {
                var o = t[e[i]];
                if (o !== n) return r === !1 ? e[i] : s(o, "function") ? o.bind(r || t) : o
            }
            return !1
        }

        function f(e, t, n) {
            var r = e.charAt(0).toUpperCase() + e.slice(1),
                i = (e + " " + E.join(r + " ") + r).split(" ");
            return s(t, "string") || s(t, "undefined") ? u(i, t) : (i = (e + " " + S.join(r + " ") + r).split(" "), a(i, t, n))
        }
        var l = "2.8.3",
            c = {},
            h = !0,
            p = t.documentElement,
            d = "modernizr",
            v = t.createElement(d),
            m = v.style,
            g, y = {}.toString,
            b = " -webkit- -moz- -o- -ms- ".split(" "),
            w = "Webkit Moz O ms",
            E = w.split(" "),
            S = w.toLowerCase().split(" "),
            x = {},
            T = {},
            N = {},
            C = [],
            k = C.slice,
            L, A = function(e, n, r, i) {
                var s, o, u, a, f = t.createElement("div"),
                    l = t.body,
                    c = l || t.createElement("body");
                if (parseInt(r, 10))
                    while (r--) u = t.createElement("div"), u.id = i ? i[r] : d + (r + 1), f.appendChild(u);
                return s = ["&#173;", '<style id="s', d, '">', e, "</style>"].join(""), f.id = d, (l ? f : c).innerHTML += s, c.appendChild(f), l || (c.style.background = "", c.style.overflow = "hidden", a = p.style.overflow, p.style.overflow = "hidden", p.appendChild(c)), o = n(f, e), l ? f.parentNode.removeChild(f) : (c.parentNode.removeChild(c), p.style.overflow = a), !!o
            },
            O = function(t) {
                var n = e.matchMedia || e.msMatchMedia;
                if (n) return n(t) && n(t).matches || !1;
                var r;
                return A("@media " + t + " { #" + d + " { position: absolute; } }", function(t) {
                    r = (e.getComputedStyle ? getComputedStyle(t, null) : t.currentStyle)["position"] == "absolute"
                }), r
            },
            M = {}.hasOwnProperty,
            _;
        !s(M, "undefined") && !s(M.call, "undefined") ? _ = function(e, t) {
            return M.call(e, t)
        } : _ = function(e, t) {
            return t in e && s(e.constructor.prototype[t], "undefined")
        }, Function.prototype.bind || (Function.prototype.bind = function(e) {
            var t = this;
            if (typeof t != "function") throw new TypeError;
            var n = k.call(arguments, 1),
                r = function() {
                    if (this instanceof r) {
                        var i = function() {};
                        i.prototype = t.prototype;
                        var s = new i,
                            o = t.apply(s, n.concat(k.call(arguments)));
                        return Object(o) === o ? o : s
                    }
                    return t.apply(e, n.concat(k.call(arguments)))
                };
            return r
        }), x.touch = function() {
            var n;
            return "ontouchstart" in e || e.DocumentTouch && t instanceof DocumentTouch ? n = !0 : A(["@media (", b.join("touch-enabled),("), d, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(e) {
                n = e.offsetTop === 9
            }), n
        }, x.csscolumns = function() {
            return f("columnCount")
        }, x.csstransforms3d = function() {
            var e = !!f("perspective");
            return e && "webkitPerspective" in p.style && A("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(t, n) {
                e = t.offsetLeft === 9 && t.offsetHeight === 3
            }), e
        };
        for (var D in x) _(x, D) && (L = D.toLowerCase(), c[L] = x[D](), C.push((c[L] ? "" : "no-") + L));
        return c.addTest = function(e, t) {
                if (typeof e == "object")
                    for (var r in e) _(e, r) && c.addTest(r, e[r]);
                else {
                    e = e.toLowerCase();
                    if (c[e] !== n) return c;
                    t = typeof t == "function" ? t() : t, typeof h != "undefined" && h && (p.className += " " + (t ? "" : "no-") + e), c[e] = t
                }
                return c
            }, r(""), v = g = null,
            function(e, t) {
                function n(e, t) {
                    var n = e.createElement("p"),
                        r = e.getElementsByTagName("head")[0] || e.documentElement;
                    return n.innerHTML = "x<style>" + t + "</style>", r.insertBefore(n.lastChild, r.firstChild)
                }

                function r() {
                    var e = y.elements;
                    return typeof e == "string" ? e.split(" ") : e
                }

                function i(e) {
                    var t = m[e[d]];
                    return t || (t = {}, v++, e[d] = v, m[v] = t), t
                }

                function s(e, n, r) {
                    n || (n = t);
                    if (g) return n.createElement(e);
                    r || (r = i(n));
                    var s;
                    return r.cache[e] ? s = r.cache[e].cloneNode() : h.test(e) ? s = (r.cache[e] = r.createElem(e)).cloneNode() : s = r.createElem(e), s.canHaveChildren && !c.test(e) && !s.tagUrn ? r.frag.appendChild(s) : s
                }

                function o(e, n) {
                    e || (e = t);
                    if (g) return e.createDocumentFragment();
                    n = n || i(e);
                    var s = n.frag.cloneNode(),
                        o = 0,
                        u = r(),
                        a = u.length;
                    for (; o < a; o++) s.createElement(u[o]);
                    return s
                }

                function u(e, t) {
                    t.cache || (t.cache = {}, t.createElem = e.createElement, t.createFrag = e.createDocumentFragment, t.frag = t.createFrag()), e.createElement = function(n) {
                        return y.shivMethods ? s(n, e, t) : t.createElem(n)
                    }, e.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + r().join().replace(/[\w\-]+/g, function(e) {
                        return t.createElem(e), t.frag.createElement(e), 'c("' + e + '")'
                    }) + ");return n}")(y, t.frag)
                }

                function a(e) {
                    e || (e = t);
                    var r = i(e);
                    return y.shivCSS && !p && !r.hasCSS && (r.hasCSS = !!n(e, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), g || u(e, r), e
                }
                var f = "3.7.0",
                    l = e.html5 || {},
                    c = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
                    h = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
                    p, d = "_html5shiv",
                    v = 0,
                    m = {},
                    g;
                (function() {
                    try {
                        var e = t.createElement("a");
                        e.innerHTML = "<xyz></xyz>", p = "hidden" in e, g = e.childNodes.length == 1 || function() {
                            t.createElement("a");
                            var e = t.createDocumentFragment();
                            return typeof e.cloneNode == "undefined" || typeof e.createDocumentFragment == "undefined" || typeof e.createElement == "undefined"
                        }()
                    } catch (n) {
                        p = !0, g = !0
                    }
                })();
                var y = {
                    elements: l.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
                    version: f,
                    shivCSS: l.shivCSS !== !1,
                    supportsUnknownElements: g,
                    shivMethods: l.shivMethods !== !1,
                    type: "default",
                    shivDocument: a,
                    createElement: s,
                    createDocumentFragment: o
                };
                e.html5 = y, a(t)
            }(this, t), c._version = l, c._prefixes = b, c._domPrefixes = S, c._cssomPrefixes = E, c.mq = O, c.testProp = function(e) {
                return u([e])
            }, c.testAllProps = f, c.testStyles = A, c.prefixed = function(e, t, n) {
                return t ? f(e, t, n) : f(e, "pfx")
            }, p.className = p.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (h ? " js " + C.join(" ") : ""), c
    }(this, this.document), define("modernizr", function(e) {
        return function() {
            var t, n;
            return t || e.Modernizr
        }
    }(this)), ! function(e, t) {
        "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
            if (!e.document) throw new Error("jQuery requires a window with a document");
            return t(e)
        } : t(e)
    }("undefined" != typeof window ? window : this, function(a, b) {
        function s(e) {
            var t = e.length,
                r = n.type(e);
            return "function" === r || n.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === r || 0 === t || "number" == typeof t && t > 0 && t - 1 in e
        }

        function x(e, t, r) {
            if (n.isFunction(t)) return n.grep(e, function(e, n) {
                return !!t.call(e, n, e) !== r
            });
            if (t.nodeType) return n.grep(e, function(e) {
                return e === t !== r
            });
            if ("string" == typeof t) {
                if (w.test(t)) return n.filter(t, e, r);
                t = n.filter(t, e)
            }
            return n.grep(e, function(e) {
                return g.call(t, e) >= 0 !== r
            })
        }

        function D(e, t) {
            while ((e = e[t]) && 1 !== e.nodeType);
            return e
        }

        function G(e) {
            var t = F[e] = {};
            return n.each(e.match(E) || [], function(e, n) {
                t[n] = !0
            }), t
        }

        function I() {
            l.removeEventListener("DOMContentLoaded", I, !1), a.removeEventListener("load", I, !1), n.ready()
        }

        function K() {
            Object.defineProperty(this.cache = {}, 0, {
                get: function() {
                    return {}
                }
            }), this.expando = n.expando + K.uid++
        }

        function P(e, t, r) {
            var i;
            if (void 0 === r && 1 === e.nodeType)
                if (i = "data-" + t.replace(O, "-$1").toLowerCase(), r = e.getAttribute(i), "string" == typeof r) {
                    try {
                        r = "true" === r ? !0 : "false" === r ? !1 : "null" === r ? null : +r + "" === r ? +r : N.test(r) ? n.parseJSON(r) : r
                    } catch (s) {}
                    M.set(e, t, r)
                } else r = void 0;
            return r
        }

        function Z() {
            return !0
        }

        function $() {
            return !1
        }

        function _() {
            try {
                return l.activeElement
            } catch (e) {}
        }

        function jb(e, t) {
            return n.nodeName(e, "table") && n.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
        }

        function kb(e) {
            return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
        }

        function lb(e) {
            var t = gb.exec(e.type);
            return t ? e.type = t[1] : e.removeAttribute("type"), e
        }

        function mb(e, t) {
            for (var n = 0, r = e.length; r > n; n++) L.set(e[n], "globalEval", !t || L.get(t[n], "globalEval"))
        }

        function nb(e, t) {
            var r, i, s, o, u, a, f, l;
            if (1 === t.nodeType) {
                if (L.hasData(e) && (o = L.access(e), u = L.set(t, o), l = o.events)) {
                    delete u.handle, u.events = {};
                    for (s in l)
                        for (r = 0, i = l[s].length; i > r; r++) n.event.add(t, s, l[s][r])
                }
                M.hasData(e) && (a = M.access(e), f = n.extend({}, a), M.set(t, f))
            }
        }

        function ob(e, t) {
            var r = e.getElementsByTagName ? e.getElementsByTagName(t || "*") : e.querySelectorAll ? e.querySelectorAll(t || "*") : [];
            return void 0 === t || t && n.nodeName(e, t) ? n.merge([e], r) : r
        }

        function pb(e, t) {
            var n = t.nodeName.toLowerCase();
            "input" === n && T.test(e.type) ? t.checked = e.checked : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
        }

        function sb(e, t) {
            var r, i = n(t.createElement(e)).appendTo(t.body),
                s = a.getDefaultComputedStyle && (r = a.getDefaultComputedStyle(i[0])) ? r.display : n.css(i[0], "display");
            return i.detach(), s
        }

        function tb(e) {
            var t = l,
                r = rb[e];
            return r || (r = sb(e, t), "none" !== r && r || (qb = (qb || n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = qb[0].contentDocument, t.write(), t.close(), r = sb(e, t), qb.detach()), rb[e] = r), r
        }

        function xb(e, t, r) {
            var i, s, o, u, a = e.style;
            return r = r || wb(e), r && (u = r.getPropertyValue(t) || r[t]), r && ("" !== u || n.contains(e.ownerDocument, e) || (u = n.style(e, t)), vb.test(u) && ub.test(t) && (i = a.width, s = a.minWidth, o = a.maxWidth, a.minWidth = a.maxWidth = a.width = u, u = r.width, a.width = i, a.minWidth = s, a.maxWidth = o)), void 0 !== u ? u + "" : u
        }

        function yb(e, t) {
            return {
                get: function() {
                    return e() ? void delete this.get : (this.get = t).apply(this, arguments)
                }
            }
        }

        function Fb(e, t) {
            if (t in e) return t;
            var n = t[0].toUpperCase() + t.slice(1),
                r = t,
                i = Eb.length;
            while (i--)
                if (t = Eb[i] + n, t in e) return t;
            return r
        }

        function Gb(e, t, n) {
            var r = Ab.exec(t);
            return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t
        }

        function Hb(e, t, r, i, s) {
            for (var o = r === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, u = 0; 4 > o; o += 2) "margin" === r && (u += n.css(e, r + R[o], !0, s)), i ? ("content" === r && (u -= n.css(e, "padding" + R[o], !0, s)), "margin" !== r && (u -= n.css(e, "border" + R[o] + "Width", !0, s))) : (u += n.css(e, "padding" + R[o], !0, s), "padding" !== r && (u += n.css(e, "border" + R[o] + "Width", !0, s)));
            return u
        }

        function Ib(e, t, r) {
            var i = !0,
                s = "width" === t ? e.offsetWidth : e.offsetHeight,
                o = wb(e),
                u = "border-box" === n.css(e, "boxSizing", !1, o);
            if (0 >= s || null == s) {
                if (s = xb(e, t, o), (0 > s || null == s) && (s = e.style[t]), vb.test(s)) return s;
                i = u && (k.boxSizingReliable() || s === e.style[t]), s = parseFloat(s) || 0
            }
            return s + Hb(e, t, r || (u ? "border" : "content"), i, o) + "px"
        }

        function Jb(e, t) {
            for (var r, i, s, o = [], u = 0, a = e.length; a > u; u++) i = e[u], i.style && (o[u] = L.get(i, "olddisplay"), r = i.style.display, t ? (o[u] || "none" !== r || (i.style.display = ""), "" === i.style.display && S(i) && (o[u] = L.access(i, "olddisplay", tb(i.nodeName)))) : (s = S(i), "none" === r && s || L.set(i, "olddisplay", s ? r : n.css(i, "display"))));
            for (u = 0; a > u; u++) i = e[u], i.style && (t && "none" !== i.style.display && "" !== i.style.display || (i.style.display = t ? o[u] || "" : "none"));
            return e
        }

        function Kb(e, t, n, r, i) {
            return new Kb.prototype.init(e, t, n, r, i)
        }

        function Sb() {
            return setTimeout(function() {
                Lb = void 0
            }), Lb = n.now()
        }

        function Tb(e, t) {
            var n, r = 0,
                i = {
                    height: e
                };
            for (t = t ? 1 : 0; 4 > r; r += 2 - t) n = R[r], i["margin" + n] = i["padding" + n] = e;
            return t && (i.opacity = i.width = e), i
        }

        function Ub(e, t, n) {
            for (var r, i = (Rb[t] || []).concat(Rb["*"]), s = 0, o = i.length; o > s; s++)
                if (r = i[s].call(n, t, e)) return r
        }

        function Vb(e, t, r) {
            var i, s, o, u, a, f, l, c, h = this,
                p = {},
                d = e.style,
                v = e.nodeType && S(e),
                m = L.get(e, "fxshow");
            r.queue || (a = n._queueHooks(e, "fx"), null == a.unqueued && (a.unqueued = 0, f = a.empty.fire, a.empty.fire = function() {
                a.unqueued || f()
            }), a.unqueued++, h.always(function() {
                h.always(function() {
                    a.unqueued--, n.queue(e, "fx").length || a.empty.fire()
                })
            })), 1 === e.nodeType && ("height" in t || "width" in t) && (r.overflow = [d.overflow, d.overflowX, d.overflowY], l = n.css(e, "display"), c = "none" === l ? L.get(e, "olddisplay") || tb(e.nodeName) : l, "inline" === c && "none" === n.css(e, "float") && (d.display = "inline-block")), r.overflow && (d.overflow = "hidden", h.always(function() {
                d.overflow = r.overflow[0], d.overflowX = r.overflow[1], d.overflowY = r.overflow[2]
            }));
            for (i in t)
                if (s = t[i], Nb.exec(s)) {
                    if (delete t[i], o = o || "toggle" === s, s === (v ? "hide" : "show")) {
                        if ("show" !== s || !m || void 0 === m[i]) continue;
                        v = !0
                    }
                    p[i] = m && m[i] || n.style(e, i)
                } else l = void 0;
            if (n.isEmptyObject(p)) "inline" === ("none" === l ? tb(e.nodeName) : l) && (d.display = l);
            else {
                m ? "hidden" in m && (v = m.hidden) : m = L.access(e, "fxshow", {}), o && (m.hidden = !v), v ? n(e).show() : h.done(function() {
                    n(e).hide()
                }), h.done(function() {
                    var t;
                    L.remove(e, "fxshow");
                    for (t in p) n.style(e, t, p[t])
                });
                for (i in p) u = Ub(v ? m[i] : 0, i, h), i in m || (m[i] = u.start, v && (u.end = u.start, u.start = "width" === i || "height" === i ? 1 : 0))
            }
        }

        function Wb(e, t) {
            var r, i, s, o, u;
            for (r in e)
                if (i = n.camelCase(r), s = t[i], o = e[r], n.isArray(o) && (s = o[1], o = e[r] = o[0]), r !== i && (e[i] = o, delete e[r]), u = n.cssHooks[i], u && "expand" in u) {
                    o = u.expand(o), delete e[i];
                    for (r in o) r in e || (e[r] = o[r], t[r] = s)
                } else t[i] = s
        }

        function Xb(e, t, r) {
            var i, s, o = 0,
                u = Qb.length,
                a = n.Deferred().always(function() {
                    delete f.elem
                }),
                f = function() {
                    if (s) return !1;
                    for (var t = Lb || Sb(), n = Math.max(0, l.startTime + l.duration - t), r = n / l.duration || 0, i = 1 - r, o = 0, u = l.tweens.length; u > o; o++) l.tweens[o].run(i);
                    return a.notifyWith(e, [l, i, n]), 1 > i && u ? n : (a.resolveWith(e, [l]), !1)
                },
                l = a.promise({
                    elem: e,
                    props: n.extend({}, t),
                    opts: n.extend(!0, {
                        specialEasing: {}
                    }, r),
                    originalProperties: t,
                    originalOptions: r,
                    startTime: Lb || Sb(),
                    duration: r.duration,
                    tweens: [],
                    createTween: function(t, r) {
                        var i = n.Tween(e, l.opts, t, r, l.opts.specialEasing[t] || l.opts.easing);
                        return l.tweens.push(i), i
                    },
                    stop: function(t) {
                        var n = 0,
                            r = t ? l.tweens.length : 0;
                        if (s) return this;
                        for (s = !0; r > n; n++) l.tweens[n].run(1);
                        return t ? a.resolveWith(e, [l, t]) : a.rejectWith(e, [l, t]), this
                    }
                }),
                c = l.props;
            for (Wb(c, l.opts.specialEasing); u > o; o++)
                if (i = Qb[o].call(l, e, c, l.opts)) return i;
            return n.map(c, Ub, l), n.isFunction(l.opts.start) && l.opts.start.call(e, l), n.fx.timer(n.extend(f, {
                elem: e,
                anim: l,
                queue: l.opts.queue
            })), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always)
        }

        function qc(e) {
            return function(t, r) {
                "string" != typeof t && (r = t, t = "*");
                var i, s = 0,
                    o = t.toLowerCase().match(E) || [];
                if (n.isFunction(r))
                    while (i = o[s++]) "+" === i[0] ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(r)) : (e[i] = e[i] || []).push(r)
            }
        }

        function rc(e, t, r, i) {
            function u(l) {
                var h;
                return s[l] = !0, n.each(e[l] || [], function(e, n) {
                    var a = n(t, r, i);
                    return "string" != typeof a || o || s[a] ? o ? !(h = a) : void 0 : (t.dataTypes.unshift(a), u(a), !1)
                }), h
            }
            var s = {},
                o = e === mc;
            return u(t.dataTypes[0]) || !s["*"] && u("*")
        }

        function sc(e, t) {
            var r, i, s = n.ajaxSettings.flatOptions || {};
            for (r in t) void 0 !== t[r] && ((s[r] ? e : i || (i = {}))[r] = t[r]);
            return i && n.extend(!0, e, i), e
        }

        function tc(e, t, n) {
            var r, i, s, o, u = e.contents,
                a = e.dataTypes;
            while ("*" === a[0]) a.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
            if (r)
                for (i in u)
                    if (u[i] && u[i].test(r)) {
                        a.unshift(i);
                        break
                    }
            if (a[0] in n) s = a[0];
            else {
                for (i in n) {
                    if (!a[0] || e.converters[i + " " + a[0]]) {
                        s = i;
                        break
                    }
                    o || (o = i)
                }
                s = s || o
            }
            return s ? (s !== a[0] && a.unshift(s), n[s]) : void 0
        }

        function uc(e, t, n, r) {
            var i, s, o, u, a, f = {},
                l = e.dataTypes.slice();
            if (l[1])
                for (o in e.converters) f[o.toLowerCase()] = e.converters[o];
            s = l.shift();
            while (s)
                if (e.responseFields[s] && (n[e.responseFields[s]] = t), !a && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), a = s, s = l.shift())
                    if ("*" === s) s = a;
                    else if ("*" !== a && a !== s) {
                if (o = f[a + " " + s] || f["* " + s], !o)
                    for (i in f)
                        if (u = i.split(" "), u[1] === s && (o = f[a + " " + u[0]] || f["* " + u[0]])) {
                            o === !0 ? o = f[i] : f[i] !== !0 && (s = u[0], l.unshift(u[1]));
                            break
                        }
                if (o !== !0)
                    if (o && e["throws"]) t = o(t);
                    else try {
                        t = o(t)
                    } catch (c) {
                        return {
                            state: "parsererror",
                            error: o ? c : "No conversion from " + a + " to " + s
                        }
                    }
            }
            return {
                state: "success",
                data: t
            }
        }

        function Ac(e, t, r, i) {
            var s;
            if (n.isArray(t)) n.each(t, function(t, n) {
                r || wc.test(e) ? i(e, n) : Ac(e + "[" + ("object" == typeof n ? t : "") + "]", n, r, i)
            });
            else if (r || "object" !== n.type(t)) i(e, t);
            else
                for (s in t) Ac(e + "[" + s + "]", t[s], r, i)
        }

        function Jc(e) {
            return n.isWindow(e) ? e : 9 === e.nodeType && e.defaultView
        }
        var c = [],
            d = c.slice,
            e = c.concat,
            f = c.push,
            g = c.indexOf,
            h = {},
            i = h.toString,
            j = h.hasOwnProperty,
            k = {},
            l = a.document,
            m = "2.1.3",
            n = function(e, t) {
                return new n.fn.init(e, t)
            },
            o = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
            p = /^-ms-/,
            q = /-([\da-z])/gi,
            r = function(e, t) {
                return t.toUpperCase()
            };
        n.fn = n.prototype = {
            jquery: m,
            constructor: n,
            selector: "",
            length: 0,
            toArray: function() {
                return d.call(this)
            },
            get: function(e) {
                return null != e ? 0 > e ? this[e + this.length] : this[e] : d.call(this)
            },
            pushStack: function(e) {
                var t = n.merge(this.constructor(), e);
                return t.prevObject = this, t.context = this.context, t
            },
            each: function(e, t) {
                return n.each(this, e, t)
            },
            map: function(e) {
                return this.pushStack(n.map(this, function(t, n) {
                    return e.call(t, n, t)
                }))
            },
            slice: function() {
                return this.pushStack(d.apply(this, arguments))
            },
            first: function() {
                return this.eq(0)
            },
            last: function() {
                return this.eq(-1)
            },
            eq: function(e) {
                var t = this.length,
                    n = +e + (0 > e ? t : 0);
                return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
            },
            end: function() {
                return this.prevObject || this.constructor(null)
            },
            push: f,
            sort: c.sort,
            splice: c.splice
        }, n.extend = n.fn.extend = function() {
            var e, t, r, i, s, o, u = arguments[0] || {},
                a = 1,
                f = arguments.length,
                l = !1;
            for ("boolean" == typeof u && (l = u, u = arguments[a] || {}, a++), "object" == typeof u || n.isFunction(u) || (u = {}), a === f && (u = this, a--); f > a; a++)
                if (null != (e = arguments[a]))
                    for (t in e) r = u[t], i = e[t], u !== i && (l && i && (n.isPlainObject(i) || (s = n.isArray(i))) ? (s ? (s = !1, o = r && n.isArray(r) ? r : []) : o = r && n.isPlainObject(r) ? r : {}, u[t] = n.extend(l, o, i)) : void 0 !== i && (u[t] = i));
            return u
        }, n.extend({
            expando: "jQuery" + (m + Math.random()).replace(/\D/g, ""),
            isReady: !0,
            error: function(e) {
                throw new Error(e)
            },
            noop: function() {},
            isFunction: function(e) {
                return "function" === n.type(e)
            },
            isArray: Array.isArray,
            isWindow: function(e) {
                return null != e && e === e.window
            },
            isNumeric: function(e) {
                return !n.isArray(e) && e - parseFloat(e) + 1 >= 0
            },
            isPlainObject: function(e) {
                return "object" !== n.type(e) || e.nodeType || n.isWindow(e) ? !1 : e.constructor && !j.call(e.constructor.prototype, "isPrototypeOf") ? !1 : !0
            },
            isEmptyObject: function(e) {
                var t;
                for (t in e) return !1;
                return !0
            },
            type: function(e) {
                return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? h[i.call(e)] || "object" : typeof e
            },
            globalEval: function(a) {
                var b, c = eval;
                a = n.trim(a), a && (1 === a.indexOf("use strict") ? (b = l.createElement("script"), b.text = a, l.head.appendChild(b).parentNode.removeChild(b)) : c(a))
            },
            camelCase: function(e) {
                return e.replace(p, "ms-").replace(q, r)
            },
            nodeName: function(e, t) {
                return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
            },
            each: function(e, t, n) {
                var r, i = 0,
                    o = e.length,
                    u = s(e);
                if (n) {
                    if (u) {
                        for (; o > i; i++)
                            if (r = t.apply(e[i], n), r === !1) break
                    } else
                        for (i in e)
                            if (r = t.apply(e[i], n), r === !1) break
                } else if (u) {
                    for (; o > i; i++)
                        if (r = t.call(e[i], i, e[i]), r === !1) break
                } else
                    for (i in e)
                        if (r = t.call(e[i], i, e[i]), r === !1) break; return e
            },
            trim: function(e) {
                return null == e ? "" : (e + "").replace(o, "")
            },
            makeArray: function(e, t) {
                var r = t || [];
                return null != e && (s(Object(e)) ? n.merge(r, "string" == typeof e ? [e] : e) : f.call(r, e)), r
            },
            inArray: function(e, t, n) {
                return null == t ? -1 : g.call(t, e, n)
            },
            merge: function(e, t) {
                for (var n = +t.length, r = 0, i = e.length; n > r; r++) e[i++] = t[r];
                return e.length = i, e
            },
            grep: function(e, t, n) {
                for (var r, i = [], s = 0, o = e.length, u = !n; o > s; s++) r = !t(e[s], s), r !== u && i.push(e[s]);
                return i
            },
            map: function(t, n, r) {
                var i, o = 0,
                    u = t.length,
                    a = s(t),
                    f = [];
                if (a)
                    for (; u > o; o++) i = n(t[o], o, r), null != i && f.push(i);
                else
                    for (o in t) i = n(t[o], o, r), null != i && f.push(i);
                return e.apply([], f)
            },
            guid: 1,
            proxy: function(e, t) {
                var r, i, s;
                return "string" == typeof t && (r = e[t], t = e, e = r), n.isFunction(e) ? (i = d.call(arguments, 2), s = function() {
                    return e.apply(t || this, i.concat(d.call(arguments)))
                }, s.guid = e.guid = e.guid || n.guid++, s) : void 0
            },
            now: Date.now,
            support: k
        }), n.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
            h["[object " + t + "]"] = t.toLowerCase()
        });
        var t = function(e) {
            function ot(e, t, r, i) {
                var s, u, f, l, c, d, g, y, S, x;
                if ((t ? t.ownerDocument || t : E) !== p && h(t), t = t || p, r = r || [], l = t.nodeType, "string" != typeof e || !e || 1 !== l && 9 !== l && 11 !== l) return r;
                if (!i && v) {
                    if (11 !== l && (s = Z.exec(e)))
                        if (f = s[1]) {
                            if (9 === l) {
                                if (u = t.getElementById(f), !u || !u.parentNode) return r;
                                if (u.id === f) return r.push(u), r
                            } else if (t.ownerDocument && (u = t.ownerDocument.getElementById(f)) && b(t, u) && u.id === f) return r.push(u), r
                        } else {
                            if (s[2]) return D.apply(r, t.getElementsByTagName(e)), r;
                            if ((f = s[3]) && n.getElementsByClassName) return D.apply(r, t.getElementsByClassName(f)), r
                        }
                    if (n.qsa && (!m || !m.test(e))) {
                        if (y = g = w, S = t, x = 1 !== l && e, 1 === l && "object" !== t.nodeName.toLowerCase()) {
                            d = o(e), (g = t.getAttribute("id")) ? y = g.replace(tt, "\\$&") : t.setAttribute("id", y), y = "[id='" + y + "'] ", c = d.length;
                            while (c--) d[c] = y + gt(d[c]);
                            S = et.test(e) && vt(t.parentNode) || t, x = d.join(",")
                        }
                        if (x) try {
                            return D.apply(r, S.querySelectorAll(x)), r
                        } catch (T) {} finally {
                            g || t.removeAttribute("id")
                        }
                    }
                }
                return a(e.replace(z, "$1"), t, r, i)
            }

            function ut() {
                function t(n, i) {
                    return e.push(n + " ") > r.cacheLength && delete t[e.shift()], t[n + " "] = i
                }
                var e = [];
                return t
            }

            function at(e) {
                return e[w] = !0, e
            }

            function ft(e) {
                var t = p.createElement("div");
                try {
                    return !!e(t)
                } catch (n) {
                    return !1
                } finally {
                    t.parentNode && t.parentNode.removeChild(t), t = null
                }
            }

            function lt(e, t) {
                var n = e.split("|"),
                    i = e.length;
                while (i--) r.attrHandle[n[i]] = t
            }

            function ct(e, t) {
                var n = t && e,
                    r = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || L) - (~e.sourceIndex || L);
                if (r) return r;
                if (n)
                    while (n = n.nextSibling)
                        if (n === t) return -1;
                return e ? 1 : -1
            }

            function ht(e) {
                return function(t) {
                    var n = t.nodeName.toLowerCase();
                    return "input" === n && t.type === e
                }
            }

            function pt(e) {
                return function(t) {
                    var n = t.nodeName.toLowerCase();
                    return ("input" === n || "button" === n) && t.type === e
                }
            }

            function dt(e) {
                return at(function(t) {
                    return t = +t, at(function(n, r) {
                        var i, s = e([], n.length, t),
                            o = s.length;
                        while (o--) n[i = s[o]] && (n[i] = !(r[i] = n[i]))
                    })
                })
            }

            function vt(e) {
                return e && "undefined" != typeof e.getElementsByTagName && e
            }

            function mt() {}

            function gt(e) {
                for (var t = 0, n = e.length, r = ""; n > t; t++) r += e[t].value;
                return r
            }

            function yt(e, t, n) {
                var r = t.dir,
                    i = n && "parentNode" === r,
                    s = x++;
                return t.first ? function(t, n, s) {
                    while (t = t[r])
                        if (1 === t.nodeType || i) return e(t, n, s)
                } : function(t, n, o) {
                    var u, a, f = [S, s];
                    if (o) {
                        while (t = t[r])
                            if ((1 === t.nodeType || i) && e(t, n, o)) return !0
                    } else
                        while (t = t[r])
                            if (1 === t.nodeType || i) {
                                if (a = t[w] || (t[w] = {}), (u = a[r]) && u[0] === S && u[1] === s) return f[2] = u[2];
                                if (a[r] = f, f[2] = e(t, n, o)) return !0
                            }
                }
            }

            function bt(e) {
                return e.length > 1 ? function(t, n, r) {
                    var i = e.length;
                    while (i--)
                        if (!e[i](t, n, r)) return !1;
                    return !0
                } : e[0]
            }

            function wt(e, t, n) {
                for (var r = 0, i = t.length; i > r; r++) ot(e, t[r], n);
                return n
            }

            function Et(e, t, n, r, i) {
                for (var s, o = [], u = 0, a = e.length, f = null != t; a > u; u++)(s = e[u]) && (!n || n(s, r, i)) && (o.push(s), f && t.push(u));
                return o
            }

            function St(e, t, n, r, i, s) {
                return r && !r[w] && (r = St(r)), i && !i[w] && (i = St(i, s)), at(function(s, o, u, a) {
                    var f, l, c, h = [],
                        p = [],
                        d = o.length,
                        v = s || wt(t || "*", u.nodeType ? [u] : u, []),
                        m = !e || !s && t ? v : Et(v, h, e, u, a),
                        g = n ? i || (s ? e : d || r) ? [] : o : m;
                    if (n && n(m, g, u, a), r) {
                        f = Et(g, p), r(f, [], u, a), l = f.length;
                        while (l--)(c = f[l]) && (g[p[l]] = !(m[p[l]] = c))
                    }
                    if (s) {
                        if (i || e) {
                            if (i) {
                                f = [], l = g.length;
                                while (l--)(c = g[l]) && f.push(m[l] = c);
                                i(null, g = [], f, a)
                            }
                            l = g.length;
                            while (l--)(c = g[l]) && (f = i ? H(s, c) : h[l]) > -1 && (s[f] = !(o[f] = c))
                        }
                    } else g = Et(g === o ? g.splice(d, g.length) : g), i ? i(null, o, g, a) : D.apply(o, g)
                })
            }

            function xt(e) {
                for (var t, n, i, s = e.length, o = r.relative[e[0].type], u = o || r.relative[" "], a = o ? 1 : 0, l = yt(function(e) {
                        return e === t
                    }, u, !0), c = yt(function(e) {
                        return H(t, e) > -1
                    }, u, !0), h = [function(e, n, r) {
                        var i = !o && (r || n !== f) || ((t = n).nodeType ? l(e, n, r) : c(e, n, r));
                        return t = null, i
                    }]; s > a; a++)
                    if (n = r.relative[e[a].type]) h = [yt(bt(h), n)];
                    else {
                        if (n = r.filter[e[a].type].apply(null, e[a].matches), n[w]) {
                            for (i = ++a; s > i; i++)
                                if (r.relative[e[i].type]) break;
                            return St(a > 1 && bt(h), a > 1 && gt(e.slice(0, a - 1).concat({
                                value: " " === e[a - 2].type ? "*" : ""
                            })).replace(z, "$1"), n, i > a && xt(e.slice(a, i)), s > i && xt(e = e.slice(i)), s > i && gt(e))
                        }
                        h.push(n)
                    }
                return bt(h)
            }

            function Tt(e, t) {
                var n = t.length > 0,
                    i = e.length > 0,
                    s = function(s, o, u, a, l) {
                        var c, h, d, v = 0,
                            m = "0",
                            g = s && [],
                            y = [],
                            b = f,
                            w = s || i && r.find.TAG("*", l),
                            E = S += null == b ? 1 : Math.random() || .1,
                            x = w.length;
                        for (l && (f = o !== p && o); m !== x && null != (c = w[m]); m++) {
                            if (i && c) {
                                h = 0;
                                while (d = e[h++])
                                    if (d(c, o, u)) {
                                        a.push(c);
                                        break
                                    }
                                l && (S = E)
                            }
                            n && ((c = !d && c) && v--, s && g.push(c))
                        }
                        if (v += m, n && m !== v) {
                            h = 0;
                            while (d = t[h++]) d(g, y, o, u);
                            if (s) {
                                if (v > 0)
                                    while (m--) g[m] || y[m] || (y[m] = M.call(a));
                                y = Et(y)
                            }
                            D.apply(a, y), l && !s && y.length > 0 && v + t.length > 1 && ot.uniqueSort(a)
                        }
                        return l && (S = E, f = b), g
                    };
                return n ? at(s) : s
            }
            var t, n, r, i, s, o, u, a, f, l, c, h, p, d, v, m, g, y, b, w = "sizzle" + 1 * new Date,
                E = e.document,
                S = 0,
                x = 0,
                T = ut(),
                N = ut(),
                C = ut(),
                k = function(e, t) {
                    return e === t && (c = !0), 0
                },
                L = 1 << 31,
                A = {}.hasOwnProperty,
                O = [],
                M = O.pop,
                _ = O.push,
                D = O.push,
                P = O.slice,
                H = function(e, t) {
                    for (var n = 0, r = e.length; r > n; n++)
                        if (e[n] === t) return n;
                    return -1
                },
                B = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                j = "[\\x20\\t\\r\\n\\f]",
                F = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                I = F.replace("w", "w#"),
                q = "\\[" + j + "*(" + F + ")(?:" + j + "*([*^$|!~]?=)" + j + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + I + "))|)" + j + "*\\]",
                R = ":(" + F + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + q + ")*)|.*)\\)|)",
                U = new RegExp(j + "+", "g"),
                z = new RegExp("^" + j + "+|((?:^|[^\\\\])(?:\\\\.)*)" + j + "+$", "g"),
                W = new RegExp("^" + j + "*," + j + "*"),
                X = new RegExp("^" + j + "*([>+~]|" + j + ")" + j + "*"),
                V = new RegExp("=" + j + "*([^\\]'\"]*?)" + j + "*\\]", "g"),
                $ = new RegExp(R),
                J = new RegExp("^" + I + "$"),
                K = {
                    ID: new RegExp("^#(" + F + ")"),
                    CLASS: new RegExp("^\\.(" + F + ")"),
                    TAG: new RegExp("^(" + F.replace("w", "w*") + ")"),
                    ATTR: new RegExp("^" + q),
                    PSEUDO: new RegExp("^" + R),
                    CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + j + "*(even|odd|(([+-]|)(\\d*)n|)" + j + "*(?:([+-]|)" + j + "*(\\d+)|))" + j + "*\\)|)", "i"),
                    bool: new RegExp("^(?:" + B + ")$", "i"),
                    needsContext: new RegExp("^" + j + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + j + "*((?:-\\d)?\\d*)" + j + "*\\)|)(?=[^-]|$)", "i")
                },
                Q = /^(?:input|select|textarea|button)$/i,
                G = /^h\d$/i,
                Y = /^[^{]+\{\s*\[native \w/,
                Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                et = /[+~]/,
                tt = /'|\\/g,
                nt = new RegExp("\\\\([\\da-f]{1,6}" + j + "?|(" + j + ")|.)", "ig"),
                rt = function(e, t, n) {
                    var r = "0x" + t - 65536;
                    return r !== r || n ? t : 0 > r ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320)
                },
                it = function() {
                    h()
                };
            try {
                D.apply(O = P.call(E.childNodes), E.childNodes), O[E.childNodes.length].nodeType
            } catch (st) {
                D = {
                    apply: O.length ? function(e, t) {
                        _.apply(e, P.call(t))
                    } : function(e, t) {
                        var n = e.length,
                            r = 0;
                        while (e[n++] = t[r++]);
                        e.length = n - 1
                    }
                }
            }
            n = ot.support = {}, s = ot.isXML = function(e) {
                var t = e && (e.ownerDocument || e).documentElement;
                return t ? "HTML" !== t.nodeName : !1
            }, h = ot.setDocument = function(e) {
                var t, i, o = e ? e.ownerDocument || e : E;
                return o !== p && 9 === o.nodeType && o.documentElement ? (p = o, d = o.documentElement, i = o.defaultView, i && i !== i.top && (i.addEventListener ? i.addEventListener("unload", it, !1) : i.attachEvent && i.attachEvent("onunload", it)), v = !s(o), n.attributes = ft(function(e) {
                    return e.className = "i", !e.getAttribute("className")
                }), n.getElementsByTagName = ft(function(e) {
                    return e.appendChild(o.createComment("")), !e.getElementsByTagName("*").length
                }), n.getElementsByClassName = Y.test(o.getElementsByClassName), n.getById = ft(function(e) {
                    return d.appendChild(e).id = w, !o.getElementsByName || !o.getElementsByName(w).length
                }), n.getById ? (r.find.ID = function(e, t) {
                    if ("undefined" != typeof t.getElementById && v) {
                        var n = t.getElementById(e);
                        return n && n.parentNode ? [n] : []
                    }
                }, r.filter.ID = function(e) {
                    var t = e.replace(nt, rt);
                    return function(e) {
                        return e.getAttribute("id") === t
                    }
                }) : (delete r.find.ID, r.filter.ID = function(e) {
                    var t = e.replace(nt, rt);
                    return function(e) {
                        var n = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                        return n && n.value === t
                    }
                }), r.find.TAG = n.getElementsByTagName ? function(e, t) {
                    return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : n.qsa ? t.querySelectorAll(e) : void 0
                } : function(e, t) {
                    var n, r = [],
                        i = 0,
                        s = t.getElementsByTagName(e);
                    if ("*" === e) {
                        while (n = s[i++]) 1 === n.nodeType && r.push(n);
                        return r
                    }
                    return s
                }, r.find.CLASS = n.getElementsByClassName && function(e, t) {
                    return v ? t.getElementsByClassName(e) : void 0
                }, g = [], m = [], (n.qsa = Y.test(o.querySelectorAll)) && (ft(function(e) {
                    d.appendChild(e).innerHTML = "<a id='" + w + "'></a><select id='" + w + "-\f]' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && m.push("[*^$]=" + j + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || m.push("\\[" + j + "*(?:value|" + B + ")"), e.querySelectorAll("[id~=" + w + "-]").length || m.push("~="), e.querySelectorAll(":checked").length || m.push(":checked"), e.querySelectorAll("a#" + w + "+*").length || m.push(".#.+[+~]")
                }), ft(function(e) {
                    var t = o.createElement("input");
                    t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && m.push("name" + j + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || m.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), m.push(",.*:")
                })), (n.matchesSelector = Y.test(y = d.matches || d.webkitMatchesSelector || d.mozMatchesSelector || d.oMatchesSelector || d.msMatchesSelector)) && ft(function(e) {
                    n.disconnectedMatch = y.call(e, "div"), y.call(e, "[s!='']:x"), g.push("!=", R)
                }), m = m.length && new RegExp(m.join("|")), g = g.length && new RegExp(g.join("|")), t = Y.test(d.compareDocumentPosition), b = t || Y.test(d.contains) ? function(e, t) {
                    var n = 9 === e.nodeType ? e.documentElement : e,
                        r = t && t.parentNode;
                    return e === r || !!r && 1 === r.nodeType && !!(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r))
                } : function(e, t) {
                    if (t)
                        while (t = t.parentNode)
                            if (t === e) return !0;
                    return !1
                }, k = t ? function(e, t) {
                    if (e === t) return c = !0, 0;
                    var r = !e.compareDocumentPosition - !t.compareDocumentPosition;
                    return r ? r : (r = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & r || !n.sortDetached && t.compareDocumentPosition(e) === r ? e === o || e.ownerDocument === E && b(E, e) ? -1 : t === o || t.ownerDocument === E && b(E, t) ? 1 : l ? H(l, e) - H(l, t) : 0 : 4 & r ? -1 : 1)
                } : function(e, t) {
                    if (e === t) return c = !0, 0;
                    var n, r = 0,
                        i = e.parentNode,
                        s = t.parentNode,
                        u = [e],
                        a = [t];
                    if (!i || !s) return e === o ? -1 : t === o ? 1 : i ? -1 : s ? 1 : l ? H(l, e) - H(l, t) : 0;
                    if (i === s) return ct(e, t);
                    n = e;
                    while (n = n.parentNode) u.unshift(n);
                    n = t;
                    while (n = n.parentNode) a.unshift(n);
                    while (u[r] === a[r]) r++;
                    return r ? ct(u[r], a[r]) : u[r] === E ? -1 : a[r] === E ? 1 : 0
                }, o) : p
            }, ot.matches = function(e, t) {
                return ot(e, null, null, t)
            }, ot.matchesSelector = function(e, t) {
                if ((e.ownerDocument || e) !== p && h(e), t = t.replace(V, "='$1']"), !(!n.matchesSelector || !v || g && g.test(t) || m && m.test(t))) try {
                    var r = y.call(e, t);
                    if (r || n.disconnectedMatch || e.document && 11 !== e.document.nodeType) return r
                } catch (i) {}
                return ot(t, p, null, [e]).length > 0
            }, ot.contains = function(e, t) {
                return (e.ownerDocument || e) !== p && h(e), b(e, t)
            }, ot.attr = function(e, t) {
                (e.ownerDocument || e) !== p && h(e);
                var i = r.attrHandle[t.toLowerCase()],
                    s = i && A.call(r.attrHandle, t.toLowerCase()) ? i(e, t, !v) : void 0;
                return void 0 !== s ? s : n.attributes || !v ? e.getAttribute(t) : (s = e.getAttributeNode(t)) && s.specified ? s.value : null
            }, ot.error = function(e) {
                throw new Error("Syntax error, unrecognized expression: " + e)
            }, ot.uniqueSort = function(e) {
                var t, r = [],
                    i = 0,
                    s = 0;
                if (c = !n.detectDuplicates, l = !n.sortStable && e.slice(0), e.sort(k), c) {
                    while (t = e[s++]) t === e[s] && (i = r.push(s));
                    while (i--) e.splice(r[i], 1)
                }
                return l = null, e
            }, i = ot.getText = function(e) {
                var t, n = "",
                    r = 0,
                    s = e.nodeType;
                if (s) {
                    if (1 === s || 9 === s || 11 === s) {
                        if ("string" == typeof e.textContent) return e.textContent;
                        for (e = e.firstChild; e; e = e.nextSibling) n += i(e)
                    } else if (3 === s || 4 === s) return e.nodeValue
                } else
                    while (t = e[r++]) n += i(t);
                return n
            }, r = ot.selectors = {
                cacheLength: 50,
                createPseudo: at,
                match: K,
                attrHandle: {},
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function(e) {
                        return e[1] = e[1].replace(nt, rt), e[3] = (e[3] || e[4] || e[5] || "").replace(nt, rt), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                    },
                    CHILD: function(e) {
                        return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || ot.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && ot.error(e[0]), e
                    },
                    PSEUDO: function(e) {
                        var t, n = !e[6] && e[2];
                        return K.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && $.test(n) && (t = o(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function(e) {
                        var t = e.replace(nt, rt).toLowerCase();
                        return "*" === e ? function() {
                            return !0
                        } : function(e) {
                            return e.nodeName && e.nodeName.toLowerCase() === t
                        }
                    },
                    CLASS: function(e) {
                        var t = T[e + " "];
                        return t || (t = new RegExp("(^|" + j + ")" + e + "(" + j + "|$)")) && T(e, function(e) {
                            return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
                        })
                    },
                    ATTR: function(e, t, n) {
                        return function(r) {
                            var i = ot.attr(r, e);
                            return null == i ? "!=" === t : t ? (i += "", "=" === t ? i === n : "!=" === t ? i !== n : "^=" === t ? n && 0 === i.indexOf(n) : "*=" === t ? n && i.indexOf(n) > -1 : "$=" === t ? n && i.slice(-n.length) === n : "~=" === t ? (" " + i.replace(U, " ") + " ").indexOf(n) > -1 : "|=" === t ? i === n || i.slice(0, n.length + 1) === n + "-" : !1) : !0
                        }
                    },
                    CHILD: function(e, t, n, r, i) {
                        var s = "nth" !== e.slice(0, 3),
                            o = "last" !== e.slice(-4),
                            u = "of-type" === t;
                        return 1 === r && 0 === i ? function(e) {
                            return !!e.parentNode
                        } : function(t, n, a) {
                            var f, l, c, h, p, d, v = s !== o ? "nextSibling" : "previousSibling",
                                m = t.parentNode,
                                g = u && t.nodeName.toLowerCase(),
                                y = !a && !u;
                            if (m) {
                                if (s) {
                                    while (v) {
                                        c = t;
                                        while (c = c[v])
                                            if (u ? c.nodeName.toLowerCase() === g : 1 === c.nodeType) return !1;
                                        d = v = "only" === e && !d && "nextSibling"
                                    }
                                    return !0
                                }
                                if (d = [o ? m.firstChild : m.lastChild], o && y) {
                                    l = m[w] || (m[w] = {}), f = l[e] || [], p = f[0] === S && f[1], h = f[0] === S && f[2], c = p && m.childNodes[p];
                                    while (c = ++p && c && c[v] || (h = p = 0) || d.pop())
                                        if (1 === c.nodeType && ++h && c === t) {
                                            l[e] = [S, p, h];
                                            break
                                        }
                                } else if (y && (f = (t[w] || (t[w] = {}))[e]) && f[0] === S) h = f[1];
                                else
                                    while (c = ++p && c && c[v] || (h = p = 0) || d.pop())
                                        if ((u ? c.nodeName.toLowerCase() === g : 1 === c.nodeType) && ++h && (y && ((c[w] || (c[w] = {}))[e] = [S, h]), c === t)) break; return h -= i, h === r || h % r === 0 && h / r >= 0
                            }
                        }
                    },
                    PSEUDO: function(e, t) {
                        var n, i = r.pseudos[e] || r.setFilters[e.toLowerCase()] || ot.error("unsupported pseudo: " + e);
                        return i[w] ? i(t) : i.length > 1 ? (n = [e, e, "", t], r.setFilters.hasOwnProperty(e.toLowerCase()) ? at(function(e, n) {
                            var r, s = i(e, t),
                                o = s.length;
                            while (o--) r = H(e, s[o]), e[r] = !(n[r] = s[o])
                        }) : function(e) {
                            return i(e, 0, n)
                        }) : i
                    }
                },
                pseudos: {
                    not: at(function(e) {
                        var t = [],
                            n = [],
                            r = u(e.replace(z, "$1"));
                        return r[w] ? at(function(e, t, n, i) {
                            var s, o = r(e, null, i, []),
                                u = e.length;
                            while (u--)(s = o[u]) && (e[u] = !(t[u] = s))
                        }) : function(e, i, s) {
                            return t[0] = e, r(t, null, s, n), t[0] = null, !n.pop()
                        }
                    }),
                    has: at(function(e) {
                        return function(t) {
                            return ot(e, t).length > 0
                        }
                    }),
                    contains: at(function(e) {
                        return e = e.replace(nt, rt),
                            function(t) {
                                return (t.textContent || t.innerText || i(t)).indexOf(e) > -1
                            }
                    }),
                    lang: at(function(e) {
                        return J.test(e || "") || ot.error("unsupported lang: " + e), e = e.replace(nt, rt).toLowerCase(),
                            function(t) {
                                var n;
                                do
                                    if (n = v ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-");
                                while ((t = t.parentNode) && 1 === t.nodeType);
                                return !1
                            }
                    }),
                    target: function(t) {
                        var n = e.location && e.location.hash;
                        return n && n.slice(1) === t.id
                    },
                    root: function(e) {
                        return e === d
                    },
                    focus: function(e) {
                        return e === p.activeElement && (!p.hasFocus || p.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                    },
                    enabled: function(e) {
                        return e.disabled === !1
                    },
                    disabled: function(e) {
                        return e.disabled === !0
                    },
                    checked: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && !!e.checked || "option" === t && !!e.selected
                    },
                    selected: function(e) {
                        return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                    },
                    empty: function(e) {
                        for (e = e.firstChild; e; e = e.nextSibling)
                            if (e.nodeType < 6) return !1;
                        return !0
                    },
                    parent: function(e) {
                        return !r.pseudos.empty(e)
                    },
                    header: function(e) {
                        return G.test(e.nodeName)
                    },
                    input: function(e) {
                        return Q.test(e.nodeName)
                    },
                    button: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && "button" === e.type || "button" === t
                    },
                    text: function(e) {
                        var t;
                        return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                    },
                    first: dt(function() {
                        return [0]
                    }),
                    last: dt(function(e, t) {
                        return [t - 1]
                    }),
                    eq: dt(function(e, t, n) {
                        return [0 > n ? n + t : n]
                    }),
                    even: dt(function(e, t) {
                        for (var n = 0; t > n; n += 2) e.push(n);
                        return e
                    }),
                    odd: dt(function(e, t) {
                        for (var n = 1; t > n; n += 2) e.push(n);
                        return e
                    }),
                    lt: dt(function(e, t, n) {
                        for (var r = 0 > n ? n + t : n; --r >= 0;) e.push(r);
                        return e
                    }),
                    gt: dt(function(e, t, n) {
                        for (var r = 0 > n ? n + t : n; ++r < t;) e.push(r);
                        return e
                    })
                }
            }, r.pseudos.nth = r.pseudos.eq;
            for (t in {
                    radio: !0,
                    checkbox: !0,
                    file: !0,
                    password: !0,
                    image: !0
                }) r.pseudos[t] = ht(t);
            for (t in {
                    submit: !0,
                    reset: !0
                }) r.pseudos[t] = pt(t);
            return mt.prototype = r.filters = r.pseudos, r.setFilters = new mt, o = ot.tokenize = function(e, t) {
                var n, i, s, o, u, a, f, l = N[e + " "];
                if (l) return t ? 0 : l.slice(0);
                u = e, a = [], f = r.preFilter;
                while (u) {
                    (!n || (i = W.exec(u))) && (i && (u = u.slice(i[0].length) || u), a.push(s = [])), n = !1, (i = X.exec(u)) && (n = i.shift(), s.push({
                        value: n,
                        type: i[0].replace(z, " ")
                    }), u = u.slice(n.length));
                    for (o in r.filter) !(i = K[o].exec(u)) || f[o] && !(i = f[o](i)) || (n = i.shift(), s.push({
                        value: n,
                        type: o,
                        matches: i
                    }), u = u.slice(n.length));
                    if (!n) break
                }
                return t ? u.length : u ? ot.error(e) : N(e, a).slice(0)
            }, u = ot.compile = function(e, t) {
                var n, r = [],
                    i = [],
                    s = C[e + " "];
                if (!s) {
                    t || (t = o(e)), n = t.length;
                    while (n--) s = xt(t[n]), s[w] ? r.push(s) : i.push(s);
                    s = C(e, Tt(i, r)), s.selector = e
                }
                return s
            }, a = ot.select = function(e, t, i, s) {
                var a, f, l, c, h, p = "function" == typeof e && e,
                    d = !s && o(e = p.selector || e);
                if (i = i || [], 1 === d.length) {
                    if (f = d[0] = d[0].slice(0), f.length > 2 && "ID" === (l = f[0]).type && n.getById && 9 === t.nodeType && v && r.relative[f[1].type]) {
                        if (t = (r.find.ID(l.matches[0].replace(nt, rt), t) || [])[0], !t) return i;
                        p && (t = t.parentNode), e = e.slice(f.shift().value.length)
                    }
                    a = K.needsContext.test(e) ? 0 : f.length;
                    while (a--) {
                        if (l = f[a], r.relative[c = l.type]) break;
                        if ((h = r.find[c]) && (s = h(l.matches[0].replace(nt, rt), et.test(f[0].type) && vt(t.parentNode) || t))) {
                            if (f.splice(a, 1), e = s.length && gt(f), !e) return D.apply(i, s), i;
                            break
                        }
                    }
                }
                return (p || u(e, d))(s, t, !v, i, et.test(e) && vt(t.parentNode) || t), i
            }, n.sortStable = w.split("").sort(k).join("") === w, n.detectDuplicates = !!c, h(), n.sortDetached = ft(function(e) {
                return 1 & e.compareDocumentPosition(p.createElement("div"))
            }), ft(function(e) {
                return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
            }) || lt("type|href|height|width", function(e, t, n) {
                return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
            }), n.attributes && ft(function(e) {
                return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
            }) || lt("value", function(e, t, n) {
                return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
            }), ft(function(e) {
                return null == e.getAttribute("disabled")
            }) || lt(B, function(e, t, n) {
                var r;
                return n ? void 0 : e[t] === !0 ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
            }), ot
        }(a);
        n.find = t, n.expr = t.selectors, n.expr[":"] = n.expr.pseudos, n.unique = t.uniqueSort, n.text = t.getText, n.isXMLDoc = t.isXML, n.contains = t.contains;
        var u = n.expr.match.needsContext,
            v = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
            w = /^.[^:#\[\.,]*$/;
        n.filter = function(e, t, r) {
            var i = t[0];
            return r && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? n.find.matchesSelector(i, e) ? [i] : [] : n.find.matches(e, n.grep(t, function(e) {
                return 1 === e.nodeType
            }))
        }, n.fn.extend({
            find: function(e) {
                var t, r = this.length,
                    i = [],
                    s = this;
                if ("string" != typeof e) return this.pushStack(n(e).filter(function() {
                    for (t = 0; r > t; t++)
                        if (n.contains(s[t], this)) return !0
                }));
                for (t = 0; r > t; t++) n.find(e, s[t], i);
                return i = this.pushStack(r > 1 ? n.unique(i) : i), i.selector = this.selector ? this.selector + " " + e : e, i
            },
            filter: function(e) {
                return this.pushStack(x(this, e || [], !1))
            },
            not: function(e) {
                return this.pushStack(x(this, e || [], !0))
            },
            is: function(e) {
                return !!x(this, "string" == typeof e && u.test(e) ? n(e) : e || [], !1).length
            }
        });
        var y, z = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
            A = n.fn.init = function(e, t) {
                var r, i;
                if (!e) return this;
                if ("string" == typeof e) {
                    if (r = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : z.exec(e), !r || !r[1] && t) return !t || t.jquery ? (t || y).find(e) : this.constructor(t).find(e);
                    if (r[1]) {
                        if (t = t instanceof n ? t[0] : t, n.merge(this, n.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : l, !0)), v.test(r[1]) && n.isPlainObject(t))
                            for (r in t) n.isFunction(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
                        return this
                    }
                    return i = l.getElementById(r[2]), i && i.parentNode && (this.length = 1, this[0] = i), this.context = l, this.selector = e, this
                }
                return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : n.isFunction(e) ? "undefined" != typeof y.ready ? y.ready(e) : e(n) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), n.makeArray(e, this))
            };
        A.prototype = n.fn, y = n(l);
        var B = /^(?:parents|prev(?:Until|All))/,
            C = {
                children: !0,
                contents: !0,
                next: !0,
                prev: !0
            };
        n.extend({
            dir: function(e, t, r) {
                var i = [],
                    s = void 0 !== r;
                while ((e = e[t]) && 9 !== e.nodeType)
                    if (1 === e.nodeType) {
                        if (s && n(e).is(r)) break;
                        i.push(e)
                    }
                return i
            },
            sibling: function(e, t) {
                for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
                return n
            }
        }), n.fn.extend({
            has: function(e) {
                var t = n(e, this),
                    r = t.length;
                return this.filter(function() {
                    for (var e = 0; r > e; e++)
                        if (n.contains(this, t[e])) return !0
                })
            },
            closest: function(e, t) {
                for (var r, i = 0, s = this.length, o = [], a = u.test(e) || "string" != typeof e ? n(e, t || this.context) : 0; s > i; i++)
                    for (r = this[i]; r && r !== t; r = r.parentNode)
                        if (r.nodeType < 11 && (a ? a.index(r) > -1 : 1 === r.nodeType && n.find.matchesSelector(r, e))) {
                            o.push(r);
                            break
                        }
                return this.pushStack(o.length > 1 ? n.unique(o) : o)
            },
            index: function(e) {
                return e ? "string" == typeof e ? g.call(n(e), this[0]) : g.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
            },
            add: function(e, t) {
                return this.pushStack(n.unique(n.merge(this.get(), n(e, t))))
            },
            addBack: function(e) {
                return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
            }
        }), n.each({
            parent: function(e) {
                var t = e.parentNode;
                return t && 11 !== t.nodeType ? t : null
            },
            parents: function(e) {
                return n.dir(e, "parentNode")
            },
            parentsUntil: function(e, t, r) {
                return n.dir(e, "parentNode", r)
            },
            next: function(e) {
                return D(e, "nextSibling")
            },
            prev: function(e) {
                return D(e, "previousSibling")
            },
            nextAll: function(e) {
                return n.dir(e, "nextSibling")
            },
            prevAll: function(e) {
                return n.dir(e, "previousSibling")
            },
            nextUntil: function(e, t, r) {
                return n.dir(e, "nextSibling", r)
            },
            prevUntil: function(e, t, r) {
                return n.dir(e, "previousSibling", r)
            },
            siblings: function(e) {
                return n.sibling((e.parentNode || {}).firstChild, e)
            },
            children: function(e) {
                return n.sibling(e.firstChild)
            },
            contents: function(e) {
                return e.contentDocument || n.merge([], e.childNodes)
            }
        }, function(e, t) {
            n.fn[e] = function(r, i) {
                var s = n.map(this, t, r);
                return "Until" !== e.slice(-5) && (i = r), i && "string" == typeof i && (s = n.filter(i, s)), this.length > 1 && (C[e] || n.unique(s), B.test(e) && s.reverse()), this.pushStack(s)
            }
        });
        var E = /\S+/g,
            F = {};
        n.Callbacks = function(e) {
            e = "string" == typeof e ? F[e] || G(e) : n.extend({}, e);
            var t, r, i, s, o, u, a = [],
                f = !e.once && [],
                l = function(n) {
                    for (t = e.memory && n, r = !0, u = s || 0, s = 0, o = a.length, i = !0; a && o > u; u++)
                        if (a[u].apply(n[0], n[1]) === !1 && e.stopOnFalse) {
                            t = !1;
                            break
                        }
                    i = !1, a && (f ? f.length && l(f.shift()) : t ? a = [] : c.disable())
                },
                c = {
                    add: function() {
                        if (a) {
                            var r = a.length;
                            ! function u(t) {
                                n.each(t, function(t, r) {
                                    var i = n.type(r);
                                    "function" === i ? e.unique && c.has(r) || a.push(r) : r && r.length && "string" !== i && u(r)
                                })
                            }(arguments), i ? o = a.length : t && (s = r, l(t))
                        }
                        return this
                    },
                    remove: function() {
                        return a && n.each(arguments, function(e, t) {
                            var r;
                            while ((r = n.inArray(t, a, r)) > -1) a.splice(r, 1), i && (o >= r && o--, u >= r && u--)
                        }), this
                    },
                    has: function(e) {
                        return e ? n.inArray(e, a) > -1 : !!a && !!a.length
                    },
                    empty: function() {
                        return a = [], o = 0, this
                    },
                    disable: function() {
                        return a = f = t = void 0, this
                    },
                    disabled: function() {
                        return !a
                    },
                    lock: function() {
                        return f = void 0, t || c.disable(), this
                    },
                    locked: function() {
                        return !f
                    },
                    fireWith: function(e, t) {
                        return !a || r && !f || (t = t || [], t = [e, t.slice ? t.slice() : t], i ? f.push(t) : l(t)), this
                    },
                    fire: function() {
                        return c.fireWith(this, arguments), this
                    },
                    fired: function() {
                        return !!r
                    }
                };
            return c
        }, n.extend({
            Deferred: function(e) {
                var t = [
                        ["resolve", "done", n.Callbacks("once memory"), "resolved"],
                        ["reject", "fail", n.Callbacks("once memory"), "rejected"],
                        ["notify", "progress", n.Callbacks("memory")]
                    ],
                    r = "pending",
                    i = {
                        state: function() {
                            return r
                        },
                        always: function() {
                            return s.done(arguments).fail(arguments), this
                        },
                        then: function() {
                            var e = arguments;
                            return n.Deferred(function(r) {
                                n.each(t, function(t, o) {
                                    var u = n.isFunction(e[t]) && e[t];
                                    s[o[1]](function() {
                                        var e = u && u.apply(this, arguments);
                                        e && n.isFunction(e.promise) ? e.promise().done(r.resolve).fail(r.reject).progress(r.notify) : r[o[0] + "With"](this === i ? r.promise() : this, u ? [e] : arguments)
                                    })
                                }), e = null
                            }).promise()
                        },
                        promise: function(e) {
                            return null != e ? n.extend(e, i) : i
                        }
                    },
                    s = {};
                return i.pipe = i.then, n.each(t, function(e, n) {
                    var o = n[2],
                        u = n[3];
                    i[n[1]] = o.add, u && o.add(function() {
                        r = u
                    }, t[1 ^ e][2].disable, t[2][2].lock), s[n[0]] = function() {
                        return s[n[0] + "With"](this === s ? i : this, arguments), this
                    }, s[n[0] + "With"] = o.fireWith
                }), i.promise(s), e && e.call(s, s), s
            },
            when: function(e) {
                var t = 0,
                    r = d.call(arguments),
                    i = r.length,
                    s = 1 !== i || e && n.isFunction(e.promise) ? i : 0,
                    o = 1 === s ? e : n.Deferred(),
                    u = function(e, t, n) {
                        return function(r) {
                            t[e] = this, n[e] = arguments.length > 1 ? d.call(arguments) : r, n === a ? o.notifyWith(t, n) : --s || o.resolveWith(t, n)
                        }
                    },
                    a, f, l;
                if (i > 1)
                    for (a = new Array(i), f = new Array(i), l = new Array(i); i > t; t++) r[t] && n.isFunction(r[t].promise) ? r[t].promise().done(u(t, l, r)).fail(o.reject).progress(u(t, f, a)) : --s;
                return s || o.resolveWith(l, r), o.promise()
            }
        });
        var H;
        n.fn.ready = function(e) {
            return n.ready.promise().done(e), this
        }, n.extend({
            isReady: !1,
            readyWait: 1,
            holdReady: function(e) {
                e ? n.readyWait++ : n.ready(!0)
            },
            ready: function(e) {
                (e === !0 ? --n.readyWait : n.isReady) || (n.isReady = !0, e !== !0 && --n.readyWait > 0 || (H.resolveWith(l, [n]), n.fn.triggerHandler && (n(l).triggerHandler("ready"), n(l).off("ready"))))
            }
        }), n.ready.promise = function(e) {
            return H || (H = n.Deferred(), "complete" === l.readyState ? setTimeout(n.ready) : (l.addEventListener("DOMContentLoaded", I, !1), a.addEventListener("load", I, !1))), H.promise(e)
        }, n.ready.promise();
        var J = n.access = function(e, t, r, i, s, o, u) {
            var a = 0,
                f = e.length,
                l = null == r;
            if ("object" === n.type(r)) {
                s = !0;
                for (a in r) n.access(e, t, a, r[a], !0, o, u)
            } else if (void 0 !== i && (s = !0, n.isFunction(i) || (u = !0), l && (u ? (t.call(e, i), t = null) : (l = t, t = function(e, t, r) {
                    return l.call(n(e), r)
                })), t))
                for (; f > a; a++) t(e[a], r, u ? i : i.call(e[a], a, t(e[a], r)));
            return s ? e : l ? t.call(e) : f ? t(e[0], r) : o
        };
        n.acceptData = function(e) {
            return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
        }, K.uid = 1, K.accepts = n.acceptData, K.prototype = {
            key: function(e) {
                if (!K.accepts(e)) return 0;
                var t = {},
                    r = e[this.expando];
                if (!r) {
                    r = K.uid++;
                    try {
                        t[this.expando] = {
                            value: r
                        }, Object.defineProperties(e, t)
                    } catch (i) {
                        t[this.expando] = r, n.extend(e, t)
                    }
                }
                return this.cache[r] || (this.cache[r] = {}), r
            },
            set: function(e, t, r) {
                var i, s = this.key(e),
                    o = this.cache[s];
                if ("string" == typeof t) o[t] = r;
                else if (n.isEmptyObject(o)) n.extend(this.cache[s], t);
                else
                    for (i in t) o[i] = t[i];
                return o
            },
            get: function(e, t) {
                var n = this.cache[this.key(e)];
                return void 0 === t ? n : n[t]
            },
            access: function(e, t, r) {
                var i;
                return void 0 === t || t && "string" == typeof t && void 0 === r ? (i = this.get(e, t), void 0 !== i ? i : this.get(e, n.camelCase(t))) : (this.set(e, t, r), void 0 !== r ? r : t)
            },
            remove: function(e, t) {
                var r, i, s, o = this.key(e),
                    u = this.cache[o];
                if (void 0 === t) this.cache[o] = {};
                else {
                    n.isArray(t) ? i = t.concat(t.map(n.camelCase)) : (s = n.camelCase(t), t in u ? i = [t, s] : (i = s, i = i in u ? [i] : i.match(E) || [])), r = i.length;
                    while (r--) delete u[i[r]]
                }
            },
            hasData: function(e) {
                return !n.isEmptyObject(this.cache[e[this.expando]] || {})
            },
            discard: function(e) {
                e[this.expando] && delete this.cache[e[this.expando]]
            }
        };
        var L = new K,
            M = new K,
            N = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
            O = /([A-Z])/g;
        n.extend({
            hasData: function(e) {
                return M.hasData(e) || L.hasData(e)
            },
            data: function(e, t, n) {
                return M.access(e, t, n)
            },
            removeData: function(e, t) {
                M.remove(e, t)
            },
            _data: function(e, t, n) {
                return L.access(e, t, n)
            },
            _removeData: function(e, t) {
                L.remove(e, t)
            }
        }), n.fn.extend({
            data: function(e, t) {
                var r, i, s, o = this[0],
                    u = o && o.attributes;
                if (void 0 === e) {
                    if (this.length && (s = M.get(o), 1 === o.nodeType && !L.get(o, "hasDataAttrs"))) {
                        r = u.length;
                        while (r--) u[r] && (i = u[r].name, 0 === i.indexOf("data-") && (i = n.camelCase(i.slice(5)), P(o, i, s[i])));
                        L.set(o, "hasDataAttrs", !0)
                    }
                    return s
                }
                return "object" == typeof e ? this.each(function() {
                    M.set(this, e)
                }) : J(this, function(t) {
                    var r, i = n.camelCase(e);
                    if (o && void 0 === t) {
                        if (r = M.get(o, e), void 0 !== r) return r;
                        if (r = M.get(o, i), void 0 !== r) return r;
                        if (r = P(o, i, void 0), void 0 !== r) return r
                    } else this.each(function() {
                        var n = M.get(this, i);
                        M.set(this, i, t), -1 !== e.indexOf("-") && void 0 !== n && M.set(this, e, t)
                    })
                }, null, t, arguments.length > 1, null, !0)
            },
            removeData: function(e) {
                return this.each(function() {
                    M.remove(this, e)
                })
            }
        }), n.extend({
            queue: function(e, t, r) {
                var i;
                return e ? (t = (t || "fx") + "queue", i = L.get(e, t), r && (!i || n.isArray(r) ? i = L.access(e, t, n.makeArray(r)) : i.push(r)), i || []) : void 0
            },
            dequeue: function(e, t) {
                t = t || "fx";
                var r = n.queue(e, t),
                    i = r.length,
                    s = r.shift(),
                    o = n._queueHooks(e, t),
                    u = function() {
                        n.dequeue(e, t)
                    };
                "inprogress" === s && (s = r.shift(), i--), s && ("fx" === t && r.unshift("inprogress"), delete o.stop, s.call(e, u, o)), !i && o && o.empty.fire()
            },
            _queueHooks: function(e, t) {
                var r = t + "queueHooks";
                return L.get(e, r) || L.access(e, r, {
                    empty: n.Callbacks("once memory").add(function() {
                        L.remove(e, [t + "queue", r])
                    })
                })
            }
        }), n.fn.extend({
            queue: function(e, t) {
                var r = 2;
                return "string" != typeof e && (t = e, e = "fx", r--), arguments.length < r ? n.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                    var r = n.queue(this, e, t);
                    n._queueHooks(this, e), "fx" === e && "inprogress" !== r[0] && n.dequeue(this, e)
                })
            },
            dequeue: function(e) {
                return this.each(function() {
                    n.dequeue(this, e)
                })
            },
            clearQueue: function(e) {
                return this.queue(e || "fx", [])
            },
            promise: function(e, t) {
                var r, i = 1,
                    s = n.Deferred(),
                    o = this,
                    u = this.length,
                    a = function() {
                        --i || s.resolveWith(o, [o])
                    };
                "string" != typeof e && (t = e, e = void 0), e = e || "fx";
                while (u--) r = L.get(o[u], e + "queueHooks"), r && r.empty && (i++, r.empty.add(a));
                return a(), s.promise(t)
            }
        });
        var Q = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
            R = ["Top", "Right", "Bottom", "Left"],
            S = function(e, t) {
                return e = t || e, "none" === n.css(e, "display") || !n.contains(e.ownerDocument, e)
            },
            T = /^(?:checkbox|radio)$/i;
        ! function() {
            var e = l.createDocumentFragment(),
                t = e.appendChild(l.createElement("div")),
                n = l.createElement("input");
            n.setAttribute("type", "radio"), n.setAttribute("checked", "checked"), n.setAttribute("name", "t"), t.appendChild(n), k.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", k.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue
        }();
        var U = "undefined";
        k.focusinBubbles = "onfocusin" in a;
        var V = /^key/,
            W = /^(?:mouse|pointer|contextmenu)|click/,
            X = /^(?:focusinfocus|focusoutblur)$/,
            Y = /^([^.]*)(?:\.(.+)|)$/;
        n.event = {
            global: {},
            add: function(e, t, r, i, s) {
                var o, u, a, f, l, c, h, p, d, v, m, g = L.get(e);
                if (g) {
                    r.handler && (o = r, r = o.handler, s = o.selector), r.guid || (r.guid = n.guid++), (f = g.events) || (f = g.events = {}), (u = g.handle) || (u = g.handle = function(t) {
                        return typeof n !== U && n.event.triggered !== t.type ? n.event.dispatch.apply(e, arguments) : void 0
                    }), t = (t || "").match(E) || [""], l = t.length;
                    while (l--) a = Y.exec(t[l]) || [], d = m = a[1], v = (a[2] || "").split(".").sort(), d && (h = n.event.special[d] || {}, d = (s ? h.delegateType : h.bindType) || d, h = n.event.special[d] || {}, c = n.extend({
                        type: d,
                        origType: m,
                        data: i,
                        handler: r,
                        guid: r.guid,
                        selector: s,
                        needsContext: s && n.expr.match.needsContext.test(s),
                        namespace: v.join(".")
                    }, o), (p = f[d]) || (p = f[d] = [], p.delegateCount = 0, h.setup && h.setup.call(e, i, v, u) !== !1 || e.addEventListener && e.addEventListener(d, u, !1)), h.add && (h.add.call(e, c), c.handler.guid || (c.handler.guid = r.guid)), s ? p.splice(p.delegateCount++, 0, c) : p.push(c), n.event.global[d] = !0)
                }
            },
            remove: function(e, t, r, i, s) {
                var o, u, a, f, l, c, h, p, d, v, m, g = L.hasData(e) && L.get(e);
                if (g && (f = g.events)) {
                    t = (t || "").match(E) || [""], l = t.length;
                    while (l--)
                        if (a = Y.exec(t[l]) || [], d = m = a[1], v = (a[2] || "").split(".").sort(), d) {
                            h = n.event.special[d] || {}, d = (i ? h.delegateType : h.bindType) || d, p = f[d] || [], a = a[2] && new RegExp("(^|\\.)" + v.join("\\.(?:.*\\.|)") + "(\\.|$)"), u = o = p.length;
                            while (o--) c = p[o], !s && m !== c.origType || r && r.guid !== c.guid || a && !a.test(c.namespace) || i && i !== c.selector && ("**" !== i || !c.selector) || (p.splice(o, 1), c.selector && p.delegateCount--, h.remove && h.remove.call(e, c));
                            u && !p.length && (h.teardown && h.teardown.call(e, v, g.handle) !== !1 || n.removeEvent(e, d, g.handle), delete f[d])
                        } else
                            for (d in f) n.event.remove(e, d + t[l], r, i, !0);
                    n.isEmptyObject(f) && (delete g.handle, L.remove(e, "events"))
                }
            },
            trigger: function(e, t, r, i) {
                var s, o, u, f, c, h, p, d = [r || l],
                    v = j.call(e, "type") ? e.type : e,
                    m = j.call(e, "namespace") ? e.namespace.split(".") : [];
                if (o = u = r = r || l, 3 !== r.nodeType && 8 !== r.nodeType && !X.test(v + n.event.triggered) && (v.indexOf(".") >= 0 && (m = v.split("."), v = m.shift(), m.sort()), c = v.indexOf(":") < 0 && "on" + v, e = e[n.expando] ? e : new n.Event(v, "object" == typeof e && e), e.isTrigger = i ? 2 : 3, e.namespace = m.join("."), e.namespace_re = e.namespace ? new RegExp("(^|\\.)" + m.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = r), t = null == t ? [e] : n.makeArray(t, [e]), p = n.event.special[v] || {}, i || !p.trigger || p.trigger.apply(r, t) !== !1)) {
                    if (!i && !p.noBubble && !n.isWindow(r)) {
                        for (f = p.delegateType || v, X.test(f + v) || (o = o.parentNode); o; o = o.parentNode) d.push(o), u = o;
                        u === (r.ownerDocument || l) && d.push(u.defaultView || u.parentWindow || a)
                    }
                    s = 0;
                    while ((o = d[s++]) && !e.isPropagationStopped()) e.type = s > 1 ? f : p.bindType || v, h = (L.get(o, "events") || {})[e.type] && L.get(o, "handle"), h && h.apply(o, t), h = c && o[c], h && h.apply && n.acceptData(o) && (e.result = h.apply(o, t), e.result === !1 && e.preventDefault());
                    return e.type = v, i || e.isDefaultPrevented() || p._default && p._default.apply(d.pop(), t) !== !1 || !n.acceptData(r) || c && n.isFunction(r[v]) && !n.isWindow(r) && (u = r[c], u && (r[c] = null), n.event.triggered = v, r[v](), n.event.triggered = void 0, u && (r[c] = u)), e.result
                }
            },
            dispatch: function(e) {
                e = n.event.fix(e);
                var t, r, i, s, o, u = [],
                    a = d.call(arguments),
                    f = (L.get(this, "events") || {})[e.type] || [],
                    l = n.event.special[e.type] || {};
                if (a[0] = e, e.delegateTarget = this, !l.preDispatch || l.preDispatch.call(this, e) !== !1) {
                    u = n.event.handlers.call(this, e, f), t = 0;
                    while ((s = u[t++]) && !e.isPropagationStopped()) {
                        e.currentTarget = s.elem, r = 0;
                        while ((o = s.handlers[r++]) && !e.isImmediatePropagationStopped())(!e.namespace_re || e.namespace_re.test(o.namespace)) && (e.handleObj = o, e.data = o.data, i = ((n.event.special[o.origType] || {}).handle || o.handler).apply(s.elem, a), void 0 !== i && (e.result = i) === !1 && (e.preventDefault(), e.stopPropagation()))
                    }
                    return l.postDispatch && l.postDispatch.call(this, e), e.result
                }
            },
            handlers: function(e, t) {
                var r, i, s, o, u = [],
                    a = t.delegateCount,
                    f = e.target;
                if (a && f.nodeType && (!e.button || "click" !== e.type))
                    for (; f !== this; f = f.parentNode || this)
                        if (f.disabled !== !0 || "click" !== e.type) {
                            for (i = [], r = 0; a > r; r++) o = t[r], s = o.selector + " ", void 0 === i[s] && (i[s] = o.needsContext ? n(s, this).index(f) >= 0 : n.find(s, this, null, [f]).length), i[s] && i.push(o);
                            i.length && u.push({
                                elem: f,
                                handlers: i
                            })
                        }
                return a < t.length && u.push({
                    elem: this,
                    handlers: t.slice(a)
                }), u
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function(e, t) {
                    return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function(e, t) {
                    var n, r, i, s = t.button;
                    return null == e.pageX && null != t.clientX && (n = e.target.ownerDocument || l, r = n.documentElement, i = n.body, e.pageX = t.clientX + (r && r.scrollLeft || i && i.scrollLeft || 0) - (r && r.clientLeft || i && i.clientLeft || 0), e.pageY = t.clientY + (r && r.scrollTop || i && i.scrollTop || 0) - (r && r.clientTop || i && i.clientTop || 0)), e.which || void 0 === s || (e.which = 1 & s ? 1 : 2 & s ? 3 : 4 & s ? 2 : 0), e
                }
            },
            fix: function(e) {
                if (e[n.expando]) return e;
                var t, r, i, s = e.type,
                    o = e,
                    u = this.fixHooks[s];
                u || (this.fixHooks[s] = u = W.test(s) ? this.mouseHooks : V.test(s) ? this.keyHooks : {}), i = u.props ? this.props.concat(u.props) : this.props, e = new n.Event(o), t = i.length;
                while (t--) r = i[t], e[r] = o[r];
                return e.target || (e.target = l), 3 === e.target.nodeType && (e.target = e.target.parentNode), u.filter ? u.filter(e, o) : e
            },
            special: {
                load: {
                    noBubble: !0
                },
                focus: {
                    trigger: function() {
                        return this !== _() && this.focus ? (this.focus(), !1) : void 0
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        return this === _() && this.blur ? (this.blur(), !1) : void 0
                    },
                    delegateType: "focusout"
                },
                click: {
                    trigger: function() {
                        return "checkbox" === this.type && this.click && n.nodeName(this, "input") ? (this.click(), !1) : void 0
                    },
                    _default: function(e) {
                        return n.nodeName(e.target, "a")
                    }
                },
                beforeunload: {
                    postDispatch: function(e) {
                        void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                    }
                }
            },
            simulate: function(e, t, r, i) {
                var s = n.extend(new n.Event, r, {
                    type: e,
                    isSimulated: !0,
                    originalEvent: {}
                });
                i ? n.event.trigger(s, null, t) : n.event.dispatch.call(t, s), s.isDefaultPrevented() && r.preventDefault()
            }
        }, n.removeEvent = function(e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n, !1)
        }, n.Event = function(e, t) {
            return this instanceof n.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? Z : $) : this.type = e, t && n.extend(this, t), this.timeStamp = e && e.timeStamp || n.now(), void(this[n.expando] = !0)) : new n.Event(e, t)
        }, n.Event.prototype = {
            isDefaultPrevented: $,
            isPropagationStopped: $,
            isImmediatePropagationStopped: $,
            preventDefault: function() {
                var e = this.originalEvent;
                this.isDefaultPrevented = Z, e && e.preventDefault && e.preventDefault()
            },
            stopPropagation: function() {
                var e = this.originalEvent;
                this.isPropagationStopped = Z, e && e.stopPropagation && e.stopPropagation()
            },
            stopImmediatePropagation: function() {
                var e = this.originalEvent;
                this.isImmediatePropagationStopped = Z, e && e.stopImmediatePropagation && e.stopImmediatePropagation(), this.stopPropagation()
            }
        }, n.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout",
            pointerenter: "pointerover",
            pointerleave: "pointerout"
        }, function(e, t) {
            n.event.special[e] = {
                delegateType: t,
                bindType: t,
                handle: function(e) {
                    var r, i = this,
                        s = e.relatedTarget,
                        o = e.handleObj;
                    return (!s || s !== i && !n.contains(i, s)) && (e.type = o.origType, r = o.handler.apply(this, arguments), e.type = t), r
                }
            }
        }), k.focusinBubbles || n.each({
            focus: "focusin",
            blur: "focusout"
        }, function(e, t) {
            var r = function(e) {
                n.event.simulate(t, e.target, n.event.fix(e), !0)
            };
            n.event.special[t] = {
                setup: function() {
                    var n = this.ownerDocument || this,
                        i = L.access(n, t);
                    i || n.addEventListener(e, r, !0), L.access(n, t, (i || 0) + 1)
                },
                teardown: function() {
                    var n = this.ownerDocument || this,
                        i = L.access(n, t) - 1;
                    i ? L.access(n, t, i) : (n.removeEventListener(e, r, !0), L.remove(n, t))
                }
            }
        }), n.fn.extend({
            on: function(e, t, r, i, s) {
                var o, u;
                if ("object" == typeof e) {
                    "string" != typeof t && (r = r || t, t = void 0);
                    for (u in e) this.on(u, t, r, e[u], s);
                    return this
                }
                if (null == r && null == i ? (i = t, r = t = void 0) : null == i && ("string" == typeof t ? (i = r, r = void 0) : (i = r, r = t, t = void 0)), i === !1) i = $;
                else if (!i) return this;
                return 1 === s && (o = i, i = function(e) {
                    return n().off(e), o.apply(this, arguments)
                }, i.guid = o.guid || (o.guid = n.guid++)), this.each(function() {
                    n.event.add(this, e, i, r, t)
                })
            },
            one: function(e, t, n, r) {
                return this.on(e, t, n, r, 1)
            },
            off: function(e, t, r) {
                var i, s;
                if (e && e.preventDefault && e.handleObj) return i = e.handleObj, n(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
                if ("object" == typeof e) {
                    for (s in e) this.off(s, t, e[s]);
                    return this
                }
                return (t === !1 || "function" == typeof t) && (r = t, t = void 0), r === !1 && (r = $), this.each(function() {
                    n.event.remove(this, e, r, t)
                })
            },
            trigger: function(e, t) {
                return this.each(function() {
                    n.event.trigger(e, t, this)
                })
            },
            triggerHandler: function(e, t) {
                var r = this[0];
                return r ? n.event.trigger(e, t, r, !0) : void 0
            }
        });
        var ab = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
            bb = /<([\w:]+)/,
            cb = /<|&#?\w+;/,
            db = /<(?:script|style|link)/i,
            eb = /checked\s*(?:[^=]|=\s*.checked.)/i,
            fb = /^$|\/(?:java|ecma)script/i,
            gb = /^true\/(.*)/,
            hb = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
            ib = {
                option: [1, "<select multiple='multiple'>", "</select>"],
                thead: [1, "<table>", "</table>"],
                col: [2, "<table><colgroup>", "</colgroup></table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                _default: [0, "", ""]
            };
        ib.optgroup = ib.option, ib.tbody = ib.tfoot = ib.colgroup = ib.caption = ib.thead, ib.th = ib.td, n.extend({
            clone: function(e, t, r) {
                var i, s, o, u, a = e.cloneNode(!0),
                    f = n.contains(e.ownerDocument, e);
                if (!(k.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || n.isXMLDoc(e)))
                    for (u = ob(a), o = ob(e), i = 0, s = o.length; s > i; i++) pb(o[i], u[i]);
                if (t)
                    if (r)
                        for (o = o || ob(e), u = u || ob(a), i = 0, s = o.length; s > i; i++) nb(o[i], u[i]);
                    else nb(e, a);
                return u = ob(a, "script"), u.length > 0 && mb(u, !f && ob(e, "script")), a
            },
            buildFragment: function(e, t, r, i) {
                for (var s, o, u, a, f, l, c = t.createDocumentFragment(), h = [], p = 0, d = e.length; d > p; p++)
                    if (s = e[p], s || 0 === s)
                        if ("object" === n.type(s)) n.merge(h, s.nodeType ? [s] : s);
                        else if (cb.test(s)) {
                    o = o || c.appendChild(t.createElement("div")), u = (bb.exec(s) || ["", ""])[1].toLowerCase(), a = ib[u] || ib._default, o.innerHTML = a[1] + s.replace(ab, "<$1></$2>") + a[2], l = a[0];
                    while (l--) o = o.lastChild;
                    n.merge(h, o.childNodes), o = c.firstChild, o.textContent = ""
                } else h.push(t.createTextNode(s));
                c.textContent = "", p = 0;
                while (s = h[p++])
                    if ((!i || -1 === n.inArray(s, i)) && (f = n.contains(s.ownerDocument, s), o = ob(c.appendChild(s), "script"), f && mb(o), r)) {
                        l = 0;
                        while (s = o[l++]) fb.test(s.type || "") && r.push(s)
                    }
                return c
            },
            cleanData: function(e) {
                for (var t, r, i, s, o = n.event.special, u = 0; void 0 !== (r = e[u]); u++) {
                    if (n.acceptData(r) && (s = r[L.expando], s && (t = L.cache[s]))) {
                        if (t.events)
                            for (i in t.events) o[i] ? n.event.remove(r, i) : n.removeEvent(r, i, t.handle);
                        L.cache[s] && delete L.cache[s]
                    }
                    delete M.cache[r[M.expando]]
                }
            }
        }), n.fn.extend({
            text: function(e) {
                return J(this, function(e) {
                    return void 0 === e ? n.text(this) : this.empty().each(function() {
                        (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = e)
                    })
                }, null, e, arguments.length)
            },
            append: function() {
                return this.domManip(arguments, function(e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = jb(this, e);
                        t.appendChild(e)
                    }
                })
            },
            prepend: function() {
                return this.domManip(arguments, function(e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = jb(this, e);
                        t.insertBefore(e, t.firstChild)
                    }
                })
            },
            before: function() {
                return this.domManip(arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this)
                })
            },
            after: function() {
                return this.domManip(arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
                })
            },
            remove: function(e, t) {
                for (var r, i = e ? n.filter(e, this) : this, s = 0; null != (r = i[s]); s++) t || 1 !== r.nodeType || n.cleanData(ob(r)), r.parentNode && (t && n.contains(r.ownerDocument, r) && mb(ob(r, "script")), r.parentNode.removeChild(r));
                return this
            },
            empty: function() {
                for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (n.cleanData(ob(e, !1)), e.textContent = "");
                return this
            },
            clone: function(e, t) {
                return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function() {
                    return n.clone(this, e, t)
                })
            },
            html: function(e) {
                return J(this, function(e) {
                    var t = this[0] || {},
                        r = 0,
                        i = this.length;
                    if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                    if ("string" == typeof e && !db.test(e) && !ib[(bb.exec(e) || ["", ""])[1].toLowerCase()]) {
                        e = e.replace(ab, "<$1></$2>");
                        try {
                            for (; i > r; r++) t = this[r] || {}, 1 === t.nodeType && (n.cleanData(ob(t, !1)), t.innerHTML = e);
                            t = 0
                        } catch (s) {}
                    }
                    t && this.empty().append(e)
                }, null, e, arguments.length)
            },
            replaceWith: function() {
                var e = arguments[0];
                return this.domManip(arguments, function(t) {
                    e = this.parentNode, n.cleanData(ob(this)), e && e.replaceChild(t, this)
                }), e && (e.length || e.nodeType) ? this : this.remove()
            },
            detach: function(e) {
                return this.remove(e, !0)
            },
            domManip: function(t, r) {
                t = e.apply([], t);
                var i, s, o, u, a, f, l = 0,
                    c = this.length,
                    h = this,
                    p = c - 1,
                    d = t[0],
                    v = n.isFunction(d);
                if (v || c > 1 && "string" == typeof d && !k.checkClone && eb.test(d)) return this.each(function(e) {
                    var n = h.eq(e);
                    v && (t[0] = d.call(this, e, n.html())), n.domManip(t, r)
                });
                if (c && (i = n.buildFragment(t, this[0].ownerDocument, !1, this), s = i.firstChild, 1 === i.childNodes.length && (i = s), s)) {
                    for (o = n.map(ob(i, "script"), kb), u = o.length; c > l; l++) a = i, l !== p && (a = n.clone(a, !0, !0), u && n.merge(o, ob(a, "script"))), r.call(this[l], a, l);
                    if (u)
                        for (f = o[o.length - 1].ownerDocument, n.map(o, lb), l = 0; u > l; l++) a = o[l], fb.test(a.type || "") && !L.access(a, "globalEval") && n.contains(f, a) && (a.src ? n._evalUrl && n._evalUrl(a.src) : n.globalEval(a.textContent.replace(hb, "")))
                }
                return this
            }
        }), n.each({
            appendTo: "append",
            prependTo: "prepend",
            insertBefore: "before",
            insertAfter: "after",
            replaceAll: "replaceWith"
        }, function(e, t) {
            n.fn[e] = function(e) {
                for (var r, i = [], s = n(e), o = s.length - 1, u = 0; o >= u; u++) r = u === o ? this : this.clone(!0), n(s[u])[t](r), f.apply(i, r.get());
                return this.pushStack(i)
            }
        });
        var qb, rb = {},
            ub = /^margin/,
            vb = new RegExp("^(" + Q + ")(?!px)[a-z%]+$", "i"),
            wb = function(e) {
                return e.ownerDocument.defaultView.opener ? e.ownerDocument.defaultView.getComputedStyle(e, null) : a.getComputedStyle(e, null)
            };
        ! function() {
            var e, t, r = l.documentElement,
                i = l.createElement("div"),
                s = l.createElement("div");
            if (s.style) {
                s.style.backgroundClip = "content-box", s.cloneNode(!0).style.backgroundClip = "", k.clearCloneStyle = "content-box" === s.style.backgroundClip, i.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute", i.appendChild(s);

                function o() {
                    s.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", s.innerHTML = "", r.appendChild(i);
                    var n = a.getComputedStyle(s, null);
                    e = "1%" !== n.top, t = "4px" === n.width, r.removeChild(i)
                }
                a.getComputedStyle && n.extend(k, {
                    pixelPosition: function() {
                        return o(), e
                    },
                    boxSizingReliable: function() {
                        return null == t && o(), t
                    },
                    reliableMarginRight: function() {
                        var e, t = s.appendChild(l.createElement("div"));
                        return t.style.cssText = s.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", t.style.marginRight = t.style.width = "0", s.style.width = "1px", r.appendChild(i), e = !parseFloat(a.getComputedStyle(t, null).marginRight), r.removeChild(i), s.removeChild(t), e
                    }
                })
            }
        }(), n.swap = function(e, t, n, r) {
            var i, s, o = {};
            for (s in t) o[s] = e.style[s], e.style[s] = t[s];
            i = n.apply(e, r || []);
            for (s in t) e.style[s] = o[s];
            return i
        };
        var zb = /^(none|table(?!-c[ea]).+)/,
            Ab = new RegExp("^(" + Q + ")(.*)$", "i"),
            Bb = new RegExp("^([+-])=(" + Q + ")", "i"),
            Cb = {
                position: "absolute",
                visibility: "hidden",
                display: "block"
            },
            Db = {
                letterSpacing: "0",
                fontWeight: "400"
            },
            Eb = ["Webkit", "O", "Moz", "ms"];
        n.extend({
            cssHooks: {
                opacity: {
                    get: function(e, t) {
                        if (t) {
                            var n = xb(e, "opacity");
                            return "" === n ? "1" : n
                        }
                    }
                }
            },
            cssNumber: {
                columnCount: !0,
                fillOpacity: !0,
                flexGrow: !0,
                flexShrink: !0,
                fontWeight: !0,
                lineHeight: !0,
                opacity: !0,
                order: !0,
                orphans: !0,
                widows: !0,
                zIndex: !0,
                zoom: !0
            },
            cssProps: {
                "float": "cssFloat"
            },
            style: function(e, t, r, i) {
                if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                    var s, o, u, a = n.camelCase(t),
                        f = e.style;
                    return t = n.cssProps[a] || (n.cssProps[a] = Fb(f, a)), u = n.cssHooks[t] || n.cssHooks[a], void 0 === r ? u && "get" in u && void 0 !== (s = u.get(e, !1, i)) ? s : f[t] : (o = typeof r, "string" === o && (s = Bb.exec(r)) && (r = (s[1] + 1) * s[2] + parseFloat(n.css(e, t)), o = "number"), null != r && r === r && ("number" !== o || n.cssNumber[a] || (r += "px"), k.clearCloneStyle || "" !== r || 0 !== t.indexOf("background") || (f[t] = "inherit"), u && "set" in u && void 0 === (r = u.set(e, r, i)) || (f[t] = r)), void 0)
                }
            },
            css: function(e, t, r, i) {
                var s, o, u, a = n.camelCase(t);
                return t = n.cssProps[a] || (n.cssProps[a] = Fb(e.style, a)), u = n.cssHooks[t] || n.cssHooks[a], u && "get" in u && (s = u.get(e, !0, r)), void 0 === s && (s = xb(e, t, i)), "normal" === s && t in Db && (s = Db[t]), "" === r || r ? (o = parseFloat(s), r === !0 || n.isNumeric(o) ? o || 0 : s) : s
            }
        }), n.each(["height", "width"], function(e, t) {
            n.cssHooks[t] = {
                get: function(e, r, i) {
                    return r ? zb.test(n.css(e, "display")) && 0 === e.offsetWidth ? n.swap(e, Cb, function() {
                        return Ib(e, t, i)
                    }) : Ib(e, t, i) : void 0
                },
                set: function(e, r, i) {
                    var s = i && wb(e);
                    return Gb(e, r, i ? Hb(e, t, i, "border-box" === n.css(e, "boxSizing", !1, s), s) : 0)
                }
            }
        }), n.cssHooks.marginRight = yb(k.reliableMarginRight, function(e, t) {
            return t ? n.swap(e, {
                display: "inline-block"
            }, xb, [e, "marginRight"]) : void 0
        }), n.each({
            margin: "",
            padding: "",
            border: "Width"
        }, function(e, t) {
            n.cssHooks[e + t] = {
                expand: function(n) {
                    for (var r = 0, i = {}, s = "string" == typeof n ? n.split(" ") : [n]; 4 > r; r++) i[e + R[r] + t] = s[r] || s[r - 2] || s[0];
                    return i
                }
            }, ub.test(e) || (n.cssHooks[e + t].set = Gb)
        }), n.fn.extend({
            css: function(e, t) {
                return J(this, function(e, t, r) {
                    var i, s, o = {},
                        u = 0;
                    if (n.isArray(t)) {
                        for (i = wb(e), s = t.length; s > u; u++) o[t[u]] = n.css(e, t[u], !1, i);
                        return o
                    }
                    return void 0 !== r ? n.style(e, t, r) : n.css(e, t)
                }, e, t, arguments.length > 1)
            },
            show: function() {
                return Jb(this, !0)
            },
            hide: function() {
                return Jb(this)
            },
            toggle: function(e) {
                return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                    S(this) ? n(this).show() : n(this).hide()
                })
            }
        }), n.Tween = Kb, Kb.prototype = {
            constructor: Kb,
            init: function(e, t, r, i, s, o) {
                this.elem = e, this.prop = r, this.easing = s || "swing", this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = o || (n.cssNumber[r] ? "" : "px")
            },
            cur: function() {
                var e = Kb.propHooks[this.prop];
                return e && e.get ? e.get(this) : Kb.propHooks._default.get(this)
            },
            run: function(e) {
                var t, r = Kb.propHooks[this.prop];
                return this.pos = t = this.options.duration ? n.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), r && r.set ? r.set(this) : Kb.propHooks._default.set(this), this
            }
        }, Kb.prototype.init.prototype = Kb.prototype, Kb.propHooks = {
            _default: {
                get: function(e) {
                    var t;
                    return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = n.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
                },
                set: function(e) {
                    n.fx.step[e.prop] ? n.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[n.cssProps[e.prop]] || n.cssHooks[e.prop]) ? n.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
                }
            }
        }, Kb.propHooks.scrollTop = Kb.propHooks.scrollLeft = {
            set: function(e) {
                e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
            }
        }, n.easing = {
            linear: function(e) {
                return e
            },
            swing: function(e) {
                return .5 - Math.cos(e * Math.PI) / 2
            }
        }, n.fx = Kb.prototype.init, n.fx.step = {};
        var Lb, Mb, Nb = /^(?:toggle|show|hide)$/,
            Ob = new RegExp("^(?:([+-])=|)(" + Q + ")([a-z%]*)$", "i"),
            Pb = /queueHooks$/,
            Qb = [Vb],
            Rb = {
                "*": [function(e, t) {
                    var r = this.createTween(e, t),
                        i = r.cur(),
                        s = Ob.exec(t),
                        o = s && s[3] || (n.cssNumber[e] ? "" : "px"),
                        u = (n.cssNumber[e] || "px" !== o && +i) && Ob.exec(n.css(r.elem, e)),
                        a = 1,
                        f = 20;
                    if (u && u[3] !== o) {
                        o = o || u[3], s = s || [], u = +i || 1;
                        do a = a || ".5", u /= a, n.style(r.elem, e, u + o); while (a !== (a = r.cur() / i) && 1 !== a && --f)
                    }
                    return s && (u = r.start = +u || +i || 0, r.unit = o, r.end = s[1] ? u + (s[1] + 1) * s[2] : +s[2]), r
                }]
            };
        n.Animation = n.extend(Xb, {
                tweener: function(e, t) {
                    n.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
                    for (var r, i = 0, s = e.length; s > i; i++) r = e[i], Rb[r] = Rb[r] || [], Rb[r].unshift(t)
                },
                prefilter: function(e, t) {
                    t ? Qb.unshift(e) : Qb.push(e)
                }
            }), n.speed = function(e, t, r) {
                var i = e && "object" == typeof e ? n.extend({}, e) : {
                    complete: r || !r && t || n.isFunction(e) && e,
                    duration: e,
                    easing: r && t || t && !n.isFunction(t) && t
                };
                return i.duration = n.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in n.fx.speeds ? n.fx.speeds[i.duration] : n.fx.speeds._default, (null == i.queue || i.queue === !0) && (i.queue = "fx"), i.old = i.complete, i.complete = function() {
                    n.isFunction(i.old) && i.old.call(this), i.queue && n.dequeue(this, i.queue)
                }, i
            }, n.fn.extend({
                fadeTo: function(e, t, n, r) {
                    return this.filter(S).css("opacity", 0).show().end().animate({
                        opacity: t
                    }, e, n, r)
                },
                animate: function(e, t, r, i) {
                    var s = n.isEmptyObject(e),
                        o = n.speed(t, r, i),
                        u = function() {
                            var t = Xb(this, n.extend({}, e), o);
                            (s || L.get(this, "finish")) && t.stop(!0)
                        };
                    return u.finish = u, s || o.queue === !1 ? this.each(u) : this.queue(o.queue, u)
                },
                stop: function(e, t, r) {
                    var i = function(e) {
                        var t = e.stop;
                        delete e.stop, t(r)
                    };
                    return "string" != typeof e && (r = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                        var t = !0,
                            s = null != e && e + "queueHooks",
                            o = n.timers,
                            u = L.get(this);
                        if (s) u[s] && u[s].stop && i(u[s]);
                        else
                            for (s in u) u[s] && u[s].stop && Pb.test(s) && i(u[s]);
                        for (s = o.length; s--;) o[s].elem !== this || null != e && o[s].queue !== e || (o[s].anim.stop(r), t = !1, o.splice(s, 1));
                        (t || !r) && n.dequeue(this, e)
                    })
                },
                finish: function(e) {
                    return e !== !1 && (e = e || "fx"), this.each(function() {
                        var t, r = L.get(this),
                            i = r[e + "queue"],
                            s = r[e + "queueHooks"],
                            o = n.timers,
                            u = i ? i.length : 0;
                        for (r.finish = !0, n.queue(this, e, []), s && s.stop && s.stop.call(this, !0), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                        for (t = 0; u > t; t++) i[t] && i[t].finish && i[t].finish.call(this);
                        delete r.finish
                    })
                }
            }), n.each(["toggle", "show", "hide"], function(e, t) {
                var r = n.fn[t];
                n.fn[t] = function(e, n, i) {
                    return null == e || "boolean" == typeof e ? r.apply(this, arguments) : this.animate(Tb(t, !0), e, n, i)
                }
            }), n.each({
                slideDown: Tb("show"),
                slideUp: Tb("hide"),
                slideToggle: Tb("toggle"),
                fadeIn: {
                    opacity: "show"
                },
                fadeOut: {
                    opacity: "hide"
                },
                fadeToggle: {
                    opacity: "toggle"
                }
            }, function(e, t) {
                n.fn[e] = function(e, n, r) {
                    return this.animate(t, e, n, r)
                }
            }), n.timers = [], n.fx.tick = function() {
                var e, t = 0,
                    r = n.timers;
                for (Lb = n.now(); t < r.length; t++) e = r[t], e() || r[t] !== e || r.splice(t--, 1);
                r.length || n.fx.stop(), Lb = void 0
            }, n.fx.timer = function(e) {
                n.timers.push(e), e() ? n.fx.start() : n.timers.pop()
            }, n.fx.interval = 13, n.fx.start = function() {
                Mb || (Mb = setInterval(n.fx.tick, n.fx.interval))
            }, n.fx.stop = function() {
                clearInterval(Mb), Mb = null
            }, n.fx.speeds = {
                slow: 600,
                fast: 200,
                _default: 400
            }, n.fn.delay = function(e, t) {
                return e = n.fx ? n.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
                    var r = setTimeout(t, e);
                    n.stop = function() {
                        clearTimeout(r)
                    }
                })
            },
            function() {
                var e = l.createElement("input"),
                    t = l.createElement("select"),
                    n = t.appendChild(l.createElement("option"));
                e.type = "checkbox", k.checkOn = "" !== e.value, k.optSelected = n.selected, t.disabled = !0, k.optDisabled = !n.disabled, e = l.createElement("input"), e.value = "t", e.type = "radio", k.radioValue = "t" === e.value
            }();
        var Yb, Zb, $b = n.expr.attrHandle;
        n.fn.extend({
            attr: function(e, t) {
                return J(this, n.attr, e, t, arguments.length > 1)
            },
            removeAttr: function(e) {
                return this.each(function() {
                    n.removeAttr(this, e)
                })
            }
        }), n.extend({
            attr: function(e, t, r) {
                var i, s, o = e.nodeType;
                if (e && 3 !== o && 8 !== o && 2 !== o) return typeof e.getAttribute === U ? n.prop(e, t, r) : (1 === o && n.isXMLDoc(e) || (t = t.toLowerCase(), i = n.attrHooks[t] || (n.expr.match.bool.test(t) ? Zb : Yb)), void 0 === r ? i && "get" in i && null !== (s = i.get(e, t)) ? s : (s = n.find.attr(e, t), null == s ? void 0 : s) : null !== r ? i && "set" in i && void 0 !== (s = i.set(e, r, t)) ? s : (e.setAttribute(t, r + ""), r) : void n.removeAttr(e, t))
            },
            removeAttr: function(e, t) {
                var r, i, s = 0,
                    o = t && t.match(E);
                if (o && 1 === e.nodeType)
                    while (r = o[s++]) i = n.propFix[r] || r, n.expr.match.bool.test(r) && (e[i] = !1), e.removeAttribute(r)
            },
            attrHooks: {
                type: {
                    set: function(e, t) {
                        if (!k.radioValue && "radio" === t && n.nodeName(e, "input")) {
                            var r = e.value;
                            return e.setAttribute("type", t), r && (e.value = r), t
                        }
                    }
                }
            }
        }), Zb = {
            set: function(e, t, r) {
                return t === !1 ? n.removeAttr(e, r) : e.setAttribute(r, r), r
            }
        }, n.each(n.expr.match.bool.source.match(/\w+/g), function(e, t) {
            var r = $b[t] || n.find.attr;
            $b[t] = function(e, t, n) {
                var i, s;
                return n || (s = $b[t], $b[t] = i, i = null != r(e, t, n) ? t.toLowerCase() : null, $b[t] = s), i
            }
        });
        var _b = /^(?:input|select|textarea|button)$/i;
        n.fn.extend({
            prop: function(e, t) {
                return J(this, n.prop, e, t, arguments.length > 1)
            },
            removeProp: function(e) {
                return this.each(function() {
                    delete this[n.propFix[e] || e]
                })
            }
        }), n.extend({
            propFix: {
                "for": "htmlFor",
                "class": "className"
            },
            prop: function(e, t, r) {
                var i, s, o, u = e.nodeType;
                if (e && 3 !== u && 8 !== u && 2 !== u) return o = 1 !== u || !n.isXMLDoc(e), o && (t = n.propFix[t] || t, s = n.propHooks[t]), void 0 !== r ? s && "set" in s && void 0 !== (i = s.set(e, r, t)) ? i : e[t] = r : s && "get" in s && null !== (i = s.get(e, t)) ? i : e[t]
            },
            propHooks: {
                tabIndex: {
                    get: function(e) {
                        return e.hasAttribute("tabindex") || _b.test(e.nodeName) || e.href ? e.tabIndex : -1
                    }
                }
            }
        }), k.optSelected || (n.propHooks.selected = {
            get: function(e) {
                var t = e.parentNode;
                return t && t.parentNode && t.parentNode.selectedIndex, null
            }
        }), n.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
            n.propFix[this.toLowerCase()] = this
        });
        var ac = /[\t\r\n\f]/g;
        n.fn.extend({
            addClass: function(e) {
                var t, r, i, s, o, u, a = "string" == typeof e && e,
                    f = 0,
                    l = this.length;
                if (n.isFunction(e)) return this.each(function(t) {
                    n(this).addClass(e.call(this, t, this.className))
                });
                if (a)
                    for (t = (e || "").match(E) || []; l > f; f++)
                        if (r = this[f], i = 1 === r.nodeType && (r.className ? (" " + r.className + " ").replace(ac, " ") : " ")) {
                            o = 0;
                            while (s = t[o++]) i.indexOf(" " + s + " ") < 0 && (i += s + " ");
                            u = n.trim(i), r.className !== u && (r.className = u)
                        }
                return this
            },
            removeClass: function(e) {
                var t, r, i, s, o, u, a = 0 === arguments.length || "string" == typeof e && e,
                    f = 0,
                    l = this.length;
                if (n.isFunction(e)) return this.each(function(t) {
                    n(this).removeClass(e.call(this, t, this.className))
                });
                if (a)
                    for (t = (e || "").match(E) || []; l > f; f++)
                        if (r = this[f], i = 1 === r.nodeType && (r.className ? (" " + r.className + " ").replace(ac, " ") : "")) {
                            o = 0;
                            while (s = t[o++])
                                while (i.indexOf(" " + s + " ") >= 0) i = i.replace(" " + s + " ", " ");
                            u = e ? n.trim(i) : "", r.className !== u && (r.className = u)
                        }
                return this
            },
            toggleClass: function(e, t) {
                var r = typeof e;
                return "boolean" == typeof t && "string" === r ? t ? this.addClass(e) : this.removeClass(e) : this.each(n.isFunction(e) ? function(r) {
                    n(this).toggleClass(e.call(this, r, this.className, t), t)
                } : function() {
                    if ("string" === r) {
                        var t, i = 0,
                            s = n(this),
                            o = e.match(E) || [];
                        while (t = o[i++]) s.hasClass(t) ? s.removeClass(t) : s.addClass(t)
                    } else(r === U || "boolean" === r) && (this.className && L.set(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : L.get(this, "__className__") || "")
                })
            },
            hasClass: function(e) {
                for (var t = " " + e + " ", n = 0, r = this.length; r > n; n++)
                    if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(ac, " ").indexOf(t) >= 0) return !0;
                return !1
            }
        });
        var bc = /\r/g;
        n.fn.extend({
            val: function(e) {
                var t, r, i, s = this[0];
                if (arguments.length) return i = n.isFunction(e), this.each(function(r) {
                    var s;
                    1 === this.nodeType && (s = i ? e.call(this, r, n(this).val()) : e, null == s ? s = "" : "number" == typeof s ? s += "" : n.isArray(s) && (s = n.map(s, function(e) {
                        return null == e ? "" : e + ""
                    })), t = n.valHooks[this.type] || n.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, s, "value") || (this.value = s))
                });
                if (s) return t = n.valHooks[s.type] || n.valHooks[s.nodeName.toLowerCase()], t && "get" in t && void 0 !== (r = t.get(s, "value")) ? r : (r = s.value, "string" == typeof r ? r.replace(bc, "") : null == r ? "" : r)
            }
        }), n.extend({
            valHooks: {
                option: {
                    get: function(e) {
                        var t = n.find.attr(e, "value");
                        return null != t ? t : n.trim(n.text(e))
                    }
                },
                select: {
                    get: function(e) {
                        for (var t, r, i = e.options, s = e.selectedIndex, o = "select-one" === e.type || 0 > s, u = o ? null : [], a = o ? s + 1 : i.length, f = 0 > s ? a : o ? s : 0; a > f; f++)
                            if (r = i[f], !(!r.selected && f !== s || (k.optDisabled ? r.disabled : null !== r.getAttribute("disabled")) || r.parentNode.disabled && n.nodeName(r.parentNode, "optgroup"))) {
                                if (t = n(r).val(), o) return t;
                                u.push(t)
                            }
                        return u
                    },
                    set: function(e, t) {
                        var r, i, s = e.options,
                            o = n.makeArray(t),
                            u = s.length;
                        while (u--) i = s[u], (i.selected = n.inArray(i.value, o) >= 0) && (r = !0);
                        return r || (e.selectedIndex = -1), o
                    }
                }
            }
        }), n.each(["radio", "checkbox"], function() {
            n.valHooks[this] = {
                set: function(e, t) {
                    return n.isArray(t) ? e.checked = n.inArray(n(e).val(), t) >= 0 : void 0
                }
            }, k.checkOn || (n.valHooks[this].get = function(e) {
                return null === e.getAttribute("value") ? "on" : e.value
            })
        }), n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
            n.fn[t] = function(e, n) {
                return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
            }
        }), n.fn.extend({
            hover: function(e, t) {
                return this.mouseenter(e).mouseleave(t || e)
            },
            bind: function(e, t, n) {
                return this.on(e, null, t, n)
            },
            unbind: function(e, t) {
                return this.off(e, null, t)
            },
            delegate: function(e, t, n, r) {
                return this.on(t, e, n, r)
            },
            undelegate: function(e, t, n) {
                return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
            }
        });
        var cc = n.now(),
            dc = /\?/;
        n.parseJSON = function(e) {
            return JSON.parse(e + "")
        }, n.parseXML = function(e) {
            var t, r;
            if (!e || "string" != typeof e) return null;
            try {
                r = new DOMParser, t = r.parseFromString(e, "text/xml")
            } catch (i) {
                t = void 0
            }
            return (!t || t.getElementsByTagName("parsererror").length) && n.error("Invalid XML: " + e), t
        };
        var ec = /#.*$/,
            fc = /([?&])_=[^&]*/,
            gc = /^(.*?):[ \t]*([^\r\n]*)$/gm,
            hc = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
            ic = /^(?:GET|HEAD)$/,
            jc = /^\/\//,
            kc = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
            lc = {},
            mc = {},
            nc = "*/".concat("*"),
            oc = a.location.href,
            pc = kc.exec(oc.toLowerCase()) || [];
        n.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: oc,
                type: "GET",
                isLocal: hc.test(pc[1]),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": nc,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript"
                },
                contents: {
                    xml: /xml/,
                    html: /html/,
                    json: /json/
                },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText",
                    json: "responseJSON"
                },
                converters: {
                    "* text": String,
                    "text html": !0,
                    "text json": n.parseJSON,
                    "text xml": n.parseXML
                },
                flatOptions: {
                    url: !0,
                    context: !0
                }
            },
            ajaxSetup: function(e, t) {
                return t ? sc(sc(e, n.ajaxSettings), t) : sc(n.ajaxSettings, e)
            },
            ajaxPrefilter: qc(lc),
            ajaxTransport: qc(mc),
            ajax: function(e, t) {
                function T(e, t, o, a) {
                    var l, g, y, w, E, x = t;
                    2 !== b && (b = 2, u && clearTimeout(u), r = void 0, s = a || "", S.readyState = e > 0 ? 4 : 0, l = e >= 200 && 300 > e || 304 === e, o && (w = tc(c, S, o)), w = uc(c, w, S, l), l ? (c.ifModified && (E = S.getResponseHeader("Last-Modified"), E && (n.lastModified[i] = E), E = S.getResponseHeader("etag"), E && (n.etag[i] = E)), 204 === e || "HEAD" === c.type ? x = "nocontent" : 304 === e ? x = "notmodified" : (x = w.state, g = w.data, y = w.error, l = !y)) : (y = x, (e || !x) && (x = "error", 0 > e && (e = 0))), S.status = e, S.statusText = (t || x) + "", l ? d.resolveWith(h, [g, x, S]) : d.rejectWith(h, [S, x, y]), S.statusCode(m), m = void 0, f && p.trigger(l ? "ajaxSuccess" : "ajaxError", [S, c, l ? g : y]), v.fireWith(h, [S, x]), f && (p.trigger("ajaxComplete", [S, c]), --n.active || n.event.trigger("ajaxStop")))
                }
                "object" == typeof e && (t = e, e = void 0), t = t || {};
                var r, i, s, o, u, a, f, l, c = n.ajaxSetup({}, t),
                    h = c.context || c,
                    p = c.context && (h.nodeType || h.jquery) ? n(h) : n.event,
                    d = n.Deferred(),
                    v = n.Callbacks("once memory"),
                    m = c.statusCode || {},
                    g = {},
                    y = {},
                    b = 0,
                    w = "canceled",
                    S = {
                        readyState: 0,
                        getResponseHeader: function(e) {
                            var t;
                            if (2 === b) {
                                if (!o) {
                                    o = {};
                                    while (t = gc.exec(s)) o[t[1].toLowerCase()] = t[2]
                                }
                                t = o[e.toLowerCase()]
                            }
                            return null == t ? null : t
                        },
                        getAllResponseHeaders: function() {
                            return 2 === b ? s : null
                        },
                        setRequestHeader: function(e, t) {
                            var n = e.toLowerCase();
                            return b || (e = y[n] = y[n] || e, g[e] = t), this
                        },
                        overrideMimeType: function(e) {
                            return b || (c.mimeType = e), this
                        },
                        statusCode: function(e) {
                            var t;
                            if (e)
                                if (2 > b)
                                    for (t in e) m[t] = [m[t], e[t]];
                                else S.always(e[S.status]);
                            return this
                        },
                        abort: function(e) {
                            var t = e || w;
                            return r && r.abort(t), T(0, t), this
                        }
                    };
                if (d.promise(S).complete = v.add, S.success = S.done, S.error = S.fail, c.url = ((e || c.url || oc) + "").replace(ec, "").replace(jc, pc[1] + "//"), c.type = t.method || t.type || c.method || c.type, c.dataTypes = n.trim(c.dataType || "*").toLowerCase().match(E) || [""], null == c.crossDomain && (a = kc.exec(c.url.toLowerCase()), c.crossDomain = !(!a || a[1] === pc[1] && a[2] === pc[2] && (a[3] || ("http:" === a[1] ? "80" : "443")) === (pc[3] || ("http:" === pc[1] ? "80" : "443")))), c.data && c.processData && "string" != typeof c.data && (c.data = n.param(c.data, c.traditional)), rc(lc, c, t, S), 2 === b) return S;
                f = n.event && c.global, f && 0 === n.active++ && n.event.trigger("ajaxStart"), c.type = c.type.toUpperCase(), c.hasContent = !ic.test(c.type), i = c.url, c.hasContent || (c.data && (i = c.url += (dc.test(i) ? "&" : "?") + c.data, delete c.data), c.cache === !1 && (c.url = fc.test(i) ? i.replace(fc, "$1_=" + cc++) : i + (dc.test(i) ? "&" : "?") + "_=" + cc++)), c.ifModified && (n.lastModified[i] && S.setRequestHeader("If-Modified-Since", n.lastModified[i]), n.etag[i] && S.setRequestHeader("If-None-Match", n.etag[i])), (c.data && c.hasContent && c.contentType !== !1 || t.contentType) && S.setRequestHeader("Content-Type", c.contentType), S.setRequestHeader("Accept", c.dataTypes[0] && c.accepts[c.dataTypes[0]] ? c.accepts[c.dataTypes[0]] + ("*" !== c.dataTypes[0] ? ", " + nc + "; q=0.01" : "") : c.accepts["*"]);
                for (l in c.headers) S.setRequestHeader(l, c.headers[l]);
                if (!c.beforeSend || c.beforeSend.call(h, S, c) !== !1 && 2 !== b) {
                    w = "abort";
                    for (l in {
                            success: 1,
                            error: 1,
                            complete: 1
                        }) S[l](c[l]);
                    if (r = rc(mc, c, t, S)) {
                        S.readyState = 1, f && p.trigger("ajaxSend", [S, c]), c.async && c.timeout > 0 && (u = setTimeout(function() {
                            S.abort("timeout")
                        }, c.timeout));
                        try {
                            b = 1, r.send(g, T)
                        } catch (x) {
                            if (!(2 > b)) throw x;
                            T(-1, x)
                        }
                    } else T(-1, "No Transport");
                    return S
                }
                return S.abort()
            },
            getJSON: function(e, t, r) {
                return n.get(e, t, r, "json")
            },
            getScript: function(e, t) {
                return n.get(e, void 0, t, "script")
            }
        }), n.each(["get", "post"], function(e, t) {
            n[t] = function(e, r, i, s) {
                return n.isFunction(r) && (s = s || i, i = r, r = void 0), n.ajax({
                    url: e,
                    type: t,
                    dataType: s,
                    data: r,
                    success: i
                })
            }
        }), n._evalUrl = function(e) {
            return n.ajax({
                url: e,
                type: "GET",
                dataType: "script",
                async: !1,
                global: !1,
                "throws": !0
            })
        }, n.fn.extend({
            wrapAll: function(e) {
                var t;
                return n.isFunction(e) ? this.each(function(t) {
                    n(this).wrapAll(e.call(this, t))
                }) : (this[0] && (t = n(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                    var e = this;
                    while (e.firstElementChild) e = e.firstElementChild;
                    return e
                }).append(this)), this)
            },
            wrapInner: function(e) {
                return this.each(n.isFunction(e) ? function(t) {
                    n(this).wrapInner(e.call(this, t))
                } : function() {
                    var t = n(this),
                        r = t.contents();
                    r.length ? r.wrapAll(e) : t.append(e)
                })
            },
            wrap: function(e) {
                var t = n.isFunction(e);
                return this.each(function(r) {
                    n(this).wrapAll(t ? e.call(this, r) : e)
                })
            },
            unwrap: function() {
                return this.parent().each(function() {
                    n.nodeName(this, "body") || n(this).replaceWith(this.childNodes)
                }).end()
            }
        }), n.expr.filters.hidden = function(e) {
            return e.offsetWidth <= 0 && e.offsetHeight <= 0
        }, n.expr.filters.visible = function(e) {
            return !n.expr.filters.hidden(e)
        };
        var vc = /%20/g,
            wc = /\[\]$/,
            xc = /\r?\n/g,
            yc = /^(?:submit|button|image|reset|file)$/i,
            zc = /^(?:input|select|textarea|keygen)/i;
        n.param = function(e, t) {
            var r, i = [],
                s = function(e, t) {
                    t = n.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
                };
            if (void 0 === t && (t = n.ajaxSettings && n.ajaxSettings.traditional), n.isArray(e) || e.jquery && !n.isPlainObject(e)) n.each(e, function() {
                s(this.name, this.value)
            });
            else
                for (r in e) Ac(r, e[r], t, s);
            return i.join("&").replace(vc, "+")
        }, n.fn.extend({
            serialize: function() {
                return n.param(this.serializeArray())
            },
            serializeArray: function() {
                return this.map(function() {
                    var e = n.prop(this, "elements");
                    return e ? n.makeArray(e) : this
                }).filter(function() {
                    var e = this.type;
                    return this.name && !n(this).is(":disabled") && zc.test(this.nodeName) && !yc.test(e) && (this.checked || !T.test(e))
                }).map(function(e, t) {
                    var r = n(this).val();
                    return null == r ? null : n.isArray(r) ? n.map(r, function(e) {
                        return {
                            name: t.name,
                            value: e.replace(xc, "\r\n")
                        }
                    }) : {
                        name: t.name,
                        value: r.replace(xc, "\r\n")
                    }
                }).get()
            }
        }), n.ajaxSettings.xhr = function() {
            try {
                return new XMLHttpRequest
            } catch (e) {}
        };
        var Bc = 0,
            Cc = {},
            Dc = {
                0: 200,
                1223: 204
            },
            Ec = n.ajaxSettings.xhr();
        a.attachEvent && a.attachEvent("onunload", function() {
            for (var e in Cc) Cc[e]()
        }), k.cors = !!Ec && "withCredentials" in Ec, k.ajax = Ec = !!Ec, n.ajaxTransport(function(e) {
            var t;
            return k.cors || Ec && !e.crossDomain ? {
                send: function(n, r) {
                    var i, s = e.xhr(),
                        o = ++Bc;
                    if (s.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                        for (i in e.xhrFields) s[i] = e.xhrFields[i];
                    e.mimeType && s.overrideMimeType && s.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                    for (i in n) s.setRequestHeader(i, n[i]);
                    t = function(e) {
                        return function() {
                            t && (delete Cc[o], t = s.onload = s.onerror = null, "abort" === e ? s.abort() : "error" === e ? r(s.status, s.statusText) : r(Dc[s.status] || s.status, s.statusText, "string" == typeof s.responseText ? {
                                text: s.responseText
                            } : void 0, s.getAllResponseHeaders()))
                        }
                    }, s.onload = t(), s.onerror = t("error"), t = Cc[o] = t("abort");
                    try {
                        s.send(e.hasContent && e.data || null)
                    } catch (u) {
                        if (t) throw u
                    }
                },
                abort: function() {
                    t && t()
                }
            } : void 0
        }), n.ajaxSetup({
            accepts: {
                script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
            },
            contents: {
                script: /(?:java|ecma)script/
            },
            converters: {
                "text script": function(e) {
                    return n.globalEval(e), e
                }
            }
        }), n.ajaxPrefilter("script", function(e) {
            void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
        }), n.ajaxTransport("script", function(e) {
            if (e.crossDomain) {
                var t, r;
                return {
                    send: function(i, s) {
                        t = n("<script>").prop({
                            async: !0,
                            charset: e.scriptCharset,
                            src: e.url
                        }).on("load error", r = function(e) {
                            t.remove(), r = null, e && s("error" === e.type ? 404 : 200, e.type)
                        }), l.head.appendChild(t[0])
                    },
                    abort: function() {
                        r && r()
                    }
                }
            }
        });
        var Fc = [],
            Gc = /(=)\?(?=&|$)|\?\?/;
        n.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function() {
                var e = Fc.pop() || n.expando + "_" + cc++;
                return this[e] = !0, e
            }
        }), n.ajaxPrefilter("json jsonp", function(e, t, r) {
            var i, s, o, u = e.jsonp !== !1 && (Gc.test(e.url) ? "url" : "string" == typeof e.data && !(e.contentType || "").indexOf("application/x-www-form-urlencoded") && Gc.test(e.data) && "data");
            return u || "jsonp" === e.dataTypes[0] ? (i = e.jsonpCallback = n.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, u ? e[u] = e[u].replace(Gc, "$1" + i) : e.jsonp !== !1 && (e.url += (dc.test(e.url) ? "&" : "?") + e.jsonp + "=" + i), e.converters["script json"] = function() {
                return o || n.error(i + " was not called"), o[0]
            }, e.dataTypes[0] = "json", s = a[i], a[i] = function() {
                o = arguments
            }, r.always(function() {
                a[i] = s, e[i] && (e.jsonpCallback = t.jsonpCallback, Fc.push(i)), o && n.isFunction(s) && s(o[0]), o = s = void 0
            }), "script") : void 0
        }), n.parseHTML = function(e, t, r) {
            if (!e || "string" != typeof e) return null;
            "boolean" == typeof t && (r = t, t = !1), t = t || l;
            var i = v.exec(e),
                s = !r && [];
            return i ? [t.createElement(i[1])] : (i = n.buildFragment([e], t, s), s && s.length && n(s).remove(), n.merge([], i.childNodes))
        };
        var Hc = n.fn.load;
        n.fn.load = function(e, t, r) {
            if ("string" != typeof e && Hc) return Hc.apply(this, arguments);
            var i, s, o, u = this,
                a = e.indexOf(" ");
            return a >= 0 && (i = n.trim(e.slice(a)), e = e.slice(0, a)), n.isFunction(t) ? (r = t, t = void 0) : t && "object" == typeof t && (s = "POST"), u.length > 0 && n.ajax({
                url: e,
                type: s,
                dataType: "html",
                data: t
            }).done(function(e) {
                o = arguments, u.html(i ? n("<div>").append(n.parseHTML(e)).find(i) : e)
            }).complete(r && function(e, t) {
                u.each(r, o || [e.responseText, t, e])
            }), this
        }, n.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
            n.fn[t] = function(e) {
                return this.on(t, e)
            }
        }), n.expr.filters.animated = function(e) {
            return n.grep(n.timers, function(t) {
                return e === t.elem
            }).length
        };
        var Ic = a.document.documentElement;
        n.offset = {
            setOffset: function(e, t, r) {
                var i, s, o, u, a, f, l, c = n.css(e, "position"),
                    h = n(e),
                    p = {};
                "static" === c && (e.style.position = "relative"), a = h.offset(), o = n.css(e, "top"), f = n.css(e, "left"), l = ("absolute" === c || "fixed" === c) && (o + f).indexOf("auto") > -1, l ? (i = h.position(), u = i.top, s = i.left) : (u = parseFloat(o) || 0, s = parseFloat(f) || 0), n.isFunction(t) && (t = t.call(e, r, a)), null != t.top && (p.top = t.top - a.top + u), null != t.left && (p.left = t.left - a.left + s), "using" in t ? t.using.call(e, p) : h.css(p)
            }
        }, n.fn.extend({
            offset: function(e) {
                if (arguments.length) return void 0 === e ? this : this.each(function(t) {
                    n.offset.setOffset(this, e, t)
                });
                var t, r, i = this[0],
                    s = {
                        top: 0,
                        left: 0
                    },
                    o = i && i.ownerDocument;
                if (o) return t = o.documentElement, n.contains(t, i) ? (typeof i.getBoundingClientRect !== U && (s = i.getBoundingClientRect()), r = Jc(o), {
                    top: s.top + r.pageYOffset - t.clientTop,
                    left: s.left + r.pageXOffset - t.clientLeft
                }) : s
            },
            position: function() {
                if (this[0]) {
                    var e, t, r = this[0],
                        i = {
                            top: 0,
                            left: 0
                        };
                    return "fixed" === n.css(r, "position") ? t = r.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), n.nodeName(e[0], "html") || (i = e.offset()), i.top += n.css(e[0], "borderTopWidth", !0), i.left += n.css(e[0], "borderLeftWidth", !0)), {
                        top: t.top - i.top - n.css(r, "marginTop", !0),
                        left: t.left - i.left - n.css(r, "marginLeft", !0)
                    }
                }
            },
            offsetParent: function() {
                return this.map(function() {
                    var e = this.offsetParent || Ic;
                    while (e && !n.nodeName(e, "html") && "static" === n.css(e, "position")) e = e.offsetParent;
                    return e || Ic
                })
            }
        }), n.each({
            scrollLeft: "pageXOffset",
            scrollTop: "pageYOffset"
        }, function(e, t) {
            var r = "pageYOffset" === t;
            n.fn[e] = function(n) {
                return J(this, function(e, n, i) {
                    var s = Jc(e);
                    return void 0 === i ? s ? s[t] : e[n] : void(s ? s.scrollTo(r ? a.pageXOffset : i, r ? i : a.pageYOffset) : e[n] = i)
                }, e, n, arguments.length, null)
            }
        }), n.each(["top", "left"], function(e, t) {
            n.cssHooks[t] = yb(k.pixelPosition, function(e, r) {
                return r ? (r = xb(e, t), vb.test(r) ? n(e).position()[t] + "px" : r) : void 0
            })
        }), n.each({
            Height: "height",
            Width: "width"
        }, function(e, t) {
            n.each({
                padding: "inner" + e,
                content: t,
                "": "outer" + e
            }, function(r, i) {
                n.fn[i] = function(i, s) {
                    var o = arguments.length && (r || "boolean" != typeof i),
                        u = r || (i === !0 || s === !0 ? "margin" : "border");
                    return J(this, function(t, r, i) {
                        var s;
                        return n.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (s = t.documentElement, Math.max(t.body["scroll" + e], s["scroll" + e], t.body["offset" + e], s["offset" + e], s["client" + e])) : void 0 === i ? n.css(t, r, u) : n.style(t, r, i, u)
                    }, t, o ? i : void 0, o, null)
                }
            })
        }), n.fn.size = function() {
            return this.length
        }, n.fn.andSelf = n.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
            return n
        });
        var Kc = a.jQuery,
            Lc = a.$;
        return n.noConflict = function(e) {
            return a.$ === n && (a.$ = Lc), e && a.jQuery === n && (a.jQuery = Kc), n
        }, typeof b === U && (a.jQuery = a.$ = n), n
    }),
    function() {
        function E(e) {
            function t(t, n, r, i, s, o) {
                for (; s >= 0 && s < o; s += e) {
                    var u = i ? i[s] : s;
                    r = n(r, t[u], u, t)
                }
                return r
            }
            return function(n, r, i, s) {
                r = v(r, s, 4);
                var o = !w(n) && d.keys(n),
                    u = (o || n).length,
                    a = e > 0 ? 0 : u - 1;
                return arguments.length < 3 && (i = n[o ? o[a] : a], a += e), t(n, r, i, o, a, u)
            }
        }

        function T(e) {
            return function(t, n, r) {
                n = m(n, r);
                var i = t != null && t.length,
                    s = e > 0 ? 0 : i - 1;
                for (; s >= 0 && s < i; s += e)
                    if (n(t[s], s, t)) return s;
                return -1
            }
        }

        function L(e, t) {
            var n = k.length,
                i = e.constructor,
                s = d.isFunction(i) && i.prototype || r,
                o = "constructor";
            d.has(e, o) && !d.contains(t, o) && t.push(o);
            while (n--) o = k[n], o in e && e[o] !== s[o] && !d.contains(t, o) && t.push(o)
        }
        var e = this,
            t = e._,
            n = Array.prototype,
            r = Object.prototype,
            i = Function.prototype,
            s = n.push,
            o = n.slice,
            u = r.toString,
            a = r.hasOwnProperty,
            f = Array.isArray,
            l = Object.keys,
            c = i.bind,
            h = Object.create,
            p = function() {},
            d = function(e) {
                if (e instanceof d) return e;
                if (!(this instanceof d)) return new d(e);
                this._wrapped = e
            };
        typeof exports != "undefined" ? (typeof module != "undefined" && module.exports && (exports = module.exports = d), exports._ = d) : e._ = d, d.VERSION = "1.8.2";
        var v = function(e, t, n) {
                if (t === void 0) return e;
                switch (n == null ? 3 : n) {
                    case 1:
                        return function(n) {
                            return e.call(t, n)
                        };
                    case 2:
                        return function(n, r) {
                            return e.call(t, n, r)
                        };
                    case 3:
                        return function(n, r, i) {
                            return e.call(t, n, r, i)
                        };
                    case 4:
                        return function(n, r, i, s) {
                            return e.call(t, n, r, i, s)
                        }
                }
                return function() {
                    return e.apply(t, arguments)
                }
            },
            m = function(e, t, n) {
                return e == null ? d.identity : d.isFunction(e) ? v(e, t, n) : d.isObject(e) ? d.matcher(e) : d.property(e)
            };
        d.iteratee = function(e, t) {
            return m(e, t, Infinity)
        };
        var g = function(e, t) {
                return function(n) {
                    var r = arguments.length;
                    if (r < 2 || n == null) return n;
                    for (var i = 1; i < r; i++) {
                        var s = arguments[i],
                            o = e(s),
                            u = o.length;
                        for (var a = 0; a < u; a++) {
                            var f = o[a];
                            if (!t || n[f] === void 0) n[f] = s[f]
                        }
                    }
                    return n
                }
            },
            y = function(e) {
                if (!d.isObject(e)) return {};
                if (h) return h(e);
                p.prototype = e;
                var t = new p;
                return p.prototype = null, t
            },
            b = Math.pow(2, 53) - 1,
            w = function(e) {
                var t = e && e.length;
                return typeof t == "number" && t >= 0 && t <= b
            };
        d.each = d.forEach = function(e, t, n) {
            t = v(t, n);
            var r, i;
            if (w(e))
                for (r = 0, i = e.length; r < i; r++) t(e[r], r, e);
            else {
                var s = d.keys(e);
                for (r = 0, i = s.length; r < i; r++) t(e[s[r]], s[r], e)
            }
            return e
        }, d.map = d.collect = function(e, t, n) {
            t = m(t, n);
            var r = !w(e) && d.keys(e),
                i = (r || e).length,
                s = Array(i);
            for (var o = 0; o < i; o++) {
                var u = r ? r[o] : o;
                s[o] = t(e[u], u, e)
            }
            return s
        }, d.reduce = d.foldl = d.inject = E(1), d.reduceRight = d.foldr = E(-1), d.find = d.detect = function(e, t, n) {
            var r;
            w(e) ? r = d.findIndex(e, t, n) : r = d.findKey(e, t, n);
            if (r !== void 0 && r !== -1) return e[r]
        }, d.filter = d.select = function(e, t, n) {
            var r = [];
            return t = m(t, n), d.each(e, function(e, n, i) {
                t(e, n, i) && r.push(e)
            }), r
        }, d.reject = function(e, t, n) {
            return d.filter(e, d.negate(m(t)), n)
        }, d.every = d.all = function(e, t, n) {
            t = m(t, n);
            var r = !w(e) && d.keys(e),
                i = (r || e).length;
            for (var s = 0; s < i; s++) {
                var o = r ? r[s] : s;
                if (!t(e[o], o, e)) return !1
            }
            return !0
        }, d.some = d.any = function(e, t, n) {
            t = m(t, n);
            var r = !w(e) && d.keys(e),
                i = (r || e).length;
            for (var s = 0; s < i; s++) {
                var o = r ? r[s] : s;
                if (t(e[o], o, e)) return !0
            }
            return !1
        }, d.contains = d.includes = d.include = function(e, t, n) {
            return w(e) || (e = d.values(e)), d.indexOf(e, t, typeof n == "number" && n) >= 0
        }, d.invoke = function(e, t) {
            var n = o.call(arguments, 2),
                r = d.isFunction(t);
            return d.map(e, function(e) {
                var i = r ? t : e[t];
                return i == null ? i : i.apply(e, n)
            })
        }, d.pluck = function(e, t) {
            return d.map(e, d.property(t))
        }, d.where = function(e, t) {
            return d.filter(e, d.matcher(t))
        }, d.findWhere = function(e, t) {
            return d.find(e, d.matcher(t))
        }, d.max = function(e, t, n) {
            var r = -Infinity,
                i = -Infinity,
                s, o;
            if (t == null && e != null) {
                e = w(e) ? e : d.values(e);
                for (var u = 0, a = e.length; u < a; u++) s = e[u], s > r && (r = s)
            } else t = m(t, n), d.each(e, function(e, n, s) {
                o = t(e, n, s);
                if (o > i || o === -Infinity && r === -Infinity) r = e, i = o
            });
            return r
        }, d.min = function(e, t, n) {
            var r = Infinity,
                i = Infinity,
                s, o;
            if (t == null && e != null) {
                e = w(e) ? e : d.values(e);
                for (var u = 0, a = e.length; u < a; u++) s = e[u], s < r && (r = s)
            } else t = m(t, n), d.each(e, function(e, n, s) {
                o = t(e, n, s);
                if (o < i || o === Infinity && r === Infinity) r = e, i = o
            });
            return r
        }, d.shuffle = function(e) {
            var t = w(e) ? e : d.values(e),
                n = t.length,
                r = Array(n);
            for (var i = 0, s; i < n; i++) s = d.random(0, i), s !== i && (r[i] = r[s]), r[s] = t[i];
            return r
        }, d.sample = function(e, t, n) {
            return t == null || n ? (w(e) || (e = d.values(e)), e[d.random(e.length - 1)]) : d.shuffle(e).slice(0, Math.max(0, t))
        }, d.sortBy = function(e, t, n) {
            return t = m(t, n), d.pluck(d.map(e, function(e, n, r) {
                return {
                    value: e,
                    index: n,
                    criteria: t(e, n, r)
                }
            }).sort(function(e, t) {
                var n = e.criteria,
                    r = t.criteria;
                if (n !== r) {
                    if (n > r || n === void 0) return 1;
                    if (n < r || r === void 0) return -1
                }
                return e.index - t.index
            }), "value")
        };
        var S = function(e) {
            return function(t, n, r) {
                var i = {};
                return n = m(n, r), d.each(t, function(r, s) {
                    var o = n(r, s, t);
                    e(i, r, o)
                }), i
            }
        };
        d.groupBy = S(function(e, t, n) {
            d.has(e, n) ? e[n].push(t) : e[n] = [t]
        }), d.indexBy = S(function(e, t, n) {
            e[n] = t
        }), d.countBy = S(function(e, t, n) {
            d.has(e, n) ? e[n]++ : e[n] = 1
        }), d.toArray = function(e) {
            return e ? d.isArray(e) ? o.call(e) : w(e) ? d.map(e, d.identity) : d.values(e) : []
        }, d.size = function(e) {
            return e == null ? 0 : w(e) ? e.length : d.keys(e).length
        }, d.partition = function(e, t, n) {
            t = m(t, n);
            var r = [],
                i = [];
            return d.each(e, function(e, n, s) {
                (t(e, n, s) ? r : i).push(e)
            }), [r, i]
        }, d.first = d.head = d.take = function(e, t, n) {
            return e == null ? void 0 : t == null || n ? e[0] : d.initial(e, e.length - t)
        }, d.initial = function(e, t, n) {
            return o.call(e, 0, Math.max(0, e.length - (t == null || n ? 1 : t)))
        }, d.last = function(e, t, n) {
            return e == null ? void 0 : t == null || n ? e[e.length - 1] : d.rest(e, Math.max(0, e.length - t))
        }, d.rest = d.tail = d.drop = function(e, t, n) {
            return o.call(e, t == null || n ? 1 : t)
        }, d.compact = function(e) {
            return d.filter(e, d.identity)
        };
        var x = function(e, t, n, r) {
            var i = [],
                s = 0;
            for (var o = r || 0, u = e && e.length; o < u; o++) {
                var a = e[o];
                if (w(a) && (d.isArray(a) || d.isArguments(a))) {
                    t || (a = x(a, t, n));
                    var f = 0,
                        l = a.length;
                    i.length += l;
                    while (f < l) i[s++] = a[f++]
                } else n || (i[s++] = a)
            }
            return i
        };
        d.flatten = function(e, t) {
            return x(e, t, !1)
        }, d.without = function(e) {
            return d.difference(e, o.call(arguments, 1))
        }, d.uniq = d.unique = function(e, t, n, r) {
            if (e == null) return [];
            d.isBoolean(t) || (r = n, n = t, t = !1), n != null && (n = m(n, r));
            var i = [],
                s = [];
            for (var o = 0, u = e.length; o < u; o++) {
                var a = e[o],
                    f = n ? n(a, o, e) : a;
                t ? ((!o || s !== f) && i.push(a), s = f) : n ? d.contains(s, f) || (s.push(f), i.push(a)) : d.contains(i, a) || i.push(a)
            }
            return i
        }, d.union = function() {
            return d.uniq(x(arguments, !0, !0))
        }, d.intersection = function(e) {
            if (e == null) return [];
            var t = [],
                n = arguments.length;
            for (var r = 0, i = e.length; r < i; r++) {
                var s = e[r];
                if (d.contains(t, s)) continue;
                for (var o = 1; o < n; o++)
                    if (!d.contains(arguments[o], s)) break;
                o === n && t.push(s)
            }
            return t
        }, d.difference = function(e) {
            var t = x(arguments, !0, !0, 1);
            return d.filter(e, function(e) {
                return !d.contains(t, e)
            })
        }, d.zip = function() {
            return d.unzip(arguments)
        }, d.unzip = function(e) {
            var t = e && d.max(e, "length").length || 0,
                n = Array(t);
            for (var r = 0; r < t; r++) n[r] = d.pluck(e, r);
            return n
        }, d.object = function(e, t) {
            var n = {};
            for (var r = 0, i = e && e.length; r < i; r++) t ? n[e[r]] = t[r] : n[e[r][0]] = e[r][1];
            return n
        }, d.indexOf = function(e, t, n) {
            var r = 0,
                i = e && e.length;
            if (typeof n == "number") r = n < 0 ? Math.max(0, i + n) : n;
            else if (n && i) return r = d.sortedIndex(e, t), e[r] === t ? r : -1;
            if (t !== t) return d.findIndex(o.call(e, r), d.isNaN);
            for (; r < i; r++)
                if (e[r] === t) return r;
            return -1
        }, d.lastIndexOf = function(e, t, n) {
            var r = e ? e.length : 0;
            typeof n == "number" && (r = n < 0 ? r + n + 1 : Math.min(r, n + 1));
            if (t !== t) return d.findLastIndex(o.call(e, 0, r), d.isNaN);
            while (--r >= 0)
                if (e[r] === t) return r;
            return -1
        }, d.findIndex = T(1), d.findLastIndex = T(-1), d.sortedIndex = function(e, t, n, r) {
            n = m(n, r, 1);
            var i = n(t),
                s = 0,
                o = e.length;
            while (s < o) {
                var u = Math.floor((s + o) / 2);
                n(e[u]) < i ? s = u + 1 : o = u
            }
            return s
        }, d.range = function(e, t, n) {
            arguments.length <= 1 && (t = e || 0, e = 0), n = n || 1;
            var r = Math.max(Math.ceil((t - e) / n), 0),
                i = Array(r);
            for (var s = 0; s < r; s++, e += n) i[s] = e;
            return i
        };
        var N = function(e, t, n, r, i) {
            if (r instanceof t) {
                var s = y(e.prototype),
                    o = e.apply(s, i);
                return d.isObject(o) ? o : s
            }
            return e.apply(n, i)
        };
        d.bind = function(e, t) {
            if (c && e.bind === c) return c.apply(e, o.call(arguments, 1));
            if (!d.isFunction(e)) throw new TypeError("Bind must be called on a function");
            var n = o.call(arguments, 2),
                r = function() {
                    return N(e, r, t, this, n.concat(o.call(arguments)))
                };
            return r
        }, d.partial = function(e) {
            var t = o.call(arguments, 1),
                n = function() {
                    var r = 0,
                        i = t.length,
                        s = Array(i);
                    for (var o = 0; o < i; o++) s[o] = t[o] === d ? arguments[r++] : t[o];
                    while (r < arguments.length) s.push(arguments[r++]);
                    return N(e, n, this, this, s)
                };
            return n
        }, d.bindAll = function(e) {
            var t, n = arguments.length,
                r;
            if (n <= 1) throw new Error("bindAll must be passed function names");
            for (t = 1; t < n; t++) r = arguments[t], e[r] = d.bind(e[r], e);
            return e
        }, d.memoize = function(e, t) {
            var n = function(r) {
                var i = n.cache,
                    s = "" + (t ? t.apply(this, arguments) : r);
                return d.has(i, s) || (i[s] = e.apply(this, arguments)), i[s]
            };
            return n.cache = {}, n
        }, d.delay = function(e, t) {
            var n = o.call(arguments, 2);
            return setTimeout(function() {
                return e.apply(null, n)
            }, t)
        }, d.defer = d.partial(d.delay, d, 1), d.throttle = function(e, t, n) {
            var r, i, s, o = null,
                u = 0;
            n || (n = {});
            var a = function() {
                u = n.leading === !1 ? 0 : d.now(), o = null, s = e.apply(r, i), o || (r = i = null)
            };
            return function() {
                var f = d.now();
                !u && n.leading === !1 && (u = f);
                var l = t - (f - u);
                return r = this, i = arguments, l <= 0 || l > t ? (o && (clearTimeout(o), o = null), u = f, s = e.apply(r, i), o || (r = i = null)) : !o && n.trailing !== !1 && (o = setTimeout(a, l)), s
            }
        }, d.debounce = function(e, t, n) {
            var r, i, s, o, u, a = function() {
                var f = d.now() - o;
                f < t && f >= 0 ? r = setTimeout(a, t - f) : (r = null, n || (u = e.apply(s, i), r || (s = i = null)))
            };
            return function() {
                s = this, i = arguments, o = d.now();
                var f = n && !r;
                return r || (r = setTimeout(a, t)), f && (u = e.apply(s, i), s = i = null), u
            }
        }, d.wrap = function(e, t) {
            return d.partial(t, e)
        }, d.negate = function(e) {
            return function() {
                return !e.apply(this, arguments)
            }
        }, d.compose = function() {
            var e = arguments,
                t = e.length - 1;
            return function() {
                var n = t,
                    r = e[t].apply(this, arguments);
                while (n--) r = e[n].call(this, r);
                return r
            }
        }, d.after = function(e, t) {
            return function() {
                if (--e < 1) return t.apply(this, arguments)
            }
        }, d.before = function(e, t) {
            var n;
            return function() {
                return --e > 0 && (n = t.apply(this, arguments)), e <= 1 && (t = null), n
            }
        }, d.once = d.partial(d.before, 2);
        var C = !{
                toString: null
            }.propertyIsEnumerable("toString"),
            k = ["valueOf", "isPrototypeOf", "toString", "propertyIsEnumerable", "hasOwnProperty", "toLocaleString"];
        d.keys = function(e) {
            if (!d.isObject(e)) return [];
            if (l) return l(e);
            var t = [];
            for (var n in e) d.has(e, n) && t.push(n);
            return C && L(e, t), t
        }, d.allKeys = function(e) {
            if (!d.isObject(e)) return [];
            var t = [];
            for (var n in e) t.push(n);
            return C && L(e, t), t
        }, d.values = function(e) {
            var t = d.keys(e),
                n = t.length,
                r = Array(n);
            for (var i = 0; i < n; i++) r[i] = e[t[i]];
            return r
        }, d.mapObject = function(e, t, n) {
            t = m(t, n);
            var r = d.keys(e),
                i = r.length,
                s = {},
                o;
            for (var u = 0; u < i; u++) o = r[u], s[o] = t(e[o], o, e);
            return s
        }, d.pairs = function(e) {
            var t = d.keys(e),
                n = t.length,
                r = Array(n);
            for (var i = 0; i < n; i++) r[i] = [t[i], e[t[i]]];
            return r
        }, d.invert = function(e) {
            var t = {},
                n = d.keys(e);
            for (var r = 0, i = n.length; r < i; r++) t[e[n[r]]] = n[r];
            return t
        }, d.functions = d.methods = function(e) {
            var t = [];
            for (var n in e) d.isFunction(e[n]) && t.push(n);
            return t.sort()
        }, d.extend = g(d.allKeys), d.extendOwn = d.assign = g(d.keys), d.findKey = function(e, t, n) {
            t = m(t, n);
            var r = d.keys(e),
                i;
            for (var s = 0, o = r.length; s < o; s++) {
                i = r[s];
                if (t(e[i], i, e)) return i
            }
        }, d.pick = function(e, t, n) {
            var r = {},
                i = e,
                s, o;
            if (i == null) return r;
            d.isFunction(t) ? (o = d.allKeys(i), s = v(t, n)) : (o = x(arguments, !1, !1, 1), s = function(e, t, n) {
                return t in n
            }, i = Object(i));
            for (var u = 0, a = o.length; u < a; u++) {
                var f = o[u],
                    l = i[f];
                s(l, f, i) && (r[f] = l)
            }
            return r
        }, d.omit = function(e, t, n) {
            if (d.isFunction(t)) t = d.negate(t);
            else {
                var r = d.map(x(arguments, !1, !1, 1), String);
                t = function(e, t) {
                    return !d.contains(r, t)
                }
            }
            return d.pick(e, t, n)
        }, d.defaults = g(d.allKeys, !0), d.clone = function(e) {
            return d.isObject(e) ? d.isArray(e) ? e.slice() : d.extend({}, e) : e
        }, d.tap = function(e, t) {
            return t(e), e
        }, d.isMatch = function(e, t) {
            var n = d.keys(t),
                r = n.length;
            if (e == null) return !r;
            var i = Object(e);
            for (var s = 0; s < r; s++) {
                var o = n[s];
                if (t[o] !== i[o] || !(o in i)) return !1
            }
            return !0
        };
        var A = function(e, t, n, r) {
            if (e === t) return e !== 0 || 1 / e === 1 / t;
            if (e == null || t == null) return e === t;
            e instanceof d && (e = e._wrapped), t instanceof d && (t = t._wrapped);
            var i = u.call(e);
            if (i !== u.call(t)) return !1;
            switch (i) {
                case "[object RegExp]":
                case "[object String]":
                    return "" + e == "" + t;
                case "[object Number]":
                    if (+e !== +e) return +t !== +t;
                    return +e === 0 ? 1 / +e === 1 / t : +e === +t;
                case "[object Date]":
                case "[object Boolean]":
                    return +e === +t
            }
            var s = i === "[object Array]";
            if (!s) {
                if (typeof e != "object" || typeof t != "object") return !1;
                var o = e.constructor,
                    a = t.constructor;
                if (o !== a && !(d.isFunction(o) && o instanceof o && d.isFunction(a) && a instanceof a) && "constructor" in e && "constructor" in t) return !1
            }
            n = n || [], r = r || [];
            var f = n.length;
            while (f--)
                if (n[f] === e) return r[f] === t;
            n.push(e), r.push(t);
            if (s) {
                f = e.length;
                if (f !== t.length) return !1;
                while (f--)
                    if (!A(e[f], t[f], n, r)) return !1
            } else {
                var l = d.keys(e),
                    c;
                f = l.length;
                if (d.keys(t).length !== f) return !1;
                while (f--) {
                    c = l[f];
                    if (!d.has(t, c) || !A(e[c], t[c], n, r)) return !1
                }
            }
            return n.pop(), r.pop(), !0
        };
        d.isEqual = function(e, t) {
            return A(e, t)
        }, d.isEmpty = function(e) {
            return e == null ? !0 : w(e) && (d.isArray(e) || d.isString(e) || d.isArguments(e)) ? e.length === 0 : d.keys(e).length === 0
        }, d.isElement = function(e) {
            return !!e && e.nodeType === 1
        }, d.isArray = f || function(e) {
            return u.call(e) === "[object Array]"
        }, d.isObject = function(e) {
            var t = typeof e;
            return t === "function" || t === "object" && !!e
        }, d.each(["Arguments", "Function", "String", "Number", "Date", "RegExp", "Error"], function(e) {
            d["is" + e] = function(t) {
                return u.call(t) === "[object " + e + "]"
            }
        }), d.isArguments(arguments) || (d.isArguments = function(e) {
            return d.has(e, "callee")
        }), typeof /./ != "function" && typeof Int8Array != "object" && (d.isFunction = function(e) {
            return typeof e == "function" || !1
        }), d.isFinite = function(e) {
            return isFinite(e) && !isNaN(parseFloat(e))
        }, d.isNaN = function(e) {
            return d.isNumber(e) && e !== +e
        }, d.isBoolean = function(e) {
            return e === !0 || e === !1 || u.call(e) === "[object Boolean]"
        }, d.isNull = function(e) {
            return e === null
        }, d.isUndefined = function(e) {
            return e === void 0
        }, d.has = function(e, t) {
            return e != null && a.call(e, t)
        }, d.noConflict = function() {
            return e._ = t, this
        }, d.identity = function(e) {
            return e
        }, d.constant = function(e) {
            return function() {
                return e
            }
        }, d.noop = function() {}, d.property = function(e) {
            return function(t) {
                return t == null ? void 0 : t[e]
            }
        }, d.propertyOf = function(e) {
            return e == null ? function() {} : function(t) {
                return e[t]
            }
        }, d.matcher = d.matches = function(e) {
            return e = d.extendOwn({}, e),
                function(t) {
                    return d.isMatch(t, e)
                }
        }, d.times = function(e, t, n) {
            var r = Array(Math.max(0, e));
            t = v(t, n, 1);
            for (var i = 0; i < e; i++) r[i] = t(i);
            return r
        }, d.random = function(e, t) {
            return t == null && (t = e, e = 0), e + Math.floor(Math.random() * (t - e + 1))
        }, d.now = Date.now || function() {
            return (new Date).getTime()
        };
        var O = {
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;",
                '"': "&quot;",
                "'": "&#x27;",
                "`": "&#x60;"
            },
            M = d.invert(O),
            _ = function(e) {
                var t = function(t) {
                        return e[t]
                    },
                    n = "(?:" + d.keys(e).join("|") + ")",
                    r = RegExp(n),
                    i = RegExp(n, "g");
                return function(e) {
                    return e = e == null ? "" : "" + e, r.test(e) ? e.replace(i, t) : e
                }
            };
        d.escape = _(O), d.unescape = _(M), d.result = function(e, t, n) {
            var r = e == null ? void 0 : e[t];
            return r === void 0 && (r = n), d.isFunction(r) ? r.call(e) : r
        };
        var D = 0;
        d.uniqueId = function(e) {
            var t = ++D + "";
            return e ? e + t : t
        }, d.templateSettings = {
            evaluate: /<%([\s\S]+?)%>/g,
            interpolate: /<%=([\s\S]+?)%>/g,
            escape: /<%-([\s\S]+?)%>/g
        };
        var P = /(.)^/,
            H = {
                "'": "'",
                "\\": "\\",
                "\r": "r",
                "\n": "n",
                "\u2028": "u2028",
                "\u2029": "u2029"
            },
            B = /\\|'|\r|\n|\u2028|\u2029/g,
            j = function(e) {
                return "\\" + H[e]
            };
        d.template = function(e, t, n) {
            !t && n && (t = n), t = d.defaults({}, t, d.templateSettings);
            var r = RegExp([(t.escape || P).source, (t.interpolate || P).source, (t.evaluate || P).source].join("|") + "|$", "g"),
                i = 0,
                s = "__p+='";
            e.replace(r, function(t, n, r, o, u) {
                return s += e.slice(i, u).replace(B, j), i = u + t.length, n ? s += "'+\n((__t=(" + n + "))==null?'':_.escape(__t))+\n'" : r ? s += "'+\n((__t=(" + r + "))==null?'':__t)+\n'" : o && (s += "';\n" + o + "\n__p+='"), t
            }), s += "';\n", t.variable || (s = "with(obj||{}){\n" + s + "}\n"), s = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + s + "return __p;\n";
            try {
                var o = new Function(t.variable || "obj", "_", s)
            } catch (u) {
                throw u.source = s, u
            }
            var a = function(e) {
                    return o.call(this, e, d)
                },
                f = t.variable || "obj";
            return a.source = "function(" + f + "){\n" + s + "}", a
        }, d.chain = function(e) {
            var t = d(e);
            return t._chain = !0, t
        };
        var F = function(e, t) {
            return e._chain ? d(t).chain() : t
        };
        d.mixin = function(e) {
            d.each(d.functions(e), function(t) {
                var n = d[t] = e[t];
                d.prototype[t] = function() {
                    var e = [this._wrapped];
                    return s.apply(e, arguments), F(this, n.apply(d, e))
                }
            })
        }, d.mixin(d), d.each(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(e) {
            var t = n[e];
            d.prototype[e] = function() {
                var n = this._wrapped;
                return t.apply(n, arguments), (e === "shift" || e === "splice") && n.length === 0 && delete n[0], F(this, n)
            }
        }), d.each(["concat", "join", "slice"], function(e) {
            var t = n[e];
            d.prototype[e] = function() {
                return F(this, t.apply(this._wrapped, arguments))
            }
        }), d.prototype.value = function() {
            return this._wrapped
        }, d.prototype.valueOf = d.prototype.toJSON = d.prototype.value, d.prototype.toString = function() {
            return "" + this._wrapped
        }, typeof define == "function" && define.amd && define("underscore", [], function() {
            return d
        })
    }.call(this),
    function(e) {
        var t = typeof self == "object" && self.self == self && self || typeof global == "object" && global.global == global && global;
        if (typeof define == "function" && define.amd) define("backbone", ["underscore", "jquery", "exports"], function(n, r, i) {
            t.Backbone = e(t, i, n, r)
        });
        else if (typeof exports != "undefined") {
            var n = require("underscore"),
                r;
            try {
                r = require("jquery")
            } catch (i) {}
            e(t, exports, n, r)
        } else t.Backbone = e(t, {}, t._, t.jQuery || t.Zepto || t.ender || t.$)
    }(function(e, t, n, r) {
        var i = e.Backbone,
            s = [],
            o = s.slice;
        t.VERSION = "1.2.0", t.$ = r, t.noConflict = function() {
            return e.Backbone = i, this
        }, t.emulateHTTP = !1, t.emulateJSON = !1;
        var u = t.Events = {},
            a = /\s+/,
            f = function(e, t, r, i, s) {
                var o = 0,
                    u;
                if (r && typeof r == "object")
                    for (u = n.keys(r); o < u.length; o++) t = e(t, u[o], r[u[o]], s);
                else if (r && a.test(r))
                    for (u = r.split(a); o < u.length; o++) t = e(t, u[o], i, s);
                else t = e(t, r, i, s);
                return t
            };
        u.on = function(e, t, n) {
            return l(this, e, t, n)
        };
        var l = function(e, t, n, r, i) {
            e._events = f(c, e._events || {}, t, n, {
                context: r,
                ctx: e,
                listening: i
            });
            if (i) {
                var s = e._listeners || (e._listeners = {});
                s[i.id] = i
            }
            return e
        };
        u.listenTo = function(e, t, r) {
            if (!e) return this;
            var i = e._listenId || (e._listenId = n.uniqueId("l")),
                s = this._listeningTo || (this._listeningTo = {}),
                o = s[i];
            if (!o) {
                var u = this._listenId || (this._listenId = n.uniqueId("l"));
                o = s[i] = {
                    obj: e,
                    objId: i,
                    id: u,
                    listeningTo: s,
                    count: 0
                }
            }
            return l(e, t, r, this, o), this
        };
        var c = function(e, t, n, r) {
            if (n) {
                var i = e[t] || (e[t] = []),
                    s = r.context,
                    o = r.ctx,
                    u = r.listening;
                u && u.count++, i.push({
                    callback: n,
                    context: s,
                    ctx: s || o,
                    listening: u
                })
            }
            return e
        };
        u.off = function(e, t, n) {
            return this._events ? (this._events = f(h, this._events, e, t, {
                context: n,
                listeners: this._listeners
            }), this) : this
        }, u.stopListening = function(e, t, r) {
            var i = this._listeningTo;
            if (!i) return this;
            var s = e ? [e._listenId] : n.keys(i);
            for (var o = 0; o < s.length; o++) {
                var u = i[s[o]];
                if (!u) break;
                u.obj.off(t, r, this)
            }
            return n.isEmpty(i) && (this._listeningTo = void 0), this
        };
        var h = function(e, t, r, i) {
            if (!e) return;
            var s = 0,
                o, u, a = i.context,
                f = i.listeners;
            if (!t && !r && !a) {
                var l = n.keys(f);
                for (; s < l.length; s++) u = f[l[s]], delete f[u.id], delete u.listeningTo[u.objId];
                return
            }
            var c = t ? [t] : n.keys(e);
            for (; s < c.length; s++) {
                t = c[s];
                var h = e[t];
                if (!h) break;
                var p = [];
                for (var d = 0; d < h.length; d++) {
                    var v = h[d];
                    r && r !== v.callback && r !== v.callback._callback || a && a !== v.context ? p.push(v) : (u = v.listening, u && --u.count === 0 && (delete f[u.id], delete u.listeningTo[u.objId]))
                }
                p.length ? e[t] = p : delete e[t]
            }
            if (n.size(e)) return e
        };
        u.once = function(e, t, r) {
            var i = f(p, {}, e, t, n.bind(this.off, this));
            return this.on(i, void 0, r)
        }, u.listenToOnce = function(e, t, r) {
            var i = f(p, {}, t, r, n.bind(this.stopListening, this, e));
            return this.listenTo(e, i)
        };
        var p = function(e, t, r, i) {
            if (r) {
                var s = e[t] = n.once(function() {
                    i(t, s), r.apply(this, arguments)
                });
                s._callback = r
            }
            return e
        };
        u.trigger = function(e) {
            if (!this._events) return this;
            var t = Math.max(0, arguments.length - 1),
                n = Array(t);
            for (var r = 0; r < t; r++) n[r] = arguments[r + 1];
            return f(d, this._events, e, void 0, n), this
        };
        var d = function(e, t, n, r) {
                if (e) {
                    var i = e[t],
                        s = e.all;
                    i && s && (s = s.slice()), i && v(i, r), s && v(s, [t].concat(r))
                }
                return e
            },
            v = function(e, t) {
                var n, r = -1,
                    i = e.length,
                    s = t[0],
                    o = t[1],
                    u = t[2];
                switch (t.length) {
                    case 0:
                        while (++r < i)(n = e[r]).callback.call(n.ctx);
                        return;
                    case 1:
                        while (++r < i)(n = e[r]).callback.call(n.ctx, s);
                        return;
                    case 2:
                        while (++r < i)(n = e[r]).callback.call(n.ctx, s, o);
                        return;
                    case 3:
                        while (++r < i)(n = e[r]).callback.call(n.ctx, s, o, u);
                        return;
                    default:
                        while (++r < i)(n = e[r]).callback.apply(n.ctx, t);
                        return
                }
            },
            m = function(e, t, r) {
                switch (e) {
                    case 1:
                        return function() {
                            return n[t](this[r])
                        };
                    case 2:
                        return function(e) {
                            return n[t](this[r], e)
                        };
                    case 3:
                        return function(e, i) {
                            return n[t](this[r], e, i)
                        };
                    case 4:
                        return function(e, i, s) {
                            return n[t](this[r], e, i, s)
                        };
                    default:
                        return function() {
                            var e = o.call(arguments);
                            return e.unshift(this[r]), n[t].apply(n, e)
                        }
                }
            },
            g = function(e, t, r) {
                n.each(t, function(t, i) {
                    n[i] && (e.prototype[i] = m(t, i, r))
                })
            };
        u.bind = u.on, u.unbind = u.off, n.extend(t, u);
        var y = t.Model = function(e, t) {
            var r = e || {};
            t || (t = {}), this.cid = n.uniqueId(this.cidPrefix), this.attributes = {}, t.collection && (this.collection = t.collection), t.parse && (r = this.parse(r, t) || {}), r = n.defaults({}, r, n.result(this, "defaults")), this.set(r, t), this.changed = {}, this.initialize.apply(this, arguments)
        };
        n.extend(y.prototype, u, {
            changed: null,
            validationError: null,
            idAttribute: "id",
            cidPrefix: "c",
            initialize: function() {},
            toJSON: function(e) {
                return n.clone(this.attributes)
            },
            sync: function() {
                return t.sync.apply(this, arguments)
            },
            get: function(e) {
                return this.attributes[e]
            },
            escape: function(e) {
                return n.escape(this.get(e))
            },
            has: function(e) {
                return this.get(e) != null
            },
            matches: function(e) {
                return !!n.iteratee(e, this)(this.attributes)
            },
            set: function(e, t, r) {
                var i, s, o, u, a, f, l, c;
                if (e == null) return this;
                typeof e == "object" ? (s = e, r = t) : (s = {})[e] = t, r || (r = {});
                if (!this._validate(s, r)) return !1;
                o = r.unset, a = r.silent, u = [], f = this._changing, this._changing = !0, f || (this._previousAttributes = n.clone(this.attributes), this.changed = {}), c = this.attributes, l = this._previousAttributes, this.idAttribute in s && (this.id = s[this.idAttribute]);
                for (i in s) t = s[i], n.isEqual(c[i], t) || u.push(i), n.isEqual(l[i], t) ? delete this.changed[i] : this.changed[i] = t, o ? delete c[i] : c[i] = t;
                if (!a) {
                    u.length && (this._pending = r);
                    for (var h = 0; h < u.length; h++) this.trigger("change:" + u[h], this, c[u[h]], r)
                }
                if (f) return this;
                if (!a)
                    while (this._pending) r = this._pending, this._pending = !1, this.trigger("change", this, r);
                return this._pending = !1, this._changing = !1, this
            },
            unset: function(e, t) {
                return this.set(e, void 0, n.extend({}, t, {
                    unset: !0
                }))
            },
            clear: function(e) {
                var t = {};
                for (var r in this.attributes) t[r] = void 0;
                return this.set(t, n.extend({}, e, {
                    unset: !0
                }))
            },
            hasChanged: function(e) {
                return e == null ? !n.isEmpty(this.changed) : n.has(this.changed, e)
            },
            changedAttributes: function(e) {
                if (!e) return this.hasChanged() ? n.clone(this.changed) : !1;
                var t, r = !1,
                    i = this._changing ? this._previousAttributes : this.attributes;
                for (var s in e) {
                    if (n.isEqual(i[s], t = e[s])) continue;
                    (r || (r = {}))[s] = t
                }
                return r
            },
            previous: function(e) {
                return e == null || !this._previousAttributes ? null : this._previousAttributes[e]
            },
            previousAttributes: function() {
                return n.clone(this._previousAttributes)
            },
            fetch: function(e) {
                e = e ? n.clone(e) : {}, e.parse === void 0 && (e.parse = !0);
                var t = this,
                    r = e.success;
                return e.success = function(n) {
                    if (!t.set(t.parse(n, e), e)) return !1;
                    r && r.call(e.context, t, n, e), t.trigger("sync", t, n, e)
                }, q(this, e), this.sync("read", this, e)
            },
            save: function(e, t, r) {
                var i, s, o, u = this.attributes,
                    a;
                e == null || typeof e == "object" ? (i = e, r = t) : (i = {})[e] = t, r = n.extend({
                    validate: !0
                }, r), a = r.wait;
                if (i && !a) {
                    if (!this.set(i, r)) return !1
                } else if (!this._validate(i, r)) return !1;
                i && a && (this.attributes = n.extend({}, u, i)), r.parse === void 0 && (r.parse = !0);
                var f = this,
                    l = r.success;
                return r.success = function(e) {
                    f.attributes = u;
                    var t = r.parse ? f.parse(e, r) : e;
                    a && (t = n.extend(i || {}, t));
                    if (n.isObject(t) && !f.set(t, r)) return !1;
                    l && l.call(r.context, f, e, r), f.trigger("sync", f, e, r)
                }, q(this, r), s = this.isNew() ? "create" : r.patch ? "patch" : "update", s === "patch" && !r.attrs && (r.attrs = i), o = this.sync(s, this, r), i && a && (this.attributes = u), o
            },
            destroy: function(e) {
                e = e ? n.clone(e) : {};
                var t = this,
                    r = e.success,
                    i = e.wait,
                    s = function() {
                        t.stopListening(), t.trigger("destroy", t, t.collection, e)
                    };
                e.success = function(n) {
                    i && s(), r && r.call(e.context, t, n, e), t.isNew() || t.trigger("sync", t, n, e)
                };
                var o = !1;
                return this.isNew() ? n.defer(e.success) : (q(this, e), o = this.sync("delete", this, e)), i || s(), o
            },
            url: function() {
                var e = n.result(this, "urlRoot") || n.result(this.collection, "url") || I();
                if (this.isNew()) return e;
                var t = this.id || this.attributes[this.idAttribute];
                return e.replace(/([^\/])$/, "$1/") + encodeURIComponent(t)
            },
            parse: function(e, t) {
                return e
            },
            clone: function() {
                return new this.constructor(this.attributes)
            },
            isNew: function() {
                return !this.has(this.idAttribute)
            },
            isValid: function(e) {
                return this._validate({}, n.extend(e || {}, {
                    validate: !0
                }))
            },
            _validate: function(e, t) {
                if (!t.validate || !this.validate) return !0;
                e = n.extend({}, this.attributes, e);
                var r = this.validationError = this.validate(e, t) || null;
                return r ? (this.trigger("invalid", this, r, n.extend(t, {
                    validationError: r
                })), !1) : !0
            }
        });
        var b = {
            keys: 1,
            values: 1,
            pairs: 1,
            invert: 1,
            pick: 0,
            omit: 0,
            chain: 1,
            isEmpty: 1
        };
        g(y, b, "attributes");
        var w = t.Collection = function(e, t) {
                t || (t = {}), t.model && (this.model = t.model), t.comparator !== void 0 && (this.comparator = t.comparator), this._reset(), this.initialize.apply(this, arguments), e && this.reset(e, n.extend({
                    silent: !0
                }, t))
            },
            E = {
                add: !0,
                remove: !0,
                merge: !0
            },
            S = {
                add: !0,
                remove: !1
            };
        n.extend(w.prototype, u, {
            model: y,
            initialize: function() {},
            toJSON: function(e) {
                return this.map(function(t) {
                    return t.toJSON(e)
                })
            },
            sync: function() {
                return t.sync.apply(this, arguments)
            },
            add: function(e, t) {
                return this.set(e, n.extend({
                    merge: !1
                }, t, S))
            },
            remove: function(e, t) {
                var r = !n.isArray(e),
                    i;
                return e = r ? [e] : n.clone(e), t || (t = {}), i = this._removeModels(e, t), !t.silent && i && this.trigger("update", this, t), r ? e[0] : e
            },
            set: function(e, t) {
                t = n.defaults({}, t, E), t.parse && (e = this.parse(e, t));
                var r = !n.isArray(e);
                e = r ? e ? [e] : [] : e.slice();
                var i, s, o, u, a, f = t.at;
                f != null && (f = +f), f < 0 && (f += this.length + 1);
                var l = this.comparator && f == null && t.sort !== !1,
                    c = n.isString(this.comparator) ? this.comparator : null,
                    h = [],
                    p = [],
                    d = {},
                    v = t.add,
                    m = t.merge,
                    g = t.remove,
                    y = !l && v && g ? [] : !1,
                    b = !1;
                for (var w = 0; w < e.length; w++) {
                    o = e[w];
                    if (u = this.get(o)) g && (d[u.cid] = !0), m && o !== u && (o = this._isModel(o) ? o.attributes : o, t.parse && (o = u.parse(o, t)), u.set(o, t), l && !a && u.hasChanged(c) && (a = !0)), e[w] = u;
                    else if (v) {
                        s = e[w] = this._prepareModel(o, t);
                        if (!s) continue;
                        h.push(s), this._addReference(s, t)
                    }
                    s = u || s;
                    if (!s) continue;
                    i = this.modelId(s.attributes), y && (s.isNew() || !d[i]) && (y.push(s), b = b || !this.models[w] || s.cid !== this.models[w].cid), d[i] = !0
                }
                if (g) {
                    for (var w = 0; w < this.length; w++) d[(s = this.models[w]).cid] || p.push(s);
                    p.length && this._removeModels(p, t)
                }
                if (h.length || b) {
                    l && (a = !0), this.length += h.length;
                    if (f != null)
                        for (var w = 0; w < h.length; w++) this.models.splice(f + w, 0, h[w]);
                    else {
                        y && (this.models.length = 0);
                        var S = y || h;
                        for (var w = 0; w < S.length; w++) this.models.push(S[w])
                    }
                }
                a && this.sort({
                    silent: !0
                });
                if (!t.silent) {
                    var x = f != null ? n.clone(t) : t;
                    for (var w = 0; w < h.length; w++) f != null && (x.index = f + w), (s = h[w]).trigger("add", s, this, x);
                    (a || b) && this.trigger("sort", this, t), (h.length || p.length) && this.trigger("update", this, t)
                }
                return r ? e[0] : e
            },
            reset: function(e, t) {
                t = t ? n.clone(t) : {};
                for (var r = 0; r < this.models.length; r++) this._removeReference(this.models[r], t);
                return t.previousModels = this.models, this._reset(), e = this.add(e, n.extend({
                    silent: !0
                }, t)), t.silent || this.trigger("reset", this, t), e
            },
            push: function(e, t) {
                return this.add(e, n.extend({
                    at: this.length
                }, t))
            },
            pop: function(e) {
                var t = this.at(this.length - 1);
                return this.remove(t, e), t
            },
            unshift: function(e, t) {
                return this.add(e, n.extend({
                    at: 0
                }, t))
            },
            shift: function(e) {
                var t = this.at(0);
                return this.remove(t, e), t
            },
            slice: function() {
                return o.apply(this.models, arguments)
            },
            get: function(e) {
                if (e == null) return void 0;
                var t = this.modelId(this._isModel(e) ? e.attributes : e);
                return this._byId[e] || this._byId[t] || this._byId[e.cid]
            },
            at: function(e) {
                return e < 0 && (e += this.length), this.models[e]
            },
            where: function(e, t) {
                var r = n.matches(e);
                return this[t ? "find" : "filter"](function(e) {
                    return r(e.attributes)
                })
            },
            findWhere: function(e) {
                return this.where(e, !0)
            },
            sort: function(e) {
                if (!this.comparator) throw new Error("Cannot sort a set without a comparator");
                return e || (e = {}), n.isString(this.comparator) || this.comparator.length === 1 ? this.models = this.sortBy(this.comparator, this) : this.models.sort(n.bind(this.comparator, this)), e.silent || this.trigger("sort", this, e), this
            },
            pluck: function(e) {
                return n.invoke(this.models, "get", e)
            },
            fetch: function(e) {
                e = e ? n.clone(e) : {}, e.parse === void 0 && (e.parse = !0);
                var t = e.success,
                    r = this;
                return e.success = function(n) {
                    var i = e.reset ? "reset" : "set";
                    r[i](n, e), t && t.call(e.context, r, n, e), r.trigger("sync", r, n, e)
                }, q(this, e), this.sync("read", this, e)
            },
            create: function(e, t) {
                t = t ? n.clone(t) : {};
                var r = t.wait;
                if (!(e = this._prepareModel(e, t))) return !1;
                r || this.add(e, t);
                var i = this,
                    s = t.success;
                return t.success = function(e, t, n) {
                    r && i.add(e, n), s && s.call(n.context, e, t, n)
                }, e.save(null, t), e
            },
            parse: function(e, t) {
                return e
            },
            clone: function() {
                return new this.constructor(this.models, {
                    model: this.model,
                    comparator: this.comparator
                })
            },
            modelId: function(e) {
                return e[this.model.prototype.idAttribute || "id"]
            },
            _reset: function() {
                this.length = 0, this.models = [], this._byId = {}
            },
            _prepareModel: function(e, t) {
                if (this._isModel(e)) return e.collection || (e.collection = this), e;
                t = t ? n.clone(t) : {}, t.collection = this;
                var r = new this.model(e, t);
                return r.validationError ? (this.trigger("invalid", this, r.validationError, t), !1) : r
            },
            _removeModels: function(e, t) {
                var n, r, i, s, o = !1;
                for (var n = 0, u = 0; n < e.length; n++) {
                    var s = e[n] = this.get(e[n]);
                    if (!s) continue;
                    var a = this.modelId(s.attributes);
                    a != null && delete this._byId[a], delete this._byId[s.cid];
                    var i = this.indexOf(s);
                    this.models.splice(i, 1), this.length--, t.silent || (t.index = i, s.trigger("remove", s, this, t)), e[u++] = s, this._removeReference(s, t), o = !0
                }
                return e.length !== u && (e = e.slice(0, u)), o
            },
            _isModel: function(e) {
                return e instanceof y
            },
            _addReference: function(e, t) {
                this._byId[e.cid] = e;
                var n = this.modelId(e.attributes);
                n != null && (this._byId[n] = e), e.on("all", this._onModelEvent, this)
            },
            _removeReference: function(e, t) {
                this === e.collection && delete e.collection, e.off("all", this._onModelEvent, this)
            },
            _onModelEvent: function(e, t, n, r) {
                if ((e === "add" || e === "remove") && n !== this) return;
                e === "destroy" && this.remove(t, r);
                if (e === "change") {
                    var i = this.modelId(t.previousAttributes()),
                        s = this.modelId(t.attributes);
                    i !== s && (i != null && delete this._byId[i], s != null && (this._byId[s] = t))
                }
                this.trigger.apply(this, arguments)
            }
        });
        var x = {
            forEach: 3,
            each: 3,
            map: 3,
            collect: 3,
            reduce: 4,
            foldl: 4,
            inject: 4,
            reduceRight: 4,
            foldr: 4,
            find: 3,
            detect: 3,
            filter: 3,
            select: 3,
            reject: 3,
            every: 3,
            all: 3,
            some: 3,
            any: 3,
            include: 2,
            contains: 2,
            invoke: 2,
            max: 3,
            min: 3,
            toArray: 1,
            size: 1,
            first: 3,
            head: 3,
            take: 3,
            initial: 3,
            rest: 3,
            tail: 3,
            drop: 3,
            last: 3,
            without: 0,
            difference: 0,
            indexOf: 3,
            shuffle: 1,
            lastIndexOf: 3,
            isEmpty: 1,
            chain: 1,
            sample: 3,
            partition: 3
        };
        g(w, x, "models");
        var T = ["groupBy", "countBy", "sortBy", "indexBy"];
        n.each(T, function(e) {
            if (!n[e]) return;
            w.prototype[e] = function(t, r) {
                var i = n.isFunction(t) ? t : function(e) {
                    return e.get(t)
                };
                return n[e](this.models, i, r)
            }
        });
        var N = t.View = function(e) {
                this.cid = n.uniqueId("views"), e || (e = {}), n.extend(this, n.pick(e, k)), this._ensureElement(), this.initialize.apply(this, arguments)
            },
            C = /^(\S+)\s*(.*)$/,
            k = ["model", "collection", "el", "id", "attributes", "className", "tagName", "events"];
        n.extend(N.prototype, u, {
            tagName: "div",
            $: function(e) {
                return this.$el.find(e)
            },
            initialize: function() {},
            render: function() {
                return this
            },
            remove: function() {
                return this._removeElement(), this.stopListening(), this
            },
            _removeElement: function() {
                this.$el.remove()
            },
            setElement: function(e) {
                return this.undelegateEvents(), this._setElement(e), this.delegateEvents(), this
            },
            _setElement: function(e) {
                this.$el = e instanceof t.$ ? e : t.$(e), this.el = this.$el[0]
            },
            delegateEvents: function(e) {
                if (!e && !(e = n.result(this, "events"))) return this;
                this.undelegateEvents();
                for (var t in e) {
                    var r = e[t];
                    n.isFunction(r) || (r = this[e[t]]);
                    if (!r) continue;
                    var i = t.match(C);
                    this.delegate(i[1], i[2], n.bind(r, this))
                }
                return this
            },
            delegate: function(e, t, n) {
                this.$el.on(e + ".delegateEvents" + this.cid, t, n)
            },
            undelegateEvents: function() {
                return this.$el && this.$el.off(".delegateEvents" + this.cid), this
            },
            undelegate: function(e, t, n) {
                this.$el.off(e + ".delegateEvents" + this.cid, t, n)
            },
            _createElement: function(e) {
                return document.createElement(e)
            },
            _ensureElement: function() {
                if (!this.el) {
                    var e = n.extend({}, n.result(this, "attributes"));
                    this.id && (e.id = n.result(this, "id")), this.className && (e["class"] = n.result(this, "className")), this.setElement(this._createElement(n.result(this, "tagName"))), this._setAttributes(e)
                } else this.setElement(n.result(this, "el"))
            },
            _setAttributes: function(e) {
                this.$el.attr(e)
            }
        }), t.sync = function(e, r, i) {
            var s = L[e];
            n.defaults(i || (i = {}), {
                emulateHTTP: t.emulateHTTP,
                emulateJSON: t.emulateJSON
            });
            var o = {
                type: s,
                dataType: "json"
            };
            i.url || (o.url = n.result(r, "url") || I()), i.data == null && r && (e === "create" || e === "update" || e === "patch") && (o.contentType = "application/json", o.data = JSON.stringify(i.attrs || r.toJSON(i))), i.emulateJSON && (o.contentType = "application/x-www-form-urlencoded", o.data = o.data ? {
                model: o.data
            } : {});
            if (i.emulateHTTP && (s === "PUT" || s === "DELETE" || s === "PATCH")) {
                o.type = "POST", i.emulateJSON && (o.data._method = s);
                var u = i.beforeSend;
                i.beforeSend = function(e) {
                    e.setRequestHeader("X-HTTP-Method-Override", s);
                    if (u) return u.apply(this, arguments)
                }
            }
            o.type !== "GET" && !i.emulateJSON && (o.processData = !1);
            var a = i.error;
            i.error = function(e, t, n) {
                i.textStatus = t, i.errorThrown = n, a && a.call(i.context, e, t, n)
            };
            var f = i.xhr = t.ajax(n.extend(o, i));
            return r.trigger("request", r, f, i), f
        };
        var L = {
            create: "POST",
            update: "PUT",
            patch: "PATCH",
            "delete": "DELETE",
            read: "GET"
        };
        t.ajax = function() {
            return t.$.ajax.apply(t.$, arguments)
        };
        var A = t.Router = function(e) {
                e || (e = {}), e.routes && (this.routes = e.routes), this._bindRoutes(), this.initialize.apply(this, arguments)
            },
            O = /\((.*?)\)/g,
            M = /(\(\?)?:\w+/g,
            _ = /\*\w+/g,
            D = /[\-{}\[\]+?.,\\\^$|#\s]/g;
        n.extend(A.prototype, u, {
            initialize: function() {},
            route: function(e, r, i) {
                n.isRegExp(e) || (e = this._routeToRegExp(e)), n.isFunction(r) && (i = r, r = ""), i || (i = this[r]);
                var s = this;
                return t.history.route(e, function(n) {
                    var o = s._extractParameters(e, n);
                    s.execute(i, o, r) !== !1 && (s.trigger.apply(s, ["route:" + r].concat(o)), s.trigger("route", r, o), t.history.trigger("route", s, r, o))
                }), this
            },
            execute: function(e, t, n) {
                e && e.apply(this, t)
            },
            navigate: function(e, n) {
                return t.history.navigate(e, n), this
            },
            _bindRoutes: function() {
                if (!this.routes) return;
                this.routes = n.result(this, "routes");
                var e, t = n.keys(this.routes);
                while ((e = t.pop()) != null) this.route(e, this.routes[e])
            },
            _routeToRegExp: function(e) {
                return e = e.replace(D, "\\$&").replace(O, "(?:$1)?").replace(M, function(e, t) {
                    return t ? e : "([^/?]+)"
                }).replace(_, "([^?]*?)"), new RegExp("^" + e + "(?:\\?([\\s\\S]*))?$")
            },
            _extractParameters: function(e, t) {
                var r = e.exec(t).slice(1);
                return n.map(r, function(e, t) {
                    return t === r.length - 1 ? e || null : e ? decodeURIComponent(e) : null
                })
            }
        });
        var P = t.History = function() {
                this.handlers = [], n.bindAll(this, "checkUrl"), typeof window != "undefined" && (this.location = window.location, this.history = window.history)
            },
            H = /^[#\/]|\s+$/g,
            B = /^\/+|\/+$/g,
            j = /#.*$/;
        P.started = !1, n.extend(P.prototype, u, {
            interval: 50,
            atRoot: function() {
                var e = this.location.pathname.replace(/[^\/]$/, "$&/");
                return e === this.root && !this.getSearch()
            },
            matchRoot: function() {
                var e = this.decodeFragment(this.location.pathname),
                    t = e.slice(0, this.root.length - 1) + "/";
                return t === this.root
            },
            decodeFragment: function(e) {
                return decodeURI(e.replace(/%25/g, "%2525"))
            },
            getSearch: function() {
                var e = this.location.href.replace(/#.*/, "").match(/\?.+/);
                return e ? e[0] : ""
            },
            getHash: function(e) {
                var t = (e || this).location.href.match(/#(.*)$/);
                return t ? t[1] : ""
            },
            getPath: function() {
                var e = this.decodeFragment(this.location.pathname + this.getSearch()).slice(this.root.length - 1);
                return e.charAt(0) === "/" ? e.slice(1) : e
            },
            getFragment: function(e) {
                return e == null && (this._usePushState || !this._wantsHashChange ? e = this.getPath() : e = this.getHash()), e.replace(H, "")
            },
            start: function(e) {
                if (P.started) throw new Error("Backbone.history has already been started");
                P.started = !0, this.options = n.extend({
                    root: "/"
                }, this.options, e), this.root = this.options.root, this._wantsHashChange = this.options.hashChange !== !1, this._hasHashChange = "onhashchange" in window, this._useHashChange = this._wantsHashChange && this._hasHashChange, this._wantsPushState = !!this.options.pushState, this._hasPushState = !!this.history && !!this.history.pushState, this._usePushState = this._wantsPushState && this._hasPushState, this.fragment = this.getFragment(), this.root = ("/" + this.root + "/").replace(B, "/");
                if (this._wantsHashChange && this._wantsPushState) {
                    if (!this._hasPushState && !this.atRoot()) {
                        var t = this.root.slice(0, -1) || "/";
                        return this.location.replace(t + "#" + this.getPath()), !0
                    }
                    this._hasPushState && this.atRoot() && this.navigate(this.getHash(), {
                        replace: !0
                    })
                }
                if (!this._hasHashChange && this._wantsHashChange && !this._usePushState) {
                    var r = document.createElement("iframe");
                    r.src = "javascript:0", r.style.display = "none", r.tabIndex = -1;
                    var i = document.body;
                    this.iframe = i.insertBefore(r, i.firstChild).contentWindow, this.iframe.document.open().close(), this.iframe.location.hash = "#" + this.fragment
                }
                var s = window.addEventListener || function(e, t) {
                    return attachEvent("on" + e, t)
                };
                this._usePushState ? s("popstate", this.checkUrl, !1) : this._useHashChange && !this.iframe ? s("hashchange", this.checkUrl, !1) : this._wantsHashChange && (this._checkUrlInterval = setInterval(this.checkUrl, this.interval));
                if (!this.options.silent) return this.loadUrl()
            },
            stop: function() {
                var e = window.removeEventListener || function(e, t) {
                    return detachEvent("on" + e, t)
                };
                this._usePushState ? e("popstate", this.checkUrl, !1) : this._useHashChange && !this.iframe && e("hashchange", this.checkUrl, !1), this.iframe && (document.body.removeChild(this.iframe.frameElement), this.iframe = null), this._checkUrlInterval && clearInterval(this._checkUrlInterval), P.started = !1
            },
            route: function(e, t) {
                this.handlers.unshift({
                    route: e,
                    callback: t
                })
            },
            checkUrl: function(e) {
                var t = this.getFragment();
                t === this.fragment && this.iframe && (t = this.getHash(this.iframe));
                if (t === this.fragment) return !1;
                this.iframe && this.navigate(t), this.loadUrl()
            },
            loadUrl: function(e) {
                return this.matchRoot() ? (e = this.fragment = this.getFragment(e), n.any(this.handlers, function(t) {
                    if (t.route.test(e)) return t.callback(e), !0
                })) : !1
            },
            navigate: function(e, t) {
                if (!P.started) return !1;
                if (!t || t === !0) t = {
                    trigger: !!t
                };
                e = this.getFragment(e || "");
                var n = this.root;
                if (e === "" || e.charAt(0) === "?") n = n.slice(0, -1) || "/";
                var r = n + e;
                e = this.decodeFragment(e.replace(j, ""));
                if (this.fragment === e) return;
                this.fragment = e;
                if (this._usePushState) this.history[t.replace ? "replaceState" : "pushState"]({}, document.title, r);
                else {
                    if (!this._wantsHashChange) return this.location.assign(r);
                    this._updateHash(this.location, e, t.replace), this.iframe && e !== this.getHash(this.iframe) && (t.replace || this.iframe.document.open().close(), this._updateHash(this.iframe.location, e, t.replace))
                }
                if (t.trigger) return this.loadUrl(e)
            },
            _updateHash: function(e, t, n) {
                if (n) {
                    var r = e.href.replace(/(javascript:|#).*$/, "");
                    e.replace(r + "#" + t)
                } else e.hash = "#" + t
            }
        }), t.history = new P;
        var F = function(e, t) {
            var r = this,
                i;
            e && n.has(e, "constructor") ? i = e.constructor : i = function() {
                return r.apply(this, arguments)
            }, n.extend(i, r, t);
            var s = function() {
                this.constructor = i
            };
            return s.prototype = r.prototype, i.prototype = new s, e && n.extend(i.prototype, e), i.__super__ = r.prototype, i
        };
        y.extend = w.extend = A.extend = N.extend = P.extend = F;
        var I = function() {
                throw new Error('A "url" property or function must be specified')
            },
            q = function(e, t) {
                var n = t.error;
                t.error = function(r) {
                    n && n.call(t.context, e, r, t), e.trigger("error", e, r, t)
                }
            };
        return t
    }),
    function(e, t) {
        if (typeof define == "function" && define.amd) define("backbone.wreqr", ["backbone", "underscore"], function(e, n) {
            return t(e, n)
        });
        else if (typeof exports != "undefined") {
            var n = require("backbone"),
                r = require("underscore");
            module.exports = t(n, r)
        } else t(e.Backbone, e._)
    }(this, function(e, t) {
        "use strict";
        var n = e.Wreqr,
            r = e.Wreqr = {};
        return e.Wreqr.VERSION = "1.3.2", e.Wreqr.noConflict = function() {
            return e.Wreqr = n, this
        }, r.Handlers = function(e, t) {
            var n = function(e) {
                this.options = e, this._wreqrHandlers = {}, t.isFunction(this.initialize) && this.initialize(e)
            };
            return n.extend = e.Model.extend, t.extend(n.prototype, e.Events, {
                setHandlers: function(e) {
                    t.each(e, function(e, n) {
                        var r = null;
                        t.isObject(e) && !t.isFunction(e) && (r = e.context, e = e.callback), this.setHandler(n, e, r)
                    }, this)
                },
                setHandler: function(e, t, n) {
                    var r = {
                        callback: t,
                        context: n
                    };
                    this._wreqrHandlers[e] = r, this.trigger("handler:add", e, t, n)
                },
                hasHandler: function(e) {
                    return !!this._wreqrHandlers[e]
                },
                getHandler: function(e) {
                    var t = this._wreqrHandlers[e];
                    if (!t) return;
                    return function() {
                        return t.callback.apply(t.context, arguments)
                    }
                },
                removeHandler: function(e) {
                    delete this._wreqrHandlers[e]
                },
                removeAllHandlers: function() {
                    this._wreqrHandlers = {}
                }
            }), n
        }(e, t), r.CommandStorage = function() {
            var n = function(e) {
                this.options = e, this._commands = {}, t.isFunction(this.initialize) && this.initialize(e)
            };
            return t.extend(n.prototype, e.Events, {
                getCommands: function(e) {
                    var t = this._commands[e];
                    return t || (t = {
                        command: e,
                        instances: []
                    }, this._commands[e] = t), t
                },
                addCommand: function(e, t) {
                    var n = this.getCommands(e);
                    n.instances.push(t)
                },
                clearCommands: function(e) {
                    var t = this.getCommands(e);
                    t.instances = []
                }
            }), n
        }(), r.Commands = function(e, t) {
            return e.Handlers.extend({
                storageType: e.CommandStorage,
                constructor: function(t) {
                    this.options = t || {}, this._initializeStorage(this.options), this.on("handler:add", this._executeCommands, this), e.Handlers.prototype.constructor.apply(this, arguments)
                },
                execute: function(e) {
                    e = arguments[0];
                    var n = t.rest(arguments);
                    this.hasHandler(e) ? this.getHandler(e).apply(this, n) : this.storage.addCommand(e, n)
                },
                _executeCommands: function(e, n, r) {
                    var i = this.storage.getCommands(e);
                    t.each(i.instances, function(e) {
                        n.apply(r, e)
                    }), this.storage.clearCommands(e)
                },
                _initializeStorage: function(e) {
                    var n, r = e.storageType || this.storageType;
                    t.isFunction(r) ? n = new r : n = r, this.storage = n
                }
            })
        }(r, t), r.RequestResponse = function(e, t) {
            return e.Handlers.extend({
                request: function(e) {
                    if (this.hasHandler(e)) return this.getHandler(e).apply(this, t.rest(arguments))
                }
            })
        }(r, t), r.EventAggregator = function(e, t) {
            var n = function() {};
            return n.extend = e.Model.extend, t.extend(n.prototype, e.Events), n
        }(e, t), r.Channel = function(n) {
            var r = function(t) {
                this.vent = new e.Wreqr.EventAggregator, this.reqres = new e.Wreqr.RequestResponse, this.commands = new e.Wreqr.Commands, this.channelName = t
            };
            return t.extend(r.prototype, {
                reset: function() {
                    return this.vent.off(), this.vent.stopListening(), this.reqres.removeAllHandlers(), this.commands.removeAllHandlers(), this
                },
                connectEvents: function(e, t) {
                    return this._connect("vent", e, t), this
                },
                connectCommands: function(e, t) {
                    return this._connect("commands", e, t), this
                },
                connectRequests: function(e, t) {
                    return this._connect("reqres", e, t), this
                },
                _connect: function(e, n, r) {
                    if (!n) return;
                    r = r || this;
                    var i = e === "vent" ? "on" : "setHandler";
                    t.each(n, function(n, s) {
                        this[e][i](s, t.bind(n, r))
                    }, this)
                }
            }), r
        }(r), r.radio = function(e, t) {
            var n = function() {
                this._channels = {}, this.vent = {}, this.commands = {}, this.reqres = {}, this._proxyMethods()
            };
            t.extend(n.prototype, {
                channel: function(e) {
                    if (!e) throw new Error("Channel must receive a name");
                    return this._getChannel(e)
                },
                _getChannel: function(t) {
                    var n = this._channels[t];
                    return n || (n = new e.Channel(t), this._channels[t] = n), n
                },
                _proxyMethods: function() {
                    t.each(["vent", "commands", "reqres"], function(e) {
                        t.each(r[e], function(t) {
                            this[e][t] = i(this, e, t)
                        }, this)
                    }, this)
                }
            });
            var r = {
                    vent: ["on", "off", "trigger", "once", "stopListening", "listenTo", "listenToOnce"],
                    commands: ["execute", "setHandler", "setHandlers", "removeHandler", "removeAllHandlers"],
                    reqres: ["request", "setHandler", "setHandlers", "removeHandler", "removeAllHandlers"]
                },
                i = function(e, n, r) {
                    return function(i) {
                        var s = e._getChannel(i)[n];
                        return s[r].apply(s, t.rest(arguments))
                    }
                };
            return new n
        }(r, t), e.Wreqr
    }),
    function(e, t) {
        if (typeof define == "function" && define.amd) define("backbone.babysitter", ["backbone", "underscore"], function(e, n) {
            return t(e, n)
        });
        else if (typeof exports != "undefined") {
            var n = require("backbone"),
                r = require("underscore");
            module.exports = t(n, r)
        } else t(e.Backbone, e._)
    }(this, function(e, t) {
        "use strict";
        var n = e.ChildViewContainer;
        return e.ChildViewContainer = function(e, t) {
            var n = function(e) {
                this._views = {}, this._indexByModel = {}, this._indexByCustom = {}, this._updateLength(), t.each(e, this.add, this)
            };
            t.extend(n.prototype, {
                add: function(e, t) {
                    var n = e.cid;
                    return this._views[n] = e, e.model && (this._indexByModel[e.model.cid] = n), t && (this._indexByCustom[t] = n), this._updateLength(), this
                },
                findByModel: function(e) {
                    return this.findByModelCid(e.cid)
                },
                findByModelCid: function(e) {
                    var t = this._indexByModel[e];
                    return this.findByCid(t)
                },
                findByCustom: function(e) {
                    var t = this._indexByCustom[e];
                    return this.findByCid(t)
                },
                findByIndex: function(e) {
                    return t.values(this._views)[e]
                },
                findByCid: function(e) {
                    return this._views[e]
                },
                remove: function(e) {
                    var n = e.cid;
                    return e.model && delete this._indexByModel[e.model.cid], t.any(this._indexByCustom, function(e, t) {
                        if (e === n) return delete this._indexByCustom[t], !0
                    }, this), delete this._views[n], this._updateLength(), this
                },
                call: function(e) {
                    this.apply(e, t.tail(arguments))
                },
                apply: function(e, n) {
                    t.each(this._views, function(r) {
                        t.isFunction(r[e]) && r[e].apply(r, n || [])
                    })
                },
                _updateLength: function() {
                    this.length = t.size(this._views)
                }
            });
            var r = ["forEach", "each", "map", "find", "detect", "filter", "select", "reject", "every", "all", "some", "any", "include", "contains", "invoke", "toArray", "first", "initial", "rest", "last", "without", "isEmpty", "pluck", "reduce"];
            return t.each(r, function(e) {
                n.prototype[e] = function() {
                    var n = t.values(this._views),
                        r = [n].concat(t.toArray(arguments));
                    return t[e].apply(t, r)
                }
            }), n
        }(e, t), e.ChildViewContainer.VERSION = "0.1.6", e.ChildViewContainer.noConflict = function() {
            return e.ChildViewContainer = n, this
        }, e.ChildViewContainer
    }),
    function(e, t) {
        if (typeof define == "function" && define.amd) define("marionette", ["backbone", "underscore", "backbone.wreqr", "backbone.babysitter"], function(n, r) {
            return e.Marionette = e.Mn = t(e, n, r)
        });
        else if (typeof exports != "undefined") {
            var n = require("backbone"),
                r = require("underscore"),
                i = require("backbone.wreqr"),
                s = require("backbone.babysitter");
            module.exports = t(e, n, r)
        } else e.Marionette = e.Mn = t(e, e.Backbone, e._)
    }(this, function(e, t, n) {
        "use strict";
        var r = e.Marionette,
            i = e.Mn,
            s = t.Marionette = {};
        s.VERSION = "2.4.1", s.noConflict = function() {
            return e.Marionette = r, e.Mn = i, this
        }, s.Deferred = t.$.Deferred, s.FEATURES = {}, s.isEnabled = function(e) {
            return !!s.FEATURES[e]
        }, s.extend = t.Model.extend, s.isNodeAttached = function(e) {
            return t.$.contains(document.documentElement, e)
        }, s.mergeOptions = function(e, t) {
            if (!e) return;
            n.extend(this, n.pick(e, t))
        }, s.getOption = function(e, t) {
            if (!e || !t) return;
            return e.options && e.options[t] !== undefined ? e.options[t] : e[t]
        }, s.proxyGetOption = function(e) {
            return s.getOption(this, e)
        }, s._getValue = function(e, t, r) {
            return n.isFunction(e) && (e = r ? e.apply(t, r) : e.call(t)), e
        }, s.normalizeMethods = function(e) {
            return n.reduce(e, function(e, t, r) {
                return n.isFunction(t) || (t = this[t]), t && (e[r] = t), e
            }, {}, this)
        }, s.normalizeUIString = function(e, t) {
            return e.replace(/@ui\.[a-zA-Z_$0-9]*/g, function(e) {
                return t[e.slice(4)]
            })
        }, s.normalizeUIKeys = function(e, t) {
            return n.reduce(e, function(e, n, r) {
                var i = s.normalizeUIString(r, t);
                return e[i] = n, e
            }, {})
        }, s.normalizeUIValues = function(e, t, r) {
            return n.each(e, function(i, o) {
                n.isString(i) ? e[o] = s.normalizeUIString(i, t) : n.isObject(i) && n.isArray(r) && (n.extend(i, s.normalizeUIValues(n.pick(i, r), t)), n.each(r, function(e) {
                    var r = i[e];
                    n.isString(r) && (i[e] = s.normalizeUIString(r, t))
                }))
            }), e
        }, s.actAsCollection = function(e, t) {
            var r = ["forEach", "each", "map", "find", "detect", "filter", "select", "reject", "every", "all", "some", "any", "include", "contains", "invoke", "toArray", "first", "initial", "rest", "last", "without", "isEmpty", "pluck"];
            n.each(r, function(r) {
                e[r] = function() {
                    var e = n.values(n.result(this, t)),
                        i = [e].concat(n.toArray(arguments));
                    return n[r].apply(n, i)
                }
            })
        };
        var o = s.deprecate = function(e, t) {
            n.isObject(e) && (e = e.prev + " is going to be removed in the future. " + "Please use " + e.next + " instead." + (e.url ? " See: " + e.url : "")), (t === undefined || !t) && !o._cache[e] && (o._warn("Deprecation warning: " + e), o._cache[e] = !0)
        };
        o._warn = typeof console != "undefined" && (console.warn || console.log) || function() {}, o._cache = {}, s._triggerMethod = function() {
                function t(e, t, n) {
                    return n.toUpperCase()
                }
                var e = /(^|:)(\w)/gi;
                return function(r, i, s) {
                    var o = arguments.length < 3;
                    o && (s = i, i = s[0]);
                    var u = "on" + i.replace(e, t),
                        a = r[u],
                        f;
                    return n.isFunction(a) && (f = a.apply(r, o ? n.rest(s) : s)), n.isFunction(r.trigger) && (o + s.length > 1 ? r.trigger.apply(r, o ? s : [i].concat(n.drop(s, 0))) : r.trigger(i)), f
                }
            }(), s.triggerMethod = function(e) {
                return s._triggerMethod(this, arguments)
            }, s.triggerMethodOn = function(e) {
                var t = n.isFunction(e.triggerMethod) ? e.triggerMethod : s.triggerMethod;
                return t.apply(e, n.rest(arguments))
            }, s.MonitorDOMRefresh = function(e) {
                function t() {
                    e._isShown = !0, i()
                }

                function r() {
                    e._isRendered = !0, i()
                }

                function i() {
					
                    e._isShown && e._isRendered && s.isNodeAttached(e.el) && n.isFunction(e.triggerMethod) && e.triggerMethod("dom:refresh")
                }
                e.on({
                    show: t,
                    render: r
                })
            },
            function(e) {
                function t(t, r, i, s) {
                    var o = s.split(/\s+/);
                    n.each(o, function(n) {
                        var s = t[n];
                        if (!s) throw new e.Error('Method "' + n + '" was configured as an event handler, but does not exist.');
                        t.listenTo(r, i, s)
                    })
                }

                function r(e, t, n, r) {
                    e.listenTo(t, n, r)
                }

                function i(e, t, r, i) {
                    var s = i.split(/\s+/);
                    n.each(s, function(n) {
                        var i = e[n];
                        e.stopListening(t, r, i)
                    })
                }

                function s(e, t, n, r) {
                    e.stopListening(t, n, r)
                }

                function o(t, r, i, s, o) {
                    if (!r || !i) return;
                    if (!n.isObject(i)) throw new e.Error({
                        message: "Bindings must be an object or function.",
                        url: "marionette.functions.html#marionettebindentityevents"
                    });
                    i = e._getValue(i, t), n.each(i, function(e, i) {
                        n.isFunction(e) ? s(t, r, i, e) : o(t, r, i, e)
                    })
                }
                e.bindEntityEvents = function(e, n, i) {
                    o(e, n, i, r, t)
                }, e.unbindEntityEvents = function(e, t, n) {
                    o(e, t, n, s, i)
                }, e.proxyBindEntityEvents = function(t, n) {
                    return e.bindEntityEvents(this, t, n)
                }, e.proxyUnbindEntityEvents = function(t, n) {
                    return e.unbindEntityEvents(this, t, n)
                }
            }(s);
        var u = ["description", "fileName", "lineNumber", "name", "message", "number"];
        return s.Error = s.extend.call(Error, {
            urlRoot: "http://marionettejs.com/docs/v" + s.VERSION + "/",
            constructor: function(e, t) {
                n.isObject(e) ? (t = e, e = t.message) : t || (t = {});
                var r = Error.call(this, e);
                n.extend(this, n.pick(r, u), n.pick(t, u)), this.captureStackTrace(), t.url && (this.url = this.urlRoot + t.url)
            },
            captureStackTrace: function() {
                Error.captureStackTrace && Error.captureStackTrace(this, s.Error)
            },
            toString: function() {
                return this.name + ": " + this.message + (this.url ? " See: " + this.url : "")
            }
        }), s.Error.extend = s.extend, s.Callbacks = function() {
            this._deferred = s.Deferred(), this._callbacks = []
        }, n.extend(s.Callbacks.prototype, {
            add: function(e, t) {
                var r = n.result(this._deferred, "promise");
                this._callbacks.push({
                    cb: e,
                    ctx: t
                }), r.then(function(n) {
                    t && (n.context = t), e.call(n.context, n.options)
                })
            },
            run: function(e, t) {
                this._deferred.resolve({
                    options: e,
                    context: t
                })
            },
            reset: function() {
                var e = this._callbacks;
                this._deferred = s.Deferred(), this._callbacks = [], n.each(e, function(e) {
                    this.add(e.cb, e.ctx)
                }, this)
            }
        }), s.Controller = function(e) {
            this.options = e || {}, n.isFunction(this.initialize) && this.initialize(this.options)
        }, s.Controller.extend = s.extend, n.extend(s.Controller.prototype, t.Events, {
            destroy: function() {
                return s._triggerMethod(this, "before:destroy", arguments), s._triggerMethod(this, "destroy", arguments), this.stopListening(), this.off(), this
            },
            triggerMethod: s.triggerMethod,
            mergeOptions: s.mergeOptions,
            getOption: s.proxyGetOption
        }), s.Object = function(e) {
            this.options = n.extend({}, n.result(this, "options"), e), this.initialize.apply(this, arguments)
        }, s.Object.extend = s.extend, n.extend(s.Object.prototype, t.Events, {
            initialize: function() {},
            destroy: function() {
                return this.triggerMethod("before:destroy"), this.triggerMethod("destroy"), this.stopListening(), this
            },
            triggerMethod: s.triggerMethod,
            mergeOptions: s.mergeOptions,
            getOption: s.proxyGetOption,
            bindEntityEvents: s.proxyBindEntityEvents,
            unbindEntityEvents: s.proxyUnbindEntityEvents
        }), s.Region = s.Object.extend({
            constructor: function(e) {
                this.options = e || {}, this.el = this.getOption("el"), this.el = this.el instanceof t.$ ? this.el[0] : this.el;
                if (!this.el) throw new s.Error({
                    name: "NoElError",
                    message: 'An "el" must be specified for a region.'
                });
                this.$el = this.getEl(this.el), s.Object.call(this, e)
            },
            show: function(e, t) {
                if (!this._ensureElement()) return;
                this._ensureViewIsIntact(e);
                var n = t || {},
                    r = e !== this.currentView,
                    i = !!n.preventDestroy,
                    o = !!n.forceShow,
                    u = !!this.currentView,
                    a = r && !i,
                    f = r || o;
                u && this.triggerMethod("before:swapOut", this.currentView, this, t), this.currentView && delete this.currentView._parent, a ? this.empty() : u && f && this.currentView.off("destroy", this.empty, this);
                if (f) {
                    e.once("destroy", this.empty, this), e.render(), e._parent = this, u && this.triggerMethod("before:swap", e, this, t), this.triggerMethod("before:show", e, this, t), s.triggerMethodOn(e, "before:show", e, this, t), u && this.triggerMethod("swapOut", this.currentView, this, t);
                    var l = s.isNodeAttached(this.el),
                        c = [],
                        h = n.triggerBeforeAttach || this.triggerBeforeAttach,
                        p = n.triggerAttach || this.triggerAttach;
                    return l && h && (c = this._displayedViews(e), this._triggerAttach(c, "before:")), this.attachHtml(e), this.currentView = e, l && p && (c = this._displayedViews(e), this._triggerAttach(c)), u && this.triggerMethod("swap", e, this, t), this.triggerMethod("show", e, this, t), s.triggerMethodOn(e, "show", e, this, t), this
                }
                return this
            },
            triggerBeforeAttach: !0,
            triggerAttach: !0,
            _triggerAttach: function(e, t) {
                var r = (t || "") + "attach";
                n.each(e, function(e) {
                    s.triggerMethodOn(e, r, e, this)
                }, this)
            },
            _displayedViews: function(e) {
                return n.union([e], n.result(e, "_getNestedViews") || [])
            },
            _ensureElement: function() {
                n.isObject(this.el) || (this.$el = this.getEl(this.el), this.el = this.$el[0]);
                if (!this.$el || this.$el.length === 0) {
                    if (this.getOption("allowMissingEl")) return !1;
                    throw new s.Error('An "el" ' + this.$el.selector + " must exist in DOM")
                }
                return !0
            },
            _ensureViewIsIntact: function(e) {
                if (!e) throw new s.Error({
                    name: "ViewNotValid",
                    message: "The view passed is undefined and therefore invalid. You must pass a view instance to show."
                });
                if (e.isDestroyed) throw new s.Error({
                    name: "ViewDestroyedError",
                    message: 'View (cid: "' + e.cid + '") has already been destroyed and cannot be used.'
                })
            },
            getEl: function(e) {
                return t.$(e, s._getValue(this.options.parentEl, this))
            },
            attachHtml: function(e) {
                this.$el.contents().detach(), this.el.appendChild(e.el)
            },
            empty: function(e) {
                var t = this.currentView,
                    n = s._getValue(e, "preventDestroy", this);
                if (!t) return;
                return t.off("destroy", this.empty, this), this.triggerMethod("before:empty", t), n || this._destroyView(), this.triggerMethod("empty", t), delete this.currentView, n && this.$el.contents().detach(), this
            },
            _destroyView: function() {
                var e = this.currentView;
                e.destroy && !e.isDestroyed ? e.destroy() : e.remove && (e.remove(), e.isDestroyed = !0)
            },
            attachView: function(e) {
                return this.currentView = e, this
            },
            hasView: function() {
                return !!this.currentView
            },
            reset: function() {
                return this.empty(), this.$el && (this.el = this.$el.selector), delete this.$el, this
            }
        }, {
            buildRegion: function(e, t) {
                if (n.isString(e)) return this._buildRegionFromSelector(e, t);
                if (e.selector || e.el || e.regionClass) return this._buildRegionFromObject(e, t);
                if (n.isFunction(e)) return this._buildRegionFromRegionClass(e);
                throw new s.Error({
                    message: "Improper region configuration type.",
                    url: "marionette.region.html#region-configuration-types"
                })
            },
            _buildRegionFromSelector: function(e, t) {
                return new t({
                    el: e
                })
            },
            _buildRegionFromObject: function(e, t) {
                var r = e.regionClass || t,
                    i = n.omit(e, "selector", "regionClass");
                return e.selector && !i.el && (i.el = e.selector), new r(i)
            },
            _buildRegionFromRegionClass: function(e) {
                return new e
            }
        }), s.RegionManager = s.Controller.extend({
            constructor: function(e) {
                this._regions = {}, this.length = 0, s.Controller.call(this, e), this.addRegions(this.getOption("regions"))
            },
            addRegions: function(e, t) {
                return e = s._getValue(e, this, arguments), n.reduce(e, function(e, r, i) {
                    return n.isString(r) && (r = {
                        selector: r
                    }), r.selector && (r = n.defaults({}, r, t)), e[i] = this.addRegion(i, r), e
                }, {}, this)
            },
            addRegion: function(e, t) {
                var n;
                return t instanceof s.Region ? n = t : n = s.Region.buildRegion(t, s.Region), this.triggerMethod("before:add:region", e, n), n._parent = this, this._store(e, n), this.triggerMethod("add:region", e, n), n
            },
            get: function(e) {
                return this._regions[e]
            },
            getRegions: function() {
                return n.clone(this._regions)
            },
            removeRegion: function(e) {
                var t = this._regions[e];
                return this._remove(e, t), t
            },
            removeRegions: function() {
                var e = this.getRegions();
                return n.each(this._regions, function(e, t) {
                    this._remove(t, e)
                }, this), e
            },
            emptyRegions: function() {
                var e = this.getRegions();
                return n.invoke(e, "empty"), e
            },
            destroy: function() {
                return this.removeRegions(), s.Controller.prototype.destroy.apply(this, arguments)
            },
            _store: function(e, t) {
                this._regions[e] || this.length++, this._regions[e] = t
            },
            _remove: function(e, t) {
                this.triggerMethod("before:remove:region", e, t), t.empty(), t.stopListening(), delete t._parent, delete this._regions[e], this.length--, this.triggerMethod("remove:region", e, t)
            }
        }), s.actAsCollection(s.RegionManager.prototype, "_regions"), s.TemplateCache = function(e) {
            this.templateId = e
        }, n.extend(s.TemplateCache, {
            templateCaches: {},
            get: function(e, t) {
                var n = this.templateCaches[e];
                return n || (n = new s.TemplateCache(e), this.templateCaches[e] = n), n.load(t)
            },
            clear: function() {
                var e, t = n.toArray(arguments),
                    r = t.length;
                if (r > 0)
                    for (e = 0; e < r; e++) delete this.templateCaches[t[e]];
                else this.templateCaches = {}
            }
        }), n.extend(s.TemplateCache.prototype, {
            load: function(e) {
                if (this.compiledTemplate) return this.compiledTemplate;
                var t = this.loadTemplate(this.templateId, e);
                return this.compiledTemplate = this.compileTemplate(t, e), this.compiledTemplate
            },
            loadTemplate: function(e, n) {
                var r = t.$(e).html();
                if (!r || r.length === 0) throw new s.Error({
                    name: "NoTemplateError",
                    message: 'Could not find template: "' + e + '"'
                });
                return r
            },
            compileTemplate: function(e, t) {
                return n.template(e, t)
            }
        }), s.Renderer = {
            render: function(e, t) {
                if (!e) throw new s.Error({
                    name: "TemplateNotFoundError",
                    message: "Cannot render the template since its false, null or undefined."
                });
                var r = n.isFunction(e) ? e : s.TemplateCache.get(e);
                return r(t)
            }
        }, s.View = t.View.extend({
            isDestroyed: !1,
            constructor: function(e) {
                n.bindAll(this, "render"), e = s._getValue(e, this), this.options = n.extend({}, n.result(this, "options"), e), this._behaviors = s.Behaviors(this), t.View.call(this, this.options), s.MonitorDOMRefresh(this)
            },
            getTemplate: function() {
                return this.getOption("template")
            },
            serializeModel: function(e) {
                return e.toJSON.apply(e, n.rest(arguments))
            },
            mixinTemplateHelpers: function(e) {
                e = e || {};
                var t = this.getOption("templateHelpers");
                return t = s._getValue(t, this), n.extend(e, t)
            },
            normalizeUIKeys: function(e) {
                var t = n.result(this, "_uiBindings");
                return s.normalizeUIKeys(e, t || n.result(this, "ui"))
            },
            normalizeUIValues: function(e, t) {
                var r = n.result(this, "ui"),
                    i = n.result(this, "_uiBindings");
                return s.normalizeUIValues(e, i || r, t)
            },
            configureTriggers: function() {
                if (!this.triggers) return;
                var e = this.normalizeUIKeys(n.result(this, "triggers"));
                return n.reduce(e, function(e, t, n) {
                    return e[n] = this._buildViewTrigger(t), e
                }, {}, this)
            },
            delegateEvents: function(e) {
                return this._delegateDOMEvents(e), this.bindEntityEvents(this.model, this.getOption("modelEvents")), this.bindEntityEvents(this.collection, this.getOption("collectionEvents")), n.each(this._behaviors, function(e) {
                    e.bindEntityEvents(this.model, e.getOption("modelEvents")), e.bindEntityEvents(this.collection, e.getOption("collectionEvents"))
                }, this), this
            },
            _delegateDOMEvents: function(e) {
                var r = s._getValue(e || this.events, this);
                r = this.normalizeUIKeys(r), n.isUndefined(e) && (this.events = r);
                var i = {},
                    o = n.result(this, "behaviorEvents") || {},
                    u = this.configureTriggers(),
                    a = n.result(this, "behaviorTriggers") || {};
                n.extend(i, o, r, u, a), t.View.prototype.delegateEvents.call(this, i)
            },
            undelegateEvents: function() {
                return t.View.prototype.undelegateEvents.apply(this, arguments), this.unbindEntityEvents(this.model, this.getOption("modelEvents")), this.unbindEntityEvents(this.collection, this.getOption("collectionEvents")), n.each(this._behaviors, function(e) {
                    e.unbindEntityEvents(this.model, e.getOption("modelEvents")), e.unbindEntityEvents(this.collection, e.getOption("collectionEvents"))
                }, this), this
            },
            _ensureViewIsIntact: function() {
                if (this.isDestroyed) throw new s.Error({
                    name: "ViewDestroyedError",
                    message: 'View (cid: "' + this.cid + '") has already been destroyed and cannot be used.'
                })
            },
            destroy: function() {
                if (this.isDestroyed) return this;
                var e = n.toArray(arguments);
                return this.triggerMethod.apply(this, ["before:destroy"].concat(e)), this.isDestroyed = !0, this.triggerMethod.apply(this, ["destroy"].concat(e)), this.unbindUIElements(), this.isRendered = !1, this.remove(), n.invoke(this._behaviors, "destroy", e), this
            },
            bindUIElements: function() {
                this._bindUIElements(), n.invoke(this._behaviors, this._bindUIElements)
            },
            _bindUIElements: function() {
                if (!this.ui) return;
                this._uiBindings || (this._uiBindings = this.ui);
                var e = n.result(this, "_uiBindings");
                this.ui = {}, n.each(e, function(e, t) {
                    this.ui[t] = this.$(e)
                }, this)
            },
            unbindUIElements: function() {
                this._unbindUIElements(), n.invoke(this._behaviors, this._unbindUIElements)
            },
            _unbindUIElements: function() {
                if (!this.ui || !this._uiBindings) return;
                n.each(this.ui, function(e, t) {
                    delete this.ui[t]
                }, this), this.ui = this._uiBindings, delete this._uiBindings
            },
            _buildViewTrigger: function(e) {
                var t = n.isObject(e),
                    r = n.defaults({}, t ? e : {}, {
                        preventDefault: !0,
                        stopPropagation: !0
                    }),
                    i = t ? r.event : e;
                return function(e) {
                    e && (e.preventDefault && r.preventDefault && e.preventDefault(), e.stopPropagation && r.stopPropagation && e.stopPropagation());
                    var t = {
                        views: this,
                        model: this.model,
                        collection: this.collection
                    };
                    this.triggerMethod(i, t)
                }
            },
            setElement: function() {
                var e = t.View.prototype.setElement.apply(this, arguments);
                return n.invoke(this._behaviors, "proxyViewProperties", this), e
            },
            triggerMethod: function() {
                var e = s._triggerMethod(this, arguments);
                return this._triggerEventOnBehaviors(arguments), this._triggerEventOnParentLayout(arguments[0], n.rest(arguments)), e
            },
            _triggerEventOnBehaviors: function(e) {
                var t = s._triggerMethod,
                    n = this._behaviors;
                for (var r = 0, i = n && n.length; r < i; r++) t(n[r], e)
            },
            _triggerEventOnParentLayout: function(e, t) {
                var r = this._parentLayoutView();
                if (!r) return;
                var i = s.getOption(r, "childViewEventPrefix"),
                    o = i + ":" + e;
                s._triggerMethod(r, [o, this].concat(t));
                var u = s.getOption(r, "childEvents"),
                    a = r.normalizeMethods(u);
                !!a && n.isFunction(a[e]) && a[e].apply(r, [this].concat(t))
            },
            _getImmediateChildren: function() {
                return []
            },
            _getNestedViews: function() {
                var e = this._getImmediateChildren();
                return e.length ? n.reduce(e, function(e, t) {
                    return t._getNestedViews ? e.concat(t._getNestedViews()) : e
                }, e) : e
            },
            _getAncestors: function() {
                var e = [],
                    t = this._parent;
                while (t) e.push(t), t = t._parent;
                return e
            },
            _parentLayoutView: function() {
                var e = this._getAncestors();
                return n.find(e, function(e) {
                    return e instanceof s.LayoutView
                })
            },
            normalizeMethods: s.normalizeMethods,
            mergeOptions: s.mergeOptions,
            getOption: s.proxyGetOption,
            bindEntityEvents: s.proxyBindEntityEvents,
            unbindEntityEvents: s.proxyUnbindEntityEvents
        }), s.ItemView = s.View.extend({
            constructor: function() {
                s.View.apply(this, arguments)
            },
            serializeData: function() {
                if (!this.model && !this.collection) return {};
                var e = [this.model || this.collection];
                return arguments.length && e.push.apply(e, arguments), this.model ? this.serializeModel.apply(this, e) : {
                    items: this.serializeCollection.apply(this, e)
                }
            },
            serializeCollection: function(e) {
                return e.toJSON.apply(e, n.rest(arguments))
            },
            render: function() {
                return this._ensureViewIsIntact(), this.triggerMethod("before:render", this), this._renderTemplate(), this.isRendered = !0, this.bindUIElements(), this.triggerMethod("render", this), this
            },
            _renderTemplate: function() {
                var e = this.getTemplate();
                if (e === !1) return;
                if (!e) throw new s.Error({
                    name: "UndefinedTemplateError",
                    message: "Cannot render the template since it is null or undefined."
                });
                var t = this.mixinTemplateHelpers(this.serializeData()),
                    n = s.Renderer.render(e, t, this);
                return this.attachElContent(n), this
            },
            attachElContent: function(e) {
                return this.$el.html(e), this
            }
        }), s.CollectionView = s.View.extend({
            childViewEventPrefix: "childview",
            sort: !0,
            constructor: function(e) {
                this.once("render", this._initialEvents), this._initChildViewStorage(), s.View.apply(this, arguments), this.on("show", this._onShowCalled), this.initRenderBuffer()
            },
            initRenderBuffer: function() {
                this._bufferedChildren = []
            },
            startBuffering: function() {
                this.initRenderBuffer(), this.isBuffering = !0
            },
            endBuffering: function() {
                this.isBuffering = !1, this._triggerBeforeShowBufferedChildren(), this.attachBuffer(this), this._triggerShowBufferedChildren(), this.initRenderBuffer()
            },
            _triggerBeforeShowBufferedChildren: function() {
                this._isShown && n.each(this._bufferedChildren, n.partial(this._triggerMethodOnChild, "before:show"))
            },
            _triggerShowBufferedChildren: function() {
                this._isShown && (n.each(this._bufferedChildren, n.partial(this._triggerMethodOnChild, "show")), this._bufferedChildren = [])
            },
            _triggerMethodOnChild: function(e, t) {
                s.triggerMethodOn(t, e)
            },
            _initialEvents: function() {
                this.collection && (this.listenTo(this.collection, "add", this._onCollectionAdd), this.listenTo(this.collection, "remove", this._onCollectionRemove), this.listenTo(this.collection, "reset", this.render), this.getOption("sort") && this.listenTo(this.collection, "sort", this._sortViews))
            },
            _onCollectionAdd: function(e, t, r) {
                var i;
                r.at !== undefined ? i = r.at : i = n.indexOf(this._filteredSortedModels(), e);
                if (this._shouldAddChild(e, i)) {
                    this.destroyEmptyView();
                    var s = this.getChildView(e);
                    this.addChild(e, s, i)
                }
            },
            _onCollectionRemove: function(e) {
                var t = this.children.findByModel(e);
                this.removeChildView(t), this.checkEmpty()
            },
            _onShowCalled: function() {
                this.children.each(n.partial(this._triggerMethodOnChild, "show"))
            },
            render: function() {
                return this._ensureViewIsIntact(), this.triggerMethod("before:render", this), this._renderChildren(), this.isRendered = !0, this.triggerMethod("render", this), this
            },
            reorder: function() {
                var e = this.children,
                    t = this._filteredSortedModels(),
                    r = n.find(t, function(t) {
                        return !e.findByModel(t)
                    });
                if (r) this.render();
                else {
                    var i = n.map(t, function(t) {
                        return e.findByModel(t).el
                    });
                    this.triggerMethod("before:reorder"), this._appendReorderedChildren(i), this.triggerMethod("reorder")
                }
            },
            resortView: function() {
                s.getOption(this, "reorderOnSort") ? this.reorder() : this.render()
            },
            _sortViews: function() {
                var e = this._filteredSortedModels(),
                    t = n.find(e, function(e, t) {
                        var n = this.children.findByModel(e);
                        return !n || n._index !== t
                    }, this);
                t && this.resortView()
            },
            _emptyViewIndex: -1,
            _appendReorderedChildren: function(e) {
                this.$el.append(e)
            },
            _renderChildren: function() {
                this.destroyEmptyView(), this.destroyChildren(), this.isEmpty(this.collection) ? this.showEmptyView() : (this.triggerMethod("before:render:collection", this), this.startBuffering(), this.showCollection(), this.endBuffering(), this.triggerMethod("render:collection", this), this.children.isEmpty() && this.showEmptyView())
            },
            showCollection: function() {
                var e, t = this._filteredSortedModels();
                n.each(t, function(t, n) {
                    e = this.getChildView(t), this.addChild(t, e, n)
                }, this)
            },
            _filteredSortedModels: function() {
                var e, t = this.getViewComparator();
                return t ? n.isString(t) || t.length === 1 ? e = this.collection.sortBy(t, this) : e = n.clone(this.collection.models).sort(n.bind(t, this)) : e = this.collection.models, this.getOption("filter") && (e = n.filter(e, function(e, t) {
                    return this._shouldAddChild(e, t)
                }, this)), e
            },
            showEmptyView: function() {
                var e = this.getEmptyView();
                if (e && !this._showingEmptyView) {
                    this.triggerMethod("before:render:empty"), this._showingEmptyView = !0;
                    var n = new t.Model;
                    this.addEmptyView(n, e), this.triggerMethod("render:empty")
                }
            },
            destroyEmptyView: function() {
                this._showingEmptyView && (this.triggerMethod("before:remove:empty"), this.destroyChildren(), delete this._showingEmptyView, this.triggerMethod("remove:empty"))
            },
            getEmptyView: function() {
                return this.getOption("emptyView")
            },
            addEmptyView: function(e, t) {
                var r = this.getOption("emptyViewOptions") || this.getOption("childViewOptions");
                n.isFunction(r) && (r = r.call(this, e, this._emptyViewIndex));
                var i = this.buildChildView(e, t, r);
                i._parent = this, this.proxyChildEvents(i), this._isShown && s.triggerMethodOn(i, "before:show"), this.children.add(i), this.renderChildView(i, this._emptyViewIndex), this._isShown && s.triggerMethodOn(i, "show")
            },
            getChildView: function(e) {
                var t = this.getOption("childView");
                if (!t) throw new s.Error({
                    name: "NoChildViewError",
                    message: 'A "childView" must be specified'
                });
                return t
            },
            addChild: function(e, t, n) {
                var r = this.getOption("childViewOptions");
                r = s._getValue(r, this, [e, n]);
                var i = this.buildChildView(e, t, r);
                return this._updateIndices(i, !0, n), this._addChildView(i, n), i._parent = this, i
            },
            _updateIndices: function(e, t, n) {
                if (!this.getOption("sort")) return;
                t && (e._index = n), this.children.each(function(n) {
                    n._index >= e._index && (n._index += t ? 1 : -1)
                })
            },
            _addChildView: function(e, t) {
                this.proxyChildEvents(e), this.triggerMethod("before:add:child", e), this._isShown && !this.isBuffering && s.triggerMethodOn(e, "before:show"), this.children.add(e), this.renderChildView(e, t), this._isShown && !this.isBuffering && s.triggerMethodOn(e, "show"), this.triggerMethod("add:child", e)
            },
            renderChildView: function(e, t) {
                return e.render(), this.attachHtml(this, e, t), e
            },
            buildChildView: function(e, t, r) {
                var i = n.extend({
                    model: e
                }, r);
                return new t(i)
            },
            removeChildView: function(e) {
                return e && (this.triggerMethod("before:remove:child", e), e.destroy ? e.destroy() : e.remove && e.remove(), delete e._parent, this.stopListening(e), this.children.remove(e), this.triggerMethod("remove:child", e), this._updateIndices(e, !1)), e
            },
            isEmpty: function() {
                return !this.collection || this.collection.length === 0
            },
            checkEmpty: function() {
                this.isEmpty(this.collection) && this.showEmptyView()
            },
            attachBuffer: function(e) {
                e.$el.append(this._createBuffer(e))
            },
            _createBuffer: function(e) {
                var t = document.createDocumentFragment();
                return n.each(e._bufferedChildren, function(e) {
                    t.appendChild(e.el)
                }), t
            },
            attachHtml: function(e, t, n) {
                e.isBuffering ? e._bufferedChildren.splice(n, 0, t) : e._insertBefore(t, n) || e._insertAfter(t)
            },
            _insertBefore: function(e, t) {
                var n, r = this.getOption("sort") && t < this.children.length - 1;
                return r && (n = this.children.find(function(e) {
                    return e._index === t + 1
                })), n ? (n.$el.before(e.el), !0) : !1
            },
            _insertAfter: function(e) {
                this.$el.append(e.el)
            },
            _initChildViewStorage: function() {
                this.children = new t.ChildViewContainer
            },
            destroy: function() {
                return this.isDestroyed ? this : (this.triggerMethod("before:destroy:collection"), this.destroyChildren(), this.triggerMethod("destroy:collection"), s.View.prototype.destroy.apply(this, arguments))
            },
            destroyChildren: function() {
                var e = this.children.map(n.identity);
                return this.children.each(this.removeChildView, this), this.checkEmpty(), e
            },
            _shouldAddChild: function(e, t) {
                var r = this.getOption("filter");
                return !n.isFunction(r) || r.call(this, e, t, this.collection)
            },
            proxyChildEvents: function(e) {
                var t = this.getOption("childViewEventPrefix");
                this.listenTo(e, "all", function() {
                    var r = n.toArray(arguments),
                        i = r[0],
                        s = this.normalizeMethods(n.result(this, "childEvents"));
                    r[0] = t + ":" + i, r.splice(1, 0, e), typeof s != "undefined" && n.isFunction(s[i]) && s[i].apply(this, r.slice(1)), this.triggerMethod.apply(this, r)
                })
            },
            _getImmediateChildren: function() {
                return n.values(this.children._views)
            },
            getViewComparator: function() {
                return this.getOption("viewComparator")
            }
        }), s.CompositeView = s.CollectionView.extend({
            constructor: function() {
                s.CollectionView.apply(this, arguments)
            },
            _initialEvents: function() {
                this.collection && (this.listenTo(this.collection, "add", this._onCollectionAdd), this.listenTo(this.collection, "remove", this._onCollectionRemove), this.listenTo(this.collection, "reset", this._renderChildren), this.getOption("sort") && this.listenTo(this.collection, "sort", this._sortViews))
            },
            getChildView: function(e) {
                var t = this.getOption("childView") || this.constructor;
                return t
            },
            serializeData: function() {
                var e = {};
                return this.model && (e = n.partial(this.serializeModel, this.model).apply(this, arguments)), e
            },
            render: function() {
                return this._ensureViewIsIntact(), this._isRendering = !0, this.resetChildViewContainer(), this.triggerMethod("before:render", this), this._renderTemplate(), this._renderChildren(), this._isRendering = !1, this.isRendered = !0, this.triggerMethod("render", this), this
            },
            _renderChildren: function() {
                (this.isRendered || this._isRendering) && s.CollectionView.prototype._renderChildren.call(this)
            },
            _renderTemplate: function() {
                var e = {};
                e = this.serializeData(), e = this.mixinTemplateHelpers(e), this.triggerMethod("before:render:template");
                var t = this.getTemplate(),
                    n = s.Renderer.render(t, e, this);
                this.attachElContent(n), this.bindUIElements(), this.triggerMethod("render:template")
            },
            attachElContent: function(e) {
                return this.$el.html(e), this
            },
            attachBuffer: function(e) {
                var t = this.getChildViewContainer(e);
                t.append(this._createBuffer(e))
            },
            _insertAfter: function(e) {
                var t = this.getChildViewContainer(this, e);
                t.append(e.el)
            },
            _appendReorderedChildren: function(e) {
                var t = this.getChildViewContainer(this);
                t.append(e)
            },
            getChildViewContainer: function(e, t) {
                if ("$childViewContainer" in e) return e.$childViewContainer;
                var n, r = s.getOption(e, "childViewContainer");
                if (r) {
                    var i = s._getValue(r, e);
                    i.charAt(0) === "@" && e.ui ? n = e.ui[i.substr(4)] : n = e.$(i);
                    if (n.length <= 0) throw new s.Error({
                        name: "ChildViewContainerMissingError",
                        message: 'The specified "childViewContainer" was not found: ' + e.childViewContainer
                    })
                } else n = e.$el;
                return e.$childViewContainer = n, n
            },
            resetChildViewContainer: function() {
                this.$childViewContainer && delete this.$childViewContainer
            }
        }), s.LayoutView = s.ItemView.extend({
            regionClass: s.Region,
            options: {
                destroyImmediate: !1
            },
            childViewEventPrefix: "childview",
            constructor: function(e) {
                e = e || {}, this._firstRender = !0, this._initializeRegions(e), s.ItemView.call(this, e)
            },
            render: function() {
                return this._ensureViewIsIntact(), this._firstRender ? this._firstRender = !1 : this._reInitializeRegions(), s.ItemView.prototype.render.apply(this, arguments)
            },
            destroy: function() {
                return this.isDestroyed ? this : (this.getOption("destroyImmediate") === !0 && this.$el.remove(), this.regionManager.destroy(), s.ItemView.prototype.destroy.apply(this, arguments))
            },
            showChildView: function(e, t) {
                return this.getRegion(e).show(t)
            },
            getChildView: function(e) {
                return this.getRegion(e).currentView
            },
            addRegion: function(e, t) {
                var n = {};
                return n[e] = t, this._buildRegions(n)[e]
            },
            addRegions: function(e) {
                return this.regions = n.extend({}, this.regions, e), this._buildRegions(e)
            },
            removeRegion: function(e) {
                return delete this.regions[e], this.regionManager.removeRegion(e)
            },
            getRegion: function(e) {
                return this.regionManager.get(e)
            },
            getRegions: function() {
                return this.regionManager.getRegions()
            },
            _buildRegions: function(e) {
                var t = {
                    regionClass: this.getOption("regionClass"),
                    parentEl: n.partial(n.result, this, "el")
                };
                return this.regionManager.addRegions(e, t)
            },
            _initializeRegions: function(e) {
                var t;
                this._initRegionManager(), t = s._getValue(this.regions, this, [e]) || {};
                var r = this.getOption.call(e, "regions");
                r = s._getValue(r, this, [e]), n.extend(t, r), t = this.normalizeUIValues(t, ["selector", "el"]), this.addRegions(t)
            },
            _reInitializeRegions: function() {
                this.regionManager.invoke("reset")
            },
            getRegionManager: function() {
                return new s.RegionManager
            },
            _initRegionManager: function() {
                this.regionManager = this.getRegionManager(), this.regionManager._parent = this, this.listenTo(this.regionManager, "before:add:region", function(e) {
                    this.triggerMethod("before:add:region", e)
                }), this.listenTo(this.regionManager, "add:region", function(e, t) {
                    this[e] = t, this.triggerMethod("add:region", e, t)
                }), this.listenTo(this.regionManager, "before:remove:region", function(e) {
                    this.triggerMethod("before:remove:region", e)
                }), this.listenTo(this.regionManager, "remove:region", function(e, t) {
                    delete this[e], this.triggerMethod("remove:region", e, t)
                })
            },
            _getImmediateChildren: function() {
                return n.chain(this.regionManager.getRegions()).pluck("currentView").compact().value()
            }
        }), s.Behavior = s.Object.extend({
            constructor: function(e, t) {
                this.views = t, this.defaults = n.result(this, "defaults") || {}, this.options = n.extend({}, this.defaults, e), this.ui = n.extend({}, n.result(t, "ui"), n.result(this, "ui")), s.Object.apply(this, arguments)
            },
            $: function() {
                return this.views.$.apply(this.views, arguments)
            },
            destroy: function() {
                return this.stopListening(), this
            },
            proxyViewProperties: function(e) {
                this.$el = e.$el, this.el = e.el
            }
        }), s.Behaviors = function(e, t) {
            function r(e, n) {
                return t.isObject(e.behaviors) ? (n = r.parseBehaviors(e, n || t.result(e, "behaviors")), r.wrap(e, n, t.keys(i)), n) : {}
            }

            function s(e, t) {
                this._view = e, this._behaviors = t, this._triggers = {}
            }

            function o(e) {
                return e._uiBindings || e.ui
            }
            var n = /^(\S+)\s*(.*)$/,
                i = {
                    behaviorTriggers: function(e, t) {
                        var n = new s(this, t);
                        return n.buildBehaviorTriggers()
                    },
                    behaviorEvents: function(r, i) {
                        var s = {};
                        return t.each(i, function(r, i) {
                            var u = {},
                                a = t.clone(t.result(r, "events")) || {};
                            a = e.normalizeUIKeys(a, o(r));
                            var f = 0;
                            t.each(a, function(e, s) {
                                var o = s.match(n),
                                    a = o[1] + "." + [this.cid, i, f++, " "].join(""),
                                    l = o[2],
                                    c = a + l,
                                    h = t.isFunction(e) ? e : r[e];
                                u[c] = t.bind(h, r)
                            }, this), s = t.extend(s, u)
                        }, this), s
                    }
                };
            return t.extend(r, {
                behaviorsLookup: function() {
                    throw new e.Error({
                        message: "You must define where your behaviors are stored.",
                        url: "marionette.behaviors.html#behaviorslookup"
                    })
                },
                getBehaviorClass: function(t, n) {
                    return t.behaviorClass ? t.behaviorClass : e._getValue(r.behaviorsLookup, this, [t, n])[n]
                },
                parseBehaviors: function(e, n) {
                    return t.chain(n).map(function(n, i) {
                        var s = r.getBehaviorClass(n, i),
                            o = new s(n, e),
                            u = r.parseBehaviors(e, t.result(o, "behaviors"));
                        return [o].concat(u)
                    }).flatten().value()
                },
                wrap: function(e, n, r) {
                    t.each(r, function(r) {
                        e[r] = t.partial(i[r], e[r], n)
                    })
                }
            }), t.extend(s.prototype, {
                buildBehaviorTriggers: function() {
                    return t.each(this._behaviors, this._buildTriggerHandlersForBehavior, this), this._triggers
                },
                _buildTriggerHandlersForBehavior: function(n, r) {
                    var i = t.clone(t.result(n, "triggers")) || {};
                    i = e.normalizeUIKeys(i, o(n)), t.each(i, t.bind(this._setHandlerForBehavior, this, n, r))
                },
                _setHandlerForBehavior: function(e, t, n, r) {
                    var i = r.replace(/^\S+/, function(e) {
                        return e + "." + "behaviortriggers" + t
                    });
                    this._triggers[i] = this._view._buildViewTrigger(n)
                }
            }), r
        }(s, n), s.AppRouter = t.Router.extend({
            constructor: function(e) {
                this.options = e || {}, t.Router.apply(this, arguments);
                var n = this.getOption("appRoutes"),
                    r = this._getController();
                this.processAppRoutes(r, n), this.on("route", this._processOnRoute, this)
            },
            appRoute: function(e, t) {
                var n = this._getController();
                this._addAppRoute(n, e, t)
            },
            _processOnRoute: function(e, t) {
                if (n.isFunction(this.onRoute)) {
                    var r = n.invert(this.getOption("appRoutes"))[e];
                    this.onRoute(e, r, t)
                }
            },
            processAppRoutes: function(e, t) {
                if (!t) return;
                var r = n.keys(t).reverse();
                n.each(r, function(n) {
                    this._addAppRoute(e, n, t[n])
                }, this)
            },
            _getController: function() {
                return this.getOption("controller")
            },
            _addAppRoute: function(e, t, r) {
                var i = e[r];
                if (!i) throw new s.Error('Method "' + r + '" was not found on the controller');
                this.route(t, r, n.bind(i, e))
            },
            mergeOptions: s.mergeOptions,
            getOption: s.proxyGetOption,
            triggerMethod: s.triggerMethod,
            bindEntityEvents: s.proxyBindEntityEvents,
            unbindEntityEvents: s.proxyUnbindEntityEvents
        }), s.Application = s.Object.extend({
            constructor: function(e) {
                this._initializeRegions(e), this._initCallbacks = new s.Callbacks, this.submodules = {}, n.extend(this, e), this._initChannel(), s.Object.call(this, e)
            },
            execute: function() {
                this.commands.execute.apply(this.commands, arguments)
            },
            request: function() {
                return this.reqres.request.apply(this.reqres, arguments)
            },
            addInitializer: function(e) {
                this._initCallbacks.add(e)
            },
            start: function(e) {
                this.triggerMethod("before:start", e), this._initCallbacks.run(e, this), this.triggerMethod("start", e)
            },
            addRegions: function(e) {
                return this._regionManager.addRegions(e)
            },
            emptyRegions: function() {
                return this._regionManager.emptyRegions()
            },
            removeRegion: function(e) {
                return this._regionManager.removeRegion(e)
            },
            getRegion: function(e) {
                return this._regionManager.get(e)
            },
            getRegions: function() {
                return this._regionManager.getRegions()
            },
            module: function(e, t) {
                var r = s.Module.getClass(t),
                    i = n.toArray(arguments);
                return i.unshift(this), r.create.apply(r, i)
            },
            getRegionManager: function() {
                return new s.RegionManager
            },
            _initializeRegions: function(e) {
                var t = n.isFunction(this.regions) ? this.regions(e) : this.regions || {};
                this._initRegionManager();
                var r = s.getOption(e, "regions");
                return n.isFunction(r) && (r = r.call(this, e)), n.extend(t, r), this.addRegions(t), this
            },
            _initRegionManager: function() {
                this._regionManager = this.getRegionManager(), this._regionManager._parent = this, this.listenTo(this._regionManager, "before:add:region", function() {
                    s._triggerMethod(this, "before:add:region", arguments)
                }), this.listenTo(this._regionManager, "add:region", function(e, t) {
                    this[e] = t, s._triggerMethod(this, "add:region", arguments)
                }), this.listenTo(this._regionManager, "before:remove:region", function() {
                    s._triggerMethod(this, "before:remove:region", arguments)
                }), this.listenTo(this._regionManager, "remove:region", function(e) {
                    delete this[e], s._triggerMethod(this, "remove:region", arguments)
                })
            },
            _initChannel: function() {
                this.channelName = n.result(this, "channelName") || "global", this.channel = n.result(this, "channel") || t.Wreqr.radio.channel(this.channelName), this.vent = n.result(this, "vent") || this.channel.vent, this.commands = n.result(this, "commands") || this.channel.commands, this.reqres = n.result(this, "reqres") || this.channel.reqres
            }
        }), s.Module = function(e, t, r) {
            this.moduleName = e, this.options = n.extend({}, this.options, r), this.initialize = r.initialize || this.initialize, this.submodules = {}, this._setupInitializersAndFinalizers(), this.app = t, n.isFunction(this.initialize) && this.initialize(e, t, this.options)
        }, s.Module.extend = s.extend, n.extend(s.Module.prototype, t.Events, {
            startWithParent: !0,
            initialize: function() {},
            addInitializer: function(e) {
                this._initializerCallbacks.add(e)
            },
            addFinalizer: function(e) {
                this._finalizerCallbacks.add(e)
            },
            start: function(e) {
                if (this._isInitialized) return;
                n.each(this.submodules, function(t) {
                    t.startWithParent && t.start(e)
                }), this.triggerMethod("before:start", e), this._initializerCallbacks.run(e, this), this._isInitialized = !0, this.triggerMethod("start", e)
            },
            stop: function() {
                if (!this._isInitialized) return;
                this._isInitialized = !1, this.triggerMethod("before:stop"), n.invoke(this.submodules, "stop"), this._finalizerCallbacks.run(undefined, this), this._initializerCallbacks.reset(), this._finalizerCallbacks.reset(), this.triggerMethod("stop")
            },
            addDefinition: function(e, t) {
                this._runModuleDefinition(e, t)
            },
            _runModuleDefinition: function(e, r) {
                if (!e) return;
                var i = n.flatten([this, this.app, t, s, t.$, n, r]);
                e.apply(this, i)
            },
            _setupInitializersAndFinalizers: function() {
                this._initializerCallbacks = new s.Callbacks, this._finalizerCallbacks = new s.Callbacks
            },
            triggerMethod: s.triggerMethod
        }), n.extend(s.Module, {
            create: function(e, t, r) {
                var i = e,
                    s = n.drop(arguments, 3);
                t = t.split(".");
                var o = t.length,
                    u = [];
                return u[o - 1] = r, n.each(t, function(t, n) {
                    var o = i;
                    i = this._getModule(o, t, e, r), this._addModuleDefinition(o, i, u[n], s)
                }, this), i
            },
            _getModule: function(e, t, r, i, s) {
                var o = n.extend({}, i),
                    u = this.getClass(i),
                    a = e[t];
                return a || (a = new u(t, r, o), e[t] = a, e.submodules[t] = a), a
            },
            getClass: function(e) {
                var t = s.Module;
                return e ? e.prototype instanceof t ? e : e.moduleClass || t : t
            },
            _addModuleDefinition: function(e, t, n, r) {
                var i = this._getDefine(n),
                    s = this._getStartWithParent(n, t);
                i && t.addDefinition(i, r), this._addStartWithParent(e, t, s)
            },
            _getStartWithParent: function(e, t) {
                var r;
                return n.isFunction(e) && e.prototype instanceof s.Module ? (r = t.constructor.prototype.startWithParent, n.isUndefined(r) ? !0 : r) : n.isObject(e) ? (r = e.startWithParent, n.isUndefined(r) ? !0 : r) : !0
            },
            _getDefine: function(e) {
                return !n.isFunction(e) || e.prototype instanceof s.Module ? n.isObject(e) ? e.define : null : e
            },
            _addStartWithParent: function(e, t, n) {
                t.startWithParent = t.startWithParent && n;
                if (!t.startWithParent || !!t.startWithParentIsConfigured) return;
                t.startWithParentIsConfigured = !0, e.addInitializer(function(e) {
                    t.startWithParent && t.start(e)
                })
            }
        }), s
    }), + function(e) {
        "use strict";

        function t() {
            var e = document.createElement("bootstrap"),
                t = {
                    WebkitTransition: "webkitTransitionEnd",
                    MozTransition: "transitionend",
                    OTransition: "oTransitionEnd otransitionend",
                    transition: "transitionend"
                };
            for (var n in t)
                if (e.style[n] !== undefined) return {
                    end: t[n]
                };
            return !1
        }
        e.fn.emulateTransitionEnd = function(t) {
            var n = !1,
                r = this;
            e(this).one("bsTransitionEnd", function() {
                n = !0
            });
            var i = function() {
                n || e(r).trigger(e.support.transition.end)
            };
            return setTimeout(i, t), this
        }, e(function() {
            e.support.transition = t();
            if (!e.support.transition) return;
            e.event.special.bsTransitionEnd = {
                bindType: e.support.transition.end,
                delegateType: e.support.transition.end,
                handle: function(t) {
                    if (e(t.target).is(this)) return t.handleObj.handler.apply(this, arguments)
                }
            }
        })
    }(jQuery), define("bootstrap.transition", ["jquery"], function() {}), window.matchMedia || (window.matchMedia = function() {
        "use strict";
        var e = window.styleMedia || window.media;
        if (!e) {
            var t = document.createElement("style"),
                n = document.getElementsByTagName("script")[0],
                r = null;
            t.type = "text/css", t.id = "matchmediajs-test", n.parentNode.insertBefore(t, n), r = "getComputedStyle" in window && window.getComputedStyle(t, null) || t.currentStyle, e = {
                matchMedium: function(e) {
                    var n = "@media " + e + "{ #matchmediajs-test { width: 1px; } }";
                    return t.styleSheet ? t.styleSheet.cssText = n : t.textContent = n, r.width === "1px"
                }
            }
        }
        return function(t) {
            return {
                matches: e.matchMedium(t || "all"),
                media: t || "all"
            }
        }
    }()), define("matchMedia", function() {}),
    function() {
        if (window.matchMedia && window.matchMedia("all").addListener) return !1;
        var e = window.matchMedia,
            t = e("only all").matches,
            n = !1,
            r = 0,
            i = [],
            s = function(t) {
                clearTimeout(r), r = setTimeout(function() {
                    for (var t = 0, n = i.length; t < n; t++) {
                        var r = i[t].mql,
                            s = i[t].listeners || [],
                            o = e(r.media).matches;
                        if (o !== r.matches) {
                            r.matches = o;
                            for (var u = 0, a = s.length; u < a; u++) s[u].call(window, r)
                        }
                    }
                }, 30)
            };
        window.matchMedia = function(r) {
            var o = e(r),
                u = [],
                a = 0;
            return o.addListener = function(e) {
                if (!t) return;
                n || (n = !0, window.addEventListener("resize", s, !0)), a === 0 && (a = i.push({
                    mql: o,
                    listeners: u
                })), u.push(e)
            }, o.removeListener = function(e) {
                for (var t = 0, n = u.length; t < n; t++) u[t] === e && u.splice(t, 1)
            }, o
        }
    }(), define("matchMediaListener", ["matchMedia"], function() {}),
    function() {
        var e = 0,
            t = ["ms", "moz", "webkit", "o"];
        for (var n = 0; n < t.length && !window.requestAnimationFrame; ++n) window.requestAnimationFrame = window[t[n] + "RequestAnimationFrame"], window.cancelAnimationFrame = window[t[n] + "CancelAnimationFrame"] || window[t[n] + "CancelRequestAnimationFrame"];
        window.requestAnimationFrame || (window.requestAnimationFrame = function(t, n) {
            var r = (new Date).getTime(),
                i = Math.max(0, 16 - (r - e)),
                s = window.setTimeout(function() {
                    t(r + i)
                }, i);
            return e = r + i, s
        }), window.cancelAnimationFrame || (window.cancelAnimationFrame = function(e) {
            clearTimeout(e)
        })
    }(), define("rAF", function() {}), define("vent", ["backbone.wreqr"], function(e) {
        return new e.EventAggregator
    }), define("app", ["backbone", "marionette", "vent"], function(e, t, n) {
        var r = new e.Marionette.Application;
        return r.on("start", function() {
            e.history.loadUrl()
        }), r
    }), define("routers/Router", ["backbone", "marionette"], function(e, t) {
        return e.Marionette.AppRouter.extend({
            urlHistory: [],
            appRoutes: _.extend({
                "(/)": "about",
                "project/*path(/)": "project",
                "publications(/)": "publication",
                "publications/*path(/)": "publication",
                "*path": "page"
            }),
            onRoute: function(e, t, n) {
                this.urlHistory.push(t)
            }
        })
    });
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function() {
        "use strict";
        _gsScope._gsDefine("TweenMax", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function(e, t, n) {
                var r = function(e) {
                        var t, n = [],
                            r = e.length;
                        for (t = 0; t !== r; n.push(e[t++]));
                        return n
                    },
                    i = function(e, t, r) {
                        n.call(this, e, t, r), this._cycle = 0, this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._dirty = !0, this.render = i.prototype.render
                    },
                    s = 1e-10,
                    o = n._internals,
                    u = o.isSelector,
                    a = o.isArray,
                    f = i.prototype = n.to({}, .1, {}),
                    l = [];
                i.version = "1.15.0", f.constructor = i, f.kill()._gc = !1, i.killTweensOf = i.killDelayedCallsTo = n.killTweensOf, i.getTweensOf = n.getTweensOf, i.lagSmoothing = n.lagSmoothing, i.ticker = n.ticker, i.render = n.render, f.invalidate = function() {
                    return this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), n.prototype.invalidate.call(this)
                }, f.updateTo = function(e, t) {
                    var r, i = this.ratio,
                        s = this.vars.immediateRender || e.immediateRender;
                    t && this._startTime < this._timeline._time && (this._startTime = this._timeline._time, this._uncache(!1), this._gc ? this._enabled(!0, !1) : this._timeline.insert(this, this._startTime - this._delay));
                    for (r in e) this.vars[r] = e[r];
                    if (this._initted || s)
                        if (t) this._initted = !1, s && this.render(0, !0, !0);
                        else if (this._gc && this._enabled(!0, !1), this._notifyPluginsOfEnabled && this._firstPT && n._onPluginEvent("_onDisable", this), this._time / this._duration > .998) {
                        var o = this._time;
                        this.render(0, !0, !1), this._initted = !1, this.render(o, !0, !1)
                    } else if (this._time > 0 || s) {
                        this._initted = !1, this._init();
                        for (var u, a = 1 / (1 - i), f = this._firstPT; f;) u = f.s + f.c, f.c *= a, f.s = u - f.c, f = f._next
                    }
                    return this
                }, f.render = function(e, t, n) {
                    this._initted || 0 === this._duration && this.vars.repeat && this.invalidate();
                    var r, i, u, a, f, c, h, p, d = this._dirty ? this.totalDuration() : this._totalDuration,
                        v = this._time,
                        m = this._totalTime,
                        g = this._cycle,
                        y = this._duration,
                        b = this._rawPrevTime;
                    if (e >= d ? (this._totalTime = d, this._cycle = this._repeat, this._yoyo && 0 !== (1 & this._cycle) ? (this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0) : (this._time = y, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1), this._reversed || (r = !0, i = "onComplete"), 0 === y && (this._initted || !this.vars.lazy || n) && (this._startTime === this._timeline._duration && (e = 0), (0 === e || 0 > b || b === s) && b !== e && (n = !0, b > s && (i = "onReverseComplete")), this._rawPrevTime = p = !t || e || b === e ? e : s)) : 1e-7 > e ? (this._totalTime = this._time = this._cycle = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== m || 0 === y && b > 0 && b !== s) && (i = "onReverseComplete", r = this._reversed), 0 > e && (this._active = !1, 0 === y && (this._initted || !this.vars.lazy || n) && (b >= 0 && (n = !0), this._rawPrevTime = p = !t || e || b === e ? e : s)), this._initted || (n = !0)) : (this._totalTime = this._time = e, 0 !== this._repeat && (a = y + this._repeatDelay, this._cycle = this._totalTime / a >> 0, 0 !== this._cycle && this._cycle === this._totalTime / a && this._cycle--, this._time = this._totalTime - this._cycle * a, this._yoyo && 0 !== (1 & this._cycle) && (this._time = y - this._time), this._time > y ? this._time = y : 0 > this._time && (this._time = 0)), this._easeType ? (f = this._time / y, c = this._easeType, h = this._easePower, (1 === c || 3 === c && f >= .5) && (f = 1 - f), 3 === c && (f *= 2), 1 === h ? f *= f : 2 === h ? f *= f * f : 3 === h ? f *= f * f * f : 4 === h && (f *= f * f * f * f), this.ratio = 1 === c ? 1 - f : 2 === c ? f : .5 > this._time / y ? f / 2 : 1 - f / 2) : this.ratio = this._ease.getRatio(this._time / y)), v === this._time && !n && g === this._cycle) return m !== this._totalTime && this._onUpdate && (t || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || l)), void 0;
                    if (!this._initted) {
                        if (this._init(), !this._initted || this._gc) return;
                        if (!n && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration)) return this._time = v, this._totalTime = m, this._rawPrevTime = b, this._cycle = g, o.lazyTweens.push(this), this._lazy = [e, t], void 0;
                        this._time && !r ? this.ratio = this._ease.getRatio(this._time / y) : r && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
                    }
                    for (this._lazy !== !1 && (this._lazy = !1), this._active || !this._paused && this._time !== v && e >= 0 && (this._active = !0), 0 === m && (2 === this._initted && e > 0 && this._init(), this._startAt && (e >= 0 ? this._startAt.render(e, t, n) : i || (i = "_dummyGS")), this.vars.onStart && (0 !== this._totalTime || 0 === y) && (t || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || l))), u = this._firstPT; u;) u.f ? u.t[u.p](u.c * this.ratio + u.s) : u.t[u.p] = u.c * this.ratio + u.s, u = u._next;
                    this._onUpdate && (0 > e && this._startAt && this._startTime && this._startAt.render(e, t, n), t || (this._totalTime !== m || r) && this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || l)), this._cycle !== g && (t || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || l)), i && (!this._gc || n) && (0 > e && this._startAt && !this._onUpdate && this._startTime && this._startAt.render(e, t, n), r && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !t && this.vars[i] && this.vars[i].apply(this.vars[i + "Scope"] || this, this.vars[i + "Params"] || l), 0 === y && this._rawPrevTime === s && p !== s && (this._rawPrevTime = 0))
                }, i.to = function(e, t, n) {
                    return new i(e, t, n)
                }, i.from = function(e, t, n) {
                    return n.runBackwards = !0, n.immediateRender = 0 != n.immediateRender, new i(e, t, n)
                }, i.fromTo = function(e, t, n, r) {
                    return r.startAt = n, r.immediateRender = 0 != r.immediateRender && 0 != n.immediateRender, new i(e, t, r)
                }, i.staggerTo = i.allTo = function(e, t, s, o, f, c, h) {
                    o = o || 0;
                    var p, d, v, m, g = s.delay || 0,
                        y = [],
                        b = function() {
                            s.onComplete && s.onComplete.apply(s.onCompleteScope || this, arguments), f.apply(h || this, c || l)
                        };
                    for (a(e) || ("string" == typeof e && (e = n.selector(e) || e), u(e) && (e = r(e))), e = e || [], 0 > o && (e = r(e), e.reverse(), o *= -1), p = e.length - 1, v = 0; p >= v; v++) {
                        d = {};
                        for (m in s) d[m] = s[m];
                        d.delay = g, v === p && f && (d.onComplete = b), y[v] = new i(e[v], t, d), g += o
                    }
                    return y
                }, i.staggerFrom = i.allFrom = function(e, t, n, r, s, o, u) {
                    return n.runBackwards = !0, n.immediateRender = 0 != n.immediateRender, i.staggerTo(e, t, n, r, s, o, u)
                }, i.staggerFromTo = i.allFromTo = function(e, t, n, r, s, o, u, a) {
                    return r.startAt = n, r.immediateRender = 0 != r.immediateRender && 0 != n.immediateRender, i.staggerTo(e, t, r, s, o, u, a)
                }, i.delayedCall = function(e, t, n, r, s) {
                    return new i(t, 0, {
                        delay: e,
                        onComplete: t,
                        onCompleteParams: n,
                        onCompleteScope: r,
                        onReverseComplete: t,
                        onReverseCompleteParams: n,
                        onReverseCompleteScope: r,
                        immediateRender: !1,
                        useFrames: s,
                        overwrite: 0
                    })
                }, i.set = function(e, t) {
                    return new i(e, 0, t)
                }, i.isTweening = function(e) {
                    return n.getTweensOf(e, !0).length > 0
                };
                var c = function(e, t) {
                        for (var r = [], i = 0, s = e._first; s;) s instanceof n ? r[i++] = s : (t && (r[i++] = s), r = r.concat(c(s, t)), i = r.length), s = s._next;
                        return r
                    },
                    h = i.getAllTweens = function(t) {
                        return c(e._rootTimeline, t).concat(c(e._rootFramesTimeline, t))
                    };
                i.killAll = function(e, n, r, i) {
                    null == n && (n = !0), null == r && (r = !0);
                    var s, o, u, a = h(0 != i),
                        f = a.length,
                        l = n && r && i;
                    for (u = 0; f > u; u++) o = a[u], (l || o instanceof t || (s = o.target === o.vars.onComplete) && r || n && !s) && (e ? o.totalTime(o._reversed ? 0 : o.totalDuration()) : o._enabled(!1, !1))
                }, i.killChildTweensOf = function(e, t) {
                    if (null != e) {
                        var s, f, l, c, h, p = o.tweenLookup;
                        if ("string" == typeof e && (e = n.selector(e) || e), u(e) && (e = r(e)), a(e))
                            for (c = e.length; --c > -1;) i.killChildTweensOf(e[c], t);
                        else {
                            s = [];
                            for (l in p)
                                for (f = p[l].target.parentNode; f;) f === e && (s = s.concat(p[l].tweens)), f = f.parentNode;
                            for (h = s.length, c = 0; h > c; c++) t && s[c].totalTime(s[c].totalDuration()), s[c]._enabled(!1, !1)
                        }
                    }
                };
                var p = function(e, n, r, i) {
                    n = n !== !1, r = r !== !1, i = i !== !1;
                    for (var s, o, u = h(i), a = n && r && i, f = u.length; --f > -1;) o = u[f], (a || o instanceof t || (s = o.target === o.vars.onComplete) && r || n && !s) && o.paused(e)
                };
                return i.pauseAll = function(e, t, n) {
                    p(!0, e, t, n)
                }, i.resumeAll = function(e, t, n) {
                    p(!1, e, t, n)
                }, i.globalTimeScale = function(t) {
                    var r = e._rootTimeline,
                        i = n.ticker.time;
                    return arguments.length ? (t = t || s, r._startTime = i - (i - r._startTime) * r._timeScale / t, r = e._rootFramesTimeline, i = n.ticker.frame, r._startTime = i - (i - r._startTime) * r._timeScale / t, r._timeScale = e._rootTimeline._timeScale = t, t) : r._timeScale
                }, f.progress = function(e) {
                    return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - e : e) + this._cycle * (this._duration + this._repeatDelay), !1) : this._time / this.duration()
                }, f.totalProgress = function(e) {
                    return arguments.length ? this.totalTime(this.totalDuration() * e, !1) : this._totalTime / this.totalDuration()
                }, f.time = function(e, t) {
                    return arguments.length ? (this._dirty && this.totalDuration(), e > this._duration && (e = this._duration), this._yoyo && 0 !== (1 & this._cycle) ? e = this._duration - e + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (e += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(e, t)) : this._time
                }, f.duration = function(t) {
                    return arguments.length ? e.prototype.duration.call(this, t) : this._duration
                }, f.totalDuration = function(e) {
                    return arguments.length ? -1 === this._repeat ? this : this.duration((e - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat, this._dirty = !1), this._totalDuration)
                }, f.repeat = function(e) {
                    return arguments.length ? (this._repeat = e, this._uncache(!0)) : this._repeat
                }, f.repeatDelay = function(e) {
                    return arguments.length ? (this._repeatDelay = e, this._uncache(!0)) : this._repeatDelay
                }, f.yoyo = function(e) {
                    return arguments.length ? (this._yoyo = e, this) : this._yoyo
                }, i
            }, !0), _gsScope._gsDefine("TimelineLite", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function(e, t, n) {
                var r = function(e) {
                        t.call(this, e), this._labels = {}, this.autoRemoveChildren = this.vars.autoRemoveChildren === !0, this.smoothChildTiming = this.vars.smoothChildTiming === !0, this._sortChildren = !0, this._onUpdate = this.vars.onUpdate;
                        var n, r, i = this.vars;
                        for (r in i) n = i[r], u(n) && -1 !== n.join("").indexOf("{self}") && (i[r] = this._swapSelfInParams(n));
                        u(i.tweens) && this.add(i.tweens, 0, i.align, i.stagger)
                    },
                    i = 1e-10,
                    s = n._internals,
                    o = s.isSelector,
                    u = s.isArray,
                    a = s.lazyTweens,
                    f = s.lazyRender,
                    l = [],
                    c = _gsScope._gsDefine.globals,
                    h = function(e) {
                        var t, n = {};
                        for (t in e) n[t] = e[t];
                        return n
                    },
                    p = function(e, t, n, r) {
                        var i = e._timeline,
                            s = i._totalTime;
                        !t && this._forcingPlayhead || i._rawPrevTime === e._startTime || (i.pause(e._startTime), t && t.apply(r || i, n || l), this._forcingPlayhead && i.seek(s))
                    },
                    d = function(e) {
                        var t, n = [],
                            r = e.length;
                        for (t = 0; t !== r; n.push(e[t++]));
                        return n
                    },
                    v = r.prototype = new t;
                return r.version = "1.15.0", v.constructor = r, v.kill()._gc = v._forcingPlayhead = !1, v.to = function(e, t, r, i) {
                    var s = r.repeat && c.TweenMax || n;
                    return t ? this.add(new s(e, t, r), i) : this.set(e, r, i)
                }, v.from = function(e, t, r, i) {
                    return this.add((r.repeat && c.TweenMax || n).from(e, t, r), i)
                }, v.fromTo = function(e, t, r, i, s) {
                    var o = i.repeat && c.TweenMax || n;
                    return t ? this.add(o.fromTo(e, t, r, i), s) : this.set(e, i, s)
                }, v.staggerTo = function(e, t, i, s, u, a, f, l) {
                    var c, p = new r({
                        onComplete: a,
                        onCompleteParams: f,
                        onCompleteScope: l,
                        smoothChildTiming: this.smoothChildTiming
                    });
                    for ("string" == typeof e && (e = n.selector(e) || e), e = e || [], o(e) && (e = d(e)), s = s || 0, 0 > s && (e = d(e), e.reverse(), s *= -1), c = 0; e.length > c; c++) i.startAt && (i.startAt = h(i.startAt)), p.to(e[c], t, h(i), c * s);
                    return this.add(p, u)
                }, v.staggerFrom = function(e, t, n, r, i, s, o, u) {
                    return n.immediateRender = 0 != n.immediateRender, n.runBackwards = !0, this.staggerTo(e, t, n, r, i, s, o, u)
                }, v.staggerFromTo = function(e, t, n, r, i, s, o, u, a) {
                    return r.startAt = n, r.immediateRender = 0 != r.immediateRender && 0 != n.immediateRender, this.staggerTo(e, t, r, i, s, o, u, a)
                }, v.call = function(e, t, r, i) {
                    return this.add(n.delayedCall(0, e, t, r), i)
                }, v.set = function(e, t, r) {
                    return r = this._parseTimeOrLabel(r, 0, !0), null == t.immediateRender && (t.immediateRender = r === this._time && !this._paused), this.add(new n(e, 0, t), r)
                }, r.exportRoot = function(e, t) {
                    e = e || {}, null == e.smoothChildTiming && (e.smoothChildTiming = !0);
                    var i, s, o = new r(e),
                        u = o._timeline;
                    for (null == t && (t = !0), u._remove(o, !0), o._startTime = 0, o._rawPrevTime = o._time = o._totalTime = u._time, i = u._first; i;) s = i._next, t && i instanceof n && i.target === i.vars.onComplete || o.add(i, i._startTime - i._delay), i = s;
                    return u.add(o, 0), o
                }, v.add = function(i, s, o, a) {
                    var f, l, c, h, p, d;
                    if ("number" != typeof s && (s = this._parseTimeOrLabel(s, 0, !0, i)), !(i instanceof e)) {
                        if (i instanceof Array || i && i.push && u(i)) {
                            for (o = o || "normal", a = a || 0, f = s, l = i.length, c = 0; l > c; c++) u(h = i[c]) && (h = new r({
                                tweens: h
                            })), this.add(h, f), "string" != typeof h && "function" != typeof h && ("sequence" === o ? f = h._startTime + h.totalDuration() / h._timeScale : "start" === o && (h._startTime -= h.delay())), f += a;
                            return this._uncache(!0)
                        }
                        if ("string" == typeof i) return this.addLabel(i, s);
                        if ("function" != typeof i) throw "Cannot add " + i + " into the timeline; it is not a tween, timeline, function, or string.";
                        i = n.delayedCall(0, i)
                    }
                    if (t.prototype.add.call(this, i, s), (this._gc || this._time === this._duration) && !this._paused && this._duration < this.duration())
                        for (p = this, d = p.rawTime() > i._startTime; p._timeline;) d && p._timeline.smoothChildTiming ? p.totalTime(p._totalTime, !0) : p._gc && p._enabled(!0, !1), p = p._timeline;
                    return this
                }, v.remove = function(t) {
                    if (t instanceof e) return this._remove(t, !1);
                    if (t instanceof Array || t && t.push && u(t)) {
                        for (var n = t.length; --n > -1;) this.remove(t[n]);
                        return this
                    }
                    return "string" == typeof t ? this.removeLabel(t) : this.kill(null, t)
                }, v._remove = function(e, n) {
                    t.prototype._remove.call(this, e, n);
                    var r = this._last;
                    return r ? this._time > r._startTime + r._totalDuration / r._timeScale && (this._time = this.duration(), this._totalTime = this._totalDuration) : this._time = this._totalTime = this._duration = this._totalDuration = 0, this
                }, v.append = function(e, t) {
                    return this.add(e, this._parseTimeOrLabel(null, t, !0, e))
                }, v.insert = v.insertMultiple = function(e, t, n, r) {
                    return this.add(e, t || 0, n, r)
                }, v.appendMultiple = function(e, t, n, r) {
                    return this.add(e, this._parseTimeOrLabel(null, t, !0, e), n, r)
                }, v.addLabel = function(e, t) {
                    return this._labels[e] = this._parseTimeOrLabel(t), this
                }, v.addPause = function(e, t, r, i) {
                    var s = n.delayedCall(0, p, ["{self}", t, r, i], this);
                    return s.data = "isPause", this.add(s, e)
                }, v.removeLabel = function(e) {
                    return delete this._labels[e], this
                }, v.getLabelTime = function(e) {
                    return null != this._labels[e] ? this._labels[e] : -1
                }, v._parseTimeOrLabel = function(t, n, r, i) {
                    var s;
                    if (i instanceof e && i.timeline === this) this.remove(i);
                    else if (i && (i instanceof Array || i.push && u(i)))
                        for (s = i.length; --s > -1;) i[s] instanceof e && i[s].timeline === this && this.remove(i[s]);
                    if ("string" == typeof n) return this._parseTimeOrLabel(n, r && "number" == typeof t && null == this._labels[n] ? t - this.duration() : 0, r);
                    if (n = n || 0, "string" != typeof t || !isNaN(t) && null == this._labels[t]) null == t && (t = this.duration());
                    else {
                        if (s = t.indexOf("="), -1 === s) return null == this._labels[t] ? r ? this._labels[t] = this.duration() + n : n : this._labels[t] + n;
                        n = parseInt(t.charAt(s - 1) + "1", 10) * Number(t.substr(s + 1)), t = s > 1 ? this._parseTimeOrLabel(t.substr(0, s - 1), 0, r) : this.duration()
                    }
                    return Number(t) + n
                }, v.seek = function(e, t) {
                    return this.totalTime("number" == typeof e ? e : this._parseTimeOrLabel(e), t !== !1)
                }, v.stop = function() {
                    return this.paused(!0)
                }, v.gotoAndPlay = function(e, t) {
                    return this.play(e, t)
                }, v.gotoAndStop = function(e, t) {
                    return this.pause(e, t)
                }, v.render = function(e, t, n) {
                    this._gc && this._enabled(!0, !1);
                    var r, s, o, u, c, h = this._dirty ? this.totalDuration() : this._totalDuration,
                        p = this._time,
                        d = this._startTime,
                        v = this._timeScale,
                        m = this._paused;
                    if (e >= h ? (this._totalTime = this._time = h, this._reversed || this._hasPausedChild() || (s = !0, u = "onComplete", 0 === this._duration && (0 === e || 0 > this._rawPrevTime || this._rawPrevTime === i) && this._rawPrevTime !== e && this._first && (c = !0, this._rawPrevTime > i && (u = "onReverseComplete"))), this._rawPrevTime = this._duration || !t || e || this._rawPrevTime === e ? e : i, e = h + 1e-4) : 1e-7 > e ? (this._totalTime = this._time = 0, (0 !== p || 0 === this._duration && this._rawPrevTime !== i && (this._rawPrevTime > 0 || 0 > e && this._rawPrevTime >= 0)) && (u = "onReverseComplete", s = this._reversed), 0 > e ? (this._active = !1, this._rawPrevTime >= 0 && this._first && (c = !0), this._rawPrevTime = e) : (this._rawPrevTime = this._duration || !t || e || this._rawPrevTime === e ? e : i, e = 0, this._initted || (c = !0))) : this._totalTime = this._time = this._rawPrevTime = e, this._time !== p && this._first || n || c) {
                        if (this._initted || (this._initted = !0), this._active || !this._paused && this._time !== p && e > 0 && (this._active = !0), 0 === p && this.vars.onStart && 0 !== this._time && (t || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || l)), this._time >= p)
                            for (r = this._first; r && (o = r._next, !this._paused || m);)(r._active || r._startTime <= this._time && !r._paused && !r._gc) && (r._reversed ? r.render((r._dirty ? r.totalDuration() : r._totalDuration) - (e - r._startTime) * r._timeScale, t, n) : r.render((e - r._startTime) * r._timeScale, t, n)), r = o;
                        else
                            for (r = this._last; r && (o = r._prev, !this._paused || m);)(r._active || p >= r._startTime && !r._paused && !r._gc) && (r._reversed ? r.render((r._dirty ? r.totalDuration() : r._totalDuration) - (e - r._startTime) * r._timeScale, t, n) : r.render((e - r._startTime) * r._timeScale, t, n)), r = o;
                        this._onUpdate && (t || (a.length && f(), this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || l))), u && (this._gc || (d === this._startTime || v !== this._timeScale) && (0 === this._time || h >= this.totalDuration()) && (s && (a.length && f(), this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !t && this.vars[u] && this.vars[u].apply(this.vars[u + "Scope"] || this, this.vars[u + "Params"] || l)))
                    }
                }, v._hasPausedChild = function() {
                    for (var e = this._first; e;) {
                        if (e._paused || e instanceof r && e._hasPausedChild()) return !0;
                        e = e._next
                    }
                    return !1
                }, v.getChildren = function(e, t, r, i) {
                    i = i || -9999999999;
                    for (var s = [], o = this._first, u = 0; o;) i > o._startTime || (o instanceof n ? t !== !1 && (s[u++] = o) : (r !== !1 && (s[u++] = o), e !== !1 && (s = s.concat(o.getChildren(!0, t, r)), u = s.length))), o = o._next;
                    return s
                }, v.getTweensOf = function(e, t) {
                    var r, i, s = this._gc,
                        o = [],
                        u = 0;
                    for (s && this._enabled(!0, !0), r = n.getTweensOf(e), i = r.length; --i > -1;)(r[i].timeline === this || t && this._contains(r[i])) && (o[u++] = r[i]);
                    return s && this._enabled(!1, !0), o
                }, v.recent = function() {
                    return this._recent
                }, v._contains = function(e) {
                    for (var t = e.timeline; t;) {
                        if (t === this) return !0;
                        t = t.timeline
                    }
                    return !1
                }, v.shiftChildren = function(e, t, n) {
                    n = n || 0;
                    for (var r, i = this._first, s = this._labels; i;) i._startTime >= n && (i._startTime += e), i = i._next;
                    if (t)
                        for (r in s) s[r] >= n && (s[r] += e);
                    return this._uncache(!0)
                }, v._kill = function(e, t) {
                    if (!e && !t) return this._enabled(!1, !1);
                    for (var n = t ? this.getTweensOf(t) : this.getChildren(!0, !0, !1), r = n.length, i = !1; --r > -1;) n[r]._kill(e, t) && (i = !0);
                    return i
                }, v.clear = function(e) {
                    var t = this.getChildren(!1, !0, !0),
                        n = t.length;
                    for (this._time = this._totalTime = 0; --n > -1;) t[n]._enabled(!1, !1);
                    return e !== !1 && (this._labels = {}), this._uncache(!0)
                }, v.invalidate = function() {
                    for (var t = this._first; t;) t.invalidate(), t = t._next;
                    return e.prototype.invalidate.call(this)
                }, v._enabled = function(e, n) {
                    if (e === this._gc)
                        for (var r = this._first; r;) r._enabled(e, !0), r = r._next;
                    return t.prototype._enabled.call(this, e, n)
                }, v.totalTime = function() {
                    this._forcingPlayhead = !0;
                    var t = e.prototype.totalTime.apply(this, arguments);
                    return this._forcingPlayhead = !1, t
                }, v.duration = function(e) {
                    return arguments.length ? (0 !== this.duration() && 0 !== e && this.timeScale(this._duration / e), this) : (this._dirty && this.totalDuration(), this._duration)
                }, v.totalDuration = function(e) {
                    if (!arguments.length) {
                        if (this._dirty) {
                            for (var t, n, r = 0, i = this._last, s = 999999999999; i;) t = i._prev, i._dirty && i.totalDuration(), i._startTime > s && this._sortChildren && !i._paused ? this.add(i, i._startTime - i._delay) : s = i._startTime, 0 > i._startTime && !i._paused && (r -= i._startTime, this._timeline.smoothChildTiming && (this._startTime += i._startTime / this._timeScale), this.shiftChildren(-i._startTime, !1, -9999999999), s = 0), n = i._startTime + i._totalDuration / i._timeScale, n > r && (r = n), i = t;
                            this._duration = this._totalDuration = r, this._dirty = !1
                        }
                        return this._totalDuration
                    }
                    return 0 !== this.totalDuration() && 0 !== e && this.timeScale(this._totalDuration / e), this
                }, v.usesFrames = function() {
                    for (var t = this._timeline; t._timeline;) t = t._timeline;
                    return t === e._rootFramesTimeline
                }, v.rawTime = function() {
                    return this._paused ? this._totalTime : (this._timeline.rawTime() - this._startTime) * this._timeScale
                }, r
            }, !0), _gsScope._gsDefine("TimelineMax", ["TimelineLite", "TweenLite", "easing.Ease"], function(e, t, n) {
                var r = function(t) {
                        e.call(this, t), this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._cycle = 0, this._yoyo = this.vars.yoyo === !0, this._dirty = !0
                    },
                    i = 1e-10,
                    s = [],
                    o = t._internals,
                    u = o.lazyTweens,
                    a = o.lazyRender,
                    f = new n(null, null, 1, 0),
                    l = r.prototype = new e;
                return l.constructor = r, l.kill()._gc = !1, r.version = "1.15.0", l.invalidate = function() {
                    return this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), e.prototype.invalidate.call(this)
                }, l.addCallback = function(e, n, r, i) {
                    return this.add(t.delayedCall(0, e, r, i), n)
                }, l.removeCallback = function(e, t) {
                    if (e)
                        if (null == t) this._kill(null, e);
                        else
                            for (var n = this.getTweensOf(e, !1), r = n.length, i = this._parseTimeOrLabel(t); --r > -1;) n[r]._startTime === i && n[r]._enabled(!1, !1);
                    return this
                }, l.tweenTo = function(e, n) {
                    n = n || {};
                    var r, i, o, u = {
                        ease: f,
                        overwrite: n.delay ? 2 : 1,
                        useFrames: this.usesFrames(),
                        immediateRender: !1
                    };
                    for (i in n) u[i] = n[i];
                    return u.time = this._parseTimeOrLabel(e), r = Math.abs(Number(u.time) - this._time) / this._timeScale || .001, o = new t(this, r, u), u.onStart = function() {
                        o.target.paused(!0), o.vars.time !== o.target.time() && r === o.duration() && o.duration(Math.abs(o.vars.time - o.target.time()) / o.target._timeScale), n.onStart && n.onStart.apply(n.onStartScope || o, n.onStartParams || s)
                    }, o
                }, l.tweenFromTo = function(e, t, n) {
                    n = n || {}, e = this._parseTimeOrLabel(e), n.startAt = {
                        onComplete: this.seek,
                        onCompleteParams: [e],
                        onCompleteScope: this
                    }, n.immediateRender = n.immediateRender !== !1;
                    var r = this.tweenTo(t, n);
                    return r.duration(Math.abs(r.vars.time - e) / this._timeScale || .001)
                }, l.render = function(e, t, n) {
                    this._gc && this._enabled(!0, !1);
                    var r, o, f, l, c, p, d = this._dirty ? this.totalDuration() : this._totalDuration,
                        v = this._duration,
                        m = this._time,
                        g = this._totalTime,
                        y = this._startTime,
                        b = this._timeScale,
                        w = this._rawPrevTime,
                        E = this._paused,
                        S = this._cycle;
                    if (e >= d ? (this._locked || (this._totalTime = d, this._cycle = this._repeat), this._reversed || this._hasPausedChild() || (o = !0, l = "onComplete", 0 === this._duration && (0 === e || 0 > w || w === i) && w !== e && this._first && (c = !0, w > i && (l = "onReverseComplete"))), this._rawPrevTime = this._duration || !t || e || this._rawPrevTime === e ? e : i, this._yoyo && 0 !== (1 & this._cycle) ? this._time = e = 0 : (this._time = v, e = v + 1e-4)) : 1e-7 > e ? (this._locked || (this._totalTime = this._cycle = 0), this._time = 0, (0 !== m || 0 === v && w !== i && (w > 0 || 0 > e && w >= 0) && !this._locked) && (l = "onReverseComplete", o = this._reversed), 0 > e ? (this._active = !1, w >= 0 && this._first && (c = !0), this._rawPrevTime = e) : (this._rawPrevTime = v || !t || e || this._rawPrevTime === e ? e : i, e = 0, this._initted || (c = !0))) : (0 === v && 0 > w && (c = !0), this._time = this._rawPrevTime = e, this._locked || (this._totalTime = e, 0 !== this._repeat && (p = v + this._repeatDelay, this._cycle = this._totalTime / p >> 0, 0 !== this._cycle && this._cycle === this._totalTime / p && this._cycle--, this._time = this._totalTime - this._cycle * p, this._yoyo && 0 !== (1 & this._cycle) && (this._time = v - this._time), this._time > v ? (this._time = v, e = v + 1e-4) : 0 > this._time ? this._time = e = 0 : e = this._time))), this._cycle !== S && !this._locked) {
                        var x = this._yoyo && 0 !== (1 & S),
                            T = x === (this._yoyo && 0 !== (1 & this._cycle)),
                            N = this._totalTime,
                            C = this._cycle,
                            k = this._rawPrevTime,
                            L = this._time;
                        if (this._totalTime = S * v, S > this._cycle ? x = !x : this._totalTime += v, this._time = m, this._rawPrevTime = 0 === v ? w - 1e-4 : w, this._cycle = S, this._locked = !0, m = x ? 0 : v, this.render(m, t, 0 === v), t || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || s), T && (m = x ? v + 1e-4 : -0.0001, this.render(m, !0, !1)), this._locked = !1, this._paused && !E) return;
                        this._time = L, this._totalTime = N, this._cycle = C, this._rawPrevTime = k
                    }
                    if (!(this._time !== m && this._first || n || c)) return g !== this._totalTime && this._onUpdate && (t || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || s)), void 0;
                    if (this._initted || (this._initted = !0), this._active || !this._paused && this._totalTime !== g && e > 0 && (this._active = !0), 0 === g && this.vars.onStart && 0 !== this._totalTime && (t || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || s)), this._time >= m)
                        for (r = this._first; r && (f = r._next, !this._paused || E);)(r._active || r._startTime <= this._time && !r._paused && !r._gc) && (r._reversed ? r.render((r._dirty ? r.totalDuration() : r._totalDuration) - (e - r._startTime) * r._timeScale, t, n) : r.render((e - r._startTime) * r._timeScale, t, n)), r = f;
                    else
                        for (r = this._last; r && (f = r._prev, !this._paused || E);)(r._active || m >= r._startTime && !r._paused && !r._gc) && (r._reversed ? r.render((r._dirty ? r.totalDuration() : r._totalDuration) - (e - r._startTime) * r._timeScale, t, n) : r.render((e - r._startTime) * r._timeScale, t, n)), r = f;
                    this._onUpdate && (t || (u.length && a(), this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || s))), l && (this._locked || this._gc || (y === this._startTime || b !== this._timeScale) && (0 === this._time || d >= this.totalDuration()) && (o && (u.length && a(), this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !t && this.vars[l] && this.vars[l].apply(this.vars[l + "Scope"] || this, this.vars[l + "Params"] || s)))
                }, l.getActive = function(e, t, n) {
                    null == e && (e = !0), null == t && (t = !0), null == n && (n = !1);
                    var r, i, s = [],
                        o = this.getChildren(e, t, n),
                        u = 0,
                        a = o.length;
                    for (r = 0; a > r; r++) i = o[r], i.isActive() && (s[u++] = i);
                    return s
                }, l.getLabelAfter = function(e) {
                    e || 0 !== e && (e = this._time);
                    var t, n = this.getLabelsArray(),
                        r = n.length;
                    for (t = 0; r > t; t++)
                        if (n[t].time > e) return n[t].name;
                    return null
                }, l.getLabelBefore = function(e) {
                    null == e && (e = this._time);
                    for (var t = this.getLabelsArray(), n = t.length; --n > -1;)
                        if (e > t[n].time) return t[n].name;
                    return null
                }, l.getLabelsArray = function() {
                    var e, t = [],
                        n = 0;
                    for (e in this._labels) t[n++] = {
                        time: this._labels[e],
                        name: e
                    };
                    return t.sort(function(e, t) {
                        return e.time - t.time
                    }), t
                }, l.progress = function(e, t) {
                    return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - e : e) + this._cycle * (this._duration + this._repeatDelay), t) : this._time / this.duration()
                }, l.totalProgress = function(e, t) {
                    return arguments.length ? this.totalTime(this.totalDuration() * e, t) : this._totalTime / this.totalDuration()
                }, l.totalDuration = function(t) {
                    return arguments.length ? -1 === this._repeat ? this : this.duration((t - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (e.prototype.totalDuration.call(this), this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat), this._totalDuration)
                }, l.time = function(e, t) {
                    return arguments.length ? (this._dirty && this.totalDuration(), e > this._duration && (e = this._duration), this._yoyo && 0 !== (1 & this._cycle) ? e = this._duration - e + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (e += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(e, t)) : this._time
                }, l.repeat = function(e) {
                    return arguments.length ? (this._repeat = e, this._uncache(!0)) : this._repeat
                }, l.repeatDelay = function(e) {
                    return arguments.length ? (this._repeatDelay = e, this._uncache(!0)) : this._repeatDelay
                }, l.yoyo = function(e) {
                    return arguments.length ? (this._yoyo = e, this) : this._yoyo
                }, l.currentLabel = function(e) {
                    return arguments.length ? this.seek(e, !0) : this.getLabelBefore(this._time + 1e-8)
                }, r
            }, !0),
            function() {
                var e = 180 / Math.PI,
                    t = [],
                    n = [],
                    r = [],
                    i = {},
                    s = _gsScope._gsDefine.globals,
                    o = function(e, t, n, r) {
                        this.a = e, this.b = t, this.c = n, this.d = r, this.da = r - e, this.ca = n - e, this.ba = t - e
                    },
                    u = ",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,",
                    a = function(e, t, n, r) {
                        var i = {
                                a: e
                            },
                            s = {},
                            o = {},
                            u = {
                                c: r
                            },
                            a = (e + t) / 2,
                            f = (t + n) / 2,
                            l = (n + r) / 2,
                            c = (a + f) / 2,
                            h = (f + l) / 2,
                            p = (h - c) / 8;
                        return i.b = a + (e - a) / 4, s.b = c + p, i.c = s.a = (i.b + s.b) / 2, s.c = o.a = (c + h) / 2, o.b = h - p, u.b = l + (r - l) / 4, o.c = u.a = (o.b + u.b) / 2, [i, s, o, u]
                    },
                    f = function(e, i, s, o, u) {
                        var f, l, c, h, p, d, v, m, g, y, b, w, E, S = e.length - 1,
                            x = 0,
                            T = e[0].a;
                        for (f = 0; S > f; f++) p = e[x], l = p.a, c = p.d, h = e[x + 1].d, u ? (b = t[f], w = n[f], E = .25 * (w + b) * i / (o ? .5 : r[f] || .5), d = c - (c - l) * (o ? .5 * i : 0 !== b ? E / b : 0), v = c + (h - c) * (o ? .5 * i : 0 !== w ? E / w : 0), m = c - (d + ((v - d) * (3 * b / (b + w) + .5) / 4 || 0))) : (d = c - .5 * (c - l) * i, v = c + .5 * (h - c) * i, m = c - (d + v) / 2), d += m, v += m, p.c = g = d, p.b = 0 !== f ? T : T = p.a + .6 * (p.c - p.a), p.da = c - l, p.ca = g - l, p.ba = T - l, s ? (y = a(l, T, g, c), e.splice(x, 1, y[0], y[1], y[2], y[3]), x += 4) : x++, T = v;
                        p = e[x], p.b = T, p.c = T + .4 * (p.d - T), p.da = p.d - p.a, p.ca = p.c - p.a, p.ba = T - p.a, s && (y = a(p.a, T, p.c, p.d), e.splice(x, 1, y[0], y[1], y[2], y[3]))
                    },
                    l = function(e, r, i, s) {
                        var u, a, f, l, c, h, p = [];
                        if (s)
                            for (e = [s].concat(e), a = e.length; --a > -1;) "string" == typeof(h = e[a][r]) && "=" === h.charAt(1) && (e[a][r] = s[r] + Number(h.charAt(0) + h.substr(2)));
                        if (u = e.length - 2, 0 > u) return p[0] = new o(e[0][r], 0, 0, e[-1 > u ? 0 : 1][r]), p;
                        for (a = 0; u > a; a++) f = e[a][r], l = e[a + 1][r], p[a] = new o(f, 0, 0, l), i && (c = e[a + 2][r], t[a] = (t[a] || 0) + (l - f) * (l - f), n[a] = (n[a] || 0) + (c - l) * (c - l));
                        return p[a] = new o(e[a][r], 0, 0, e[a + 1][r]), p
                    },
                    c = function(e, s, o, a, c, h) {
                        var p, d, v, m, g, y, b, w, E = {},
                            S = [],
                            x = h || e[0];
                        c = "string" == typeof c ? "," + c + "," : u, null == s && (s = 1);
                        for (d in e[0]) S.push(d);
                        if (e.length > 1) {
                            for (w = e[e.length - 1], b = !0, p = S.length; --p > -1;)
                                if (d = S[p], Math.abs(x[d] - w[d]) > .05) {
                                    b = !1;
                                    break
                                }
                            b && (e = e.concat(), h && e.unshift(h), e.push(e[1]), h = e[e.length - 3])
                        }
                        for (t.length = n.length = r.length = 0, p = S.length; --p > -1;) d = S[p], i[d] = -1 !== c.indexOf("," + d + ","), E[d] = l(e, d, i[d], h);
                        for (p = t.length; --p > -1;) t[p] = Math.sqrt(t[p]), n[p] = Math.sqrt(n[p]);
                        if (!a) {
                            for (p = S.length; --p > -1;)
                                if (i[d])
                                    for (v = E[S[p]], y = v.length - 1, m = 0; y > m; m++) g = v[m + 1].da / n[m] + v[m].da / t[m], r[m] = (r[m] || 0) + g * g;
                            for (p = r.length; --p > -1;) r[p] = Math.sqrt(r[p])
                        }
                        for (p = S.length, m = o ? 4 : 1; --p > -1;) d = S[p], v = E[d], f(v, s, o, a, i[d]), b && (v.splice(0, m), v.splice(v.length - m, m));
                        return E
                    },
                    h = function(e, t, n) {
                        t = t || "soft";
                        var r, i, s, u, a, f, l, c, h, p, d, v = {},
                            m = "cubic" === t ? 3 : 2,
                            g = "soft" === t,
                            y = [];
                        if (g && n && (e = [n].concat(e)), null == e || m + 1 > e.length) throw "invalid Bezier data";
                        for (h in e[0]) y.push(h);
                        for (f = y.length; --f > -1;) {
                            for (h = y[f], v[h] = a = [], p = 0, c = e.length, l = 0; c > l; l++) r = null == n ? e[l][h] : "string" == typeof(d = e[l][h]) && "=" === d.charAt(1) ? n[h] + Number(d.charAt(0) + d.substr(2)) : Number(d), g && l > 1 && c - 1 > l && (a[p++] = (r + a[p - 2]) / 2), a[p++] = r;
                            for (c = p - m + 1, p = 0, l = 0; c > l; l += m) r = a[l], i = a[l + 1], s = a[l + 2], u = 2 === m ? 0 : a[l + 3], a[p++] = d = 3 === m ? new o(r, i, s, u) : new o(r, (2 * i + r) / 3, (2 * i + s) / 3, s);
                            a.length = p
                        }
                        return v
                    },
                    p = function(e, t, n) {
                        for (var r, i, s, o, u, a, f, l, c, h, p, d = 1 / n, v = e.length; --v > -1;)
                            for (h = e[v], s = h.a, o = h.d - s, u = h.c - s, a = h.b - s, r = i = 0, l = 1; n >= l; l++) f = d * l, c = 1 - f, r = i - (i = (f * f * o + 3 * c * (f * u + c * a)) * f), p = v * n + l - 1, t[p] = (t[p] || 0) + r * r
                    },
                    d = function(e, t) {
                        t = t >> 0 || 6;
                        var n, r, i, s, o = [],
                            u = [],
                            a = 0,
                            f = 0,
                            l = t - 1,
                            c = [],
                            h = [];
                        for (n in e) p(e[n], o, t);
                        for (i = o.length, r = 0; i > r; r++) a += Math.sqrt(o[r]), s = r % t, h[s] = a, s === l && (f += a, s = r / t >> 0, c[s] = h, u[s] = f, a = 0, h = []);
                        return {
                            length: f,
                            lengths: u,
                            segments: c
                        }
                    },
                    v = _gsScope._gsDefine.plugin({
                        propName: "bezier",
                        priority: -1,
                        version: "1.3.4",
                        API: 2,
                        global: !0,
                        init: function(e, t, n) {
                            this._target = e, t instanceof Array && (t = {
                                values: t
                            }), this._func = {}, this._round = {}, this._props = [], this._timeRes = null == t.timeResolution ? 6 : parseInt(t.timeResolution, 10);
                            var r, i, s, o, u, a = t.values || [],
                                f = {},
                                l = a[0],
                                p = t.autoRotate || n.vars.orientToBezier;
                            this._autoRotate = p ? p instanceof Array ? p : [
                                ["x", "y", "rotation", p === !0 ? 0 : Number(p) || 0]
                            ] : null;
                            for (r in l) this._props.push(r);
                            for (s = this._props.length; --s > -1;) r = this._props[s], this._overwriteProps.push(r), i = this._func[r] = "function" == typeof e[r], f[r] = i ? e[r.indexOf("set") || "function" != typeof e["get" + r.substr(3)] ? r : "get" + r.substr(3)]() : parseFloat(e[r]), u || f[r] !== a[0][r] && (u = f);
                            if (this._beziers = "cubic" !== t.type && "quadratic" !== t.type && "soft" !== t.type ? c(a, isNaN(t.curviness) ? 1 : t.curviness, !1, "thruBasic" === t.type, t.correlate, u) : h(a, t.type, f), this._segCount = this._beziers[r].length, this._timeRes) {
                                var v = d(this._beziers, this._timeRes);
                                this._length = v.length, this._lengths = v.lengths, this._segments = v.segments, this._l1 = this._li = this._s1 = this._si = 0, this._l2 = this._lengths[0], this._curSeg = this._segments[0], this._s2 = this._curSeg[0], this._prec = 1 / this._curSeg.length
                            }
                            if (p = this._autoRotate)
                                for (this._initialRotations = [], p[0] instanceof Array || (this._autoRotate = p = [p]), s = p.length; --s > -1;) {
                                    for (o = 0; 3 > o; o++) r = p[s][o], this._func[r] = "function" == typeof e[r] ? e[r.indexOf("set") || "function" != typeof e["get" + r.substr(3)] ? r : "get" + r.substr(3)] : !1;
                                    r = p[s][2], this._initialRotations[s] = this._func[r] ? this._func[r].call(this._target) : this._target[r]
                                }
                            return this._startRatio = n.vars.runBackwards ? 1 : 0, !0
                        },
                        set: function(t) {
                            var n, r, i, s, o, u, a, f, l, c, h = this._segCount,
                                p = this._func,
                                d = this._target,
                                v = t !== this._startRatio;
                            if (this._timeRes) {
                                if (l = this._lengths, c = this._curSeg, t *= this._length, i = this._li, t > this._l2 && h - 1 > i) {
                                    for (f = h - 1; f > i && t >= (this._l2 = l[++i]););
                                    this._l1 = l[i - 1], this._li = i, this._curSeg = c = this._segments[i], this._s2 = c[this._s1 = this._si = 0]
                                } else if (this._l1 > t && i > 0) {
                                    for (; i > 0 && (this._l1 = l[--i]) >= t;);
                                    0 === i && this._l1 > t ? this._l1 = 0 : i++, this._l2 = l[i], this._li = i, this._curSeg = c = this._segments[i], this._s1 = c[(this._si = c.length - 1) - 1] || 0, this._s2 = c[this._si]
                                }
                                if (n = i, t -= this._l1, i = this._si, t > this._s2 && c.length - 1 > i) {
                                    for (f = c.length - 1; f > i && t >= (this._s2 = c[++i]););
                                    this._s1 = c[i - 1], this._si = i
                                } else if (this._s1 > t && i > 0) {
                                    for (; i > 0 && (this._s1 = c[--i]) >= t;);
                                    0 === i && this._s1 > t ? this._s1 = 0 : i++, this._s2 = c[i], this._si = i
                                }
                                u = (i + (t - this._s1) / (this._s2 - this._s1)) * this._prec
                            } else n = 0 > t ? 0 : t >= 1 ? h - 1 : h * t >> 0, u = (t - n * (1 / h)) * h;
                            for (r = 1 - u, i = this._props.length; --i > -1;) s = this._props[i], o = this._beziers[s][n], a = (u * u * o.da + 3 * r * (u * o.ca + r * o.ba)) * u + o.a, this._round[s] && (a = Math.round(a)), p[s] ? d[s](a) : d[s] = a;
                            if (this._autoRotate) {
                                var m, g, y, b, w, E, S, x = this._autoRotate;
                                for (i = x.length; --i > -1;) s = x[i][2], E = x[i][3] || 0, S = x[i][4] === !0 ? 1 : e, o = this._beziers[x[i][0]], m = this._beziers[x[i][1]], o && m && (o = o[n], m = m[n], g = o.a + (o.b - o.a) * u, b = o.b + (o.c - o.b) * u, g += (b - g) * u, b += (o.c + (o.d - o.c) * u - b) * u, y = m.a + (m.b - m.a) * u, w = m.b + (m.c - m.b) * u, y += (w - y) * u, w += (m.c + (m.d - m.c) * u - w) * u, a = v ? Math.atan2(w - y, b - g) * S + E : this._initialRotations[i], p[s] ? d[s](a) : d[s] = a)
                            }
                        }
                    }),
                    m = v.prototype;
                v.bezierThrough = c, v.cubicToQuadratic = a, v._autoCSS = !0, v.quadraticToCubic = function(e, t, n) {
                    return new o(e, (2 * t + e) / 3, (2 * t + n) / 3, n)
                }, v._cssRegister = function() {
                    var e = s.CSSPlugin;
                    if (e) {
                        var t = e._internals,
                            n = t._parseToProxy,
                            r = t._setPluginRatio,
                            i = t.CSSPropTween;
                        t._registerComplexSpecialProp("bezier", {
                            parser: function(e, t, s, o, u, a) {
                                t instanceof Array && (t = {
                                    values: t
                                }), a = new v;
                                var f, l, c, h = t.values,
                                    p = h.length - 1,
                                    d = [],
                                    m = {};
                                if (0 > p) return u;
                                for (f = 0; p >= f; f++) c = n(e, h[f], o, u, a, p !== f), d[f] = c.end;
                                for (l in t) m[l] = t[l];
                                return m.values = d, u = new i(e, "bezier", 0, 0, c.pt, 2), u.data = c, u.plugin = a, u.setRatio = r, 0 === m.autoRotate && (m.autoRotate = !0), !m.autoRotate || m.autoRotate instanceof Array || (f = m.autoRotate === !0 ? 0 : Number(m.autoRotate), m.autoRotate = null != c.end.left ? [
                                    ["left", "top", "rotation", f, !1]
                                ] : null != c.end.x ? [
                                    ["x", "y", "rotation", f, !1]
                                ] : !1), m.autoRotate && (o._transform || o._enableTransforms(!1), c.autoRotate = o._target._gsTransform), a._onInitTween(c.proxy, m, o._tween), u
                            }
                        })
                    }
                }, m._roundProps = function(e, t) {
                    for (var n = this._overwriteProps, r = n.length; --r > -1;)(e[n[r]] || e.bezier || e.bezierThrough) && (this._round[n[r]] = t)
                }, m._kill = function(e) {
                    var t, n, r = this._props;
                    for (t in this._beziers)
                        if (t in e)
                            for (delete this._beziers[t], delete this._func[t], n = r.length; --n > -1;) r[n] === t && r.splice(n, 1);
                    return this._super._kill.call(this, e)
                }
            }(), _gsScope._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function(e, t) {
                var n, r, i, s, o = function() {
                        e.call(this, "css"), this._overwriteProps.length = 0, this.setRatio = o.prototype.setRatio
                    },
                    u = _gsScope._gsDefine.globals,
                    a = {},
                    f = o.prototype = new e("css");
                f.constructor = o, o.version = "1.15.0", o.API = 2, o.defaultTransformPerspective = 0, o.defaultSkewType = "compensated", f = "px", o.suffixMap = {
                    top: f,
                    right: f,
                    bottom: f,
                    left: f,
                    width: f,
                    height: f,
                    fontSize: f,
                    padding: f,
                    margin: f,
                    perspective: f,
                    lineHeight: ""
                };
                var l, c, h, p, d, v, m = /(?:\d|\-\d|\.\d|\-\.\d)+/g,
                    g = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,
                    y = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,
                    b = /(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g,
                    w = /(?:\d|\-|\+|=|#|\.)*/g,
                    E = /opacity *= *([^)]*)/i,
                    S = /opacity:([^;]*)/i,
                    x = /alpha\(opacity *=.+?\)/i,
                    T = /^(rgb|hsl)/,
                    N = /([A-Z])/g,
                    C = /-([a-z])/gi,
                    k = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,
                    L = function(e, t) {
                        return t.toUpperCase()
                    },
                    A = /(?:Left|Right|Width)/i,
                    O = /(M11|M12|M21|M22)=[\d\-\.e]+/gi,
                    M = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,
                    _ = /,(?=[^\)]*(?:\(|$))/gi,
                    D = Math.PI / 180,
                    P = 180 / Math.PI,
                    H = {},
                    B = document,
                    j = function(e) {
                        return B.createElementNS ? B.createElementNS("http://www.w3.org/1999/xhtml", e) : B.createElement(e)
                    },
                    F = j("div"),
                    I = j("img"),
                    q = o._internals = {
                        _specialProps: a
                    },
                    R = navigator.userAgent,
                    U = function() {
                        var e = R.indexOf("Android"),
                            t = j("a");
                        return h = -1 !== R.indexOf("Safari") && -1 === R.indexOf("Chrome") && (-1 === e || Number(R.substr(e + 8, 1)) > 3), d = h && 6 > Number(R.substr(R.indexOf("Version/") + 8, 1)), p = -1 !== R.indexOf("Firefox"), (/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(R) || /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(R)) && (v = parseFloat(RegExp.$1)), t ? (t.style.cssText = "top:1px;opacity:.55;", /^0.55/.test(t.style.opacity)) : !1
                    }(),
                    z = function(e) {
                        return E.test("string" == typeof e ? e : (e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1
                    },
                    W = function(e) {
                        window.console && console.log(e)
                    },
                    X = "",
                    V = "",
                    $ = function(e, t) {
                        t = t || F;
                        var n, r, i = t.style;
                        if (void 0 !== i[e]) return e;
                        for (e = e.charAt(0).toUpperCase() + e.substr(1), n = ["O", "Moz", "ms", "Ms", "Webkit"], r = 5; --r > -1 && void 0 === i[n[r] + e];);
                        return r >= 0 ? (V = 3 === r ? "ms" : n[r], X = "-" + V.toLowerCase() + "-", V + e) : null
                    },
                    J = B.defaultView ? B.defaultView.getComputedStyle : function() {},
                    K = o.getStyle = function(e, t, n, r, i) {
                        var s;
                        return U || "opacity" !== t ? (!r && e.style[t] ? s = e.style[t] : (n = n || J(e)) ? s = n[t] || n.getPropertyValue(t) || n.getPropertyValue(t.replace(N, "-$1").toLowerCase()) : e.currentStyle && (s = e.currentStyle[t]), null == i || s && "none" !== s && "auto" !== s && "auto auto" !== s ? s : i) : z(e)
                    },
                    Q = q.convertToPixels = function(e, n, r, i, s) {
                        if ("px" === i || !i) return r;
                        if ("auto" === i || !r) return 0;
                        var u, a, f, l = A.test(n),
                            c = e,
                            h = F.style,
                            p = 0 > r;
                        if (p && (r = -r), "%" === i && -1 !== n.indexOf("border")) u = r / 100 * (l ? e.clientWidth : e.clientHeight);
                        else {
                            if (h.cssText = "border:0 solid red;position:" + K(e, "position") + ";line-height:0;", "%" !== i && c.appendChild) h[l ? "borderLeftWidth" : "borderTopWidth"] = r + i;
                            else {
                                if (c = e.parentNode || B.body, a = c._gsCache, f = t.ticker.frame, a && l && a.time === f) return a.width * r / 100;
                                h[l ? "width" : "height"] = r + i
                            }
                            c.appendChild(F), u = parseFloat(F[l ? "offsetWidth" : "offsetHeight"]), c.removeChild(F), l && "%" === i && o.cacheWidths !== !1 && (a = c._gsCache = c._gsCache || {}, a.time = f, a.width = 100 * (u / r)), 0 !== u || s || (u = Q(e, n, r, i, !0))
                        }
                        return p ? -u : u
                    },
                    G = q.calculateOffset = function(e, t, n) {
                        if ("absolute" !== K(e, "position", n)) return 0;
                        var r = "left" === t ? "Left" : "Top",
                            i = K(e, "margin" + r, n);
                        return e["offset" + r] - (Q(e, t, parseFloat(i), i.replace(w, "")) || 0)
                    },
                    Y = function(e, t) {
                        var n, r, i = {};
                        if (t = t || J(e, null))
                            if (n = t.length)
                                for (; --n > -1;) i[t[n].replace(C, L)] = t.getPropertyValue(t[n]);
                            else
                                for (n in t) i[n] = t[n];
                        else if (t = e.currentStyle || e.style)
                            for (n in t) "string" == typeof n && void 0 === i[n] && (i[n.replace(C, L)] = t[n]);
                        return U || (i.opacity = z(e)), r = _t(e, t, !1), i.rotation = r.rotation, i.skewX = r.skewX, i.scaleX = r.scaleX, i.scaleY = r.scaleY, i.x = r.x, i.y = r.y, Nt && (i.z = r.z, i.rotationX = r.rotationX, i.rotationY = r.rotationY, i.scaleZ = r.scaleZ), i.filters && delete i.filters, i
                    },
                    Z = function(e, t, n, r, i) {
                        var s, o, u, a = {},
                            f = e.style;
                        for (o in n) "cssText" !== o && "length" !== o && isNaN(o) && (t[o] !== (s = n[o]) || i && i[o]) && -1 === o.indexOf("Origin") && ("number" == typeof s || "string" == typeof s) && (a[o] = "auto" !== s || "left" !== o && "top" !== o ? "" !== s && "auto" !== s && "none" !== s || "string" != typeof t[o] || "" === t[o].replace(b, "") ? s : 0 : G(e, o), void 0 !== f[o] && (u = new pt(f, o, f[o], u)));
                        if (r)
                            for (o in r) "className" !== o && (a[o] = r[o]);
                        return {
                            difs: a,
                            firstMPT: u
                        }
                    },
                    et = {
                        width: ["Left", "Right"],
                        height: ["Top", "Bottom"]
                    },
                    tt = ["marginLeft", "marginRight", "marginTop", "marginBottom"],
                    nt = function(e, t, n) {
                        var r = parseFloat("width" === t ? e.offsetWidth : e.offsetHeight),
                            i = et[t],
                            s = i.length;
                        for (n = n || J(e, null); --s > -1;) r -= parseFloat(K(e, "padding" + i[s], n, !0)) || 0, r -= parseFloat(K(e, "border" + i[s] + "Width", n, !0)) || 0;
                        return r
                    },
                    rt = function(e, t) {
                        (null == e || "" === e || "auto" === e || "auto auto" === e) && (e = "0 0");
                        var n = e.split(" "),
                            r = -1 !== e.indexOf("left") ? "0%" : -1 !== e.indexOf("right") ? "100%" : n[0],
                            i = -1 !== e.indexOf("top") ? "0%" : -1 !== e.indexOf("bottom") ? "100%" : n[1];
                        return null == i ? i = "0" : "center" === i && (i = "50%"), ("center" === r || isNaN(parseFloat(r)) && -1 === (r + "").indexOf("=")) && (r = "50%"), t && (t.oxp = -1 !== r.indexOf("%"), t.oyp = -1 !== i.indexOf("%"), t.oxr = "=" === r.charAt(1), t.oyr = "=" === i.charAt(1), t.ox = parseFloat(r.replace(b, "")), t.oy = parseFloat(i.replace(b, ""))), r + " " + i + (n.length > 2 ? " " + n[2] : "")
                    },
                    it = function(e, t) {
                        return "string" == typeof e && "=" === e.charAt(1) ? parseInt(e.charAt(0) + "1", 10) * parseFloat(e.substr(2)) : parseFloat(e) - parseFloat(t)
                    },
                    st = function(e, t) {
                        return null == e ? t : "string" == typeof e && "=" === e.charAt(1) ? parseInt(e.charAt(0) + "1", 10) * parseFloat(e.substr(2)) + t : parseFloat(e)
                    },
                    ot = function(e, t, n, r) {
                        var i, s, o, u, a = 1e-6;
                        return null == e ? u = t : "number" == typeof e ? u = e : (i = 360, s = e.split("_"), o = Number(s[0].replace(b, "")) * (-1 === e.indexOf("rad") ? 1 : P) - ("=" === e.charAt(1) ? 0 : t), s.length && (r && (r[n] = t + o), -1 !== e.indexOf("short") && (o %= i, o !== o % (i / 2) && (o = 0 > o ? o + i : o - i)), -1 !== e.indexOf("_cw") && 0 > o ? o = (o + 9999999999 * i) % i - (0 | o / i) * i : -1 !== e.indexOf("ccw") && o > 0 && (o = (o - 9999999999 * i) % i - (0 | o / i) * i)), u = t + o), a > u && u > -a && (u = 0), u
                    },
                    ut = {
                        aqua: [0, 255, 255],
                        lime: [0, 255, 0],
                        silver: [192, 192, 192],
                        black: [0, 0, 0],
                        maroon: [128, 0, 0],
                        teal: [0, 128, 128],
                        blue: [0, 0, 255],
                        navy: [0, 0, 128],
                        white: [255, 255, 255],
                        fuchsia: [255, 0, 255],
                        olive: [128, 128, 0],
                        yellow: [255, 255, 0],
                        orange: [255, 165, 0],
                        gray: [128, 128, 128],
                        purple: [128, 0, 128],
                        green: [0, 128, 0],
                        red: [255, 0, 0],
                        pink: [255, 192, 203],
                        cyan: [0, 255, 255],
                        transparent: [255, 255, 255, 0]
                    },
                    at = function(e, t, n) {
                        return e = 0 > e ? e + 1 : e > 1 ? e - 1 : e, 0 | 255 * (1 > 6 * e ? t + 6 * (n - t) * e : .5 > e ? n : 2 > 3 * e ? t + 6 * (n - t) * (2 / 3 - e) : t) + .5
                    },
                    ft = o.parseColor = function(e) {
                        var t, n, r, i, s, o;
                        return e && "" !== e ? "number" == typeof e ? [e >> 16, 255 & e >> 8, 255 & e] : ("," === e.charAt(e.length - 1) && (e = e.substr(0, e.length - 1)), ut[e] ? ut[e] : "#" === e.charAt(0) ? (4 === e.length && (t = e.charAt(1), n = e.charAt(2), r = e.charAt(3), e = "#" + t + t + n + n + r + r), e = parseInt(e.substr(1), 16), [e >> 16, 255 & e >> 8, 255 & e]) : "hsl" === e.substr(0, 3) ? (e = e.match(m), i = Number(e[0]) % 360 / 360, s = Number(e[1]) / 100, o = Number(e[2]) / 100, n = .5 >= o ? o * (s + 1) : o + s - o * s, t = 2 * o - n, e.length > 3 && (e[3] = Number(e[3])), e[0] = at(i + 1 / 3, t, n), e[1] = at(i, t, n), e[2] = at(i - 1 / 3, t, n), e) : (e = e.match(m) || ut.transparent, e[0] = Number(e[0]), e[1] = Number(e[1]), e[2] = Number(e[2]), e.length > 3 && (e[3] = Number(e[3])), e)) : ut.black
                    },
                    lt = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#.+?\\b";
                for (f in ut) lt += "|" + f + "\\b";
                lt = RegExp(lt + ")", "gi");
                var ct = function(e, t, n, r) {
                        if (null == e) return function(e) {
                            return e
                        };
                        var i, s = t ? (e.match(lt) || [""])[0] : "",
                            o = e.split(s).join("").match(y) || [],
                            u = e.substr(0, e.indexOf(o[0])),
                            a = ")" === e.charAt(e.length - 1) ? ")" : "",
                            f = -1 !== e.indexOf(" ") ? " " : ",",
                            l = o.length,
                            c = l > 0 ? o[0].replace(m, "") : "";
                        return l ? i = t ? function(e) {
                            var t, h, p, d;
                            if ("number" == typeof e) e += c;
                            else if (r && _.test(e)) {
                                for (d = e.replace(_, "|").split("|"), p = 0; d.length > p; p++) d[p] = i(d[p]);
                                return d.join(",")
                            }
                            if (t = (e.match(lt) || [s])[0], h = e.split(t).join("").match(y) || [], p = h.length, l > p--)
                                for (; l > ++p;) h[p] = n ? h[0 | (p - 1) / 2] : o[p];
                            return u + h.join(f) + f + t + a + (-1 !== e.indexOf("inset") ? " inset" : "")
                        } : function(e) {
                            var t, s, h;
                            if ("number" == typeof e) e += c;
                            else if (r && _.test(e)) {
                                for (s = e.replace(_, "|").split("|"), h = 0; s.length > h; h++) s[h] = i(s[h]);
                                return s.join(",")
                            }
                            if (t = e.match(y) || [], h = t.length, l > h--)
                                for (; l > ++h;) t[h] = n ? t[0 | (h - 1) / 2] : o[h];
                            return u + t.join(f) + a
                        } : function(e) {
                            return e
                        }
                    },
                    ht = function(e) {
                        return e = e.split(","),
                            function(t, n, r, i, s, o, u) {
                                var a, f = (n + "").split(" ");
                                for (u = {}, a = 0; 4 > a; a++) u[e[a]] = f[a] = f[a] || f[(a - 1) / 2 >> 0];
                                return i.parse(t, u, s, o)
                            }
                    },
                    pt = (q._setPluginRatio = function(e) {
                        this.plugin.setRatio(e);
                        for (var t, n, r, i, s = this.data, o = s.proxy, u = s.firstMPT, a = 1e-6; u;) t = o[u.v], u.r ? t = Math.round(t) : a > t && t > -a && (t = 0), u.t[u.p] = t, u = u._next;
                        if (s.autoRotate && (s.autoRotate.rotation = o.rotation), 1 === e)
                            for (u = s.firstMPT; u;) {
                                if (n = u.t, n.type) {
                                    if (1 === n.type) {
                                        for (i = n.xs0 + n.s + n.xs1, r = 1; n.l > r; r++) i += n["xn" + r] + n["xs" + (r + 1)];
                                        n.e = i
                                    }
                                } else n.e = n.s + n.xs0;
                                u = u._next
                            }
                    }, function(e, t, n, r, i) {
                        this.t = e, this.p = t, this.v = n, this.r = i, r && (r._prev = this, this._next = r)
                    }),
                    dt = (q._parseToProxy = function(e, t, n, r, i, s) {
                        var o, u, a, f, l, c = r,
                            h = {},
                            p = {},
                            d = n._transform,
                            v = H;
                        for (n._transform = null, H = t, r = l = n.parse(e, t, r, i), H = v, s && (n._transform = d, c && (c._prev = null, c._prev && (c._prev._next = null))); r && r !== c;) {
                            if (1 >= r.type && (u = r.p, p[u] = r.s + r.c, h[u] = r.s, s || (f = new pt(r, "s", u, f, r.r), r.c = 0), 1 === r.type))
                                for (o = r.l; --o > 0;) a = "xn" + o, u = r.p + "_" + a, p[u] = r.data[a], h[u] = r[a], s || (f = new pt(r, a, u, f, r.rxp[a]));
                            r = r._next
                        }
                        return {
                            proxy: h,
                            end: p,
                            firstMPT: f,
                            pt: l
                        }
                    }, q.CSSPropTween = function(e, t, r, i, o, u, a, f, l, c, h) {
                        this.t = e, this.p = t, this.s = r, this.c = i, this.n = a || t, e instanceof dt || s.push(this.n), this.r = f, this.type = u || 0, l && (this.pr = l, n = !0), this.b = void 0 === c ? r : c, this.e = void 0 === h ? r + i : h, o && (this._next = o, o._prev = this)
                    }),
                    vt = o.parseComplex = function(e, t, n, r, i, s, o, u, a, f) {
                        n = n || s || "", o = new dt(e, t, 0, 0, o, f ? 2 : 1, null, !1, u, n, r), r += "";
                        var c, h, p, d, v, y, b, w, E, S, x, N, C = n.split(", ").join(",").split(" "),
                            k = r.split(", ").join(",").split(" "),
                            L = C.length,
                            A = l !== !1;
                        for ((-1 !== r.indexOf(",") || -1 !== n.indexOf(",")) && (C = C.join(" ").replace(_, ", ").split(" "), k = k.join(" ").replace(_, ", ").split(" "), L = C.length), L !== k.length && (C = (s || "").split(" "), L = C.length), o.plugin = a, o.setRatio = f, c = 0; L > c; c++)
                            if (d = C[c], v = k[c], w = parseFloat(d), w || 0 === w) o.appendXtra("", w, it(v, w), v.replace(g, ""), A && -1 !== v.indexOf("px"), !0);
                            else if (i && ("#" === d.charAt(0) || ut[d] || T.test(d))) N = "," === v.charAt(v.length - 1) ? ")," : ")", d = ft(d), v = ft(v), E = d.length + v.length > 6, E && !U && 0 === v[3] ? (o["xs" + o.l] += o.l ? " transparent" : "transparent", o.e = o.e.split(k[c]).join("transparent")) : (U || (E = !1), o.appendXtra(E ? "rgba(" : "rgb(", d[0], v[0] - d[0], ",", !0, !0).appendXtra("", d[1], v[1] - d[1], ",", !0).appendXtra("", d[2], v[2] - d[2], E ? "," : N, !0), E && (d = 4 > d.length ? 1 : d[3], o.appendXtra("", d, (4 > v.length ? 1 : v[3]) - d, N, !1)));
                        else if (y = d.match(m)) {
                            if (b = v.match(g), !b || b.length !== y.length) return o;
                            for (p = 0, h = 0; y.length > h; h++) x = y[h], S = d.indexOf(x, p), o.appendXtra(d.substr(p, S - p), Number(x), it(b[h], x), "", A && "px" === d.substr(S + x.length, 2), 0 === h), p = S + x.length;
                            o["xs" + o.l] += d.substr(p)
                        } else o["xs" + o.l] += o.l ? " " + d : d;
                        if (-1 !== r.indexOf("=") && o.data) {
                            for (N = o.xs0 + o.data.s, c = 1; o.l > c; c++) N += o["xs" + c] + o.data["xn" + c];
                            o.e = N + o["xs" + c]
                        }
                        return o.l || (o.type = -1, o.xs0 = o.e), o.xfirst || o
                    },
                    mt = 9;
                for (f = dt.prototype, f.l = f.pr = 0; --mt > 0;) f["xn" + mt] = 0, f["xs" + mt] = "";
                f.xs0 = "", f._next = f._prev = f.xfirst = f.data = f.plugin = f.setRatio = f.rxp = null, f.appendXtra = function(e, t, n, r, i, s) {
                    var o = this,
                        u = o.l;
                    return o["xs" + u] += s && u ? " " + e : e || "", n || 0 === u || o.plugin ? (o.l++, o.type = o.setRatio ? 2 : 1, o["xs" + o.l] = r || "", u > 0 ? (o.data["xn" + u] = t + n, o.rxp["xn" + u] = i, o["xn" + u] = t, o.plugin || (o.xfirst = new dt(o, "xn" + u, t, n, o.xfirst || o, 0, o.n, i, o.pr), o.xfirst.xs0 = 0), o) : (o.data = {
                        s: t + n
                    }, o.rxp = {}, o.s = t, o.c = n, o.r = i, o)) : (o["xs" + u] += t + (r || ""), o)
                };
                var gt = function(e, t) {
                        t = t || {}, this.p = t.prefix ? $(e) || e : e, a[e] = a[this.p] = this, this.format = t.formatter || ct(t.defaultValue, t.color, t.collapsible, t.multi), t.parser && (this.parse = t.parser), this.clrs = t.color, this.multi = t.multi, this.keyword = t.keyword, this.dflt = t.defaultValue, this.pr = t.priority || 0
                    },
                    yt = q._registerComplexSpecialProp = function(e, t, n) {
                        "object" != typeof t && (t = {
                            parser: n
                        });
                        var r, i, s = e.split(","),
                            o = t.defaultValue;
                        for (n = n || [o], r = 0; s.length > r; r++) t.prefix = 0 === r && t.prefix, t.defaultValue = n[r] || o, i = new gt(s[r], t)
                    },
                    bt = function(e) {
                        if (!a[e]) {
                            var t = e.charAt(0).toUpperCase() + e.substr(1) + "Plugin";
                            yt(e, {
                                parser: function(e, n, r, i, s, o, f) {
                                    var l = u.com.greensock.plugins[t];
                                    return l ? (l._cssRegister(), a[r].parse(e, n, r, i, s, o, f)) : (W("Error: " + t + " js file not loaded."), s)
                                }
                            })
                        }
                    };
                f = gt.prototype, f.parseComplex = function(e, t, n, r, i, s) {
                    var o, u, a, f, l, c, h = this.keyword;
                    if (this.multi && (_.test(n) || _.test(t) ? (u = t.replace(_, "|").split("|"), a = n.replace(_, "|").split("|")) : h && (u = [t], a = [n])), a) {
                        for (f = a.length > u.length ? a.length : u.length, o = 0; f > o; o++) t = u[o] = u[o] || this.dflt, n = a[o] = a[o] || this.dflt, h && (l = t.indexOf(h), c = n.indexOf(h), l !== c && (n = -1 === c ? a : u, n[o] += " " + h));
                        t = u.join(", "), n = a.join(", ")
                    }
                    return vt(e, this.p, t, n, this.clrs, this.dflt, r, this.pr, i, s)
                }, f.parse = function(e, t, n, r, s, o) {
                    return this.parseComplex(e.style, this.format(K(e, this.p, i, !1, this.dflt)), this.format(t), s, o)
                }, o.registerSpecialProp = function(e, t, n) {
                    yt(e, {
                        parser: function(e, r, i, s, o, u) {
                            var a = new dt(e, i, 0, 0, o, 2, i, !1, n);
                            return a.plugin = u, a.setRatio = t(e, r, s._tween, i), a
                        },
                        priority: n
                    })
                };
                var wt, Et = "scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","),
                    St = $("transform"),
                    xt = X + "transform",
                    Tt = $("transformOrigin"),
                    Nt = null !== $("perspective"),
                    Ct = q.Transform = function() {
                        this.perspective = parseFloat(o.defaultTransformPerspective) || 0, this.force3D = o.defaultForce3D !== !1 && Nt ? o.defaultForce3D || "auto" : !1
                    },
                    kt = window.SVGElement,
                    Lt = function(e, t, n) {
                        var r, i = B.createElementNS("http://www.w3.org/2000/svg", e),
                            s = /([a-z])([A-Z])/g;
                        for (r in n) i.setAttributeNS(null, r.replace(s, "$1-$2").toLowerCase(), n[r]);
                        return t.appendChild(i), i
                    },
                    At = document.documentElement,
                    Ot = function() {
                        var e, t, n, r = v || /Android/i.test(R) && !window.chrome;
                        return B.createElementNS && !r && (e = Lt("svg", At), t = Lt("rect", e, {
                            width: 100,
                            height: 50,
                            x: 100
                        }), n = t.getBoundingClientRect().width, t.style[Tt] = "50% 50%", t.style[St] = "scaleX(0.5)", r = n === t.getBoundingClientRect().width, At.removeChild(e)), r
                    }(),
                    Mt = function(e, t, n) {
                        var r = e.getBBox();
                        t = rt(t).split(" "), n.xOrigin = (-1 !== t[0].indexOf("%") ? parseFloat(t[0]) / 100 * r.width : parseFloat(t[0])) + r.x, n.yOrigin = (-1 !== t[1].indexOf("%") ? parseFloat(t[1]) / 100 * r.height : parseFloat(t[1])) + r.y
                    },
                    _t = q.getTransform = function(e, t, n, r) {
                        if (e._gsTransform && n && !r) return e._gsTransform;
                        var s, u, a, f, l, c, h, p, d, v, m = n ? e._gsTransform || new Ct : new Ct,
                            g = 0 > m.scaleX,
                            y = 2e-5,
                            b = 1e5,
                            w = Nt ? parseFloat(K(e, Tt, t, !1, "0 0 0").split(" ")[2]) || m.zOrigin || 0 : 0,
                            E = parseFloat(o.defaultTransformPerspective) || 0;
                        if (St ? u = K(e, xt, t, !0) : e.currentStyle && (u = e.currentStyle.filter.match(O), u = u && 4 === u.length ? [u[0].substr(4), Number(u[2].substr(4)), Number(u[1].substr(4)), u[3].substr(4), m.x || 0, m.y || 0].join(",") : ""), s = !u || "none" === u || "matrix(1, 0, 0, 1, 0, 0)" === u, m.svg = !!(kt && "function" == typeof e.getBBox && e.getCTM && (!e.parentNode || e.parentNode.getBBox && e.parentNode.getCTM)), m.svg && (Mt(e, K(e, Tt, i, !1, "50% 50%") + "", m), wt = o.useSVGTransformAttr || Ot, a = e.getAttribute("transform"), s && a && -1 !== a.indexOf("matrix") && (u = a, s = 0)), !s) {
                            for (a = (u || "").match(/(?:\-|\b)[\d\-\.e]+\b/gi) || [], f = a.length; --f > -1;) l = Number(a[f]), a[f] = (c = l - (l |= 0)) ? (0 | c * b + (0 > c ? -0.5 : .5)) / b + l : l;
                            if (16 === a.length) {
                                var S = a[8],
                                    x = a[9],
                                    T = a[10],
                                    N = a[12],
                                    C = a[13],
                                    k = a[14];
                                m.zOrigin && (k = -m.zOrigin, N = S * k - a[12], C = x * k - a[13], k = T * k + m.zOrigin - a[14]);
                                var L, A, M, _, D, H = a[0],
                                    B = a[1],
                                    j = a[2],
                                    F = a[3],
                                    I = a[4],
                                    q = a[5],
                                    R = a[6],
                                    U = a[7],
                                    z = a[11],
                                    W = Math.atan2(B, q);
                                m.rotation = W * P, W && (_ = Math.cos(-W), D = Math.sin(-W), H = H * _ + I * D, A = B * _ + q * D, q = B * -D + q * _, R = j * -D + R * _, B = A), W = Math.atan2(S, H), m.rotationY = W * P, W && (_ = Math.cos(-W), D = Math.sin(-W), L = H * _ - S * D, A = B * _ - x * D, M = j * _ - T * D, x = B * D + x * _, T = j * D + T * _, z = F * D + z * _, H = L, B = A, j = M), W = Math.atan2(R, T), m.rotationX = W * P, W && (_ = Math.cos(-W), D = Math.sin(-W), L = I * _ + S * D, A = q * _ + x * D, M = R * _ + T * D, S = I * -D + S * _, x = q * -D + x * _, T = R * -D + T * _, z = U * -D + z * _, I = L, q = A, R = M), m.scaleX = (0 | Math.sqrt(H * H + B * B) * b + .5) / b, m.scaleY = (0 | Math.sqrt(q * q + x * x) * b + .5) / b, m.scaleZ = (0 | Math.sqrt(R * R + T * T) * b + .5) / b, m.skewX = 0, m.perspective = z ? 1 / (0 > z ? -z : z) : 0, m.x = N, m.y = C, m.z = k
                            } else if (!(Nt && !r && a.length && m.x === a[4] && m.y === a[5] && (m.rotationX || m.rotationY) || void 0 !== m.x && "none" === K(e, "display", t))) {
                                var X = a.length >= 6,
                                    V = X ? a[0] : 1,
                                    $ = a[1] || 0,
                                    J = a[2] || 0,
                                    Q = X ? a[3] : 1;
                                m.x = a[4] || 0, m.y = a[5] || 0, h = Math.sqrt(V * V + $ * $), p = Math.sqrt(Q * Q + J * J), d = V || $ ? Math.atan2($, V) * P : m.rotation || 0, v = J || Q ? Math.atan2(J, Q) * P + d : m.skewX || 0, Math.abs(v) > 90 && 270 > Math.abs(v) && (g ? (h *= -1, v += 0 >= d ? 180 : -180, d += 0 >= d ? 180 : -180) : (p *= -1, v += 0 >= v ? 180 : -180)), m.scaleX = h, m.scaleY = p, m.rotation = d, m.skewX = v, Nt && (m.rotationX = m.rotationY = m.z = 0, m.perspective = E, m.scaleZ = 1)
                            }
                            m.zOrigin = w;
                            for (f in m) y > m[f] && m[f] > -y && (m[f] = 0)
                        }
                        return n && (e._gsTransform = m), m
                    },
                    Dt = function(e) {
                        var t, n, r = this.data,
                            i = -r.rotation * D,
                            s = i + r.skewX * D,
                            o = 1e5,
                            u = (0 | Math.cos(i) * r.scaleX * o) / o,
                            a = (0 | Math.sin(i) * r.scaleX * o) / o,
                            f = (0 | Math.sin(s) * -r.scaleY * o) / o,
                            l = (0 | Math.cos(s) * r.scaleY * o) / o,
                            c = this.t.style,
                            h = this.t.currentStyle;
                        if (h) {
                            n = a, a = -f, f = -n, t = h.filter, c.filter = "";
                            var p, d, m = this.t.offsetWidth,
                                g = this.t.offsetHeight,
                                y = "absolute" !== h.position,
                                b = "progid:DXImageTransform.Microsoft.Matrix(M11=" + u + ", M12=" + a + ", M21=" + f + ", M22=" + l,
                                S = r.x + m * r.xPercent / 100,
                                x = r.y + g * r.yPercent / 100;
                            if (null != r.ox && (p = (r.oxp ? .01 * m * r.ox : r.ox) - m / 2, d = (r.oyp ? .01 * g * r.oy : r.oy) - g / 2, S += p - (p * u + d * a), x += d - (p * f + d * l)), y ? (p = m / 2, d = g / 2, b += ", Dx=" + (p - (p * u + d * a) + S) + ", Dy=" + (d - (p * f + d * l) + x) + ")") : b += ", sizingMethod='auto expand')", c.filter = -1 !== t.indexOf("DXImageTransform.Microsoft.Matrix(") ? t.replace(M, b) : b + " " + t, (0 === e || 1 === e) && 1 === u && 0 === a && 0 === f && 1 === l && (y && -1 === b.indexOf("Dx=0, Dy=0") || E.test(t) && 100 !== parseFloat(RegExp.$1) || -1 === t.indexOf(t.indexOf("Alpha")) && c.removeAttribute("filter")), !y) {
                                var T, N, C, k = 8 > v ? 1 : -1;
                                for (p = r.ieOffsetX || 0, d = r.ieOffsetY || 0, r.ieOffsetX = Math.round((m - ((0 > u ? -u : u) * m + (0 > a ? -a : a) * g)) / 2 + S), r.ieOffsetY = Math.round((g - ((0 > l ? -l : l) * g + (0 > f ? -f : f) * m)) / 2 + x), mt = 0; 4 > mt; mt++) N = tt[mt], T = h[N], n = -1 !== T.indexOf("px") ? parseFloat(T) : Q(this.t, N, parseFloat(T), T.replace(w, "")) || 0, C = n !== r[N] ? 2 > mt ? -r.ieOffsetX : -r.ieOffsetY : 2 > mt ? p - r.ieOffsetX : d - r.ieOffsetY, c[N] = (r[N] = Math.round(n - C * (0 === mt || 2 === mt ? 1 : k))) + "px"
                            }
                        }
                    },
                    Pt = q.set3DTransformRatio = function(e) {
                        var t, n, r, i, s, o, u, a, f, l, c, h, d, v, m, g, y, b, w, E, S, x, T, N, C, k = this.data,
                            L = this.t.style,
                            A = k.rotation * D,
                            O = k.scaleX,
                            M = k.scaleY,
                            _ = k.scaleZ,
                            P = k.x,
                            H = k.y,
                            B = k.z,
                            j = k.perspective;
                        if (!(1 !== e && 0 !== e || "auto" !== k.force3D || k.rotationY || k.rotationX || 1 !== _ || j || B)) return Ht.call(this, e), void 0;
                        if (p) {
                            var F = 1e-4;
                            F > O && O > -F && (O = _ = 2e-5), F > M && M > -F && (M = _ = 2e-5), !j || k.z || k.rotationX || k.rotationY || (j = 0)
                        }
                        if (A || k.skewX) b = Math.cos(A), w = Math.sin(A), t = b, s = w, k.skewX && (A -= k.skewX * D, b = Math.cos(A), w = Math.sin(A), "simple" === k.skewType && (E = Math.tan(k.skewX * D), E = Math.sqrt(1 + E * E), b *= E, w *= E)), n = -w, o = b;
                        else {
                            if (!(k.rotationY || k.rotationX || 1 !== _ || j || k.svg)) return L[St] = (k.xPercent || k.yPercent ? "translate(" + k.xPercent + "%," + k.yPercent + "%) translate3d(" : "translate3d(") + P + "px," + H + "px," + B + "px)" + (1 !== O || 1 !== M ? " scale(" + O + "," + M + ")" : ""), void 0;
                            t = o = 1, n = s = 0
                        }
                        c = 1, r = i = u = a = f = l = h = d = v = 0, m = j ? -1 / j : 0, g = k.zOrigin, y = 1e5, C = ",", A = k.rotationY * D, A && (b = Math.cos(A), w = Math.sin(A), f = c * -w, d = m * -w, r = t * w, u = s * w, c *= b, m *= b, t *= b, s *= b), A = k.rotationX * D, A && (b = Math.cos(A), w = Math.sin(A), E = n * b + r * w, S = o * b + u * w, x = l * b + c * w, T = v * b + m * w, r = n * -w + r * b, u = o * -w + u * b, c = l * -w + c * b, m = v * -w + m * b, n = E, o = S, l = x, v = T), 1 !== _ && (r *= _, u *= _, c *= _, m *= _), 1 !== M && (n *= M, o *= M, l *= M, v *= M), 1 !== O && (t *= O, s *= O, f *= O, d *= O), g && (h -= g, i = r * h, a = u * h, h = c * h + g), k.svg && (i += k.xOrigin - (k.xOrigin * t + k.yOrigin * n), a += k.yOrigin - (k.xOrigin * s + k.yOrigin * o)), i = (E = (i += P) - (i |= 0)) ? (0 | E * y + (0 > E ? -0.5 : .5)) / y + i : i, a = (E = (a += H) - (a |= 0)) ? (0 | E * y + (0 > E ? -0.5 : .5)) / y + a : a, h = (E = (h += B) - (h |= 0)) ? (0 | E * y + (0 > E ? -0.5 : .5)) / y + h : h, N = k.xPercent || k.yPercent ? "translate(" + k.xPercent + "%," + k.yPercent + "%) matrix3d(" : "matrix3d(", N += (0 | t * y) / y + C + (0 | s * y) / y + C + (0 | f * y) / y, N += C + (0 | d * y) / y + C + (0 | n * y) / y + C + (0 | o * y) / y, N += C + (0 | l * y) / y + C + (0 | v * y) / y + C + (0 | r * y) / y, N += C + (0 | u * y) / y + C + (0 | c * y) / y + C + (0 | m * y) / y, N += C + i + C + a + C + h + C + (j ? 1 + -h / j : 1) + ")", L[St] = N
                    },
                    Ht = q.set2DTransformRatio = function(e) {
                        var t, n, r, i, s, o, u, a, f, l, c, h = this.data,
                            p = this.t,
                            d = p.style,
                            v = h.x,
                            m = h.y;
                        return !(h.rotationX || h.rotationY || h.z || h.force3D === !0 || "auto" === h.force3D && 1 !== e && 0 !== e) || h.svg && wt || !Nt ? (i = h.scaleX, s = h.scaleY, h.rotation || h.skewX || h.svg ? (t = h.rotation * D, n = t - h.skewX * D, r = 1e5, o = Math.cos(t) * i, u = Math.sin(t) * i, a = Math.sin(n) * -s, f = Math.cos(n) * s, h.svg && (v += h.xOrigin - (h.xOrigin * o + h.yOrigin * a), m += h.yOrigin - (h.xOrigin * u + h.yOrigin * f), c = 1e-6, c > v && v > -c && (v = 0), c > m && m > -c && (m = 0)), l = (0 | o * r) / r + "," + (0 | u * r) / r + "," + (0 | a * r) / r + "," + (0 | f * r) / r + "," + v + "," + m + ")", h.svg && wt ? p.setAttribute("transform", "matrix(" + l) : d[St] = (h.xPercent || h.yPercent ? "translate(" + h.xPercent + "%," + h.yPercent + "%) matrix(" : "matrix(") + l) : d[St] = (h.xPercent || h.yPercent ? "translate(" + h.xPercent + "%," + h.yPercent + "%) matrix(" : "matrix(") + i + ",0,0," + s + "," + v + "," + m + ")", void 0) : (this.setRatio = Pt, Pt.call(this, e), void 0)
                    };
                f = Ct.prototype, f.x = f.y = f.z = f.skewX = f.skewY = f.rotation = f.rotationX = f.rotationY = f.zOrigin = f.xPercent = f.yPercent = 0, f.scaleX = f.scaleY = f.scaleZ = 1, yt("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent", {
                    parser: function(e, t, n, r, s, u, a) {
                        if (r._lastParsedTransform === a) return s;
                        r._lastParsedTransform = a;
                        var f, l, c, h, p, d, v, m = r._transform = _t(e, i, !0, a.parseTransform),
                            g = e.style,
                            y = 1e-6,
                            b = Et.length,
                            w = a,
                            E = {};
                        if ("string" == typeof w.transform && St) c = F.style, c[St] = w.transform, c.display = "block", c.position = "absolute", B.body.appendChild(F), f = _t(F, null, !1), B.body.removeChild(F);
                        else if ("object" == typeof w) {
                            if (f = {
                                    scaleX: st(null != w.scaleX ? w.scaleX : w.scale, m.scaleX),
                                    scaleY: st(null != w.scaleY ? w.scaleY : w.scale, m.scaleY),
                                    scaleZ: st(w.scaleZ, m.scaleZ),
                                    x: st(w.x, m.x),
                                    y: st(w.y, m.y),
                                    z: st(w.z, m.z),
                                    xPercent: st(w.xPercent, m.xPercent),
                                    yPercent: st(w.yPercent, m.yPercent),
                                    perspective: st(w.transformPerspective, m.perspective)
                                }, v = w.directionalRotation, null != v)
                                if ("object" == typeof v)
                                    for (c in v) w[c] = v[c];
                                else w.rotation = v;
                                "string" == typeof w.x && -1 !== w.x.indexOf("%") && (f.x = 0, f.xPercent = st(w.x, m.xPercent)), "string" == typeof w.y && -1 !== w.y.indexOf("%") && (f.y = 0, f.yPercent = st(w.y, m.yPercent)), f.rotation = ot("rotation" in w ? w.rotation : "shortRotation" in w ? w.shortRotation + "_short" : "rotationZ" in w ? w.rotationZ : m.rotation, m.rotation, "rotation", E), Nt && (f.rotationX = ot("rotationX" in w ? w.rotationX : "shortRotationX" in w ? w.shortRotationX + "_short" : m.rotationX || 0, m.rotationX, "rotationX", E), f.rotationY = ot("rotationY" in w ? w.rotationY : "shortRotationY" in w ? w.shortRotationY + "_short" : m.rotationY || 0, m.rotationY, "rotationY", E)), f.skewX = null == w.skewX ? m.skewX : ot(w.skewX, m.skewX), f.skewY = null == w.skewY ? m.skewY : ot(w.skewY, m.skewY), (l = f.skewY - m.skewY) && (f.skewX += l, f.rotation += l)
                        }
                        for (Nt && null != w.force3D && (m.force3D = w.force3D, d = !0), m.skewType = w.skewType || m.skewType || o.defaultSkewType, p = m.force3D || m.z || m.rotationX || m.rotationY || f.z || f.rotationX || f.rotationY || f.perspective, p || null == w.scale || (f.scaleZ = 1); --b > -1;) n = Et[b], h = f[n] - m[n], (h > y || -y > h || null != w[n] || null != H[n]) && (d = !0, s = new dt(m, n, m[n], h, s), n in E && (s.e = E[n]), s.xs0 = 0, s.plugin = u, r._overwriteProps.push(s.n));
                        return h = w.transformOrigin, h && m.svg && (Mt(e, h, f), s = new dt(m, "xOrigin", m.xOrigin, f.xOrigin - m.xOrigin, s, -1, "transformOrigin"), s.b = m.xOrigin, s.e = s.xs0 = f.xOrigin, s = new dt(m, "yOrigin", m.yOrigin, f.yOrigin - m.yOrigin, s, -1, "transformOrigin"), s.b = m.yOrigin, s.e = s.xs0 = f.yOrigin, h = "0px 0px"), (h || Nt && p && m.zOrigin) && (St ? (d = !0, n = Tt, h = (h || K(e, n, i, !1, "50% 50%")) + "", s = new dt(g, n, 0, 0, s, -1, "transformOrigin"), s.b = g[n], s.plugin = u, Nt ? (c = m.zOrigin, h = h.split(" "), m.zOrigin = (h.length > 2 && (0 === c || "0px" !== h[2]) ? parseFloat(h[2]) : c) || 0, s.xs0 = s.e = h[0] + " " + (h[1] || "50%") + " 0px", s = new dt(m, "zOrigin", 0, 0, s, -1, s.n), s.b = c, s.xs0 = s.e = m.zOrigin) : s.xs0 = s.e = h) : rt(h + "", m)), d && (r._transformType = m.svg && wt || !p && 3 !== this._transformType ? 2 : 3), s
                    },
                    prefix: !0
                }), yt("boxShadow", {
                    defaultValue: "0px 0px 0px 0px #999",
                    prefix: !0,
                    color: !0,
                    multi: !0,
                    keyword: "inset"
                }), yt("borderRadius", {
                    defaultValue: "0px",
                    parser: function(e, t, n, s, o) {
                        t = this.format(t);
                        var u, a, f, l, c, h, p, d, v, m, g, y, b, w, E, S, x = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"],
                            T = e.style;
                        for (v = parseFloat(e.offsetWidth), m = parseFloat(e.offsetHeight), u = t.split(" "), a = 0; x.length > a; a++) this.p.indexOf("border") && (x[a] = $(x[a])), c = l = K(e, x[a], i, !1, "0px"), -1 !== c.indexOf(" ") && (l = c.split(" "), c = l[0], l = l[1]), h = f = u[a], p = parseFloat(c), y = c.substr((p + "").length), b = "=" === h.charAt(1), b ? (d = parseInt(h.charAt(0) + "1", 10), h = h.substr(2), d *= parseFloat(h), g = h.substr((d + "").length - (0 > d ? 1 : 0)) || "") : (d = parseFloat(h), g = h.substr((d + "").length)), "" === g && (g = r[n] || y), g !== y && (w = Q(e, "borderLeft", p, y), E = Q(e, "borderTop", p, y), "%" === g ? (c = 100 * (w / v) + "%", l = 100 * (E / m) + "%") : "em" === g ? (S = Q(e, "borderLeft", 1, "em"), c = w / S + "em", l = E / S + "em") : (c = w + "px", l = E + "px"), b && (h = parseFloat(c) + d + g, f = parseFloat(l) + d + g)), o = vt(T, x[a], c + " " + l, h + " " + f, !1, "0px", o);
                        return o
                    },
                    prefix: !0,
                    formatter: ct("0px 0px 0px 0px", !1, !0)
                }), yt("backgroundPosition", {
                    defaultValue: "0 0",
                    parser: function(e, t, n, r, s, o) {
                        var u, a, f, l, c, h, p = "background-position",
                            d = i || J(e, null),
                            m = this.format((d ? v ? d.getPropertyValue(p + "-x") + " " + d.getPropertyValue(p + "-y") : d.getPropertyValue(p) : e.currentStyle.backgroundPositionX + " " + e.currentStyle.backgroundPositionY) || "0 0"),
                            g = this.format(t);
                        if (-1 !== m.indexOf("%") != (-1 !== g.indexOf("%")) && (h = K(e, "backgroundImage").replace(k, ""), h && "none" !== h)) {
                            for (u = m.split(" "), a = g.split(" "), I.setAttribute("src", h), f = 2; --f > -1;) m = u[f], l = -1 !== m.indexOf("%"), l !== (-1 !== a[f].indexOf("%")) && (c = 0 === f ? e.offsetWidth - I.width : e.offsetHeight - I.height, u[f] = l ? parseFloat(m) / 100 * c + "px" : 100 * (parseFloat(m) / c) + "%");
                            m = u.join(" ")
                        }
                        return this.parseComplex(e.style, m, g, s, o)
                    },
                    formatter: rt
                }), yt("backgroundSize", {
                    defaultValue: "0 0",
                    formatter: rt
                }), yt("perspective", {
                    defaultValue: "0px",
                    prefix: !0
                }), yt("perspectiveOrigin", {
                    defaultValue: "50% 50%",
                    prefix: !0
                }), yt("transformStyle", {
                    prefix: !0
                }), yt("backfaceVisibility", {
                    prefix: !0
                }), yt("userSelect", {
                    prefix: !0
                }), yt("margin", {
                    parser: ht("marginTop,marginRight,marginBottom,marginLeft")
                }), yt("padding", {
                    parser: ht("paddingTop,paddingRight,paddingBottom,paddingLeft")
                }), yt("clip", {
                    defaultValue: "rect(0px,0px,0px,0px)",
                    parser: function(e, t, n, r, s, o) {
                        var u, a, f;
                        return 9 > v ? (a = e.currentStyle, f = 8 > v ? " " : ",", u = "rect(" + a.clipTop + f + a.clipRight + f + a.clipBottom + f + a.clipLeft + ")", t = this.format(t).split(",").join(f)) : (u = this.format(K(e, this.p, i, !1, this.dflt)), t = this.format(t)), this.parseComplex(e.style, u, t, s, o)
                    }
                }), yt("textShadow", {
                    defaultValue: "0px 0px 0px #999",
                    color: !0,
                    multi: !0
                }), yt("autoRound,strictUnits", {
                    parser: function(e, t, n, r, i) {
                        return i
                    }
                }), yt("border", {
                    defaultValue: "0px solid #000",
                    parser: function(e, t, n, r, s, o) {
                        return this.parseComplex(e.style, this.format(K(e, "borderTopWidth", i, !1, "0px") + " " + K(e, "borderTopStyle", i, !1, "solid") + " " + K(e, "borderTopColor", i, !1, "#000")), this.format(t), s, o)
                    },
                    color: !0,
                    formatter: function(e) {
                        var t = e.split(" ");
                        return t[0] + " " + (t[1] || "solid") + " " + (e.match(lt) || ["#000"])[0]
                    }
                }), yt("borderWidth", {
                    parser: ht("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")
                }), yt("float,cssFloat,styleFloat", {
                    parser: function(e, t, n, r, i) {
                        var s = e.style,
                            o = "cssFloat" in s ? "cssFloat" : "styleFloat";
                        return new dt(s, o, 0, 0, i, -1, n, !1, 0, s[o], t)
                    }
                });
                var Bt = function(e) {
                    var t, n = this.t,
                        r = n.filter || K(this.data, "filter") || "",
                        i = 0 | this.s + this.c * e;
                    100 === i && (-1 === r.indexOf("atrix(") && -1 === r.indexOf("radient(") && -1 === r.indexOf("oader(") ? (n.removeAttribute("filter"), t = !K(this.data, "filter")) : (n.filter = r.replace(x, ""), t = !0)), t || (this.xn1 && (n.filter = r = r || "alpha(opacity=" + i + ")"), -1 === r.indexOf("pacity") ? 0 === i && this.xn1 || (n.filter = r + " alpha(opacity=" + i + ")") : n.filter = r.replace(E, "opacity=" + i))
                };
                yt("opacity,alpha,autoAlpha", {
                    defaultValue: "1",
                    parser: function(e, t, n, r, s, o) {
                        var u = parseFloat(K(e, "opacity", i, !1, "1")),
                            a = e.style,
                            f = "autoAlpha" === n;
                        return "string" == typeof t && "=" === t.charAt(1) && (t = ("-" === t.charAt(0) ? -1 : 1) * parseFloat(t.substr(2)) + u), f && 1 === u && "hidden" === K(e, "visibility", i) && 0 !== t && (u = 0), U ? s = new dt(a, "opacity", u, t - u, s) : (s = new dt(a, "opacity", 100 * u, 100 * (t - u), s), s.xn1 = f ? 1 : 0, a.zoom = 1, s.type = 2, s.b = "alpha(opacity=" + s.s + ")", s.e = "alpha(opacity=" + (s.s + s.c) + ")", s.data = e, s.plugin = o, s.setRatio = Bt), f && (s = new dt(a, "visibility", 0, 0, s, -1, null, !1, 0, 0 !== u ? "inherit" : "hidden", 0 === t ? "hidden" : "inherit"), s.xs0 = "inherit", r._overwriteProps.push(s.n), r._overwriteProps.push(n)), s
                    }
                });
                var jt = function(e, t) {
                        t && (e.removeProperty ? ("ms" === t.substr(0, 2) && (t = "M" + t.substr(1)), e.removeProperty(t.replace(N, "-$1").toLowerCase())) : e.removeAttribute(t))
                    },
                    Ft = function(e) {
                        if (this.t._gsClassPT = this, 1 === e || 0 === e) {
                            this.t.setAttribute("class", 0 === e ? this.b : this.e);
                            for (var t = this.data, n = this.t.style; t;) t.v ? n[t.p] = t.v : jt(n, t.p), t = t._next;
                            1 === e && this.t._gsClassPT === this && (this.t._gsClassPT = null)
                        } else this.t.getAttribute("class") !== this.e && this.t.setAttribute("class", this.e)
                    };
                yt("className", {
                    parser: function(e, t, r, s, o, u, a) {
                        var f, l, c, h, p, d = e.getAttribute("class") || "",
                            v = e.style.cssText;
                        if (o = s._classNamePT = new dt(e, r, 0, 0, o, 2), o.setRatio = Ft, o.pr = -11, n = !0, o.b = d, l = Y(e, i), c = e._gsClassPT) {
                            for (h = {}, p = c.data; p;) h[p.p] = 1, p = p._next;
                            c.setRatio(1)
                        }
                        return e._gsClassPT = o, o.e = "=" !== t.charAt(1) ? t : d.replace(RegExp("\\s*\\b" + t.substr(2) + "\\b"), "") + ("+" === t.charAt(0) ? " " + t.substr(2) : ""), s._tween._duration && (e.setAttribute("class", o.e), f = Z(e, l, Y(e), a, h), e.setAttribute("class", d), o.data = f.firstMPT, e.style.cssText = v, o = o.xfirst = s.parse(e, f.difs, o, u)), o
                    }
                });
                var It = function(e) {
                    if ((1 === e || 0 === e) && this.data._totalTime === this.data._totalDuration && "isFromStart" !== this.data.data) {
                        var t, n, r, i, s = this.t.style,
                            o = a.transform.parse;
                        if ("all" === this.e) s.cssText = "", i = !0;
                        else
                            for (t = this.e.split(" ").join("").split(","), r = t.length; --r > -1;) n = t[r], a[n] && (a[n].parse === o ? i = !0 : n = "transformOrigin" === n ? Tt : a[n].p), jt(s, n);
                        i && (jt(s, St), this.t._gsTransform && delete this.t._gsTransform)
                    }
                };
                for (yt("clearProps", {
                        parser: function(e, t, r, i, s) {
                            return s = new dt(e, r, 0, 0, s, 2), s.setRatio = It, s.e = t, s.pr = -10, s.data = i._tween, n = !0, s
                        }
                    }), f = "bezier,throwProps,physicsProps,physics2D".split(","), mt = f.length; mt--;) bt(f[mt]);
                f = o.prototype, f._firstPT = f._lastParsedTransform = f._transform = null, f._onInitTween = function(e, t, u) {
                    if (!e.nodeType) return !1;
                    this._target = e, this._tween = u, this._vars = t, l = t.autoRound, n = !1, r = t.suffixMap || o.suffixMap, i = J(e, ""), s = this._overwriteProps;
                    var a, f, p, v, m, g, y, b, w, E = e.style;
                    if (c && "" === E.zIndex && (a = K(e, "zIndex", i), ("auto" === a || "" === a) && this._addLazySet(E, "zIndex", 0)), "string" == typeof t && (v = E.cssText, a = Y(e, i), E.cssText = v + ";" + t, a = Z(e, a, Y(e)).difs, !U && S.test(t) && (a.opacity = parseFloat(RegExp.$1)), t = a, E.cssText = v), this._firstPT = f = this.parse(e, t, null), this._transformType) {
                        for (w = 3 === this._transformType, St ? h && (c = !0, "" === E.zIndex && (y = K(e, "zIndex", i), ("auto" === y || "" === y) && this._addLazySet(E, "zIndex", 0)), d && this._addLazySet(E, "WebkitBackfaceVisibility", this._vars.WebkitBackfaceVisibility || (w ? "visible" : "hidden"))) : E.zoom = 1, p = f; p && p._next;) p = p._next;
                        b = new dt(e, "transform", 0, 0, null, 2), this._linkCSSP(b, null, p), b.setRatio = w && Nt ? Pt : St ? Ht : Dt, b.data = this._transform || _t(e, i, !0), s.pop()
                    }
                    if (n) {
                        for (; f;) {
                            for (g = f._next, p = v; p && p.pr > f.pr;) p = p._next;
                            (f._prev = p ? p._prev : m) ? f._prev._next = f: v = f, (f._next = p) ? p._prev = f : m = f, f = g
                        }
                        this._firstPT = v
                    }
                    return !0
                }, f.parse = function(e, t, n, s) {
                    var o, u, f, c, h, p, d, v, m, g, y = e.style;
                    for (o in t) p = t[o], u = a[o], u ? n = u.parse(e, p, o, this, n, s, t) : (h = K(e, o, i) + "", m = "string" == typeof p, "color" === o || "fill" === o || "stroke" === o || -1 !== o.indexOf("Color") || m && T.test(p) ? (m || (p = ft(p), p = (p.length > 3 ? "rgba(" : "rgb(") + p.join(",") + ")"), n = vt(y, o, h, p, !0, "transparent", n, 0, s)) : !m || -1 === p.indexOf(" ") && -1 === p.indexOf(",") ? (f = parseFloat(h), d = f || 0 === f ? h.substr((f + "").length) : "", ("" === h || "auto" === h) && ("width" === o || "height" === o ? (f = nt(e, o, i), d = "px") : "left" === o || "top" === o ? (f = G(e, o, i), d = "px") : (f = "opacity" !== o ? 0 : 1, d = "")), g = m && "=" === p.charAt(1), g ? (c = parseInt(p.charAt(0) + "1", 10), p = p.substr(2), c *= parseFloat(p), v = p.replace(w, "")) : (c = parseFloat(p), v = m ? p.substr((c + "").length) || "" : ""), "" === v && (v = o in r ? r[o] : d), p = c || 0 === c ? (g ? c + f : c) + v : t[o], d !== v && "" !== v && (c || 0 === c) && f && (f = Q(e, o, f, d), "%" === v ? (f /= Q(e, o, 100, "%") / 100, t.strictUnits !== !0 && (h = f + "%")) : "em" === v ? f /= Q(e, o, 1, "em") : "px" !== v && (c = Q(e, o, c, v), v = "px"), g && (c || 0 === c) && (p = c + f + v)), g && (c += f), !f && 0 !== f || !c && 0 !== c ? void 0 !== y[o] && (p || "NaN" != p + "" && null != p) ? (n = new dt(y, o, c || f || 0, 0, n, -1, o, !1, 0, h, p), n.xs0 = "none" !== p || "display" !== o && -1 === o.indexOf("Style") ? p : h) : W("invalid " + o + " tween value: " + t[o]) : (n = new dt(y, o, f, c - f, n, 0, o, l !== !1 && ("px" === v || "zIndex" === o), 0, h, p), n.xs0 = v)) : n = vt(y, o, h, p, !0, null, n, 0, s)), s && n && !n.plugin && (n.plugin = s);
                    return n
                }, f.setRatio = function(e) {
                    var t, n, r, i = this._firstPT,
                        s = 1e-6;
                    if (1 !== e || this._tween._time !== this._tween._duration && 0 !== this._tween._time)
                        if (e || this._tween._time !== this._tween._duration && 0 !== this._tween._time || this._tween._rawPrevTime === -0.000001)
                            for (; i;) {
                                if (t = i.c * e + i.s, i.r ? t = Math.round(t) : s > t && t > -s && (t = 0), i.type)
                                    if (1 === i.type)
                                        if (r = i.l, 2 === r) i.t[i.p] = i.xs0 + t + i.xs1 + i.xn1 + i.xs2;
                                        else if (3 === r) i.t[i.p] = i.xs0 + t + i.xs1 + i.xn1 + i.xs2 + i.xn2 + i.xs3;
                                else if (4 === r) i.t[i.p] = i.xs0 + t + i.xs1 + i.xn1 + i.xs2 + i.xn2 + i.xs3 + i.xn3 + i.xs4;
                                else if (5 === r) i.t[i.p] = i.xs0 + t + i.xs1 + i.xn1 + i.xs2 + i.xn2 + i.xs3 + i.xn3 + i.xs4 + i.xn4 + i.xs5;
                                else {
                                    for (n = i.xs0 + t + i.xs1, r = 1; i.l > r; r++) n += i["xn" + r] + i["xs" + (r + 1)];
                                    i.t[i.p] = n
                                } else -1 === i.type ? i.t[i.p] = i.xs0 : i.setRatio && i.setRatio(e);
                                else i.t[i.p] = t + i.xs0;
                                i = i._next
                            } else
                                for (; i;) 2 !== i.type ? i.t[i.p] = i.b : i.setRatio(e), i = i._next;
                        else
                            for (; i;) 2 !== i.type ? i.t[i.p] = i.e : i.setRatio(e), i = i._next
                }, f._enableTransforms = function(e) {
                    this._transform = this._transform || _t(this._target, i, !0), this._transformType = this._transform.svg && wt || !e && 3 !== this._transformType ? 2 : 3
                };
                var qt = function() {
                    this.t[this.p] = this.e, this.data._linkCSSP(this, this._next, null, !0)
                };
                f._addLazySet = function(e, t, n) {
                    var r = this._firstPT = new dt(e, t, 0, 0, this._firstPT, 2);
                    r.e = n, r.setRatio = qt, r.data = this
                }, f._linkCSSP = function(e, t, n, r) {
                    return e && (t && (t._prev = e), e._next && (e._next._prev = e._prev), e._prev ? e._prev._next = e._next : this._firstPT === e && (this._firstPT = e._next, r = !0), n ? n._next = e : r || null !== this._firstPT || (this._firstPT = e), e._next = t, e._prev = n), e
                }, f._kill = function(t) {
                    var n, r, i, s = t;
                    if (t.autoAlpha || t.alpha) {
                        s = {};
                        for (r in t) s[r] = t[r];
                        s.opacity = 1, s.autoAlpha && (s.visibility = 1)
                    }
                    return t.className && (n = this._classNamePT) && (i = n.xfirst, i && i._prev ? this._linkCSSP(i._prev, n._next, i._prev._prev) : i === this._firstPT && (this._firstPT = n._next), n._next && this._linkCSSP(n._next, n._next._next, i._prev), this._classNamePT = null), e.prototype._kill.call(this, s)
                };
                var Rt = function(e, t, n) {
                    var r, i, s, o;
                    if (e.slice)
                        for (i = e.length; --i > -1;) Rt(e[i], t, n);
                    else
                        for (r = e.childNodes, i = r.length; --i > -1;) s = r[i], o = s.type, s.style && (t.push(Y(s)), n && n.push(s)), 1 !== o && 9 !== o && 11 !== o || !s.childNodes.length || Rt(s, t, n)
                };
                return o.cascadeTo = function(e, n, r) {
                    var i, s, o, u = t.to(e, n, r),
                        a = [u],
                        f = [],
                        l = [],
                        c = [],
                        h = t._internals.reservedProps;
                    for (e = u._targets || u.target, Rt(e, f, c), u.render(n, !0), Rt(e, l), u.render(0, !0), u._enabled(!0), i = c.length; --i > -1;)
                        if (s = Z(c[i], f[i], l[i]), s.firstMPT) {
                            s = s.difs;
                            for (o in r) h[o] && (s[o] = r[o]);
                            a.push(t.to(c[i], n, s))
                        }
                    return a
                }, e.activate([o]), o
            }, !0),
            function() {
                var e = _gsScope._gsDefine.plugin({
                        propName: "roundProps",
                        priority: -1,
                        API: 2,
                        init: function(e, t, n) {
                            return this._tween = n, !0
                        }
                    }),
                    t = e.prototype;
                t._onInitAllProps = function() {
                    for (var e, t, n, r = this._tween, i = r.vars.roundProps instanceof Array ? r.vars.roundProps : r.vars.roundProps.split(","), s = i.length, o = {}, u = r._propLookup.roundProps; --s > -1;) o[i[s]] = 1;
                    for (s = i.length; --s > -1;)
                        for (e = i[s], t = r._firstPT; t;) n = t._next, t.pg ? t.t._roundProps(o, !0) : t.n === e && (this._add(t.t, e, t.s, t.c), n && (n._prev = t._prev), t._prev ? t._prev._next = n : r._firstPT === t && (r._firstPT = n), t._next = t._prev = null, r._propLookup[e] = u), t = n;
                    return !1
                }, t._add = function(e, t, n, r) {
                    this._addTween(e, t, n, n + r, t, !0), this._overwriteProps.push(t)
                }
            }(), _gsScope._gsDefine.plugin({
                propName: "attr",
                API: 2,
                version: "0.3.3",
                init: function(e, t) {
                    var n, r, i;
                    if ("function" != typeof e.setAttribute) return !1;
                    this._target = e, this._proxy = {}, this._start = {}, this._end = {};
                    for (n in t) this._start[n] = this._proxy[n] = r = e.getAttribute(n), i = this._addTween(this._proxy, n, parseFloat(r), t[n], n), this._end[n] = i ? i.s + i.c : t[n], this._overwriteProps.push(n);
                    return !0
                },
                set: function(e) {
                    this._super.setRatio.call(this, e);
                    for (var t, n = this._overwriteProps, r = n.length, i = 1 === e ? this._end : e ? this._proxy : this._start; --r > -1;) t = n[r], this._target.setAttribute(t, i[t] + "")
                }
            }), _gsScope._gsDefine.plugin({
                propName: "directionalRotation",
                version: "0.2.1",
                API: 2,
                init: function(e, t) {
                    "object" != typeof t && (t = {
                        rotation: t
                    }), this.finals = {};
                    var n, r, i, s, o, u, a = t.useRadians === !0 ? 2 * Math.PI : 360,
                        f = 1e-6;
                    for (n in t) "useRadians" !== n && (u = (t[n] + "").split("_"), r = u[0], i = parseFloat("function" != typeof e[n] ? e[n] : e[n.indexOf("set") || "function" != typeof e["get" + n.substr(3)] ? n : "get" + n.substr(3)]()), s = this.finals[n] = "string" == typeof r && "=" === r.charAt(1) ? i + parseInt(r.charAt(0) + "1", 10) * Number(r.substr(2)) : Number(r) || 0, o = s - i, u.length && (r = u.join("_"), -1 !== r.indexOf("short") && (o %= a, o !== o % (a / 2) && (o = 0 > o ? o + a : o - a)), -1 !== r.indexOf("_cw") && 0 > o ? o = (o + 9999999999 * a) % a - (0 | o / a) * a : -1 !== r.indexOf("ccw") && o > 0 && (o = (o - 9999999999 * a) % a - (0 | o / a) * a)), (o > f || -f > o) && (this._addTween(e, n, i, i + o, n), this._overwriteProps.push(n)));
                    return !0
                },
                set: function(e) {
                    var t;
                    if (1 !== e) this._super.setRatio.call(this, e);
                    else
                        for (t = this._firstPT; t;) t.f ? t.t[t.p](this.finals[t.p]) : t.t[t.p] = this.finals[t.p], t = t._next
                }
            })._autoCSS = !0, _gsScope._gsDefine("easing.Back", ["easing.Ease"], function(e) {
                var t, n, r, i = _gsScope.GreenSockGlobals || _gsScope,
                    s = i.com.greensock,
                    o = 2 * Math.PI,
                    u = Math.PI / 2,
                    a = s._class,
                    f = function(t, n) {
                        var r = a("easing." + t, function() {}, !0),
                            i = r.prototype = new e;
                        return i.constructor = r, i.getRatio = n, r
                    },
                    l = e.register || function() {},
                    c = function(e, t, n, r) {
                        var i = a("easing." + e, {
                            easeOut: new t,
                            easeIn: new n,
                            easeInOut: new r
                        }, !0);
                        return l(i, e), i
                    },
                    h = function(e, t, n) {
                        this.t = e, this.v = t, n && (this.next = n, n.prev = this, this.c = n.v - t, this.gap = n.t - e)
                    },
                    p = function(t, n) {
                        var r = a("easing." + t, function(e) {
                                this._p1 = e || 0 === e ? e : 1.70158, this._p2 = 1.525 * this._p1
                            }, !0),
                            i = r.prototype = new e;
                        return i.constructor = r, i.getRatio = n, i.config = function(e) {
                            return new r(e)
                        }, r
                    },
                    d = c("Back", p("BackOut", function(e) {
                        return (e -= 1) * e * ((this._p1 + 1) * e + this._p1) + 1
                    }), p("BackIn", function(e) {
                        return e * e * ((this._p1 + 1) * e - this._p1)
                    }), p("BackInOut", function(e) {
                        return 1 > (e *= 2) ? .5 * e * e * ((this._p2 + 1) * e - this._p2) : .5 * ((e -= 2) * e * ((this._p2 + 1) * e + this._p2) + 2)
                    })),
                    v = a("easing.SlowMo", function(e, t, n) {
                        t = t || 0 === t ? t : .7, null == e ? e = .7 : e > 1 && (e = 1), this._p = 1 !== e ? t : 0, this._p1 = (1 - e) / 2, this._p2 = e, this._p3 = this._p1 + this._p2, this._calcEnd = n === !0
                    }, !0),
                    m = v.prototype = new e;
                return m.constructor = v, m.getRatio = function(e) {
                    var t = e + (.5 - e) * this._p;
                    return this._p1 > e ? this._calcEnd ? 1 - (e = 1 - e / this._p1) * e : t - (e = 1 - e / this._p1) * e * e * e * t : e > this._p3 ? this._calcEnd ? 1 - (e = (e - this._p3) / this._p1) * e : t + (e - t) * (e = (e - this._p3) / this._p1) * e * e * e : this._calcEnd ? 1 : t
                }, v.ease = new v(.7, .7), m.config = v.config = function(e, t, n) {
                    return new v(e, t, n)
                }, t = a("easing.SteppedEase", function(e) {
                    e = e || 1, this._p1 = 1 / e, this._p2 = e + 1
                }, !0), m = t.prototype = new e, m.constructor = t, m.getRatio = function(e) {
                    return 0 > e ? e = 0 : e >= 1 && (e = .999999999), (this._p2 * e >> 0) * this._p1
                }, m.config = t.config = function(e) {
                    return new t(e)
                }, n = a("easing.RoughEase", function(t) {
                    t = t || {};
                    for (var n, r, i, s, o, u, a = t.taper || "none", f = [], l = 0, c = 0 | (t.points || 20), p = c, d = t.randomize !== !1, v = t.clamp === !0, m = t.template instanceof e ? t.template : null, g = "number" == typeof t.strength ? .4 * t.strength : .4; --p > -1;) n = d ? Math.random() : 1 / c * p, r = m ? m.getRatio(n) : n, "none" === a ? i = g : "out" === a ? (s = 1 - n, i = s * s * g) : "in" === a ? i = n * n * g : .5 > n ? (s = 2 * n, i = .5 * s * s * g) : (s = 2 * (1 - n), i = .5 * s * s * g), d ? r += Math.random() * i - .5 * i : p % 2 ? r += .5 * i : r -= .5 * i, v && (r > 1 ? r = 1 : 0 > r && (r = 0)), f[l++] = {
                        x: n,
                        y: r
                    };
                    for (f.sort(function(e, t) {
                            return e.x - t.x
                        }), u = new h(1, 1, null), p = c; --p > -1;) o = f[p], u = new h(o.x, o.y, u);
                    this._prev = new h(0, 0, 0 !== u.t ? u : u.next)
                }, !0), m = n.prototype = new e, m.constructor = n, m.getRatio = function(e) {
                    var t = this._prev;
                    if (e > t.t) {
                        for (; t.next && e >= t.t;) t = t.next;
                        t = t.prev
                    } else
                        for (; t.prev && t.t >= e;) t = t.prev;
                    return this._prev = t, t.v + (e - t.t) / t.gap * t.c
                }, m.config = function(e) {
                    return new n(e)
                }, n.ease = new n, c("Bounce", f("BounceOut", function(e) {
                    return 1 / 2.75 > e ? 7.5625 * e * e : 2 / 2.75 > e ? 7.5625 * (e -= 1.5 / 2.75) * e + .75 : 2.5 / 2.75 > e ? 7.5625 * (e -= 2.25 / 2.75) * e + .9375 : 7.5625 * (e -= 2.625 / 2.75) * e + .984375
                }), f("BounceIn", function(e) {
                    return 1 / 2.75 > (e = 1 - e) ? 1 - 7.5625 * e * e : 2 / 2.75 > e ? 1 - (7.5625 * (e -= 1.5 / 2.75) * e + .75) : 2.5 / 2.75 > e ? 1 - (7.5625 * (e -= 2.25 / 2.75) * e + .9375) : 1 - (7.5625 * (e -= 2.625 / 2.75) * e + .984375)
                }), f("BounceInOut", function(e) {
                    var t = .5 > e;
                    return e = t ? 1 - 2 * e : 2 * e - 1, e = 1 / 2.75 > e ? 7.5625 * e * e : 2 / 2.75 > e ? 7.5625 * (e -= 1.5 / 2.75) * e + .75 : 2.5 / 2.75 > e ? 7.5625 * (e -= 2.25 / 2.75) * e + .9375 : 7.5625 * (e -= 2.625 / 2.75) * e + .984375, t ? .5 * (1 - e) : .5 * e + .5
                })), c("Circ", f("CircOut", function(e) {
                    return Math.sqrt(1 - (e -= 1) * e)
                }), f("CircIn", function(e) {
                    return -(Math.sqrt(1 - e * e) - 1)
                }), f("CircInOut", function(e) {
                    return 1 > (e *= 2) ? -0.5 * (Math.sqrt(1 - e * e) - 1) : .5 * (Math.sqrt(1 - (e -= 2) * e) + 1)
                })), r = function(t, n, r) {
                    var i = a("easing." + t, function(e, t) {
                            this._p1 = e || 1, this._p2 = t || r, this._p3 = this._p2 / o * (Math.asin(1 / this._p1) || 0)
                        }, !0),
                        s = i.prototype = new e;
                    return s.constructor = i, s.getRatio = n, s.config = function(e, t) {
                        return new i(e, t)
                    }, i
                }, c("Elastic", r("ElasticOut", function(e) {
                    return this._p1 * Math.pow(2, -10 * e) * Math.sin((e - this._p3) * o / this._p2) + 1
                }, .3), r("ElasticIn", function(e) {
                    return -(this._p1 * Math.pow(2, 10 * (e -= 1)) * Math.sin((e - this._p3) * o / this._p2))
                }, .3), r("ElasticInOut", function(e) {
                    return 1 > (e *= 2) ? -0.5 * this._p1 * Math.pow(2, 10 * (e -= 1)) * Math.sin((e - this._p3) * o / this._p2) : .5 * this._p1 * Math.pow(2, -10 * (e -= 1)) * Math.sin((e - this._p3) * o / this._p2) + 1
                }, .45)), c("Expo", f("ExpoOut", function(e) {
                    return 1 - Math.pow(2, -10 * e)
                }), f("ExpoIn", function(e) {
                    return Math.pow(2, 10 * (e - 1)) - .001
                }), f("ExpoInOut", function(e) {
                    return 1 > (e *= 2) ? .5 * Math.pow(2, 10 * (e - 1)) : .5 * (2 - Math.pow(2, -10 * (e - 1)))
                })), c("Sine", f("SineOut", function(e) {
                    return Math.sin(e * u)
                }), f("SineIn", function(e) {
                    return -Math.cos(e * u) + 1
                }), f("SineInOut", function(e) {
                    return -0.5 * (Math.cos(Math.PI * e) - 1)
                })), a("easing.EaseLookup", {
                    find: function(t) {
                        return e.map[t]
                    }
                }, !0), l(i.SlowMo, "SlowMo", "ease,"), l(n, "RoughEase", "ease,"), l(t, "SteppedEase", "ease,"), d
            }, !0)
    }), _gsScope._gsDefine && _gsScope._gsQueue.pop()(),
    function(e, t) {
        "use strict";
        var n = e.GreenSockGlobals = e.GreenSockGlobals || e;
        if (!n.TweenLite) {
            var r, i, s, o, u, a = function(e) {
                    var t, r = e.split("."),
                        i = n;
                    for (t = 0; r.length > t; t++) i[r[t]] = i = i[r[t]] || {};
                    return i
                },
                f = a("com.greensock"),
                l = 1e-10,
                c = function(e) {
                    var t, n = [],
                        r = e.length;
                    for (t = 0; t !== r; n.push(e[t++]));
                    return n
                },
                h = function() {},
                p = function() {
                    var e = Object.prototype.toString,
                        t = e.call([]);
                    return function(n) {
                        return null != n && (n instanceof Array || "object" == typeof n && !!n.push && e.call(n) === t)
                    }
                }(),
                d = {},
                v = function(r, i, s, o) {
                    this.sc = d[r] ? d[r].sc : [], d[r] = this, this.gsClass = null, this.func = s;
                    var u = [];
                    this.check = function(f) {
                        for (var l, c, h, p, m = i.length, g = m; --m > -1;)(l = d[i[m]] || new v(i[m], [])).gsClass ? (u[m] = l.gsClass, g--) : f && l.sc.push(this);
                        if (0 === g && s)
                            for (c = ("com.greensock." + r).split("."), h = c.pop(), p = a(c.join("."))[h] = this.gsClass = s.apply(s, u), o && (n[h] = p, "function" == typeof define && define.amd ? define((e.GreenSockAMDPath ? e.GreenSockAMDPath + "/" : "") + r.split(".").pop(), [], function() {
                                    return p
                                }) : r === t && "undefined" != typeof module && module.exports && (module.exports = p)), m = 0; this.sc.length > m; m++) this.sc[m].check()
                    }, this.check(!0)
                },
                m = e._gsDefine = function(e, t, n, r) {
                    return new v(e, t, n, r)
                },
                g = f._class = function(e, t, n) {
                    return t = t || function() {}, m(e, [], function() {
                        return t
                    }, n), t
                };
            m.globals = n;
            var y = [0, 0, 1, 1],
                b = [],
                w = g("easing.Ease", function(e, t, n, r) {
                    this._func = e, this._type = n || 0, this._power = r || 0, this._params = t ? y.concat(t) : y
                }, !0),
                E = w.map = {},
                S = w.register = function(e, t, n, r) {
                    for (var i, s, o, u, a = t.split(","), l = a.length, c = (n || "easeIn,easeOut,easeInOut").split(","); --l > -1;)
                        for (s = a[l], i = r ? g("easing." + s, null, !0) : f.easing[s] || {}, o = c.length; --o > -1;) u = c[o], E[s + "." + u] = E[u + s] = i[u] = e.getRatio ? e : e[u] || new e
                };
            for (s = w.prototype, s._calcEnd = !1, s.getRatio = function(e) {
                    if (this._func) return this._params[0] = e, this._func.apply(null, this._params);
                    var t = this._type,
                        n = this._power,
                        r = 1 === t ? 1 - e : 2 === t ? e : .5 > e ? 2 * e : 2 * (1 - e);
                    return 1 === n ? r *= r : 2 === n ? r *= r * r : 3 === n ? r *= r * r * r : 4 === n && (r *= r * r * r * r), 1 === t ? 1 - r : 2 === t ? r : .5 > e ? r / 2 : 1 - r / 2
                }, r = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"], i = r.length; --i > -1;) s = r[i] + ",Power" + i, S(new w(null, null, 1, i), s, "easeOut", !0), S(new w(null, null, 2, i), s, "easeIn" + (0 === i ? ",easeNone" : "")), S(new w(null, null, 3, i), s, "easeInOut");
            E.linear = f.easing.Linear.easeIn, E.swing = f.easing.Quad.easeInOut;
            var x = g("events.EventDispatcher", function(e) {
                this._listeners = {}, this._eventTarget = e || this
            });
            s = x.prototype, s.addEventListener = function(e, t, n, r, i) {
                i = i || 0;
                var s, a, f = this._listeners[e],
                    l = 0;
                for (null == f && (this._listeners[e] = f = []), a = f.length; --a > -1;) s = f[a], s.c === t && s.s === n ? f.splice(a, 1) : 0 === l && i > s.pr && (l = a + 1);
                f.splice(l, 0, {
                    c: t,
                    s: n,
                    up: r,
                    pr: i
                }), this !== o || u || o.wake()
            }, s.removeEventListener = function(e, t) {
                var n, r = this._listeners[e];
                if (r)
                    for (n = r.length; --n > -1;)
                        if (r[n].c === t) return r.splice(n, 1), void 0
            }, s.dispatchEvent = function(e) {
                var t, n, r, i = this._listeners[e];
                if (i)
                    for (t = i.length, n = this._eventTarget; --t > -1;) r = i[t], r && (r.up ? r.c.call(r.s || n, {
                        type: e,
                        target: n
                    }) : r.c.call(r.s || n))
            };
            var T = e.requestAnimationFrame,
                N = e.cancelAnimationFrame,
                C = Date.now || function() {
                    return (new Date).getTime()
                },
                k = C();
            for (r = ["ms", "moz", "webkit", "o"], i = r.length; --i > -1 && !T;) T = e[r[i] + "RequestAnimationFrame"], N = e[r[i] + "CancelAnimationFrame"] || e[r[i] + "CancelRequestAnimationFrame"];
            g("Ticker", function(e, t) {
                var n, r, i, s, a, f = this,
                    c = C(),
                    p = t !== !1 && T,
                    d = 500,
                    v = 33,
                    m = "tick",
                    g = function(e) {
                        var t, o, u = C() - k;
                        u > d && (c += u - v), k += u, f.time = (k - c) / 1e3, t = f.time - a, (!n || t > 0 || e === !0) && (f.frame++, a += t + (t >= s ? .004 : s - t), o = !0), e !== !0 && (i = r(g)), o && f.dispatchEvent(m)
                    };
                x.call(f), f.time = f.frame = 0, f.tick = function() {
                    g(!0)
                }, f.lagSmoothing = function(e, t) {
                    d = e || 1 / l, v = Math.min(t, d, 0)
                }, f.sleep = function() {
                    null != i && (p && N ? N(i) : clearTimeout(i), r = h, i = null, f === o && (u = !1))
                }, f.wake = function() {
                    null !== i ? f.sleep() : f.frame > 10 && (k = C() - d + 5), r = 0 === n ? h : p && T ? T : function(e) {
                        return setTimeout(e, 0 | 1e3 * (a - f.time) + 1)
                    }, f === o && (u = !0), g(2)
                }, f.fps = function(e) {
                    return arguments.length ? (n = e, s = 1 / (n || 60), a = this.time + s, f.wake(), void 0) : n
                }, f.useRAF = function(e) {
                    return arguments.length ? (f.sleep(), p = e, f.fps(n), void 0) : p
                }, f.fps(e), setTimeout(function() {
                    p && (!i || 5 > f.frame) && f.useRAF(!1)
                }, 1500)
            }), s = f.Ticker.prototype = new f.events.EventDispatcher, s.constructor = f.Ticker;
            var L = g("core.Animation", function(e, t) {
                if (this.vars = t = t || {}, this._duration = this._totalDuration = e || 0, this._delay = Number(t.delay) || 0, this._timeScale = 1, this._active = t.immediateRender === !0, this.data = t.data, this._reversed = t.reversed === !0, z) {
                    u || o.wake();
                    var n = this.vars.useFrames ? U : z;
                    n.add(this, n._time), this.vars.paused && this.paused(!0)
                }
            });
            o = L.ticker = new f.Ticker, s = L.prototype, s._dirty = s._gc = s._initted = s._paused = !1, s._totalTime = s._time = 0, s._rawPrevTime = -1, s._next = s._last = s._onUpdate = s._timeline = s.timeline = null, s._paused = !1;
            var A = function() {
                u && C() - k > 2e3 && o.wake(), setTimeout(A, 2e3)
            };
            A(), s.play = function(e, t) {
                return null != e && this.seek(e, t), this.reversed(!1).paused(!1)
            }, s.pause = function(e, t) {
                return null != e && this.seek(e, t), this.paused(!0)
            }, s.resume = function(e, t) {
                return null != e && this.seek(e, t), this.paused(!1)
            }, s.seek = function(e, t) {
                return this.totalTime(Number(e), t !== !1)
            }, s.restart = function(e, t) {
                return this.reversed(!1).paused(!1).totalTime(e ? -this._delay : 0, t !== !1, !0)
            }, s.reverse = function(e, t) {
                return null != e && this.seek(e || this.totalDuration(), t), this.reversed(!0).paused(!1)
            }, s.render = function() {}, s.invalidate = function() {
                return this._time = this._totalTime = 0, this._initted = this._gc = !1, this._rawPrevTime = -1, (this._gc || !this.timeline) && this._enabled(!0), this
            }, s.isActive = function() {
                var e, t = this._timeline,
                    n = this._startTime;
                return !t || !this._gc && !this._paused && t.isActive() && (e = t.rawTime()) >= n && n + this.totalDuration() / this._timeScale > e
            }, s._enabled = function(e, t) {
                return u || o.wake(), this._gc = !e, this._active = this.isActive(), t !== !0 && (e && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !e && this.timeline && this._timeline._remove(this, !0)), !1
            }, s._kill = function() {
                return this._enabled(!1, !1)
            }, s.kill = function(e, t) {
                return this._kill(e, t), this
            }, s._uncache = function(e) {
                for (var t = e ? this : this.timeline; t;) t._dirty = !0, t = t.timeline;
                return this
            }, s._swapSelfInParams = function(e) {
                for (var t = e.length, n = e.concat(); --t > -1;) "{self}" === e[t] && (n[t] = this);
                return n
            }, s.eventCallback = function(e, t, n, r) {
                if ("on" === (e || "").substr(0, 2)) {
                    var i = this.vars;
                    if (1 === arguments.length) return i[e];
                    null == t ? delete i[e] : (i[e] = t, i[e + "Params"] = p(n) && -1 !== n.join("").indexOf("{self}") ? this._swapSelfInParams(n) : n, i[e + "Scope"] = r), "onUpdate" === e && (this._onUpdate = t)
                }
                return this
            }, s.delay = function(e) {
                return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + e - this._delay), this._delay = e, this) : this._delay
            }, s.duration = function(e) {
                return arguments.length ? (this._duration = this._totalDuration = e, this._uncache(!0), this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== e && this.totalTime(this._totalTime * (e / this._duration), !0), this) : (this._dirty = !1, this._duration)
            }, s.totalDuration = function(e) {
                return this._dirty = !1, arguments.length ? this.duration(e) : this._totalDuration
            }, s.time = function(e, t) {
                return arguments.length ? (this._dirty && this.totalDuration(), this.totalTime(e > this._duration ? this._duration : e, t)) : this._time
            }, s.totalTime = function(e, t, n) {
                if (u || o.wake(), !arguments.length) return this._totalTime;
                if (this._timeline) {
                    if (0 > e && !n && (e += this.totalDuration()), this._timeline.smoothChildTiming) {
                        this._dirty && this.totalDuration();
                        var r = this._totalDuration,
                            i = this._timeline;
                        if (e > r && !n && (e = r), this._startTime = (this._paused ? this._pauseTime : i._time) - (this._reversed ? r - e : e) / this._timeScale, i._dirty || this._uncache(!1), i._timeline)
                            for (; i._timeline;) i._timeline._time !== (i._startTime + i._totalTime) / i._timeScale && i.totalTime(i._totalTime, !0), i = i._timeline
                    }
                    this._gc && this._enabled(!0, !1), (this._totalTime !== e || 0 === this._duration) && (this.render(e, t, !1), P.length && W())
                }
                return this
            }, s.progress = s.totalProgress = function(e, t) {
                return arguments.length ? this.totalTime(this.duration() * e, t) : this._time / this.duration()
            }, s.startTime = function(e) {
                return arguments.length ? (e !== this._startTime && (this._startTime = e, this.timeline && this.timeline._sortChildren && this.timeline.add(this, e - this._delay)), this) : this._startTime
            }, s.endTime = function(e) {
                return this._startTime + (0 != e ? this.totalDuration() : this.duration()) / this._timeScale
            }, s.timeScale = function(e) {
                if (!arguments.length) return this._timeScale;
                if (e = e || l, this._timeline && this._timeline.smoothChildTiming) {
                    var t = this._pauseTime,
                        n = t || 0 === t ? t : this._timeline.totalTime();
                    this._startTime = n - (n - this._startTime) * this._timeScale / e
                }
                return this._timeScale = e, this._uncache(!1)
            }, s.reversed = function(e) {
                return arguments.length ? (e != this._reversed && (this._reversed = e, this.totalTime(this._timeline && !this._timeline.smoothChildTiming ? this.totalDuration() - this._totalTime : this._totalTime, !0)), this) : this._reversed
            }, s.paused = function(e) {
                if (!arguments.length) return this._paused;
                if (e != this._paused && this._timeline) {
                    u || e || o.wake();
                    var t = this._timeline,
                        n = t.rawTime(),
                        r = n - this._pauseTime;
                    !e && t.smoothChildTiming && (this._startTime += r, this._uncache(!1)), this._pauseTime = e ? n : null, this._paused = e, this._active = this.isActive(), !e && 0 !== r && this._initted && this.duration() && this.render(t.smoothChildTiming ? this._totalTime : (n - this._startTime) / this._timeScale, !0, !0)
                }
                return this._gc && !e && this._enabled(!0, !1), this
            };
            var O = g("core.SimpleTimeline", function(e) {
                L.call(this, 0, e), this.autoRemoveChildren = this.smoothChildTiming = !0
            });
            s = O.prototype = new L, s.constructor = O, s.kill()._gc = !1, s._first = s._last = s._recent = null, s._sortChildren = !1, s.add = s.insert = function(e, t) {
                var n, r;
                if (e._startTime = Number(t || 0) + e._delay, e._paused && this !== e._timeline && (e._pauseTime = e._startTime + (this.rawTime() - e._startTime) / e._timeScale), e.timeline && e.timeline._remove(e, !0), e.timeline = e._timeline = this, e._gc && e._enabled(!0, !0), n = this._last, this._sortChildren)
                    for (r = e._startTime; n && n._startTime > r;) n = n._prev;
                return n ? (e._next = n._next, n._next = e) : (e._next = this._first, this._first = e), e._next ? e._next._prev = e : this._last = e, e._prev = n, this._recent = e, this._timeline && this._uncache(!0), this
            }, s._remove = function(e, t) {
                return e.timeline === this && (t || e._enabled(!1, !0), e._prev ? e._prev._next = e._next : this._first === e && (this._first = e._next), e._next ? e._next._prev = e._prev : this._last === e && (this._last = e._prev), e._next = e._prev = e.timeline = null, e === this._recent && (this._recent = this._last), this._timeline && this._uncache(!0)), this
            }, s.render = function(e, t, n) {
                var r, i = this._first;
                for (this._totalTime = this._time = this._rawPrevTime = e; i;) r = i._next, (i._active || e >= i._startTime && !i._paused) && (i._reversed ? i.render((i._dirty ? i.totalDuration() : i._totalDuration) - (e - i._startTime) * i._timeScale, t, n) : i.render((e - i._startTime) * i._timeScale, t, n)), i = r
            }, s.rawTime = function() {
                return u || o.wake(), this._totalTime
            };
            var M = g("TweenLite", function(t, n, r) {
                    if (L.call(this, n, r), this.render = M.prototype.render, null == t) throw "Cannot tween a null target.";
                    this.target = t = "string" != typeof t ? t : M.selector(t) || t;
                    var i, s, o, u = t.jquery || t.length && t !== e && t[0] && (t[0] === e || t[0].nodeType && t[0].style && !t.nodeType),
                        a = this.vars.overwrite;
                    if (this._overwrite = a = null == a ? R[M.defaultOverwrite] : "number" == typeof a ? a >> 0 : R[a], (u || t instanceof Array || t.push && p(t)) && "number" != typeof t[0])
                        for (this._targets = o = c(t), this._propLookup = [], this._siblings = [], i = 0; o.length > i; i++) s = o[i], s ? "string" != typeof s ? s.length && s !== e && s[0] && (s[0] === e || s[0].nodeType && s[0].style && !s.nodeType) ? (o.splice(i--, 1), this._targets = o = o.concat(c(s))) : (this._siblings[i] = X(s, this, !1), 1 === a && this._siblings[i].length > 1 && $(s, this, null, 1, this._siblings[i])) : (s = o[i--] = M.selector(s), "string" == typeof s && o.splice(i + 1, 1)) : o.splice(i--, 1);
                    else this._propLookup = {}, this._siblings = X(t, this, !1), 1 === a && this._siblings.length > 1 && $(t, this, null, 1, this._siblings);
                    (this.vars.immediateRender || 0 === n && 0 === this._delay && this.vars.immediateRender !== !1) && (this._time = -l, this.render(-this._delay))
                }, !0),
                _ = function(t) {
                    return t && t.length && t !== e && t[0] && (t[0] === e || t[0].nodeType && t[0].style && !t.nodeType)
                },
                D = function(e, t) {
                    var n, r = {};
                    for (n in e) q[n] || n in t && "transform" !== n && "x" !== n && "y" !== n && "width" !== n && "height" !== n && "className" !== n && "border" !== n || !(!j[n] || j[n] && j[n]._autoCSS) || (r[n] = e[n], delete e[n]);
                    e.css = r
                };
            s = M.prototype = new L, s.constructor = M, s.kill()._gc = !1, s.ratio = 0, s._firstPT = s._targets = s._overwrittenProps = s._startAt = null, s._notifyPluginsOfEnabled = s._lazy = !1, M.version = "1.15.0", M.defaultEase = s._ease = new w(null, null, 1, 1), M.defaultOverwrite = "auto", M.ticker = o, M.autoSleep = !0, M.lagSmoothing = function(e, t) {
                o.lagSmoothing(e, t)
            }, M.selector = e.$ || e.jQuery || function(t) {
                var n = e.$ || e.jQuery;
                return n ? (M.selector = n, n(t)) : "undefined" == typeof document ? t : document.querySelectorAll ? document.querySelectorAll(t) : document.getElementById("#" === t.charAt(0) ? t.substr(1) : t)
            };
            var P = [],
                H = {},
                B = M._internals = {
                    isArray: p,
                    isSelector: _,
                    lazyTweens: P
                },
                j = M._plugins = {},
                F = B.tweenLookup = {},
                I = 0,
                q = B.reservedProps = {
                    ease: 1,
                    delay: 1,
                    overwrite: 1,
                    onComplete: 1,
                    onCompleteParams: 1,
                    onCompleteScope: 1,
                    useFrames: 1,
                    runBackwards: 1,
                    startAt: 1,
                    onUpdate: 1,
                    onUpdateParams: 1,
                    onUpdateScope: 1,
                    onStart: 1,
                    onStartParams: 1,
                    onStartScope: 1,
                    onReverseComplete: 1,
                    onReverseCompleteParams: 1,
                    onReverseCompleteScope: 1,
                    onRepeat: 1,
                    onRepeatParams: 1,
                    onRepeatScope: 1,
                    easeParams: 1,
                    yoyo: 1,
                    immediateRender: 1,
                    repeat: 1,
                    repeatDelay: 1,
                    data: 1,
                    paused: 1,
                    reversed: 1,
                    autoCSS: 1,
                    lazy: 1,
                    onOverwrite: 1
                },
                R = {
                    none: 0,
                    all: 1,
                    auto: 2,
                    concurrent: 3,
                    allOnStart: 4,
                    preexisting: 5,
                    "true": 1,
                    "false": 0
                },
                U = L._rootFramesTimeline = new O,
                z = L._rootTimeline = new O,
                W = B.lazyRender = function() {
                    var e, t = P.length;
                    for (H = {}; --t > -1;) e = P[t], e && e._lazy !== !1 && (e.render(e._lazy[0], e._lazy[1], !0), e._lazy = !1);
                    P.length = 0
                };
            z._startTime = o.time, U._startTime = o.frame, z._active = U._active = !0, setTimeout(W, 1), L._updateRoot = M.render = function() {
                var e, t, n;
                if (P.length && W(), z.render((o.time - z._startTime) * z._timeScale, !1, !1), U.render((o.frame - U._startTime) * U._timeScale, !1, !1), P.length && W(), !(o.frame % 120)) {
                    for (n in F) {
                        for (t = F[n].tweens, e = t.length; --e > -1;) t[e]._gc && t.splice(e, 1);
                        0 === t.length && delete F[n]
                    }
                    if (n = z._first, (!n || n._paused) && M.autoSleep && !U._first && 1 === o._listeners.tick.length) {
                        for (; n && n._paused;) n = n._next;
                        n || o.sleep()
                    }
                }
            }, o.addEventListener("tick", L._updateRoot);
            var X = function(e, t, n) {
                    var r, i, s = e._gsTweenID;
                    if (F[s || (e._gsTweenID = s = "t" + I++)] || (F[s] = {
                            target: e,
                            tweens: []
                        }), t && (r = F[s].tweens, r[i = r.length] = t, n))
                        for (; --i > -1;) r[i] === t && r.splice(i, 1);
                    return F[s].tweens
                },
                V = function(e, t, n, r) {
                    var i, s, o = e.vars.onOverwrite;
                    return o && (i = o(e, t, n, r)), o = M.onOverwrite, o && (s = o(e, t, n, r)), i !== !1 && s !== !1
                },
                $ = function(e, t, n, r, i) {
                    var s, o, u, a;
                    if (1 === r || r >= 4) {
                        for (a = i.length, s = 0; a > s; s++)
                            if ((u = i[s]) !== t) u._gc || V(u, t) && u._enabled(!1, !1) && (o = !0);
                            else if (5 === r) break;
                        return o
                    }
                    var f, c = t._startTime + l,
                        h = [],
                        p = 0,
                        d = 0 === t._duration;
                    for (s = i.length; --s > -1;)(u = i[s]) === t || u._gc || u._paused || (u._timeline !== t._timeline ? (f = f || J(t, 0, d), 0 === J(u, f, d) && (h[p++] = u)) : c >= u._startTime && u._startTime + u.totalDuration() / u._timeScale > c && ((d || !u._initted) && 2e-10 >= c - u._startTime || (h[p++] = u)));
                    for (s = p; --s > -1;)
                        if (u = h[s], 2 === r && u._kill(n, e, t) && (o = !0), 2 !== r || !u._firstPT && u._initted) {
                            if (2 !== r && !V(u, t)) continue;
                            u._enabled(!1, !1) && (o = !0)
                        }
                    return o
                },
                J = function(e, t, n) {
                    for (var r = e._timeline, i = r._timeScale, s = e._startTime; r._timeline;) {
                        if (s += r._startTime, i *= r._timeScale, r._paused) return -100;
                        r = r._timeline
                    }
                    return s /= i, s > t ? s - t : n && s === t || !e._initted && 2 * l > s - t ? l : (s += e.totalDuration() / e._timeScale / i) > t + l ? 0 : s - t - l
                };
            s._init = function() {
                var e, t, n, r, i, s = this.vars,
                    o = this._overwrittenProps,
                    u = this._duration,
                    a = !!s.immediateRender,
                    f = s.ease;
                if (s.startAt) {
                    this._startAt && (this._startAt.render(-1, !0), this._startAt.kill()), i = {};
                    for (r in s.startAt) i[r] = s.startAt[r];
                    if (i.overwrite = !1, i.immediateRender = !0, i.lazy = a && s.lazy !== !1, i.startAt = i.delay = null, this._startAt = M.to(this.target, 0, i), a)
                        if (this._time > 0) this._startAt = null;
                        else if (0 !== u) return
                } else if (s.runBackwards && 0 !== u)
                    if (this._startAt) this._startAt.render(-1, !0), this._startAt.kill(), this._startAt = null;
                    else {
                        0 !== this._time && (a = !1), n = {};
                        for (r in s) q[r] && "autoCSS" !== r || (n[r] = s[r]);
                        if (n.overwrite = 0, n.data = "isFromStart", n.lazy = a && s.lazy !== !1, n.immediateRender = a, this._startAt = M.to(this.target, 0, n), a) {
                            if (0 === this._time) return
                        } else this._startAt._init(), this._startAt._enabled(!1), this.vars.immediateRender && (this._startAt = null)
                    }
                if (this._ease = f = f ? f instanceof w ? f : "function" == typeof f ? new w(f, s.easeParams) : E[f] || M.defaultEase : M.defaultEase, s.easeParams instanceof Array && f.config && (this._ease = f.config.apply(f, s.easeParams)), this._easeType = this._ease._type, this._easePower = this._ease._power, this._firstPT = null, this._targets)
                    for (e = this._targets.length; --e > -1;) this._initProps(this._targets[e], this._propLookup[e] = {}, this._siblings[e], o ? o[e] : null) && (t = !0);
                else t = this._initProps(this.target, this._propLookup, this._siblings, o);
                if (t && M._onPluginEvent("_onInitAllProps", this), o && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)), s.runBackwards)
                    for (n = this._firstPT; n;) n.s += n.c, n.c = -n.c, n = n._next;
                this._onUpdate = s.onUpdate, this._initted = !0
            }, s._initProps = function(t, n, r, i) {
                var s, o, u, a, f, l;
                if (null == t) return !1;
                H[t._gsTweenID] && W(), this.vars.css || t.style && t !== e && t.nodeType && j.css && this.vars.autoCSS !== !1 && D(this.vars, t);
                for (s in this.vars) {
                    if (l = this.vars[s], q[s]) l && (l instanceof Array || l.push && p(l)) && -1 !== l.join("").indexOf("{self}") && (this.vars[s] = l = this._swapSelfInParams(l, this));
                    else if (j[s] && (a = new j[s])._onInitTween(t, this.vars[s], this)) {
                        for (this._firstPT = f = {
                                _next: this._firstPT,
                                t: a,
                                p: "setRatio",
                                s: 0,
                                c: 1,
                                f: !0,
                                n: s,
                                pg: !0,
                                pr: a._priority
                            }, o = a._overwriteProps.length; --o > -1;) n[a._overwriteProps[o]] = this._firstPT;
                        (a._priority || a._onInitAllProps) && (u = !0), (a._onDisable || a._onEnable) && (this._notifyPluginsOfEnabled = !0)
                    } else this._firstPT = n[s] = f = {
                        _next: this._firstPT,
                        t: t,
                        p: s,
                        f: "function" == typeof t[s],
                        n: s,
                        pg: !1,
                        pr: 0
                    }, f.s = f.f ? t[s.indexOf("set") || "function" != typeof t["get" + s.substr(3)] ? s : "get" + s.substr(3)]() : parseFloat(t[s]), f.c = "string" == typeof l && "=" === l.charAt(1) ? parseInt(l.charAt(0) + "1", 10) * Number(l.substr(2)) : Number(l) - f.s || 0;
                    f && f._next && (f._next._prev = f)
                }
                return i && this._kill(i, t) ? this._initProps(t, n, r, i) : this._overwrite > 1 && this._firstPT && r.length > 1 && $(t, this, n, this._overwrite, r) ? (this._kill(n, t), this._initProps(t, n, r, i)) : (this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration) && (H[t._gsTweenID] = !0), u)
            }, s.render = function(e, t, n) {
                var r, i, s, o, u = this._time,
                    a = this._duration,
                    f = this._rawPrevTime;
                if (e >= a) this._totalTime = this._time = a, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1, this._reversed || (r = !0, i = "onComplete"), 0 === a && (this._initted || !this.vars.lazy || n) && (this._startTime === this._timeline._duration && (e = 0), (0 === e || 0 > f || f === l && "isPause" !== this.data) && f !== e && (n = !0, f > l && (i = "onReverseComplete")), this._rawPrevTime = o = !t || e || f === e ? e : l);
                else if (1e-7 > e) this._totalTime = this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== u || 0 === a && f > 0 && f !== l) && (i = "onReverseComplete", r = this._reversed), 0 > e && (this._active = !1, 0 === a && (this._initted || !this.vars.lazy || n) && (f >= 0 && (f !== l || "isPause" !== this.data) && (n = !0), this._rawPrevTime = o = !t || e || f === e ? e : l)), this._initted || (n = !0);
                else if (this._totalTime = this._time = e, this._easeType) {
                    var c = e / a,
                        h = this._easeType,
                        p = this._easePower;
                    (1 === h || 3 === h && c >= .5) && (c = 1 - c), 3 === h && (c *= 2), 1 === p ? c *= c : 2 === p ? c *= c * c : 3 === p ? c *= c * c * c : 4 === p && (c *= c * c * c * c), this.ratio = 1 === h ? 1 - c : 2 === h ? c : .5 > e / a ? c / 2 : 1 - c / 2
                } else this.ratio = this._ease.getRatio(e / a);
                if (this._time !== u || n) {
                    if (!this._initted) {
                        if (this._init(), !this._initted || this._gc) return;
                        if (!n && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration)) return this._time = this._totalTime = u, this._rawPrevTime = f, P.push(this), this._lazy = [e, t], void 0;
                        this._time && !r ? this.ratio = this._ease.getRatio(this._time / a) : r && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
                    }
                    for (this._lazy !== !1 && (this._lazy = !1), this._active || !this._paused && this._time !== u && e >= 0 && (this._active = !0), 0 === u && (this._startAt && (e >= 0 ? this._startAt.render(e, t, n) : i || (i = "_dummyGS")), this.vars.onStart && (0 !== this._time || 0 === a) && (t || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || b))), s = this._firstPT; s;) s.f ? s.t[s.p](s.c * this.ratio + s.s) : s.t[s.p] = s.c * this.ratio + s.s, s = s._next;
                    this._onUpdate && (0 > e && this._startAt && e !== -0.0001 && this._startAt.render(e, t, n), t || (this._time !== u || r) && this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || b)), i && (!this._gc || n) && (0 > e && this._startAt && !this._onUpdate && e !== -0.0001 && this._startAt.render(e, t, n), r && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !t && this.vars[i] && this.vars[i].apply(this.vars[i + "Scope"] || this, this.vars[i + "Params"] || b), 0 === a && this._rawPrevTime === l && o !== l && (this._rawPrevTime = 0))
                }
            }, s._kill = function(e, t, n) {
                if ("all" === e && (e = null), null != e || null != t && t !== this.target) {
                    t = "string" != typeof t ? t || this._targets || this.target : M.selector(t) || t;
                    var r, i, s, o, u, a, f, l, c;
                    if ((p(t) || _(t)) && "number" != typeof t[0])
                        for (r = t.length; --r > -1;) this._kill(e, t[r]) && (a = !0);
                    else {
                        if (this._targets) {
                            for (r = this._targets.length; --r > -1;)
                                if (t === this._targets[r]) {
                                    u = this._propLookup[r] || {}, this._overwrittenProps = this._overwrittenProps || [], i = this._overwrittenProps[r] = e ? this._overwrittenProps[r] || {} : "all";
                                    break
                                }
                        } else {
                            if (t !== this.target) return !1;
                            u = this._propLookup, i = this._overwrittenProps = e ? this._overwrittenProps || {} : "all"
                        }
                        if (u) {
                            if (f = e || u, l = e !== i && "all" !== i && e !== u && ("object" != typeof e || !e._tempKill), n && (M.onOverwrite || this.vars.onOverwrite)) {
                                for (s in f) u[s] && (c || (c = []), c.push(s));
                                if (!V(this, n, t, c)) return !1
                            }
                            for (s in f)(o = u[s]) && (o.pg && o.t._kill(f) && (a = !0), o.pg && 0 !== o.t._overwriteProps.length || (o._prev ? o._prev._next = o._next : o === this._firstPT && (this._firstPT = o._next), o._next && (o._next._prev = o._prev), o._next = o._prev = null), delete u[s]), l && (i[s] = 1);
                            !this._firstPT && this._initted && this._enabled(!1, !1)
                        }
                    }
                    return a
                }
                return this._lazy = !1, this._enabled(!1, !1)
            }, s.invalidate = function() {
                return this._notifyPluginsOfEnabled && M._onPluginEvent("_onDisable", this), this._firstPT = this._overwrittenProps = this._startAt = this._onUpdate = null, this._notifyPluginsOfEnabled = this._active = this._lazy = !1, this._propLookup = this._targets ? {} : [], L.prototype.invalidate.call(this), this.vars.immediateRender && (this._time = -l, this.render(-this._delay)), this
            }, s._enabled = function(e, t) {
                if (u || o.wake(), e && this._gc) {
                    var n, r = this._targets;
                    if (r)
                        for (n = r.length; --n > -1;) this._siblings[n] = X(r[n], this, !0);
                    else this._siblings = X(this.target, this, !0)
                }
                return L.prototype._enabled.call(this, e, t), this._notifyPluginsOfEnabled && this._firstPT ? M._onPluginEvent(e ? "_onEnable" : "_onDisable", this) : !1
            }, M.to = function(e, t, n) {
                return new M(e, t, n)
            }, M.from = function(e, t, n) {
                return n.runBackwards = !0, n.immediateRender = 0 != n.immediateRender, new M(e, t, n)
            }, M.fromTo = function(e, t, n, r) {
                return r.startAt = n, r.immediateRender = 0 != r.immediateRender && 0 != n.immediateRender, new M(e, t, r)
            }, M.delayedCall = function(e, t, n, r, i) {
                return new M(t, 0, {
                    delay: e,
                    onComplete: t,
                    onCompleteParams: n,
                    onCompleteScope: r,
                    onReverseComplete: t,
                    onReverseCompleteParams: n,
                    onReverseCompleteScope: r,
                    immediateRender: !1,
                    lazy: !1,
                    useFrames: i,
                    overwrite: 0
                })
            }, M.set = function(e, t) {
                return new M(e, 0, t)
            }, M.getTweensOf = function(e, t) {
                if (null == e) return [];
                e = "string" != typeof e ? e : M.selector(e) || e;
                var n, r, i, s;
                if ((p(e) || _(e)) && "number" != typeof e[0]) {
                    for (n = e.length, r = []; --n > -1;) r = r.concat(M.getTweensOf(e[n], t));
                    for (n = r.length; --n > -1;)
                        for (s = r[n], i = n; --i > -1;) s === r[i] && r.splice(n, 1)
                } else
                    for (r = X(e).concat(), n = r.length; --n > -1;)(r[n]._gc || t && !r[n].isActive()) && r.splice(n, 1);
                return r
            }, M.killTweensOf = M.killDelayedCallsTo = function(e, t, n) {
                "object" == typeof t && (n = t, t = !1);
                for (var r = M.getTweensOf(e, t), i = r.length; --i > -1;) r[i]._kill(n, e)
            };
            var K = g("plugins.TweenPlugin", function(e, t) {
                this._overwriteProps = (e || "").split(","), this._propName = this._overwriteProps[0], this._priority = t || 0, this._super = K.prototype
            }, !0);
            if (s = K.prototype, K.version = "1.10.1", K.API = 2, s._firstPT = null, s._addTween = function(e, t, n, r, i, s) {
                    var o, u;
                    return null != r && (o = "number" == typeof r || "=" !== r.charAt(1) ? Number(r) - n : parseInt(r.charAt(0) + "1", 10) * Number(r.substr(2))) ? (this._firstPT = u = {
                        _next: this._firstPT,
                        t: e,
                        p: t,
                        s: n,
                        c: o,
                        f: "function" == typeof e[t],
                        n: i || t,
                        r: s
                    }, u._next && (u._next._prev = u), u) : void 0
                }, s.setRatio = function(e) {
                    for (var t, n = this._firstPT, r = 1e-6; n;) t = n.c * e + n.s, n.r ? t = Math.round(t) : r > t && t > -r && (t = 0), n.f ? n.t[n.p](t) : n.t[n.p] = t, n = n._next
                }, s._kill = function(e) {
                    var t, n = this._overwriteProps,
                        r = this._firstPT;
                    if (null != e[this._propName]) this._overwriteProps = [];
                    else
                        for (t = n.length; --t > -1;) null != e[n[t]] && n.splice(t, 1);
                    for (; r;) null != e[r.n] && (r._next && (r._next._prev = r._prev), r._prev ? (r._prev._next = r._next, r._prev = null) : this._firstPT === r && (this._firstPT = r._next)), r = r._next;
                    return !1
                }, s._roundProps = function(e, t) {
                    for (var n = this._firstPT; n;)(e[this._propName] || null != n.n && e[n.n.split(this._propName + "_").join("")]) && (n.r = t), n = n._next
                }, M._onPluginEvent = function(e, t) {
                    var n, r, i, s, o, u = t._firstPT;
                    if ("_onInitAllProps" === e) {
                        for (; u;) {
                            for (o = u._next, r = i; r && r.pr > u.pr;) r = r._next;
                            (u._prev = r ? r._prev : s) ? u._prev._next = u: i = u, (u._next = r) ? r._prev = u : s = u, u = o
                        }
                        u = t._firstPT = i
                    }
                    for (; u;) u.pg && "function" == typeof u.t[e] && u.t[e]() && (n = !0), u = u._next;
                    return n
                }, K.activate = function(e) {
                    for (var t = e.length; --t > -1;) e[t].API === K.API && (j[(new e[t])._propName] = e[t]);
                    return !0
                }, m.plugin = function(e) {
                    if (!(e && e.propName && e.init && e.API)) throw "illegal plugin definition.";
                    var t, n = e.propName,
                        r = e.priority || 0,
                        i = e.overwriteProps,
                        s = {
                            init: "_onInitTween",
                            set: "setRatio",
                            kill: "_kill",
                            round: "_roundProps",
                            initAll: "_onInitAllProps"
                        },
                        o = g("plugins." + n.charAt(0).toUpperCase() + n.substr(1) + "Plugin", function() {
                            K.call(this, n, r), this._overwriteProps = i || []
                        }, e.global === !0),
                        u = o.prototype = new K(n);
                    u.constructor = o, o.API = e.API;
                    for (t in s) "function" == typeof e[t] && (u[s[t]] = e[t]);
                    return o.version = e.version, K.activate([o]), o
                }, r = e._gsQueue) {
                for (i = 0; r.length > i; i++) r[i]();
                for (s in d) d[s].func || e.console.log("GSAP encountered missing dependency: com.greensock." + s)
            }
            u = !1
        }
    }("undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window, "TweenMax"), define("tweenmax", ["jquery"], function(e) {
        return function() {
            var t, n;
            return t || e.TweenMax
        }
    }(this)), 
	
	define("models/Project", ["backbone", "underscore", "jquery"], function(e, t, n) {
        return e.Model.extend({
            defaults: {
                url: "",
                slug: "",
                order: 0,
                originalOrder: 0,
                title: "",
                active: !1,
                loaded: !1,
                element: null,
                yPos: -1,
                yPosBottom: -1,
                height: 0,
                inView: !1,
                scrollProgress: 0,
                loadNewY: -1,
                passedLoadNewY: !1
            }
        })
    }), define("collections/Projects", ["backbone", "underscore", "jquery", "models/Project"], function(e, t, n, r) {
        return e.Collection.extend({
            model: r,
            comparator: "order"
        })
    }), define("models/App", ["backbone", "underscore", "jquery", "collections/Projects"], function(e, t, n, r) {
        var i = e.Model.extend({
            defaults: {
                windowHeight: 0,
                windowWidth: 0,
                viewWidth: 0,
                viewHeight: 0,
                headerHeight: 0,
                isFrontPage: !1,
                mediaQueries: {
                    minWidthSmall: "(min-width: 700px)",
                    minWidthMedium: "(min-width: 1000px)",
                    minWidthLarge: "(min-width: 1150px)"
                },
                isTouchDevice: Modernizr.touch,
                ajaxUrl: "",
                ajaxNonce: "",
                siteUrl: "",
                themeUrl: "",
                projectsData: new r
            }
        });
        return new i
    }), define("regions/TransitionRegion", ["backbone", "marionette", "vent"], function(e, t, n) {
        var r = e.Marionette.Region.extend({
            show: function(e, n) {
                if (!this._ensureElement()) return;
                this._ensureViewIsIntact(e);
                var r = this,
                    i = n || {},
                    s = e !== this.currentView,
                    o = !!i.preventDestroy,
                    u = !!i.forceShow,
                    a = !!this.currentView,
                    f = s && !o,
                    l = s || u;
                a && this.triggerMethod("before:swapOut", this.currentView, this, n);
                if (l) {
                    e.once("destroy", this.empty, this), e.render(), a && (this.currentView.off("destroy", this.empty, this), this.triggerMethod("before:swap", e, this, n)), this.triggerMethod("before:show", e, this, n), t.triggerMethodOn(e, "before:show", e, this, n), this.attachHtml(e);
                    if (a) {
                        var c = this.currentView;
                        this.triggerMethod("swapOut", e, this, n), c.transitionOut(), t.triggerMethodOn(c, "transitionOut", c, this, n), e.waitForOldView ? this.listenToOnce(c, "views:transitionOutEnd", function() {
                            r._startViewTransitionIn(e, a, f, c)
                        }) : this._startViewTransitionIn(e, a, f, c)
                    }
                    return this.currentView = e, a ? this.triggerMethod("swap", e, this, n) : this._startViewTransitionIn(e, a, f, c), this
                }
                return this
            },
            _viewTransitionIn: function(e) {
                var n = this;
                n.triggerMethod("before:transitionIn", e, n), e.transitionIn(), t.triggerMethodOn(e, "transitionIn", e, n), n.listenToOnce(e, "views:transitionInEnd", function() {
                    n.triggerMethod("show", e, n), t.triggerMethodOn(e, "show", e, n)
                })
            },
            _startViewTransitionIn: function(e, t, n, r) {
                if (!!e.waitForDataLoaded && !e.isDataLoaded) {
                    var i = this;
                    this.listenToOnce(e, "views:dataLoaded", function() {
                        i._viewTransitionIn(e)
                    })
                } else this._viewTransitionIn(e);
                t && n && (r.destroy && !r.isDestroyed ? r.destroy() : r.remove && (r.remove(), r.isDestroyed = !0))
            },
            empty: function() {
                var e = this.currentView;
                if (!e) return;
                return
            },
            attachHtml: function(e) {
                this.el.appendChild(e.el)
            }
        });
        return r
    }), define("isMobile", [], function() {
        var e = {
            Android: function() {
                return navigator.userAgent.match(/Android/i)
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i)
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i)
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i)
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i)
            },
            any: function() {
                return e.Android() || e.BlackBerry() || e.iOS() || e.Opera() || e.Windows()
            }
        };
        return e
    }), define("views/layout/Header", ["backbone", "marionette", "vent", "jquery", "underscore", "isMobile", "tweenmax", "models/App"], function(e, t, n, r, i, s, o, u) {
        var a = e.Marionette.ItemView.extend({
            el: "header.header-top",
            template: !1,
            ui: {
                mobileMenu: ".mobile-menu",
                mobileMenuClose: ".mobile-menu-close"
            },
            events: {
                "click @ui.mobileMenu": "toggleMenu",
                "click @ui.mobileMenuClose": "toggleMenu"
            },
            initialize: function(e) {},
            toggleMenu: function() {
                n.trigger(r("body").hasClass("sidebar-open") ? "sidebar:close" : "sidebar:open")
            },
            resize: function() {
                u.set("headerHeight", this.$el.height())
            }
        });
        return a
    }), define("views/layout/Sidebar", ["backbone", "marionette", "vent", "jquery", "underscore", "isMobile", "tweenmax", "models/App"], function(e, t, n, r, i, s, o, u) {
        var a = e.Marionette.ItemView.extend({
            el: "aside.nav-sidebar",
            template: !1,
            ui: {
                thumb: ".nav-thumb",
                allItems: "nav ul li a",
                navMain: ".nav-main",
                aboutItem: 'nav ul li[data-postname="about-contact"]',
                projectItemContainer: ".nav-projects",
                projectItems: ".nav-projects li"
            },
            events: {
                "click @ui.aboutItem": "scrollToTop",
                "mouseover @ui.projectItems": "projectHover",
                "mousemove @ui.projectItems": "projectMove",
                "mouseleave @ui.projectItemContainer": "projectLeave"
            },
            initialize: function(e) {
                this.listenTo(n, "sidebar:open", this.animateIn), this.listenTo(n, "sidebar:close", this.animateOut), this.listenTo(u.get("projectsData"), "change:inView", this.projectInViewChange), this.listenTo(u.get("projectsData"), "change:scrollProgress", this.changeActiveProjectProgessBar), this.isMobile = s.any(), this.isMobile && (this.isTablet = r("html").hasClass("tablet") ? !0 : !1), this.debouncedResizeEnd = i.debounce(this.resizeEnd, 250)
            },
            onShow: function() {},
            onRender: function() {
                !this.isMobile && !r("body").hasClass("no-sidebar") && this.animateIn(), this.thumbBounding = this.ui.thumb[0].getBoundingClientRect()
            },
            onDestroy: function() {},
            resize: function() {
                var e = this;
                t.triggerMethodOn(e, "resize"), this.debouncedResizeEnd()
            },
            resizeEnd: function() {
                var e = this;
                window.innerWidth >= 1025 && !r("body").hasClass("is-overlay") && (r("body").hasClass("no-sidebar") || n.trigger("sidebar:open")), t.triggerMethodOn(e, "resizeEnd")
            },
            animateIn: function() {
                if (r("body").hasClass("sidebar-open")) return;
                var e = new TimelineMax({
                    paused: !0
                });
                e.to(this.el, 1, {
                    autoAlpha: 1,
                    x: "0%",
                    ease: Expo.easeInOut
                }), e.restart(), r("body").addClass("sidebar-open")/*r("body").addClass("sidebar-open intro-done is-ready"), r("#container").addClass("show-intro app-started") eguyz*/
            },
            animateOut: function() {
                var e = this.isTablet ? window.innerWidth <= 1024 ? "-98%" : "-100%" : "-100%",
                    t = this.isTablet ? 1 : 0,
                    n = new TimelineMax({
                        paused: !0
                    });
                n.to(this.el, .9, {
                    x: e,
                    ease: Expo.easeInOut
                }), n.set(this.el, {
                    autoAlpha: t
                }), n.restart(), r("body").removeClass("sidebar-open"), r(".nav-sidebar").scrollTop(0)
            },
            scrollToTop: function(e) {
                e.preventDefault(), TweenLite.to(r(".enable-smooth > h1"), .7, {
                    autoAlpha: 0,
                    onComplete: function() {
                        this.target.remove()
                    }
                }), r(window).scrollTop(0)
            },
            projectHover: function(e) {
                if (this.isMobile || !e.currentTarget.hasAttribute("data-thumb")) return;
                var t = e.currentTarget.getAttribute("data-thumb");
                this.ui.thumb.css("background-image", "url(" + t + ")")
            },
            projectMove: function(e) {
                if (this.isMobile) return;
                var t = {
                    x: e.clientX + this.thumbBounding.width / 2.5,
                    y: e.clientY - this.thumbBounding.height / 2.5
                };
                TweenLite.set(this.ui.thumb, {
                    autoAlpha: 1,
                    x: t.x,
                    y: t.y,
                    z: 0
                })
            },
            projectLeave: function(e) {
                TweenLite.set(this.ui.thumb, {
                    autoAlpha: 0
                })
            },
            projectInViewChange: function(e) {
                var t = this,
                    n = e.get("slug"),
                    i = this.ui.projectItems.filter('[data-postname="' + n + '"]');
                if (e.get("inView")) {
                    i.addClass("active"), this.$menuItemActive = i, this.currentTween && this.currentTween.progress() !== 1 && (this.currentTween.vars.onComplete(), this.currentTween.kill());
                    if (i.is(":last-child")) this.currentTween = TweenLite.to(i, .5, {
                        autoAlpha: 0,
                        height: 0,
                        ease: Expo.easeOut,
                        onComplete: function() {
                            i.removeClass("not-active"), t.ui.projectItemContainer.prepend(i), TweenLite.to(i, 1, {
                                autoAlpha: 1,
                                clearProps: "all"
                            }), TweenLite.to(i, .4, {
                                height: "80px"
                            })
                        }
                    });
                    else {
                        var s = i.prevAll("li");
                        s = r(s.toArray().reverse()), this.currentTween = TweenLite.to(s, .7, {
                            autoAlpha: 0,
                            height: 0,
                            ease: Expo.easeOut,
                            onComplete: function() {
                                s.addClass("not-active"), t.ui.projectItemContainer.append(s), TweenLite.to(s, 1, {
                                    autoAlpha: 1,
                                    clearProps: "all"
                                }), TweenLite.to(s, .4, {
                                    height: "80px"
                                })
                            }
                        })
                    }
                } else i.removeClass("active")
            },
            changeActiveProjectProgessBar: function(e) {
                var t = e.get("scrollProgress");
                this.$menuItemActive && this.$menuItemActive.length > 0 && this.$menuItemActive.find(".progress-bar").css({
                    height: Math.max(0, Math.min(100, t * 100)) + "%"
                })
            }
        });
        return a
    }), 
	
	
	define("views/Preloader", ["backbone", "marionette", "vent", "jquery", "underscore", "isMobile", "models/App", "tweenmax"], function(e, t, n, r, i, s, o, u) {
        var a = t.ItemView.extend({
            template: !1,
            el: "#preloader",
            ui: {
                preloaderElements: ".els"
            },
            events: {},
            initialize: function() {
                this.loadingComplete = !1, this.timelineDone = !1, this.tl = null, this.listenTo(n, "preloader:loadcomplete", this.loadComplete)
            },
            onRender: function() {				
                var e = this;
                e.timelineDone = !1, this.animateIn(function() {
                    e.timelineDone = !0, e.loadingComplete && e.animateOut()
                })
            },
            loadComplete: function() {								
                this.loadingComplete = !0, this.timelineDone && this.animateOut()
            },
            animateIn: function(e) {				
                this.tl = new TimelineMax({
                    paused: !0,
                    onComplete: e
                }), this.tl.to(this.$el, 1, {
                    autoAlpha: 1
                }), this.tl.staggerTo(this.ui.preloaderElements, 1, {
                    autoAlpha: 1,
                    ease: Expo.easeInOut
                }, .08, 0), this.tl.restart()
            },
            animateOut: function(e) {
                TweenLite.killTweensOf(this.tl), this.tl = new TimelineMax({
                    paused: !0,
                    onComplete: i.bind(this.destroy, this)
                }), this.tl.staggerTo(this.ui.preloaderElements, .6, {
                    autoAlpha: 0,
                    ease: Expo.easeInOut
                }, .05), this.tl.to(this.$el, 1, {
                    autoAlpha: 0
                }, "-=1"), this.tl.restart(), 
				//eguyz
				r("body").addClass("no-sidebar"),	
				r("body").removeClass("sidebar-open"),
				r("#container").addClass("show-intro app-started"),
				r(".intro-text").removeAttr("style"),
				//r(".intro-text").css("visibility", "visible"),
				r(".intro-text").css("opacity", "1"),
				r(".nav-sidebar").addClass("remove-sidebar");
            }
        });
        return a
    }), 
	
	
	
	
	define("views/App", ["backbone", "marionette", "vent", "jquery", "underscore", "tweenmax", "models/App", "regions/TransitionRegion", "views/layout/Header", "views/layout/Sidebar", "views/Preloader"], function(e, t, n, r, i, s, o, u, a, f, l) {
        var c = e.Marionette.LayoutView.extend({
            el: "body",
            template: !1,
            ui: {
                window: r(window),
                body: r("body"),
                container: "#container"
            },
            events: {
                "click a": "internalNavigate"
            },
            initialize: function(e) {
                this.addRegions({
                    navRegion: {
                        selector: ".nav-bar"
                    },
                    overlayRegion: {
                        selector: "#overlay",
                        regionClass: u
                    },
                    preloaderViewRegion: {
                        selector: e.preloaderViewElement
                    },
                    mainViewRegion: {
                        selector: e.mainViewElement,
                        regionClass: u
                    }
                }), this.headerView = (new a).render(), this.sidebarView = (new f).render(), this.preloaderView = new l, this.preloaderViewRegion.attachView(this.preloaderView), this.listenTo(n, "showContentLoader", this.addContentLoaderTo), this.listenTo(n, "hideContentLoader", this.removeContentLoader), this.listenTo(this.mainViewRegion, "before:transitionIn", this.onBeforeViewTransitionIn), this.listenTo(this.mainViewRegion, "before:show", this.onBeforeViewShow), this.listenTo(this.mainViewRegion, "show", this.onViewShow), this.ui.window.on("resize", i.bind(i.throttle(this.resize, 20), this))
            },
            onRender: function() {
                this.ui.window = r(window), this.resize();
                var e = document,
                    t = e.body,
                    n = e.createElement("div");
                return n.className = "sf", t.appendChild(n), window.sw = n.offsetWidth - n.clientWidth, t.removeChild(n), this
            },
            onBeforeViewTransitionIn: function() {
                this.ui.window.scrollTop(0)
            },
            onBeforeViewShow: function(e) {
                this.ui.container.addClass("app-started"), this.resize("beforeViewShow"), this.resizeEnd()
            },
            onViewShow: function(e) {
                document.title = e.pageData.get("title")
            },
            onViewHide: function(e) {},
            showPreloader: function(e) {
                this.preloaderView.render(), typeof e == "function" && this.preloaderView.once("destroy", e)
            },
            fixScrollbar: function() {
                var e = window.innerWidth - (window.innerWidth >= 1025 ? 280 : 0);
                r("body").css({
                    width: window.innerWidth + window.sw
                }), r(".view-article.about").css({
                    width: e
                })
            },
            resize: function(e) {
                this.cacheScrollValues(), this.cacheWindowDimensions(), this.$el.addClass("resizing"), this.headerView.resize(), this.sidebarView.resize(), this.mainViewRegion.currentView && i.isFunction(this.mainViewRegion.currentView.resize) && this.mainViewRegion.currentView.resize(e), !r("html").hasClass("mobile") && !r("html").hasClass("tablet") && this.fixScrollbar(), this.resizeEnd()
            },
            resizeEnd: i.debounce(function() {
                this.cacheScrollValues(), this.cacheWindowDimensions(), this.$el.removeClass("resizing")
            }, 500),
            cacheWindowDimensions: function() {
                o.set({
                    windowWidth: window.innerWidth ? window.innerWidth : this.ui.window.width(),
                    windowHeight: window.innerHeight ? window.innerHeight : this.ui.window.height()
                })
            },
            cacheScrollValues: function() {
                o.set({
                    scrollTop: this.ui.window.scrollTop(),
                    scrollLeft: this.ui.window.scrollLeft()
                })
            },
            internalNavigate: function(t) {
                var n = r(t.currentTarget),
                    i = n.attr("href") || n.attr("data-href"),
                    s = n.attr("target") == "_blank";
                if (n.hasClass("external-link")) return !0;
                if (n.hasClass("no-hijack")) return !1;
                var o = window.location.protocol + "//" + window.location.host;
                if ((i.indexOf(o) == 0 || i.indexOf("://") == -1) && !s && i.substr(0, 1) != "#") {
                    t.preventDefault();
                    var u = i.replace(o, "");
                    e.history.navigate(u, {
                        trigger: !0
                    })
                }
            },
            addContentLoaderTo: function(e) {
                if (r(e).length > 0 && r(e).find(".content-loader").length == 0) {
                    var t = r('<div class="content-loader"><span></span></div>').appendTo(r(e));
                    i.delay(function() {
                        t.addClass("active")
                    }, 1)
                }
            },
            removeContentLoader: function() {
                r(".content-loader").removeClass("active"), i.delay(function() {
                    r(".content-loader").remove()
                }, 500)
            }
        });
        return c
    }), 
	
	define("controllers/Preloader", ["app", "backbone", "underscore", "marionette", "vent", "models/App", "collections/Projects", "models/Project"], function(e, t, n, r, i, s, o, u) {
        return t.Marionette.Object.extend({
            initialize: function() {
                var e = this;
                this.isLoaded = !1, this.loadCount = 0, this.loadedCount = 0, e.load()
            },
            load: function() {
                this.loadProjects(), this.loadCount = 1
            },
            loaded: function() {
                var e = this;
                this.loadedCount++, this.loadedCount >= this.loadCount && (e.trigger("loadcomplete"), i.trigger("preloader:loadcomplete"))
            },
            loadProjects: function() {
				
                var e = this,
                    t = s.get("projectsData");
                $.post(s.get("ajaxUrl"), {
                    action: "get_projects"
                }).done(function(r) {
                    r && n.each(r, function(e) {
						
                        var n = new u({
                            url: e.permalink,
                            title: e.title,
                            slug: e.slug,
                            order: e.order,
                            originalOrder: e.order
                        });
                        t.push(n)
                    }), s.set({
                        projectsData: t
                    }), e.loaded()
                })
            }
        })
    }), 
	
	
	define("models/PageData", ["backbone", "underscore"], function(e, t) {
        return e.Model.extend({
            defaults: {
                title: "",
                classNames: "",
                viewData: ""
            },
            parse: function(e) {
                var t = $(e),
                    n = t.find(this.get("element"));
                return {
                    title: t.filter("title").text() || t.find("title").text(),
                    classNames: n.attr("class"),
                    viewData: n
                }
            }
        })
    }), 
	
	
	
	define("views/Page", ["backbone", "marionette", "vent", "jquery", "underscore", "modernizr", "isMobile", "models/App", "models/PageData", "tweenmax"], function(e, t, n, r, i, s, o, u, a, f) {
        var l = e.Marionette.ItemView.extend({
            tagName: "div",
            className: "views",
            template: !1,
            ui: {
                section: ".view-article",
                sidebar: ".nav-sidebar",
                smoothEls: "*[data-ease]"
            },
            events: {},
            defaultOptions: {
                waitForOldView: !0,
                waitForDataLoaded: !0,
                minimumHeight: !1,
                isOverlayView: !1
            },
            initialize: function(e) {
				//eguyz
                var t = this;
                return this.options = i.extend(this.defaultOptions, e), this.waitForOldView = this.options.waitForOldView, this.waitForDataLoaded = this.options.waitForDataLoaded, this.minimumHeight = this.options.minimumHeight, this.isOverlayView = this.options.isOverlayView, this.isScrollDisabled = !1, this.originalUrl = e.url, this.isDataLoaded = !1, this.isMobile = o.any(), this.scrollTop = 0, this.lastScrollTop = 0, this.targetY = 0, this.ease = .1, this.rAF = undefined, this.$el = r(this.el), this.hasCss3d = s.csstransforms3d, this.prefixedTransform = s.prefixed("transform"), this.debouncedResizeEnd = i.debounce(this.resizeEnd, 250), this.pageData = new a({
                    element: e.elementSelector
                }), this.preventScroll = !1, e.isInitialView ? t.parseStaticDomContent("html") : this.pageData.fetch({
                    url: this.originalUrl,
                    dataType: "html",
                    success: i.bind(this.dataLoaded, this),
                    error: function(e, n) {
                        n.status == 404 && t.parseStaticDomContent(n.responseText)
                    }
                }), this
            },
            parseStaticDomContent: function(e) {
                this.pageData.set(this.pageData.parse(e)), this.dataLoaded()
            },
            dataLoaded: function() {
                var e = this;
                this.trigger("views:dataLoaded"), this.isDataLoaded = !0, this.$el.addClass(this.pageData.get("classNames")).html(this.pageData.get("viewData").html()), this.viewDataAdded()
            },
            viewDataAdded: function() {
                var e = this;
                this.bindUIElements(), this.isOverlayView ? r("body").addClass("is-overlay") : (this.listenTo(n, "overlay:open", this.disableScroll), this.listenTo(n, "overlay:closed", this.enableScroll), r("body").hasClass("is-overlay") || this.enableScroll()), t.triggerMethodOn(this, "viewDataAdded"), this.resize()
            },
            debounceScroll: function() {
                this.scrollTop = r(window).scrollTop(), t.triggerMethodOn(this, "scroll", this.scrollTop)
            },
            scrollTo: function(e) {
                window.scrollTo(0, e)
            },
            smooth: function() {
                var e = this;
                if (!this.ui.section[0] || this.isMobile) return;
                this.rAF = requestAnimationFrame(this.smooth.bind(this)), this.targetY += (this.scrollTop - this.targetY) * this.ease;
                var t = this.targetY.toFixed(2),
                    n = this.hasCss3d ? "translate3d(0, -" + t + "px, 0)" : "translate(0, -" + t + "px)",
                    i = this.ui.section[0].style;
                i[this.prefixedTransform] = n, this.ui.smoothEls.each(function() {
                    var t = r(this),
                        n = r(".mask", this),
                        i = t.offset().top,
                        s = t.outerHeight(),
                        o = i + s;
                    if (!(o < e.scrollTop - 100 || i > e.scrollTop + window.innerHeight) && o > e.scrollTop - 100 && i < e.scrollTop + window.innerHeight) {
                        n && TweenLite.to(n, 4, {
                            x: "100%",
                            ease: Expo.easeOut
                        });
                        var u = parseFloat(t.data("ease")) || .1,
                            a = ((o - e.targetY) * u).toFixed(0),
                            f = e.hasCss3d ? "translate3d(0, -" + a + "px, 0)" : "translate(0, -" + a + "px)",
                            l = this.style;
                        l[e.prefixedTransform] = f
                    }
                })
            },
            transitionIn: function() {
				
                var e = this;
                e.trigger("views:transitionInStart"), i.delay(function() {
					
                    e.$el.addClass("view-visible")
                }, 1), i.delay(function() {
                    e.trigger("views:transitionInEnd")
                }, 1301)
            },
            transitionOut: function() {
                var e = this;
                n.trigger("views:hide", this.pageData), e.trigger("views:transitionOutStart"), e.$el.removeClass("view-visible"), i.delay(function() {
                    e.trigger("views:transitionOutEnd")
                }, 1300)
            },
            resize: function(e) {
                var n = this,
                    r = u.get("windowHeight") - u.get("headerHeight");
                u.set("viewWidth", this.$el.width()), u.set("viewHeight", this.$el.height()), this.minimumHeight && this.$el.css({
                    "min-height": r
                })/*, this.bounding = this.isOverlayView ? this.isMobile ? this.ui.section[0].getBoundingClientRect().height : this.ui.section[0].getBoundingClientRect().height - window.innerHeight : this.ui.section[0].getBoundingClientRect().height, t.triggerMethodOn(n, "resize"), this.debouncedResizeEnd() eguyz*/
            },
            resizeEnd: function() {
                var e = this;
                this.$el.hasClass("enable-smooth") && this.$el.css({
                    height: this.bounding
                }), t.triggerMethodOn(e, "resizeEnd")
            },
            disableScroll: function() {
                this.lastScrollTop = r(window).scrollTop(), this.isScrollDisabled = !0, this.isMobile || (cancelAnimationFrame(this.rAF), this.rAF = undefined), r(window).off("scroll.pageview." + this.cid), this.$el.removeClass("enable-smooth"), this.$el.addClass("disable-smooth"), this.$el.css({
                    height: window.innerHeight
                })
            },
            enableScroll: function() {
                var e = this;
                this.isScrollDisabled = !1, this.$el.removeClass("disable-smooth"), this.$el.addClass("enable-smooth"), this.$el.hasClass("enable-smooth") && this.$el.css({
                    height: this.bounding
                }), setTimeout(function() {
                    e.targetY = e.lastScrollTop, e.scrollTo(e.lastScrollTop)
                }, 650), this.isMobile || this.smooth(), r(window).on("scroll.pageview." + this.cid, i.bind(this.debounceScroll, this))
            },
            onDestroy: function() {
                this.disableScroll()
            },
            onTransitionIn: function() {},
            onTransitionOut: function() {},
            onRender: function() {},
            onShow: function() {},
            onResize: function() {},
            onResizeEnd: function() {}
        });
        return l
    }),
    function() {
        "use strict";

        function s(e) {
            e.fn.swiper = function(n) {
                var r;
                return e(this).each(function() {
                    var e = new t(this, n);
                    r || (r = e)
                }), r
            }
        }
        var e, t = function(n, r) {
            function f() {
                return a.params.direction === "horizontal"
            }

            function l(e) {
                return Math.floor(e)
            }

            function c() {
                a.autoplayTimeoutId = setTimeout(function() {
                    a.params.loop ? (a.fixLoop(), a._slideNext()) : a.isEnd ? r.autoplayStopOnLast ? a.stopAutoplay() : a._slideTo(0) : a._slideNext()
                }, a.params.autoplay)
            }

            function p(t, n) {
                var r = e(t.target);
                if (!r.is(n))
                    if (typeof n == "string") r = r.parents(n);
                    else if (n.nodeType) {
                    var i;
                    return r.parents().each(function(e, t) {
                        t === n && (i = n)
                    }), i ? n : undefined
                }
                return r.length === 0 ? undefined : r[0]
            }

            function L(e, t) {
                t = t || {};
                var n = window.MutationObserver || window.WebkitMutationObserver,
                    r = new n(function(e) {
                        e.forEach(function(e) {
                            a.onResize(!0), a.emit("onObserverUpdate", a, e)
                        })
                    });
                r.observe(e, {
                    attributes: typeof t.attributes == "undefined" ? !0 : t.attributes,
                    childList: typeof t.childList == "undefined" ? !0 : t.childList,
                    characterData: typeof t.characterData == "undefined" ? !0 : t.characterData
                }), a.observers.push(r)
            }

            function A(e) {
                e.originalEvent && (e = e.originalEvent);
                var t = e.keyCode || e.charCode;
                if (!a.params.allowSwipeToNext && (f() && t === 39 || !f() && t === 40)) return !1;
                if (!a.params.allowSwipeToPrev && (f() && t === 37 || !f() && t === 38)) return !1;
                if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey) return;
                if (document.activeElement && document.activeElement.nodeName && (document.activeElement.nodeName.toLowerCase() === "input" || document.activeElement.nodeName.toLowerCase() === "textarea")) return;
                if (t === 37 || t === 39 || t === 38 || t === 40) {
                    var n = !1;
                    if (a.container.parents(".swiper-slide").length > 0 && a.container.parents(".swiper-slide-active").length === 0) return;
                    var r = {
                            left: window.pageXOffset,
                            top: window.pageYOffset
                        },
                        i = window.innerWidth,
                        s = window.innerHeight,
                        o = a.container.offset();
                    a.rtl && (o.left = o.left - a.container[0].scrollLeft);
                    var u = [
                        [o.left, o.top],
                        [o.left + a.width, o.top],
                        [o.left, o.top + a.height],
                        [o.left + a.width, o.top + a.height]
                    ];
                    for (var l = 0; l < u.length; l++) {
                        var c = u[l];
                        c[0] >= r.left && c[0] <= r.left + i && c[1] >= r.top && c[1] <= r.top + s && (n = !0)
                    }
                    if (!n) return
                }
                if (f()) {
                    if (t === 37 || t === 39) e.preventDefault ? e.preventDefault() : e.returnValue = !1;
                    (t === 39 && !a.rtl || t === 37 && a.rtl) && a.slideNext(), (t === 37 && !a.rtl || t === 39 && a.rtl) && a.slidePrev()
                } else {
                    if (t === 38 || t === 40) e.preventDefault ? e.preventDefault() : e.returnValue = !1;
                    t === 40 && a.slideNext(), t === 38 && a.slidePrev()
                }
            }

            function M(e) {
                e.originalEvent && (e = e.originalEvent);
                var t = a.mousewheel.event,
                    n = 0;
                if (e.detail) n = -e.detail;
                else if (t === "mousewheel")
                    if (a.params.mousewheelForceToAxis)
                        if (f()) {
                            if (!(Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDeltaY))) return;
                            n = e.wheelDeltaX
                        } else {
                            if (!(Math.abs(e.wheelDeltaY) > Math.abs(e.wheelDeltaX))) return;
                            n = e.wheelDeltaY
                        } else n = e.wheelDelta;
                else if (t === "DOMMouseScroll") n = -e.detail;
                else if (t === "wheel")
                    if (a.params.mousewheelForceToAxis)
                        if (f()) {
                            if (!(Math.abs(e.deltaX) > Math.abs(e.deltaY))) return;
                            n = -e.deltaX
                        } else {
                            if (!(Math.abs(e.deltaY) > Math.abs(e.deltaX))) return;
                            n = -e.deltaY
                        } else n = Math.abs(e.deltaX) > Math.abs(e.deltaY) ? -e.deltaX : -e.deltaY;
                a.params.mousewheelInvert && (n = -n);
                if (!a.params.freeMode) {
                    if ((new window.Date).getTime() - a.mousewheel.lastScrollTime > 60)
                        if (n < 0) {
                            if (!a.isEnd) a.slideNext();
                            else if (a.params.mousewheelReleaseOnEdges) return !0
                        } else if (!a.isBeginning) a.slidePrev();
                    else if (a.params.mousewheelReleaseOnEdges) return !0;
                    a.mousewheel.lastScrollTime = (new window.Date).getTime()
                } else {
                    var r = a.getWrapperTranslate() + n;
                    r > 0 && (r = 0), r < a.maxTranslate() && (r = a.maxTranslate()), a.setWrapperTransition(0), a.setWrapperTranslate(r), a.updateProgress(), a.updateActiveIndex(), a.params.freeModeSticky && (clearTimeout(a.mousewheel.timeout), a.mousewheel.timeout = setTimeout(function() {
                        a.slideReset()
                    }, 300));
                    if (r === 0 || r === a.maxTranslate()) return
                }
                return a.params.autoplay && a.stopAutoplay(), e.preventDefault ? e.preventDefault() : e.returnValue = !1, !1
            }

            function _(t, n) {
                t = e(t);
                var r, i, s;
                r = t.attr("data-swiper-parallax") || "0", i = t.attr("data-swiper-parallax-x"), s = t.attr("data-swiper-parallax-y"), i || s ? (i = i || "0", s = s || "0") : f() ? (i = r, s = "0") : (s = r, i = "0"), i.indexOf("%") >= 0 ? i = parseInt(i, 10) * n + "%" : i = i * n + "px", s.indexOf("%") >= 0 ? s = parseInt(s, 10) * n + "%" : s = s * n + "px", t.transform("translate3d(" + i + ", " + s + ",0px)")
            }

            function H(e) {
                return e.indexOf("on") !== 0 && (e[0] !== e[0].toUpperCase() ? e = "on" + e[0].toUpperCase() + e.substring(1) : e = "on" + e), e
            }
            if (this instanceof t) {
                var i = {
                        direction: "horizontal",
                        touchEventsTarget: "container",
                        initialSlide: 0,
                        speed: 300,
                        autoplay: !1,
                        autoplayDisableOnInteraction: !0,
                        freeMode: !1,
                        freeModeMomentum: !0,
                        freeModeMomentumRatio: 1,
                        freeModeMomentumBounce: !0,
                        freeModeMomentumBounceRatio: 1,
                        freeModeSticky: !1,
                        setWrapperSize: !1,
                        virtualTranslate: !1,
                        effect: "slide",
                        coverflow: {
                            rotate: 50,
                            stretch: 0,
                            depth: 100,
                            modifier: 1,
                            slideShadows: !0
                        },
                        cube: {
                            slideShadows: !0,
                            shadow: !0,
                            shadowOffset: 20,
                            shadowScale: .94
                        },
                        fade: {
                            crossFade: !1
                        },
                        parallax: !1,
                        scrollbar: null,
                        scrollbarHide: !0,
                        keyboardControl: !1,
                        mousewheelControl: !1,
                        mousewheelReleaseOnEdges: !1,
                        mousewheelInvert: !1,
                        mousewheelForceToAxis: !1,
                        hashnav: !1,
                        spaceBetween: 0,
                        slidesPerView: 1,
                        slidesPerColumn: 1,
                        slidesPerColumnFill: "column",
                        slidesPerGroup: 1,
                        centeredSlides: !1,
                        slidesOffsetBefore: 0,
                        slidesOffsetAfter: 0,
                        roundLengths: !1,
                        touchRatio: 1,
                        touchAngle: 45,
                        simulateTouch: !0,
                        shortSwipes: !0,
                        longSwipes: !0,
                        longSwipesRatio: .5,
                        longSwipesMs: 300,
                        followFinger: !0,
                        onlyExternal: !1,
                        threshold: 0,
                        touchMoveStopPropagation: !0,
                        pagination: null,
                        paginationElement: "span",
                        paginationClickable: !1,
                        paginationHide: !1,
                        paginationBulletRender: null,
                        resistance: !0,
                        resistanceRatio: .85,
                        nextButton: null,
                        prevButton: null,
                        watchSlidesProgress: !1,
                        watchSlidesVisibility: !1,
                        grabCursor: !1,
                        preventClicks: !0,
                        preventClicksPropagation: !0,
                        slideToClickedSlide: !1,
                        lazyLoading: !1,
                        lazyLoadingInPrevNext: !1,
                        lazyLoadingOnTransitionStart: !1,
                        preloadImages: !0,
                        updateOnImagesReady: !0,
                        loop: !1,
                        loopAdditionalSlides: 0,
                        loopedSlides: null,
                        control: undefined,
                        controlInverse: !1,
                        controlBy: "slide",
                        allowSwipeToPrev: !0,
                        allowSwipeToNext: !0,
                        swipeHandler: null,
                        noSwiping: !0,
                        noSwipingClass: "swiper-no-swiping",
                        slideClass: "swiper-slide",
                        slideActiveClass: "swiper-slide-active",
                        slideVisibleClass: "swiper-slide-visible",
                        slideDuplicateClass: "swiper-slide-duplicate",
                        slideNextClass: "swiper-slide-next",
                        slidePrevClass: "swiper-slide-prev",
                        wrapperClass: "swiper-wrapper",
                        bulletClass: "swiper-pagination-bullet",
                        bulletActiveClass: "swiper-pagination-bullet-active",
                        buttonDisabledClass: "swiper-button-disabled",
                        paginationHiddenClass: "swiper-pagination-hidden",
                        observer: !1,
                        observeParents: !1,
                        a11y: !1,
                        prevSlideMessage: "Previous slide",
                        nextSlideMessage: "Next slide",
                        firstSlideMessage: "This is the first slide",
                        lastSlideMessage: "This is the last slide",
                        paginationBulletMessage: "Go to slide {{index}}",
                        runCallbacksOnInit: !0
                    },
                    s = r && r.virtualTranslate;
                r = r || {};
                for (var o in i)
                    if (typeof r[o] == "undefined") r[o] = i[o];
                    else if (typeof r[o] == "object")
                    for (var u in i[o]) typeof r[o][u] == "undefined" && (r[o][u] = i[o][u]);
                var a = this;
                a.version = "3.1.0", a.params = r, a.classNames = [], typeof e != "undefined" && typeof Dom7 != "undefined" && (e = Dom7);
                if (typeof e == "undefined") {
                    typeof Dom7 == "undefined" ? e = window.Dom7 || window.Zepto || window.jQuery : e = Dom7;
                    if (!e) return
                }
                a.$ = e, a.container = e(n);
                if (a.container.length === 0) return;
                if (a.container.length > 1) {
                    a.container.each(function() {
                        new t(this, r)
                    });
                    return
                }
                a.container[0].swiper = a, a.container.data("swiper", a), a.classNames.push("swiper-container-" + a.params.direction), a.params.freeMode && a.classNames.push("swiper-container-free-mode"), a.support.flexbox || (a.classNames.push("swiper-container-no-flexbox"), a.params.slidesPerColumn = 1);
                if (a.params.parallax || a.params.watchSlidesVisibility) a.params.watchSlidesProgress = !0;
                ["cube", "coverflow"].indexOf(a.params.effect) >= 0 && (a.support.transforms3d ? (a.params.watchSlidesProgress = !0, a.classNames.push("swiper-container-3d")) : a.params.effect = "slide"), a.params.effect !== "slide" && a.classNames.push("swiper-container-" + a.params.effect), a.params.effect === "cube" && (a.params.resistanceRatio = 0, a.params.slidesPerView = 1, a.params.slidesPerColumn = 1, a.params.slidesPerGroup = 1, a.params.centeredSlides = !1, a.params.spaceBetween = 0, a.params.virtualTranslate = !0, a.params.setWrapperSize = !1), a.params.effect === "fade" && (a.params.slidesPerView = 1, a.params.slidesPerColumn = 1, a.params.slidesPerGroup = 1, a.params.watchSlidesProgress = !0, a.params.spaceBetween = 0, typeof s == "undefined" && (a.params.virtualTranslate = !0)), a.params.grabCursor && a.support.touch && (a.params.grabCursor = !1), a.wrapper = a.container.children("." + a.params.wrapperClass), a.params.pagination && (a.paginationContainer = e(a.params.pagination), a.params.paginationClickable && a.paginationContainer.addClass("swiper-pagination-clickable")), a.rtl = f() && (a.container[0].dir.toLowerCase() === "rtl" || a.container.css("direction") === "rtl"), a.rtl && a.classNames.push("swiper-container-rtl"), a.rtl && (a.wrongRTL = a.wrapper.css("display") === "-webkit-box"), a.params.slidesPerColumn > 1 && a.classNames.push("swiper-container-multirow"), a.device.android && a.classNames.push("swiper-container-android"), a.container.addClass(a.classNames.join(" ")), a.translate = 0, a.progress = 0, a.velocity = 0, a.lockSwipeToNext = function() {
                    a.params.allowSwipeToNext = !1
                }, a.lockSwipeToPrev = function() {
                    a.params.allowSwipeToPrev = !1
                }, a.lockSwipes = function() {
                    a.params.allowSwipeToNext = a.params.allowSwipeToPrev = !1
                }, a.unlockSwipeToNext = function() {
                    a.params.allowSwipeToNext = !0
                }, a.unlockSwipeToPrev = function() {
                    a.params.allowSwipeToPrev = !0
                }, a.unlockSwipes = function() {
                    a.params.allowSwipeToNext = a.params.allowSwipeToPrev = !0
                }, a.params.grabCursor && (a.container[0].style.cursor = "move", a.container[0].style.cursor = "-webkit-grab", a.container[0].style.cursor = "-moz-grab", a.container[0].style.cursor = "grab"), a.imagesToLoad = [], a.imagesLoaded = 0, a.loadImage = function(e, t, n, r) {
                    function s() {
                        r && r()
                    }
                    var i;
                    !e.complete || !n ? t ? (i = new window.Image, i.onload = s, i.onerror = s, i.src = t) : s() : s()
                }, a.preloadImages = function() {
                    function e() {
                        if (typeof a == "undefined" || a === null) return;
                        a.imagesLoaded !== undefined && a.imagesLoaded++, a.imagesLoaded === a.imagesToLoad.length && (a.params.updateOnImagesReady && a.update(), a.emit("onImagesReady", a))
                    }
                    a.imagesToLoad = a.container.find("img");
                    for (var t = 0; t < a.imagesToLoad.length; t++) a.loadImage(a.imagesToLoad[t], a.imagesToLoad[t].currentSrc || a.imagesToLoad[t].getAttribute("src"), !0, e)
                }, a.autoplayTimeoutId = undefined, a.autoplaying = !1, a.autoplayPaused = !1, a.startAutoplay = function() {
                    if (typeof a.autoplayTimeoutId != "undefined") return !1;
                    if (!a.params.autoplay) return !1;
                    if (a.autoplaying) return !1;
                    a.autoplaying = !0, a.emit("onAutoplayStart", a), c()
                }, a.stopAutoplay = function(e) {
                    if (!a.autoplayTimeoutId) return;
                    a.autoplayTimeoutId && clearTimeout(a.autoplayTimeoutId), a.autoplaying = !1, a.autoplayTimeoutId = undefined, a.emit("onAutoplayStop", a)
                }, a.pauseAutoplay = function(e) {
                    if (a.autoplayPaused) return;
                    a.autoplayTimeoutId && clearTimeout(a.autoplayTimeoutId), a.autoplayPaused = !0, e === 0 ? (a.autoplayPaused = !1, c()) : a.wrapper.transitionEnd(function() {
                        if (!a) return;
                        a.autoplayPaused = !1, a.autoplaying ? c() : a.stopAutoplay()
                    })
                }, a.minTranslate = function() {
                    return -a.snapGrid[0]
                }, a.maxTranslate = function() {
                    return -a.snapGrid[a.snapGrid.length - 1]
                }, a.updateContainerSize = function() {
                    var e, t;
                    typeof a.params.width != "undefined" ? e = a.params.width : e = a.container[0].clientWidth, typeof a.params.height != "undefined" ? t = a.params.height : t = a.container[0].clientHeight;
                    if (e === 0 && f() || t === 0 && !f()) return;
                    e = e - parseInt(a.container.css("padding-left"), 10) - parseInt(a.container.css("padding-right"), 10), t = t - parseInt(a.container.css("padding-top"), 10) - parseInt(a.container.css("padding-bottom"), 10), a.width = e, a.height = t, a.size = f() ? a.width : a.height
                }, a.updateSlidesSize = function() {
                    a.slides = a.wrapper.children("." + a.params.slideClass), a.snapGrid = [], a.slidesGrid = [], a.slidesSizesGrid = [];
                    var e = a.params.spaceBetween,
                        t = -a.params.slidesOffsetBefore,
                        n, r = 0,
                        i = 0;
                    typeof e == "string" && e.indexOf("%") >= 0 && (e = parseFloat(e.replace("%", "")) / 100 * a.size), a.virtualSize = -e, a.rtl ? a.slides.css({
                        marginLeft: "",
                        marginTop: ""
                    }) : a.slides.css({
                        marginRight: "",
                        marginBottom: ""
                    });
                    var s;
                    a.params.slidesPerColumn > 1 && (Math.floor(a.slides.length / a.params.slidesPerColumn) === a.slides.length / a.params.slidesPerColumn ? s = a.slides.length : s = Math.ceil(a.slides.length / a.params.slidesPerColumn) * a.params.slidesPerColumn);
                    var o, u = a.params.slidesPerColumn,
                        c = s / u,
                        h = c - (a.params.slidesPerColumn * c - a.slides.length);
                    for (n = 0; n < a.slides.length; n++) {
                        o = 0;
                        var p = a.slides.eq(n);
                        if (a.params.slidesPerColumn > 1) {
                            var d, v, m;
                            a.params.slidesPerColumnFill === "column" ? (v = Math.floor(n / u), m = n - v * u, (v > h || v === h && m === u - 1) && ++m >= u && (m = 0, v++), d = v + m * s / u, p.css({
                                "-webkit-box-ordinal-group": d,
                                "-moz-box-ordinal-group": d,
                                "-ms-flex-order": d,
                                "-webkit-order": d,
                                order: d
                            })) : (m = Math.floor(n / c), v = n - m * c), p.css({
                                "margin-top": m !== 0 && a.params.spaceBetween && a.params.spaceBetween + "px"
                            }).attr("data-swiper-column", v).attr("data-swiper-row", m)
                        }
                        if (p.css("display") === "none") continue;
                        a.params.slidesPerView === "auto" ? (o = f() ? p.outerWidth(!0) : p.outerHeight(!0), a.params.roundLengths && (o = l(o))) : (o = (a.size - (a.params.slidesPerView - 1) * e) / a.params.slidesPerView, a.params.roundLengths && (o = l(o)), f() ? a.slides[n].style.width = o + "px" : a.slides[n].style.height = o + "px"), a.slides[n].swiperSlideSize = o, a.slidesSizesGrid.push(o), a.params.centeredSlides ? (t = t + o / 2 + r / 2 + e, n === 0 && (t = t - a.size / 2 - e), Math.abs(t) < .001 && (t = 0), i % a.params.slidesPerGroup === 0 && a.snapGrid.push(t), a.slidesGrid.push(t)) : (i % a.params.slidesPerGroup === 0 && a.snapGrid.push(t), a.slidesGrid.push(t), t = t + o + e), a.virtualSize += o + e, r = o, i++
                    }
                    a.virtualSize = Math.max(a.virtualSize, a.size) + a.params.slidesOffsetAfter;
                    var g;
                    a.rtl && a.wrongRTL && (a.params.effect === "slide" || a.params.effect === "coverflow") && a.wrapper.css({
                        width: a.virtualSize + a.params.spaceBetween + "px"
                    });
                    if (!a.support.flexbox || a.params.setWrapperSize) f() ? a.wrapper.css({
                        width: a.virtualSize + a.params.spaceBetween + "px"
                    }) : a.wrapper.css({
                        height: a.virtualSize + a.params.spaceBetween + "px"
                    });
                    if (a.params.slidesPerColumn > 1) {
                        a.virtualSize = (o + a.params.spaceBetween) * s, a.virtualSize = Math.ceil(a.virtualSize / a.params.slidesPerColumn) - a.params.spaceBetween, a.wrapper.css({
                            width: a.virtualSize + a.params.spaceBetween + "px"
                        });
                        if (a.params.centeredSlides) {
                            g = [];
                            for (n = 0; n < a.snapGrid.length; n++) a.snapGrid[n] < a.virtualSize + a.snapGrid[0] && g.push(a.snapGrid[n]);
                            a.snapGrid = g
                        }
                    }
                    if (!a.params.centeredSlides) {
                        g = [];
                        for (n = 0; n < a.snapGrid.length; n++) a.snapGrid[n] <= a.virtualSize - a.size && g.push(a.snapGrid[n]);
                        a.snapGrid = g, Math.floor(a.virtualSize - a.size) > Math.floor(a.snapGrid[a.snapGrid.length - 1]) && a.snapGrid.push(a.virtualSize - a.size)
                    }
                    a.snapGrid.length === 0 && (a.snapGrid = [0]), a.params.spaceBetween !== 0 && (f() ? a.rtl ? a.slides.css({
                        marginLeft: e + "px"
                    }) : a.slides.css({
                        marginRight: e + "px"
                    }) : a.slides.css({
                        marginBottom: e + "px"
                    })), a.params.watchSlidesProgress && a.updateSlidesOffset()
                }, a.updateSlidesOffset = function() {
                    for (var e = 0; e < a.slides.length; e++) a.slides[e].swiperSlideOffset = f() ? a.slides[e].offsetLeft : a.slides[e].offsetTop
                }, a.updateSlidesProgress = function(e) {
                    typeof e == "undefined" && (e = a.translate || 0);
                    if (a.slides.length === 0) return;
                    typeof a.slides[0].swiperSlideOffset == "undefined" && a.updateSlidesOffset();
                    var t = a.params.centeredSlides ? -e + a.size / 2 : -e;
                    a.rtl && (t = a.params.centeredSlides ? e - a.size / 2 : e);
                    var n = a.container[0].getBoundingClientRect(),
                        r = f() ? "left" : "top",
                        i = f() ? "right" : "bottom";
                    a.slides.removeClass(a.params.slideVisibleClass);
                    for (var s = 0; s < a.slides.length; s++) {
                        var o = a.slides[s],
                            u = a.params.centeredSlides === !0 ? o.swiperSlideSize / 2 : 0,
                            l = (t - o.swiperSlideOffset - u) / (o.swiperSlideSize + a.params.spaceBetween);
                        if (a.params.watchSlidesVisibility) {
                            var c = -(t - o.swiperSlideOffset - u),
                                h = c + a.slidesSizesGrid[s],
                                p = c >= 0 && c < a.size || h > 0 && h <= a.size || c <= 0 && h >= a.size;
                            p && a.slides.eq(s).addClass(a.params.slideVisibleClass)
                        }
                        o.progress = a.rtl ? -l : l
                    }
                }, a.updateProgress = function(e) {
                    typeof e == "undefined" && (e = a.translate || 0);
                    var t = a.maxTranslate() - a.minTranslate();
                    t === 0 ? (a.progress = 0, a.isBeginning = a.isEnd = !0) : (a.progress = (e - a.minTranslate()) / t, a.isBeginning = a.progress <= 0, a.isEnd = a.progress >= 1), a.isBeginning && a.emit("onReachBeginning", a), a.isEnd && a.emit("onReachEnd", a), a.params.watchSlidesProgress && a.updateSlidesProgress(e), a.emit("onProgress", a, a.progress)
                }, a.updateActiveIndex = function() {
                    var e = a.rtl ? a.translate : -a.translate,
                        t, n, r;
                    for (n = 0; n < a.slidesGrid.length; n++) typeof a.slidesGrid[n + 1] != "undefined" ? e >= a.slidesGrid[n] && e < a.slidesGrid[n + 1] - (a.slidesGrid[n + 1] - a.slidesGrid[n]) / 2 ? t = n : e >= a.slidesGrid[n] && e < a.slidesGrid[n + 1] && (t = n + 1) : e >= a.slidesGrid[n] && (t = n);
                    if (t < 0 || typeof t == "undefined") t = 0;
                    r = Math.floor(t / a.params.slidesPerGroup), r >= a.snapGrid.length && (r = a.snapGrid.length - 1);
                    if (t === a.activeIndex) return;
                    a.snapIndex = r, a.previousIndex = a.activeIndex, a.activeIndex = t, a.updateClasses()
                }, a.updateClasses = function() {
                    a.slides.removeClass(a.params.slideActiveClass + " " + a.params.slideNextClass + " " + a.params.slidePrevClass);
                    var t = a.slides.eq(a.activeIndex);
                    t.addClass(a.params.slideActiveClass), t.next("." + a.params.slideClass).addClass(a.params.slideNextClass), t.prev("." + a.params.slideClass).addClass(a.params.slidePrevClass);
                    if (a.bullets && a.bullets.length > 0) {
                        a.bullets.removeClass(a.params.bulletActiveClass);
                        var n;
                        a.params.loop ? (n = Math.ceil(a.activeIndex - a.loopedSlides) / a.params.slidesPerGroup, n > a.slides.length - 1 - a.loopedSlides * 2 && (n -= a.slides.length - a.loopedSlides * 2), n > a.bullets.length - 1 && (n -= a.bullets.length)) : typeof a.snapIndex != "undefined" ? n = a.snapIndex : n = a.activeIndex || 0, a.paginationContainer.length > 1 ? a.bullets.each(function() {
                            e(this).index() === n && e(this).addClass(a.params.bulletActiveClass)
                        }) : a.bullets.eq(n).addClass(a.params.bulletActiveClass)
                    }
                    a.params.loop || (a.params.prevButton && (a.isBeginning ? (e(a.params.prevButton).addClass(a.params.buttonDisabledClass), a.params.a11y && a.a11y && a.a11y.disable(e(a.params.prevButton))) : (e(a.params.prevButton).removeClass(a.params.buttonDisabledClass), a.params.a11y && a.a11y && a.a11y.enable(e(a.params.prevButton)))), a.params.nextButton && (a.isEnd ? (e(a.params.nextButton).addClass(a.params.buttonDisabledClass), a.params.a11y && a.a11y && a.a11y.disable(e(a.params.nextButton))) : (e(a.params.nextButton).removeClass(a.params.buttonDisabledClass), a.params.a11y && a.a11y && a.a11y.enable(e(a.params.nextButton)))))
                }, a.updatePagination = function() {
                    if (!a.params.pagination) return;
                    if (a.paginationContainer && a.paginationContainer.length > 0) {
                        var e = "",
                            t = a.params.loop ? Math.ceil((a.slides.length - a.loopedSlides * 2) / a.params.slidesPerGroup) : a.snapGrid.length;
                        for (var n = 0; n < t; n++) a.params.paginationBulletRender ? e += a.params.paginationBulletRender(n, a.params.bulletClass) : e += "<" + a.params.paginationElement + ' class="' + a.params.bulletClass + '"></' + a.params.paginationElement + ">";
                        a.paginationContainer.html(e), a.bullets = a.paginationContainer.find("." + a.params.bulletClass), a.params.paginationClickable && a.params.a11y && a.a11y && a.a11y.initPagination()
                    }
                }, a.update = function(e) {
                    function t() {
                        r = Math.min(Math.max(a.translate, a.maxTranslate()), a.minTranslate()), a.setWrapperTranslate(r), a.updateActiveIndex(), a.updateClasses()
                    }
                    a.updateContainerSize(), a.updateSlidesSize(), a.updateProgress(), a.updatePagination(), a.updateClasses(), a.params.scrollbar && a.scrollbar && a.scrollbar.set();
                    if (e) {
                        var n, r;
                        a.controller && a.controller.spline && (a.controller.spline = undefined), a.params.freeMode ? t() : ((a.params.slidesPerView === "auto" || a.params.slidesPerView > 1) && a.isEnd && !a.params.centeredSlides ? n = a.slideTo(a.slides.length - 1, 0, !1, !0) : n = a.slideTo(a.activeIndex, 0, !1, !0), n || t())
                    }
                }, a.onResize = function(e) {
                    var t = a.params.allowSwipeToPrev,
                        n = a.params.allowSwipeToNext;
                    a.params.allowSwipeToPrev = a.params.allowSwipeToNext = !0, a.updateContainerSize(), a.updateSlidesSize(), (a.params.slidesPerView === "auto" || a.params.freeMode || e) && a.updatePagination(), a.params.scrollbar && a.scrollbar && a.scrollbar.set(), a.controller && a.controller.spline && (a.controller.spline = undefined);
                    if (a.params.freeMode) {
                        var r = Math.min(Math.max(a.translate, a.maxTranslate()), a.minTranslate());
                        a.setWrapperTranslate(r), a.updateActiveIndex(), a.updateClasses()
                    } else a.updateClasses(), (a.params.slidesPerView === "auto" || a.params.slidesPerView > 1) && a.isEnd && !a.params.centeredSlides ? a.slideTo(a.slides.length - 1, 0, !1, !0) : a.slideTo(a.activeIndex, 0, !1, !0);
                    a.params.allowSwipeToPrev = t, a.params.allowSwipeToNext = n
                };
                var h = ["mousedown", "mousemove", "mouseup"];
                window.navigator.pointerEnabled ? h = ["pointerdown", "pointermove", "pointerup"] : window.navigator.msPointerEnabled && (h = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), a.touchEvents = {
                    start: a.support.touch || !a.params.simulateTouch ? "touchstart" : h[0],
                    move: a.support.touch || !a.params.simulateTouch ? "touchmove" : h[1],
                    end: a.support.touch || !a.params.simulateTouch ? "touchend" : h[2]
                }, (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && (a.params.touchEventsTarget === "container" ? a.container : a.wrapper).addClass("swiper-wp8-" + a.params.direction), a.initEvents = function(t) {
                    var n = t ? "off" : "on",
                        i = t ? "removeEventListener" : "addEventListener",
                        s = a.params.touchEventsTarget === "container" ? a.container[0] : a.wrapper[0],
                        o = a.support.touch ? s : document,
                        u = a.params.nested ? !0 : !1;
                    a.browser.ie ? (s[i](a.touchEvents.start, a.onTouchStart, !1), o[i](a.touchEvents.move, a.onTouchMove, u), o[i](a.touchEvents.end, a.onTouchEnd, !1)) : (a.support.touch && (s[i](a.touchEvents.start, a.onTouchStart, !1), s[i](a.touchEvents.move, a.onTouchMove, u), s[i](a.touchEvents.end, a.onTouchEnd, !1)), r.simulateTouch && !a.device.ios && !a.device.android && (s[i]("mousedown", a.onTouchStart, !1), document[i]("mousemove", a.onTouchMove, u), document[i]("mouseup", a.onTouchEnd, !1))), window[i]("resize", a.onResize), a.params.nextButton && (e(a.params.nextButton)[n]("click", a.onClickNext), a.params.a11y && a.a11y && e(a.params.nextButton)[n]("keydown", a.a11y.onEnterKey)), a.params.prevButton && (e(a.params.prevButton)[n]("click", a.onClickPrev), a.params.a11y && a.a11y && e(a.params.prevButton)[n]("keydown", a.a11y.onEnterKey)), a.params.pagination && a.params.paginationClickable && (e(a.paginationContainer)[n]("click", "." + a.params.bulletClass, a.onClickIndex), a.params.a11y && a.a11y && e(a.paginationContainer)[n]("keydown", "." + a.params.bulletClass, a.a11y.onEnterKey)), (a.params.preventClicks || a.params.preventClicksPropagation) && s[i]("click", a.preventClicks, !0)
                }, a.attachEvents = function(e) {
                    a.initEvents()
                }, a.detachEvents = function() {
                    a.initEvents(!0)
                }, a.allowClick = !0, a.preventClicks = function(e) {
                    a.allowClick || (a.params.preventClicks && e.preventDefault(), a.params.preventClicksPropagation && a.animating && (e.stopPropagation(), e.stopImmediatePropagation()))
                }, a.onClickNext = function(e) {
                    e.preventDefault();
                    if (a.isEnd && !a.params.loop) return;
                    a.slideNext()
                }, a.onClickPrev = function(e) {
                    e.preventDefault();
                    if (a.isBeginning && !a.params.loop) return;
                    a.slidePrev()
                }, a.onClickIndex = function(t) {
                    t.preventDefault();
                    var n = e(this).index() * a.params.slidesPerGroup;
                    a.params.loop && (n += a.loopedSlides), a.slideTo(n)
                }, a.updateClickedSlide = function(t) {
                    var n = p(t, "." + a.params.slideClass),
                        r = !1;
                    if (n)
                        for (var i = 0; i < a.slides.length; i++) a.slides[i] === n && (r = !0);
                    if (!n || !r) {
                        a.clickedSlide = undefined, a.clickedIndex = undefined;
                        return
                    }
                    a.clickedSlide = n, a.clickedIndex = e(n).index();
                    if (a.params.slideToClickedSlide && a.clickedIndex !== undefined && a.clickedIndex !== a.activeIndex) {
                        var s = a.clickedIndex,
                            o;
                        if (a.params.loop) {
                            o = e(a.clickedSlide).attr("data-swiper-slide-index");
                            if (s > a.slides.length - a.params.slidesPerView) a.fixLoop(), s = a.wrapper.children("." + a.params.slideClass + '[data-swiper-slide-index="' + o + '"]').eq(0).index(), setTimeout(function() {
                                a.slideTo(s)
                            }, 0);
                            else if (s < a.params.slidesPerView - 1) {
                                a.fixLoop();
                                var u = a.wrapper.children("." + a.params.slideClass + '[data-swiper-slide-index="' + o + '"]');
                                s = u.eq(u.length - 1).index(), setTimeout(function() {
                                    a.slideTo(s)
                                }, 0)
                            } else a.slideTo(s)
                        } else a.slideTo(s)
                    }
                };
                var d, v, m, g, y, b, w, E = "input, select, textarea, button",
                    S = Date.now(),
                    x, T = [],
                    N;
                a.animating = !1, a.touches = {
                    startX: 0,
                    startY: 0,
                    currentX: 0,
                    currentY: 0,
                    diff: 0
                };
                var C, k;
                a.onTouchStart = function(t) {
                    t.originalEvent && (t = t.originalEvent), C = t.type === "touchstart";
                    if (!C && "which" in t && t.which === 3) return;
                    if (a.params.noSwiping && p(t, "." + a.params.noSwipingClass)) {
                        a.allowClick = !0;
                        return
                    }
                    if (a.params.swipeHandler && !p(t, a.params.swipeHandler)) return;
                    d = !0, v = !1, g = undefined, k = undefined, a.touches.startX = a.touches.currentX = t.type === "touchstart" ? t.targetTouches[0].pageX : t.pageX, a.touches.startY = a.touches.currentY = t.type === "touchstart" ? t.targetTouches[0].pageY : t.pageY, m = Date.now(), a.allowClick = !0, a.updateContainerSize(), a.swipeDirection = undefined, a.params.threshold > 0 && (w = !1);
                    if (t.type !== "touchstart") {
                        var n = !0;
                        e(t.target).is(E) && (n = !1), document.activeElement && e(document.activeElement).is(E) && document.activeElement.blur(), n && t.preventDefault()
                    }
                    a.emit("onTouchStart", a, t)
                }, a.onTouchMove = function(t) {
                    t.originalEvent && (t = t.originalEvent);
                    if (C && t.type === "mousemove") return;
                    if (t.preventedByNestedSwiper) return;
                    if (a.params.onlyExternal) {
                        a.allowClick = !1, d && (a.touches.startX = a.touches.currentX = t.type === "touchmove" ? t.targetTouches[0].pageX : t.pageX, a.touches.startY = a.touches.currentY = t.type === "touchmove" ? t.targetTouches[0].pageY : t.pageY, m = Date.now());
                        return
                    }
                    if (C && document.activeElement && t.target === document.activeElement && e(t.target).is(E)) {
                        v = !0, a.allowClick = !1;
                        return
                    }
                    a.emit("onTouchMove", a, t);
                    if (t.targetTouches && t.targetTouches.length > 1) return;
                    a.touches.currentX = t.type === "touchmove" ? t.targetTouches[0].pageX : t.pageX, a.touches.currentY = t.type === "touchmove" ? t.targetTouches[0].pageY : t.pageY;
                    if (typeof g == "undefined") {
                        var n = Math.atan2(Math.abs(a.touches.currentY - a.touches.startY), Math.abs(a.touches.currentX - a.touches.startX)) * 180 / Math.PI;
                        g = f() ? n > a.params.touchAngle : 90 - n > a.params.touchAngle
                    }
                    g && a.emit("onTouchMoveOpposite", a, t), typeof k == "undefined" && a.browser.ieTouch && (a.touches.currentX !== a.touches.startX || a.touches.currentY !== a.touches.startY) && (k = !0);
                    if (!d) return;
                    if (g) {
                        d = !1;
                        return
                    }
                    if (!k && a.browser.ieTouch) return;
                    a.allowClick = !1, a.emit("onSliderMove", a, t), t.preventDefault(), a.params.touchMoveStopPropagation && !a.params.nested && t.stopPropagation(), v || (r.loop && a.fixLoop(), b = a.getWrapperTranslate(), a.setWrapperTransition(0), a.animating && a.wrapper.trigger("webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd"), a.params.autoplay && a.autoplaying && (a.params.autoplayDisableOnInteraction ? a.stopAutoplay() : a.pauseAutoplay()), N = !1, a.params.grabCursor && (a.container[0].style.cursor = "move", a.container[0].style.cursor = "-webkit-grabbing", a.container[0].style.cursor = "-moz-grabbin", a.container[0].style.cursor = "grabbing")), v = !0;
					
                    var i = a.touches.diff = f() ? a.touches.currentX - a.touches.startX : a.touches.currentY - a.touches.startY;
                    i *= a.params.touchRatio, a.rtl && (i = -i), a.swipeDirection = i > 0 ? "prev" : "next", y = i + b;
                    var s = !0;
                    i > 0 && y > a.minTranslate() ? (s = !1, a.params.resistance && (y = a.minTranslate() - 1 + Math.pow(-a.minTranslate() + b + i, a.params.resistanceRatio))) : i < 0 && y < a.maxTranslate() && (s = !1, a.params.resistance && (y = a.maxTranslate() + 1 - Math.pow(a.maxTranslate() - b - i, a.params.resistanceRatio))), s && (t.preventedByNestedSwiper = !0), !a.params.allowSwipeToNext && a.swipeDirection === "next" && y < b && (y = b), !a.params.allowSwipeToPrev && a.swipeDirection === "prev" && y > b && (y = b);
                    if (!a.params.followFinger) return;
                    if (a.params.threshold > 0) {
                        if (!(Math.abs(i) > a.params.threshold || w)) {
                            y = b;
                            return
                        }
                        if (!w) {
                            w = !0, a.touches.startX = a.touches.currentX, a.touches.startY = a.touches.currentY, y = b, a.touches.diff = f() ? a.touches.currentX - a.touches.startX : a.touches.currentY - a.touches.startY;
                            return
                        }
                    }(a.params.freeMode || a.params.watchSlidesProgress) && a.updateActiveIndex(), a.params.freeMode && (T.length === 0 && T.push({
                        position: a.touches[f() ? "startX" : "startY"],
                        time: m
                    }), T.push({
                        position: a.touches[f() ? "currentX" : "currentY"],
                        time: (new window.Date).getTime()
                    })), a.updateProgress(y), a.setWrapperTranslate(y)
                }, a.onTouchEnd = function(t) {
                    t.originalEvent && (t = t.originalEvent), a.emit("onTouchEnd", a, t);
                    if (!d) return;
                    a.params.grabCursor && v && d && (a.container[0].style.cursor = "move", a.container[0].style.cursor = "-webkit-grab", a.container[0].style.cursor = "-moz-grab", a.container[0].style.cursor = "grab");
                    var n = Date.now(),
                        r = n - m;
                    a.allowClick && (a.updateClickedSlide(t), a.emit("onTap", a, t), r < 300 && n - S > 300 && (x && clearTimeout(x), x = setTimeout(function() {
                        if (!a) return;
                        a.params.paginationHide && a.paginationContainer.length > 0 && !e(t.target).hasClass(a.params.bulletClass) && a.paginationContainer.toggleClass(a.params.paginationHiddenClass), a.emit("onClick", a, t)
                    }, 300)), r < 300 && n - S < 300 && (x && clearTimeout(x), a.emit("onDoubleTap", a, t))), S = Date.now(), setTimeout(function() {
                        a && (a.allowClick = !0)
                    }, 0);
                    if (!d || !v || !a.swipeDirection || a.touches.diff === 0 || y === b) {
                        d = v = !1;
                        return
                    }
                    d = v = !1;
                    var i;
                    a.params.followFinger ? i = a.rtl ? a.translate : -a.translate : i = -y;
                    if (a.params.freeMode) {
                        if (i < -a.minTranslate()) {
                            a.slideTo(a.activeIndex);
                            return
                        }
                        if (i > -a.maxTranslate()) {
                            a.slides.length < a.snapGrid.length ? a.slideTo(a.snapGrid.length - 1) : a.slideTo(a.slides.length - 1);
                            return
                        }
                        if (a.params.freeModeMomentum) {
                            if (T.length > 1) {
                                var s = T.pop(),
                                    o = T.pop(),
                                    u = s.position - o.position,
                                    f = s.time - o.time;
                                a.velocity = u / f, a.velocity = a.velocity / 2, Math.abs(a.velocity) < .02 && (a.velocity = 0);
                                if (f > 150 || (new window.Date).getTime() - s.time > 300) a.velocity = 0
                            } else a.velocity = 0;
                            T.length = 0;
                            var l = 1e3 * a.params.freeModeMomentumRatio,
                                c = a.velocity * l,
                                h = a.translate + c;
                            a.rtl && (h = -h);
                            var p = !1,
                                g, w = Math.abs(a.velocity) * 20 * a.params.freeModeMomentumBounceRatio;
                            if (h < a.maxTranslate()) a.params.freeModeMomentumBounce ? (h + a.maxTranslate() < -w && (h = a.maxTranslate() - w), g = a.maxTranslate(), p = !0, N = !0) : h = a.maxTranslate();
                            else if (h > a.minTranslate()) a.params.freeModeMomentumBounce ? (h - a.minTranslate() > w && (h = a.minTranslate() + w), g = a.minTranslate(), p = !0, N = !0) : h = a.minTranslate();
                            else if (a.params.freeModeSticky) {
                                var E = 0,
                                    C;
                                for (E = 0; E < a.snapGrid.length; E += 1)
                                    if (a.snapGrid[E] > -h) {
                                        C = E;
                                        break
                                    }
                                Math.abs(a.snapGrid[C] - h) < Math.abs(a.snapGrid[C - 1] - h) || a.swipeDirection === "next" ? h = a.snapGrid[C] : h = a.snapGrid[C - 1], a.rtl || (h = -h)
                            }
                            if (a.velocity !== 0) a.rtl ? l = Math.abs((-h - a.translate) / a.velocity) : l = Math.abs((h - a.translate) / a.velocity);
                            else if (a.params.freeModeSticky) {
                                a.slideReset();
                                return
                            }
                            a.params.freeModeMomentumBounce && p ? (a.updateProgress(g), a.setWrapperTransition(l), a.setWrapperTranslate(h), a.onTransitionStart(), a.animating = !0, a.wrapper.transitionEnd(function() {
                                if (!a || !N) return;
                                a.emit("onMomentumBounce", a), a.setWrapperTransition(a.params.speed), a.setWrapperTranslate(g), a.wrapper.transitionEnd(function() {
                                    if (!a) return;
                                    a.onTransitionEnd()
                                })
                            })) : a.velocity ? (a.updateProgress(h), a.setWrapperTransition(l), a.setWrapperTranslate(h), a.onTransitionStart(), a.animating || (a.animating = !0, a.wrapper.transitionEnd(function() {
                                if (!a) return;
                                a.onTransitionEnd()
                            }))) : a.updateProgress(h), a.updateActiveIndex()
                        }
                        if (!a.params.freeModeMomentum || r >= a.params.longSwipesMs) a.updateProgress(), a.updateActiveIndex();
                        return
                    }
                    var k, L = 0,
                        A = a.slidesSizesGrid[0];
                    for (k = 0; k < a.slidesGrid.length; k += a.params.slidesPerGroup) typeof a.slidesGrid[k + a.params.slidesPerGroup] != "undefined" ? i >= a.slidesGrid[k] && i < a.slidesGrid[k + a.params.slidesPerGroup] && (L = k, A = a.slidesGrid[k + a.params.slidesPerGroup] - a.slidesGrid[k]) : i >= a.slidesGrid[k] && (L = k, A = a.slidesGrid[a.slidesGrid.length - 1] - a.slidesGrid[a.slidesGrid.length - 2]);
                    var O = (i - a.slidesGrid[L]) / A;
                    if (r > a.params.longSwipesMs) {
                        if (!a.params.longSwipes) {
                            a.slideTo(a.activeIndex);
                            return
                        }
                        a.swipeDirection === "next" && (O >= a.params.longSwipesRatio ? a.slideTo(L + a.params.slidesPerGroup) : a.slideTo(L)), a.swipeDirection === "prev" && (O > 1 - a.params.longSwipesRatio ? a.slideTo(L + a.params.slidesPerGroup) : a.slideTo(L))
                    } else {
                        if (!a.params.shortSwipes) {
                            a.slideTo(a.activeIndex);
                            return
                        }
                        a.swipeDirection === "next" && a.slideTo(L + a.params.slidesPerGroup), a.swipeDirection === "prev" && a.slideTo(L)
                    }
                }, a._slideTo = function(e, t) {
                    return a.slideTo(e, t, !0, !0)
                }, a.slideTo = function(e, t, n, r) {
                    typeof n == "undefined" && (n = !0), typeof e == "undefined" && (e = 0), e < 0 && (e = 0), a.snapIndex = Math.floor(e / a.params.slidesPerGroup), a.snapIndex >= a.snapGrid.length && (a.snapIndex = a.snapGrid.length - 1);
                    var i = -a.snapGrid[a.snapIndex];
                    if (!a.params.allowSwipeToNext && i < a.translate && i < a.minTranslate()) return !1;
                    if (!a.params.allowSwipeToPrev && i > a.translate && i > a.maxTranslate()) return !1;
                    a.params.autoplay && a.autoplaying && (r || !a.params.autoplayDisableOnInteraction ? a.pauseAutoplay(t) : a.stopAutoplay()), a.updateProgress(i);
                    for (var s = 0; s < a.slidesGrid.length; s++) - Math.floor(i * 100) >= Math.floor(a.slidesGrid[s] * 100) && (e = s);
                    typeof t == "undefined" && (t = a.params.speed), a.previousIndex = a.activeIndex || 0, a.activeIndex = e;
                    if (i === a.translate) return a.updateClasses(), !1;
                    a.updateClasses(), a.onTransitionStart(n);
                    var o = f() ? i : 0,
                        u = f() ? 0 : i;
                    return t === 0 ? (a.setWrapperTransition(0), a.setWrapperTranslate(i), a.onTransitionEnd(n)) : (a.setWrapperTransition(t), a.setWrapperTranslate(i), a.animating || (a.animating = !0, a.wrapper.transitionEnd(function() {
                        if (!a) return;
                        a.onTransitionEnd(n)
                    }))), !0
                }, a.onTransitionStart = function(e) {
                    typeof e == "undefined" && (e = !0), a.lazy && a.lazy.onTransitionStart(), e && (a.emit("onTransitionStart", a), a.activeIndex !== a.previousIndex && a.emit("onSlideChangeStart", a))
                }, a.onTransitionEnd = function(e) {
                    a.animating = !1, a.setWrapperTransition(0), typeof e == "undefined" && (e = !0), a.lazy && a.lazy.onTransitionEnd(), e && (a.emit("onTransitionEnd", a), a.activeIndex !== a.previousIndex && a.emit("onSlideChangeEnd", a)), a.params.hashnav && a.hashnav && a.hashnav.setHash()
                }, a.slideNext = function(e, t, n) {
                    if (a.params.loop) {
                        if (a.animating) return !1;
                        a.fixLoop();
                        var r = a.container[0].clientLeft;
                        return a.slideTo(a.activeIndex + a.params.slidesPerGroup, t, e, n)
                    }
                    return a.slideTo(a.activeIndex + a.params.slidesPerGroup, t, e, n)
                }, a._slideNext = function(e) {
                    return a.slideNext(!0, e, !0)
                }, a.slidePrev = function(e, t, n) {
                    if (a.params.loop) {
                        if (a.animating) return !1;
                        a.fixLoop();
                        var r = a.container[0].clientLeft;
                        return a.slideTo(a.activeIndex - 1, t, e, n)
                    }
                    return a.slideTo(a.activeIndex - 1, t, e, n)
                }, a._slidePrev = function(e) {
                    return a.slidePrev(!0, e, !0)
                }, a.slideReset = function(e, t, n) {
                    return a.slideTo(a.activeIndex, t, e)
                }, a.setWrapperTransition = function(e, t) {
                    a.wrapper.transition(e), a.params.effect !== "slide" && a.effects[a.params.effect] && a.effects[a.params.effect].setTransition(e), a.params.parallax && a.parallax && a.parallax.setTransition(e), a.params.scrollbar && a.scrollbar && a.scrollbar.setTransition(e), a.params.control && a.controller && a.controller.setTransition(e, t), a.emit("onSetTransition", a, e)
                }, a.setWrapperTranslate = function(e, t, n) {
                    var r = 0,
                        i = 0,
                        s = 0;
                    f() ? r = a.rtl ? -e : e : i = e, a.params.virtualTranslate || (a.support.transforms3d ? a.wrapper.transform("translate3d(" + r + "px, " + i + "px, " + s + "px)") : a.wrapper.transform("translate(" + r + "px, " + i + "px)")), a.translate = f() ? r : i, t && a.updateActiveIndex(), a.params.effect !== "slide" && a.effects[a.params.effect] && a.effects[a.params.effect].setTranslate(a.translate), a.params.parallax && a.parallax && a.parallax.setTranslate(a.translate), a.params.scrollbar && a.scrollbar && a.scrollbar.setTranslate(a.translate), a.params.control && a.controller && a.controller.setTranslate(a.translate, n), a.emit("onSetTranslate", a, a.translate)
                }, a.getTranslate = function(e, t) {
                    var n, r, i, s;
                    return typeof t == "undefined" && (t = "x"), a.params.virtualTranslate ? a.rtl ? -a.translate : a.translate : (i = window.getComputedStyle(e, null), window.WebKitCSSMatrix ? s = new window.WebKitCSSMatrix(i.webkitTransform === "none" ? "" : i.webkitTransform) : (s = i.MozTransform || i.OTransform || i.MsTransform || i.msTransform || i.transform || i.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,"), n = s.toString().split(",")), t === "x" && (window.WebKitCSSMatrix ? r = s.m41 : n.length === 16 ? r = parseFloat(n[12]) : r = parseFloat(n[4])), t === "y" && (window.WebKitCSSMatrix ? r = s.m42 : n.length === 16 ? r = parseFloat(n[13]) : r = parseFloat(n[5])), a.rtl && r && (r = -r), r || 0)
                }, a.getWrapperTranslate = function(e) {
                    return typeof e == "undefined" && (e = f() ? "x" : "y"), a.getTranslate(a.wrapper[0], e)
                }, a.observers = [], a.initObservers = function() {
                    if (a.params.observeParents) {
                        var e = a.container.parents();
                        for (var t = 0; t < e.length; t++) L(e[t])
                    }
                    L(a.container[0], {
                        childList: !1
                    }), L(a.wrapper[0], {
                        attributes: !1
                    })
                }, a.disconnectObservers = function() {
                    for (var e = 0; e < a.observers.length; e++) a.observers[e].disconnect();
                    a.observers = []
                }, a.createLoop = function() {
                    a.wrapper.children("." + a.params.slideClass + "." + a.params.slideDuplicateClass).remove();
                    var t = a.wrapper.children("." + a.params.slideClass);
                    a.loopedSlides = parseInt(a.params.loopedSlides || a.params.slidesPerView, 10), a.loopedSlides = a.loopedSlides + a.params.loopAdditionalSlides, a.loopedSlides > t.length && (a.loopedSlides = t.length);
                    var n = [],
                        r = [],
                        i;
                    t.each(function(i, s) {
                        var o = e(this);
                        i < a.loopedSlides && r.push(s), i < t.length && i >= t.length - a.loopedSlides && n.push(s), o.attr("data-swiper-slide-index", i)
                    });
                    for (i = 0; i < r.length; i++) a.wrapper.append(e(r[i].cloneNode(!0)).addClass(a.params.slideDuplicateClass));
                    for (i = n.length - 1; i >= 0; i--) a.wrapper.prepend(e(n[i].cloneNode(!0)).addClass(a.params.slideDuplicateClass))
                }, a.destroyLoop = function() {
                    a.wrapper.children("." + a.params.slideClass + "." + a.params.slideDuplicateClass).remove(), a.slides.removeAttr("data-swiper-slide-index")
                }, a.fixLoop = function() {
                    var e;
                    if (a.activeIndex < a.loopedSlides) e = a.slides.length - a.loopedSlides * 3 + a.activeIndex, e += a.loopedSlides, a.slideTo(e, 0, !1, !0);
                    else if (a.params.slidesPerView === "auto" && a.activeIndex >= a.loopedSlides * 2 || a.activeIndex > a.slides.length - a.params.slidesPerView * 2) e = -a.slides.length + a.activeIndex + a.loopedSlides, e += a.loopedSlides, a.slideTo(e, 0, !1, !0)
                }, a.appendSlide = function(e) {
                    a.params.loop && a.destroyLoop();
                    if (typeof e == "object" && e.length)
                        for (var t = 0; t < e.length; t++) e[t] && a.wrapper.append(e[t]);
                    else a.wrapper.append(e);
                    a.params.loop && a.createLoop(), (!a.params.observer || !a.support.observer) && a.update(!0)
                }, a.prependSlide = function(e) {
                    a.params.loop && a.destroyLoop();
                    var t = a.activeIndex + 1;
                    if (typeof e == "object" && e.length) {
                        for (var n = 0; n < e.length; n++) e[n] && a.wrapper.prepend(e[n]);
                        t = a.activeIndex + e.length
                    } else a.wrapper.prepend(e);
                    a.params.loop && a.createLoop(), (!a.params.observer || !a.support.observer) && a.update(!0), a.slideTo(t, 0, !1)
                }, a.removeSlide = function(e) {
                    a.params.loop && (a.destroyLoop(), a.slides = a.wrapper.children("." + a.params.slideClass));
                    var t = a.activeIndex,
                        n;
                    if (typeof e == "object" && e.length) {
                        for (var r = 0; r < e.length; r++) n = e[r], a.slides[n] && a.slides.eq(n).remove(), n < t && t--;
                        t = Math.max(t, 0)
                    } else n = e, a.slides[n] && a.slides.eq(n).remove(), n < t && t--, t = Math.max(t, 0);
                    a.params.loop && a.createLoop(), (!a.params.observer || !a.support.observer) && a.update(!0), a.params.loop ? a.slideTo(t + a.loopedSlides, 0, !1) : a.slideTo(t, 0, !1)
                }, a.removeAllSlides = function() {
                    var e = [];
                    for (var t = 0; t < a.slides.length; t++) e.push(t);
                    a.removeSlide(e)
                }, a.effects = {
                    fade: {
                        setTranslate: function() {
                            for (var e = 0; e < a.slides.length; e++) {
                                var t = a.slides.eq(e),
                                    n = t[0].swiperSlideOffset,
                                    r = -n;
                                a.params.virtualTranslate || (r -= a.translate);
                                var i = 0;
                                f() || (i = r, r = 0);
                                var s = a.params.fade.crossFade ? Math.max(1 - Math.abs(t[0].progress), 0) : 1 + Math.min(Math.max(t[0].progress, -1), 0);
                                t.css({
                                    opacity: s
                                }).transform("translate3d(" + r + "px, " + i + "px, 0px)")
                            }
                        },
                        setTransition: function(e) {
                            a.slides.transition(e);
                            if (a.params.virtualTranslate && e !== 0) {
                                var t = !1;
                                a.slides.transitionEnd(function() {
                                    if (t) return;
                                    if (!a) return;
                                    t = !0, a.animating = !1;
                                    var e = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"];
                                    for (var n = 0; n < e.length; n++) a.wrapper.trigger(e[n])
                                })
                            }
                        }
                    },
                    cube: {
                        setTranslate: function() {
                            var t = 0,
                                n;
                            a.params.cube.shadow && (f() ? (n = a.wrapper.find(".swiper-cube-shadow"), n.length === 0 && (n = e('<div class="swiper-cube-shadow"></div>'), a.wrapper.append(n)), n.css({
                                height: a.width + "px"
                            })) : (n = a.container.find(".swiper-cube-shadow"), n.length === 0 && (n = e('<div class="swiper-cube-shadow"></div>'), a.container.append(n))));
                            for (var r = 0; r < a.slides.length; r++) {
                                var i = a.slides.eq(r),
                                    s = r * 90,
                                    o = Math.floor(s / 360);
                                a.rtl && (s = -s, o = Math.floor(-s / 360));
                                var u = Math.max(Math.min(i[0].progress, 1), -1),
                                    l = 0,
                                    c = 0,
                                    h = 0;
                                r % 4 === 0 ? (l = -o * 4 * a.size, h = 0) : (r - 1) % 4 === 0 ? (l = 0, h = -o * 4 * a.size) : (r - 2) % 4 === 0 ? (l = a.size + o * 4 * a.size, h = a.size) : (r - 3) % 4 === 0 && (l = -a.size, h = 3 * a.size + a.size * 4 * o), a.rtl && (l = -l), f() || (c = l, l = 0);
                                var p = "rotateX(" + (f() ? 0 : -s) + "deg) rotateY(" + (f() ? s : 0) + "deg) translate3d(" + l + "px, " + c + "px, " + h + "px)";
                                u <= 1 && u > -1 && (t = r * 90 + u * 90, a.rtl && (t = -r * 90 - u * 90)), i.transform(p);
                                if (a.params.cube.slideShadows) {
                                    var d = f() ? i.find(".swiper-slide-shadow-left") : i.find(".swiper-slide-shadow-top"),
                                        v = f() ? i.find(".swiper-slide-shadow-right") : i.find(".swiper-slide-shadow-bottom");
                                    d.length === 0 && (d = e('<div class="swiper-slide-shadow-' + (f() ? "left" : "top") + '"></div>'), i.append(d)), v.length === 0 && (v = e('<div class="swiper-slide-shadow-' + (f() ? "right" : "bottom") + '"></div>'), i.append(v));
                                    var m = i[0].progress;
                                    d.length && (d[0].style.opacity = -i[0].progress), v.length && (v[0].style.opacity = i[0].progress)
                                }
                            }
                            a.wrapper.css({
                                "-webkit-transform-origin": "50% 50% -" + a.size / 2 + "px",
                                "-moz-transform-origin": "50% 50% -" + a.size / 2 + "px",
                                "-ms-transform-origin": "50% 50% -" + a.size / 2 + "px",
                                "transform-origin": "50% 50% -" + a.size / 2 + "px"
                            });
                            if (a.params.cube.shadow)
                                if (f()) n.transform("translate3d(0px, " + (a.width / 2 + a.params.cube.shadowOffset) + "px, " + -a.width / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + a.params.cube.shadowScale + ")");
                                else {
                                    var g = Math.abs(t) - Math.floor(Math.abs(t) / 90) * 90,
                                        y = 1.5 - (Math.sin(g * 2 * Math.PI / 360) / 2 + Math.cos(g * 2 * Math.PI / 360) / 2),
                                        b = a.params.cube.shadowScale,
                                        w = a.params.cube.shadowScale / y,
                                        E = a.params.cube.shadowOffset;
                                    n.transform("scale3d(" + b + ", 1, " + w + ") translate3d(0px, " + (a.height / 2 + E) + "px, " + -a.height / 2 / w + "px) rotateX(-90deg)")
                                }
                            var S = a.isSafari || a.isUiWebView ? -a.size / 2 : 0;
                            a.wrapper.transform("translate3d(0px,0," + S + "px) rotateX(" + (f() ? 0 : t) + "deg) rotateY(" + (f() ? -t : 0) + "deg)")
                        },
                        setTransition: function(e) {
                            a.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), a.params.cube.shadow && !f() && a.container.find(".swiper-cube-shadow").transition(e)
                        }
                    },
                    coverflow: {
                        setTranslate: function() {
                            var t = a.translate,
                                n = f() ? -t + a.width / 2 : -t + a.height / 2,
                                r = f() ? a.params.coverflow.rotate : -a.params.coverflow.rotate,
                                i = a.params.coverflow.depth;
                            for (var s = 0, o = a.slides.length; s < o; s++) {
                                var u = a.slides.eq(s),
                                    l = a.slidesSizesGrid[s],
                                    c = u[0].swiperSlideOffset,
                                    h = (n - c - l / 2) / l * a.params.coverflow.modifier,
                                    p = f() ? r * h : 0,
                                    d = f() ? 0 : r * h,
                                    v = -i * Math.abs(h),
                                    m = f() ? 0 : a.params.coverflow.stretch * h,
                                    g = f() ? a.params.coverflow.stretch * h : 0;
                                Math.abs(g) < .001 && (g = 0), Math.abs(m) < .001 && (m = 0), Math.abs(v) < .001 && (v = 0), Math.abs(p) < .001 && (p = 0), Math.abs(d) < .001 && (d = 0);
                                var y = "translate3d(" + g + "px," + m + "px," + v + "px)  rotateX(" + d + "deg) rotateY(" + p + "deg)";
                                u.transform(y), u[0].style.zIndex = -Math.abs(Math.round(h)) + 1;
                                if (a.params.coverflow.slideShadows) {
                                    var b = f() ? u.find(".swiper-slide-shadow-left") : u.find(".swiper-slide-shadow-top"),
                                        w = f() ? u.find(".swiper-slide-shadow-right") : u.find(".swiper-slide-shadow-bottom");
                                    b.length === 0 && (b = e('<div class="swiper-slide-shadow-' + (f() ? "left" : "top") + '"></div>'), u.append(b)), w.length === 0 && (w = e('<div class="swiper-slide-shadow-' + (f() ? "right" : "bottom") + '"></div>'), u.append(w)), b.length && (b[0].style.opacity = h > 0 ? h : 0), w.length && (w[0].style.opacity = -h > 0 ? -h : 0)
                                }
                            }
                            if (a.browser.ie) {
                                var E = a.wrapper[0].style;
                                E.perspectiveOrigin = n + "px 50%"
                            }
                        },
                        setTransition: function(e) {
                            a.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)
                        }
                    }
                }, a.lazy = {
                    initialImageLoaded: !1,
                    loadImageInSlide: function(t, n) {
                        if (typeof t == "undefined") return;
                        typeof n == "undefined" && (n = !0);
                        if (a.slides.length === 0) return;
                        var r = a.slides.eq(t),
                            i = r.find(".swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)");
                        r.hasClass("swiper-lazy") && !r.hasClass("swiper-lazy-loaded") && !r.hasClass("swiper-lazy-loading") && i.add(r[0]);
                        if (i.length === 0) return;
                        i.each(function() {
                            var t = e(this);
                            t.addClass("swiper-lazy-loading");
                            var i = t.attr("data-background"),
                                s = t.attr("data-src");
                            a.loadImage(t[0], s || i, !1, function() {
                                i ? (t.css("background-image", "url(" + i + ")"), t.removeAttr("data-background")) : (t.attr("src", s), t.removeAttr("data-src")), t.addClass("swiper-lazy-loaded").removeClass("swiper-lazy-loading"), r.find(".swiper-lazy-preloader, .preloader").remove();
                                if (a.params.loop && n) {
                                    var e = r.attr("data-swiper-slide-index");
                                    if (r.hasClass(a.params.slideDuplicateClass)) {
                                        var o = a.wrapper.children('[data-swiper-slide-index="' + e + '"]:not(.' + a.params.slideDuplicateClass + ")");
                                        a.lazy.loadImageInSlide(o.index(), !1)
                                    } else {
                                        var u = a.wrapper.children("." + a.params.slideDuplicateClass + '[data-swiper-slide-index="' + e + '"]');
                                        a.lazy.loadImageInSlide(u.index(), !1)
                                    }
                                }
                                a.emit("onLazyImageReady", a, r[0], t[0])
                            }), a.emit("onLazyImageLoad", a, r[0], t[0])
                        })
                    },
                    load: function() {
                        var t;
                        if (a.params.watchSlidesVisibility) a.wrapper.children("." + a.params.slideVisibleClass).each(function() {
                            a.lazy.loadImageInSlide(e(this).index())
                        });
                        else if (a.params.slidesPerView > 1)
                            for (t = a.activeIndex; t < a.activeIndex + a.params.slidesPerView; t++) a.slides[t] && a.lazy.loadImageInSlide(t);
                        else a.lazy.loadImageInSlide(a.activeIndex);
                        if (a.params.lazyLoadingInPrevNext)
                            if (a.params.slidesPerView > 1) {
                                for (t = a.activeIndex + a.params.slidesPerView; t < a.activeIndex + a.params.slidesPerView + a.params.slidesPerView; t++) a.slides[t] && a.lazy.loadImageInSlide(t);
                                for (t = a.activeIndex - a.params.slidesPerView; t < a.activeIndex; t++) a.slides[t] && a.lazy.loadImageInSlide(t)
                            } else {
                                var n = a.wrapper.children("." + a.params.slideNextClass);
                                n.length > 0 && a.lazy.loadImageInSlide(n.index());
                                var r = a.wrapper.children("." + a.params.slidePrevClass);
                                r.length > 0 && a.lazy.loadImageInSlide(r.index())
                            }
                    },
                    onTransitionStart: function() {
                        a.params.lazyLoading && (a.params.lazyLoadingOnTransitionStart || !a.params.lazyLoadingOnTransitionStart && !a.lazy.initialImageLoaded) && a.lazy.load()
                    },
                    onTransitionEnd: function() {
                        a.params.lazyLoading && !a.params.lazyLoadingOnTransitionStart && a.lazy.load()
                    }
                }, a.scrollbar = {
                    set: function() {
                        if (!a.params.scrollbar) return;
                        var t = a.scrollbar;
                        t.track = e(a.params.scrollbar), t.drag = t.track.find(".swiper-scrollbar-drag"), t.drag.length === 0 && (t.drag = e('<div class="swiper-scrollbar-drag"></div>'), t.track.append(t.drag)), t.drag[0].style.width = "", t.drag[0].style.height = "", t.trackSize = f() ? t.track[0].offsetWidth : t.track[0].offsetHeight, t.divider = a.size / a.virtualSize, t.moveDivider = t.divider * (t.trackSize / a.size), t.dragSize = t.trackSize * t.divider, f() ? t.drag[0].style.width = t.dragSize + "px" : t.drag[0].style.height = t.dragSize + "px", t.divider >= 1 ? t.track[0].style.display = "none" : t.track[0].style.display = "", a.params.scrollbarHide && (t.track[0].style.opacity = 0)
                    },
                    setTranslate: function() {
                        if (!a.params.scrollbar) return;
                        var e, t = a.scrollbar,
                            n = a.translate || 0,
                            r, i = t.dragSize;
                        r = (t.trackSize - t.dragSize) * a.progress, a.rtl && f() ? (r = -r, r > 0 ? (i = t.dragSize - r, r = 0) : -r + t.dragSize > t.trackSize && (i = t.trackSize + r)) : r < 0 ? (i = t.dragSize + r, r = 0) : r + t.dragSize > t.trackSize && (i = t.trackSize - r), f() ? (a.support.transforms3d ? t.drag.transform("translate3d(" + r + "px, 0, 0)") : t.drag.transform("translateX(" + r + "px)"), t.drag[0].style.width = i + "px") : (a.support.transforms3d ? t.drag.transform("translate3d(0px, " + r + "px, 0)") : t.drag.transform("translateY(" + r + "px)"), t.drag[0].style.height = i + "px"), a.params.scrollbarHide && (clearTimeout(t.timeout), t.track[0].style.opacity = 1, t.timeout = setTimeout(function() {
                            t.track[0].style.opacity = 0, t.track.transition(400)
                        }, 1e3))
                    },
                    setTransition: function(e) {
                        if (!a.params.scrollbar) return;
                        a.scrollbar.drag.transition(e)
                    }
                }, a.controller = {
                    LinearSpline: function(e, t) {
                        this.x = e, this.y = t, this.lastIndex = e.length - 1;
                        var n, r, i = this.x.length;
                        this.interpolate = function(e) {
                            return e ? (r = s(this.x, e), n = r - 1, (e - this.x[n]) * (this.y[r] - this.y[n]) / (this.x[r] - this.x[n]) + this.y[n]) : 0
                        };
                        var s = function() {
                            var e, t, n;
                            return function(r, i) {
                                t = -1, e = r.length;
                                while (e - t > 1) r[n = e + t >> 1] <= i ? t = n : e = n;
                                return e
                            }
                        }()
                    },
                    getInterpolateFunction: function(e) {
                        a.controller.spline || (a.controller.spline = a.params.loop ? new a.controller.LinearSpline(a.slidesGrid, e.slidesGrid) : new a.controller.LinearSpline(a.snapGrid, e.snapGrid))
                    },
                    setTranslate: function(e, n) {
                        function o(t) {
                            e = t.rtl && t.params.direction === "horizontal" ? -a.translate : a.translate, a.params.controlBy === "slide" && (a.controller.getInterpolateFunction(t), s = -a.controller.spline.interpolate(-e));
                            if (!s || a.params.controlBy === "container") i = (t.maxTranslate() - t.minTranslate()) / (a.maxTranslate() - a.minTranslate()), s = (e - a.minTranslate()) * i + t.minTranslate();
                            a.params.controlInverse && (s = t.maxTranslate() - s), t.updateProgress(s), t.setWrapperTranslate(s, !1, a), t.updateActiveIndex()
                        }
                        var r = a.params.control,
                            i, s;
                        if (a.isArray(r))
                            for (var u = 0; u < r.length; u++) r[u] !== n && r[u] instanceof t && o(r[u]);
                        else r instanceof t && n !== r && o(r)
                    },
                    setTransition: function(e, n) {
                        function s(t) {
                            t.setWrapperTransition(e, a), e !== 0 && (t.onTransitionStart(), t.wrapper.transitionEnd(function() {
                                if (!r) return;
                                t.params.loop && a.params.controlBy === "slide" && t.fixLoop(), t.onTransitionEnd()
                            }))
                        }
                        var r = a.params.control,
                            i;
                        if (a.isArray(r))
                            for (i = 0; i < r.length; i++) r[i] !== n && r[i] instanceof t && s(r[i]);
                        else r instanceof t && n !== r && s(r)
                    }
                }, a.hashnav = {
                    init: function() {
                        if (!a.params.hashnav) return;
                        a.hashnav.initialized = !0;
                        var e = document.location.hash.replace("#", "");
                        if (!e) return;
                        var t = 0;
                        for (var n = 0, r = a.slides.length; n < r; n++) {
                            var i = a.slides.eq(n),
                                s = i.attr("data-hash");
                            if (s === e && !i.hasClass(a.params.slideDuplicateClass)) {
                                var o = i.index();
                                a.slideTo(o, t, a.params.runCallbacksOnInit, !0)
                            }
                        }
                    },
                    setHash: function() {
                        if (!a.hashnav.initialized || !a.params.hashnav) return;
                        document.location.hash = a.slides.eq(a.activeIndex).attr("data-hash") || ""
                    }
                }, a.disableKeyboardControl = function() {
                    e(document).off("keydown", A)
                }, a.enableKeyboardControl = function() {
                    e(document).on("keydown", A)
                }, a.mousewheel = {
                    event: !1,
                    lastScrollTime: (new window.Date).getTime()
                };
                if (a.params.mousewheelControl) {
                    document.onmousewheel !== undefined && (a.mousewheel.event = "mousewheel");
                    if (!a.mousewheel.event) try {
                        new window.WheelEvent("wheel"), a.mousewheel.event = "wheel"
                    } catch (O) {}
                    a.mousewheel.event || (a.mousewheel.event = "DOMMouseScroll")
                }
                a.disableMousewheelControl = function() {
                    return a.mousewheel.event ? (a.container.off(a.mousewheel.event, M), !0) : !1
                }, a.enableMousewheelControl = function() {
                    return a.mousewheel.event ? (a.container.on(a.mousewheel.event, M), !0) : !1
                }, a.parallax = {
                    setTranslate: function() {
                        a.container.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function() {
                            _(this, a.progress)
                        }), a.slides.each(function() {
                            var t = e(this);
                            t.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function() {
                                var e = Math.min(Math.max(t[0].progress, -1), 1);
                                _(this, e)
                            })
                        })
                    },
                    setTransition: function(t) {
                        typeof t == "undefined" && (t = a.params.speed), a.container.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function() {
                            var n = e(this),
                                r = parseInt(n.attr("data-swiper-parallax-duration"), 10) || t;
                            t === 0 && (r = 0), n.transition(r)
                        })
                    }
                }, a._plugins = [];
                for (var D in a.plugins) {
                    var P = a.plugins[D](a, a.params[D]);
                    P && a._plugins.push(P)
                }
                return a.callPlugins = function(e) {
                    for (var t = 0; t < a._plugins.length; t++) e in a._plugins[t] && a._plugins[t][e](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
                }, a.emitterEventListeners = {}, a.emit = function(e) {
                    a.params[e] && a.params[e](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                    var t;
                    if (a.emitterEventListeners[e])
                        for (t = 0; t < a.emitterEventListeners[e].length; t++) a.emitterEventListeners[e][t](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                    a.callPlugins && a.callPlugins(e, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
                }, a.on = function(e, t) {
                    return e = H(e), a.emitterEventListeners[e] || (a.emitterEventListeners[e] = []), a.emitterEventListeners[e].push(t), a
                }, a.off = function(e, t) {
                    var n;
                    e = H(e);
                    if (typeof t == "undefined") return a.emitterEventListeners[e] = [], a;
                    if (!a.emitterEventListeners[e] || a.emitterEventListeners[e].length === 0) return;
                    for (n = 0; n < a.emitterEventListeners[e].length; n++) a.emitterEventListeners[e][n] === t && a.emitterEventListeners[e].splice(n, 1);
                    return a
                }, a.once = function(e, t) {
                    e = H(e);
                    var n = function() {
                        t(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]), a.off(e, n)
                    };
                    return a.on(e, n), a
                }, a.a11y = {
                    makeFocusable: function(e) {
                        return e.attr("tabIndex", "0"), e
                    },
                    addRole: function(e, t) {
                        return e.attr("role", t), e
                    },
                    addLabel: function(e, t) {
                        return e.attr("aria-label", t), e
                    },
                    disable: function(e) {
                        return e.attr("aria-disabled", !0), e
                    },
                    enable: function(e) {
                        return e.attr("aria-disabled", !1), e
                    },
                    onEnterKey: function(t) {
                        if (t.keyCode !== 13) return;
                        e(t.target).is(a.params.nextButton) ? (a.onClickNext(t), a.isEnd ? a.a11y.notify(a.params.lastSlideMessage) : a.a11y.notify(a.params.nextSlideMessage)) : e(t.target).is(a.params.prevButton) && (a.onClickPrev(t), a.isBeginning ? a.a11y.notify(a.params.firstSlideMessage) : a.a11y.notify(a.params.prevSlideMessage)), e(t.target).is("." + a.params.bulletClass) && e(t.target)[0].click()
                    },
                    liveRegion: e('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),
                    notify: function(e) {
                        var t = a.a11y.liveRegion;
                        if (t.length === 0) return;
                        t.html(""), t.html(e)
                    },
                    init: function() {
                        if (a.params.nextButton) {
                            var t = e(a.params.nextButton);
                            a.a11y.makeFocusable(t), a.a11y.addRole(t, "button"), a.a11y.addLabel(t, a.params.nextSlideMessage)
                        }
                        if (a.params.prevButton) {
                            var n = e(a.params.prevButton);
                            a.a11y.makeFocusable(n), a.a11y.addRole(n, "button"), a.a11y.addLabel(n, a.params.prevSlideMessage)
                        }
                        e(a.container).append(a.a11y.liveRegion)
                    },
                    initPagination: function() {
                        a.params.pagination && a.params.paginationClickable && a.bullets && a.bullets.length && a.bullets.each(function() {
                            var t = e(this);
                            a.a11y.makeFocusable(t), a.a11y.addRole(t, "button"), a.a11y.addLabel(t, a.params.paginationBulletMessage.replace(/{{index}}/, t.index() + 1))
                        })
                    },
                    destroy: function() {
                        a.a11y.liveRegion && a.a11y.liveRegion.length > 0 && a.a11y.liveRegion.remove()
                    }
                }, a.init = function() {
                    a.params.loop && a.createLoop(), a.updateContainerSize(), a.updateSlidesSize(), a.updatePagination(), a.params.scrollbar && a.scrollbar && a.scrollbar.set(), a.params.effect !== "slide" && a.effects[a.params.effect] && (a.params.loop || a.updateProgress(), a.effects[a.params.effect].setTranslate()), a.params.loop ? a.slideTo(a.params.initialSlide + a.loopedSlides, 0, a.params.runCallbacksOnInit) : (a.slideTo(a.params.initialSlide, 0, a.params.runCallbacksOnInit), a.params.initialSlide === 0 && (a.parallax && a.params.parallax && a.parallax.setTranslate(), a.lazy && a.params.lazyLoading && (a.lazy.load(), a.lazy.initialImageLoaded = !0))), a.attachEvents(), a.params.observer && a.support.observer && a.initObservers(), a.params.preloadImages && !a.params.lazyLoading && a.preloadImages(), a.params.autoplay && a.startAutoplay(), a.params.keyboardControl && a.enableKeyboardControl && a.enableKeyboardControl(), a.params.mousewheelControl && a.enableMousewheelControl && a.enableMousewheelControl(), a.params.hashnav && a.hashnav && a.hashnav.init(), a.params.a11y && a.a11y && a.a11y.init(), a.emit("onInit", a)
                }, a.cleanupStyles = function() {
                    a.container.removeClass(a.classNames.join(" ")).removeAttr("style"), a.wrapper.removeAttr("style"), a.slides && a.slides.length && a.slides.removeClass([a.params.slideVisibleClass, a.params.slideActiveClass, a.params.slideNextClass, a.params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-column").removeAttr("data-swiper-row"), a.paginationContainer && a.paginationContainer.length && a.paginationContainer.removeClass(a.params.paginationHiddenClass), a.bullets && a.bullets.length && a.bullets.removeClass(a.params.bulletActiveClass), a.params.prevButton && e(a.params.prevButton).removeClass(a.params.buttonDisabledClass), a.params.nextButton && e(a.params.nextButton).removeClass(a.params.buttonDisabledClass), a.params.scrollbar && a.scrollbar && (a.scrollbar.track && a.scrollbar.track.length && a.scrollbar.track.removeAttr("style"), a.scrollbar.drag && a.scrollbar.drag.length && a.scrollbar.drag.removeAttr("style"))
                }, a.destroy = function(e, t) {
                    a.detachEvents(), a.stopAutoplay(), a.params.loop && a.destroyLoop(), t && a.cleanupStyles(), a.disconnectObservers(), a.params.keyboardControl && a.disableKeyboardControl && a.disableKeyboardControl(), a.params.mousewheelControl && a.disableMousewheelControl && a.disableMousewheelControl(), a.params.a11y && a.a11y && a.a11y.destroy(), a.emit("onDestroy"), e !== !1 && (a = null)
                }, a.init(), a
            }
            return new t(n, r)
        };
        t.prototype = {
            isSafari: function() {
                var e = navigator.userAgent.toLowerCase();
                return e.indexOf("safari") >= 0 && e.indexOf("chrome") < 0 && e.indexOf("android") < 0
            }(),
            isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),
            isArray: function(e) {
                return Object.prototype.toString.apply(e) === "[object Array]"
            },
            browser: {
                ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
                ieTouch: window.navigator.msPointerEnabled && window.navigator.msMaxTouchPoints > 1 || window.navigator.pointerEnabled && window.navigator.maxTouchPoints > 1
            },
            device: function() {
                var e = navigator.userAgent,
                    t = e.match(/(Android);?[\s\/]+([\d.]+)?/),
                    n = e.match(/(iPad).*OS\s([\d_]+)/),
                    r = e.match(/(iPod)(.*OS\s([\d_]+))?/),
                    i = !n && e.match(/(iPhone\sOS)\s([\d_]+)/);
                return {
                    ios: n || i || r,
                    android: t
                }
            }(),
            support: {
                touch: window.Modernizr && Modernizr.touch === !0 || function() {
                    return !!("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch)
                }(),
                transforms3d: window.Modernizr && Modernizr.csstransforms3d === !0 || function() {
                    var e = document.createElement("div").style;
                    return "webkitPerspective" in e || "MozPerspective" in e || "OPerspective" in e || "MsPerspective" in e || "perspective" in e
                }(),
                flexbox: function() {
                    var e = document.createElement("div").style,
                        t = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" ");
                    for (var n = 0; n < t.length; n++)
                        if (t[n] in e) return !0
                }(),
                observer: function() {
                    return "MutationObserver" in window || "WebkitMutationObserver" in window
                }()
            },
            plugins: {}
        };
        var n = ["jQuery", "Zepto", "Dom7"];
        for (var r = 0; r < n.length; r++) window[n[r]] && s(window[n[r]]);
        var i;
        typeof Dom7 == "undefined" ? i = window.Dom7 || window.Zepto || window.jQuery : i = Dom7, i && ("transitionEnd" in i.fn || (i.fn.transitionEnd = function(e) {
            function s(r) {
                if (r.target !== this) return;
                e.call(this, r);
                for (n = 0; n < t.length; n++) i.off(t[n], s)
            }
            var t = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"],
                n, r, i = this;
            if (e)
                for (n = 0; n < t.length; n++) i.on(t[n], s);
            return this
        }), "transform" in i.fn || (i.fn.transform = function(e) {
            for (var t = 0; t < this.length; t++) {
                var n = this[t].style;
                n.webkitTransform = n.MsTransform = n.msTransform = n.MozTransform = n.OTransform = n.transform = e
            }
            return this
        }), "transition" in i.fn || (i.fn.transition = function(e) {
            typeof e != "string" && (e += "ms");
            for (var t = 0; t < this.length; t++) {
                var n = this[t].style;
                n.webkitTransitionDuration = n.MsTransitionDuration = n.msTransitionDuration = n.MozTransitionDuration = n.OTransitionDuration = n.transitionDuration = e
            }
            return this
        })), window.Swiper = t
    }(), typeof module != "undefined" ? module.exports = window.Swiper : typeof define == "function" && define.amd && define("swiper", [], function() {
        "use strict";
        return window.Swiper
    }), define("views/components/Slideshow", ["backbone", "marionette", "vent", "jquery", "underscore", "isMobile", "swiper", "models/App"], function(e, t, n, r, i, s, o, u) {
        var a = e.Marionette.ItemView.extend({
            tagName: "div",
            className: "view-slideshow",
            template: !1,
            ui: {
                close: ".close",
                thumbs: ".thumbs",
                thumb: ".thumb",
                arrows: ".arrow"
            },
            events: {
                "click @ui.close": "animateOut",
                "click @ui.thumb": "swipeTo",
                "mousemove @ui.thumbs": "thumbsMove",
                "mouseleave @ui.thumbs": "thumbsLeave"
            },
            initialize: function(e) {
                var t = this,
                    n = e.target.getAttribute("data-id") || e.target.getAttribute("data-slide");
                this.scrollTop = e.position, this.$el.css("width", window.innerWidth + window.sw), r("body").addClass("overflow"), r("body").addClass("is-overlay"), this.isMobile = s.any(), this.images = document.getElementById(n).querySelectorAll("img[data-path]"), this.ref = document.querySelector('[data-slide="' + n + '"]') || e.target, this.bounding = this.ref.getBoundingClientRect(), this.debouncedResizeEnd = i.debounce(this.resizeEnd, 250), this.initDom(), this.loadImages()
            },
            initDom: function() {
                var e = this,
                    t = {
                        itemsWidth: 75,
                        rowPadding: 200
                    },
                    n = e.images[0].getAttribute("data-size") == "photo_size_portrait" ? "is-portrait" : "is-landscape";
                this.scaleFactor = n == "is-portrait" ? .8 : 1, this.$slideshow = r("<div></div>").addClass("slideshow").css({
                    position: "absolute",
                    transform: "translateX(" + this.bounding.left + "px) translateY(" + this.bounding.top + "px) translateZ(0)",
                    width: this.bounding.width,
                    height: this.bounding.height
                }), this.$close = r("<div><div></div><span>Close</span></div>").addClass("close"), this.$index = r("<span></span>").addClass("index"), this.$thumbs = r("<div></div>").addClass("thumbs"), this.$container = r("<div></div>").addClass("swiper-container " + n), this.$wrapper = r("<div></div>").addClass("swiper-wrapper"), this.$arrowLeft = r("<div><span></span></div>").addClass("swiper-button-prev arrow left"), this.$arrowRight = r("<div><span></span></div>").addClass("swiper-button-next arrow right"), this.$el.append(this.$slideshow, [this.$index, this.$thumbs, this.$arrowLeft, this.$arrowRight, this.$close]), this.$slideshow.append(this.$container), this.$container.append(this.$wrapper), this.$thumbs.css("width", t.itemsWidth * this.images.length + t.rowPadding)
            },
            loadImages: function() {
                var e = this;
                for (var t = 0; t < this.images.length; t++) {
                    var n = e.images[t],
                        i = n.getAttribute("data-path"),
                        s = n.getAttribute("data-thumbnail"),
                        o = n.getAttribute("data-size"),
                        u = r("<div></div>").addClass("swiper-slide " + o),
                        a = r("<div></div>").addClass("swiper-slide-img").css("background-image", "url(" + i + ")"),
                        f = r("<div></div>").addClass("thumb").attr("data-slide", t).css("background-image", "url(" + s + ")");
                    e.$wrapper.append(u), u.append(a), e.$thumbs.append(f)
                }
                r(window).resize(this.resize.bind(this)), setTimeout(function() {
                    e.createSlideShow()
                }, 400)
            },
            createSlideShow: function() {
                var e = this;
                this.slider = new Swiper(this.$container, {
                    speed: 900,
                    onSlideChangeStart: this.updateArrow.bind(this),
                    onSlideChangeEnd: this.updateIndex.bind(this),
                    prevButton: ".swiper-button-prev",
                    nextButton: ".swiper-button-next"
                }), this.thumbsBounding = this.$thumbs[0].getBoundingClientRect(), this.bindUIElements(), this.animateIn(), this.updateIndex(), this.updateArrow()
            },
            animateIn: function() {
                var e = this;
                this.centerX = (window.innerWidth - this.bounding.width) / 2, this.centerY = (window.innerHeight - this.bounding.height) / 2, this.getScale();
                var t = new TimelineMax({
                    paused: !0,
                    onComplete: function() {
                        e.$el.addClass("is-ready")
                    }
                });
                t.to(this.$el, .4, {
                    autoAlpha: 1
                }), t.to(this.$slideshow, 1, {
                    x: this.centerX,
                    y: this.centerY,
                    scale: this.scale,
                    ease: Expo.easeInOut
                }), t.to(this.$close, .6, {
                    autoAlpha: 1
                }, 1.5), t.to(this.$index, .6, {
                    autoAlpha: 1
                }, 1.5), t.to(this.$arrowLeft, .6, {
                    autoAlpha: 1
                }, 1.5), t.to(this.$arrowRight, .6, {
                    autoAlpha: 1
                }, 1.5), t.restart(), this.isMobile || TweenMax.delayedCall(.4, function() {
                    n.trigger("sidebar:close")
                })
            },
            animateOut: function() {
                window.scrollTo(0, this.scrollTop), this.$el.removeClass("is-ready");
                var e = new TimelineMax({
                    paused: !0,
                    onComplete: this.destroy.bind(this)
                });
                e.to(this.$el, .4, {
                    scale: 1
                }), e.to(this.$thumbs, .6, {
                    autoAlpha: 0
                }, 0), e.to(this.$close, .6, {
                    autoAlpha: 0
                }, 0), e.to(this.$index, .6, {
                    autoAlpha: 0
                }, 0), e.to(this.$arrowLeft, .6, {
                    autoAlpha: 0
                }, 0), e.to(this.$arrowRight, .6, {
                    autoAlpha: 0
                }, 0), e.to(this.$slideshow, .9, {
                    width: this.bounding.width,
                    height: this.bounding.height,
                    x: this.bounding.left,
                    y: this.bounding.top,
                    scale: 1,
                    ease: Expo.easeInOut
                }), e.to(this.$el, .4, {
                    autoAlpha: 0
                }), this.slider.slideTo(0, 900, e.restart()), this.isMobile || TweenMax.delayedCall(.4, function() {
                    n.trigger("sidebar:open")
                })
            },
            updateIndex: function() {
                this.$index.html(this.slider.activeIndex + 1 + " / " + this.slider.slides.length)
            },
            updateArrow: function() {
                var e = this;
                this.ui.arrows.each(function() {
                    var t = r(this),
                        n = r(this).hasClass("right") ? r('.thumb[data-slide="' + (e.slider.activeIndex + 1) + '"]').css("background-image") : r('.thumb[data-slide="' + (e.slider.activeIndex - 1) + '"]').css("background-image");
                    r("span", t).css("background-image", n)
                })
            },
            swipeTo: function(e) {
                var t = e.currentTarget.getAttribute("data-slide");
                this.slider.slideTo(t, 900)
            },
            thumbsMove: function(e) {
                TweenLite.to(this.$container, 1, {
                    scale: .9,
                    y: -40,
                    ease: Expo.easeOut
                });
                if (this.thumbsBounding.width < window.innerWidth) return;
                var t = Math.min(Math.max(e.clientX, 0), this.thumbsBounding.width - window.innerWidth);
                TweenLite.to(this.ui.thumbs, 1.5, {
                    x: -t,
                    z: 0,
                    ease: Expo.easeOut
                })
            },
            thumbsLeave: function(e) {
                TweenLite.to(this.$container, .7, {
                    scale: 1,
                    y: 0,
                    ease: Expo.easeInOut
                })
            },
            getScale: function() {
                this.scale = this.bounding.height >= window.innerHeight * this.scaleFactor ? window.innerHeight * this.scaleFactor / this.bounding.height : 1.1, this.scale = Math.min(Math.max(this.scale, .2), 1.1);
                if (this.isMobile || window.innerWidth <= 768) this.scale = Math.min(Math.max(this.scale, .2), 1)
            },
            resize: function() {
                var e = this;
                this.bounding = this.ref.getBoundingClientRect(), this.resizeBounding = this.$slideshow[0].getBoundingClientRect(), this.resizeCenterX = (window.innerWidth - this.resizeBounding.width) / 2, this.resizeCenterY = (window.innerHeight - this.resizeBounding.height) / 2, this.getScale(), TweenLite.set(this.$slideshow, {
                    x: this.resizeCenterX,
                    y: this.resizeCenterY,
                    scale: 1,
                    autoAlpha: 0
                }), t.triggerMethodOn(e, "resize"), this.debouncedResizeEnd()
            },
            resizeEnd: function() {
                var e = this;
                TweenLite.to(this.$slideshow, 1, {
                    x: this.resizeCenterX,
                    y: this.resizeCenterY,
                    scale: this.scale,
                    autoAlpha: 1
                }), this.$el.css("width", window.innerWidth + window.sw), t.triggerMethodOn(e, "resizeEnd")
            },
            onDestroy: function() {
                r("body").removeClass("overflow"), r("body").removeClass("is-overlay"), r(window).off("resize")
            }
        });
        return a
    }), define("views/OnePager", ["backbone", "marionette", "vent", "jquery", "underscore", "isMobile", "models/App", "models/PageData", "views/Page", "models/Project", "views/components/Slideshow"], function(e, t, n, r, i, s, o, u, a, f, l) {
        var c = a.extend({
            ui: i.extend({
                projectsHolder: ".homeholder",//".projects-holder",
                slideButtons: ".button-photo-slideshow",
                slideImageButton: ".homeholder *[data-slide]",//".projects-holder *[data-slide]",
                stickies: ".home header h1", //".project header h1",
                formInput: '.form input[type="submit"]'
            }, a.prototype.ui),
            events: {
                "click @ui.slideButtons": "initGallery",
                "click @ui.slideImageButton": "initGallery",
                "click @ui.formInput": "ajaxNewsletter"
            },
            initialize: function(e) {
                e.minimumHeight = !0, this.isMobile = s.any(), a.prototype.initialize.apply(this, arguments)
            },
            onViewDataAdded: function() {
                var e = this;
                this.projects = o.get("projectsData"), this.projectCount = this.projects.length;
                var t = e.ui.projectsHolder.find(".project"),
                    n = "",
                    r = 0;
                t.length > 0 && (n = t.data("postname"), r = t.data("order") - 1), this.projects.each(function(i) {
                    var s = n == i.get("slug"),
                        o = i.get("originalOrder"),
                        u = o - r;
                    u = u <= 0 ? e.projectCount + u : u, i.set({
                        active: !1,
                        loaded: s,
                        element: s ? t : null,
                        yPos: -1,
                        yPosBottom: -1,
                        height: 0,
                        inView: s,
                        scrollProgress: 0,
                        loadNewY: -1,
                        passedLoadNewY: !1,
                        order: u
                    }), s && (e.projectActive = i)
                }), this.projects.sort(), e.projectActive || this.loadNewProject(), this.listenTo(this.projects, "change:inView", this.projectInViewChange)
            },
            onResize: function() {
                var e = this.ui.projectsHolder.position().top;
                if (this.projects) {
                    var t = this.projects.where({
                        loaded: !0
                    });
                    t && i.each(t, function(t) {
                        var n = t.get("element"),
                            r = n.outerHeight(),
                            i = e + n.position().top;
                        t.set({
                            yPos: i,
                            yPosBottom: i + r,
                            height: r,
                            loadNewY: i + r * .6
                        })
                    })
                }
            },
            onResizeEnd: function() {},
            onScroll: function(e) {
                var t = this;
                if (!this.ui.projectsHolder || this.ui.projectsHolder.length == 0) return !0;
                var n = e + o.get("windowHeight");
                this.ui.stickies && this.ui.stickies.each(function(e) {
                    var n = r(this);
                    if (this.getBoundingClientRect().top <= 29) this.state || (t.$sticky && t.$sticky.remove(), t.$sticky = n.clone(), t.$sticky.data("id", e), t.$el.append(t.$sticky), TweenLite.set(this, {
                        autoAlpha: 0
                    }), TweenLite.set(t.$sticky, {
                        autoAlpha: 1
                    }), this.state = !0);
                    else if (this.getBoundingClientRect().top >= 45 && this.state) {
                        var i = t.$sticky.data("id") - 1;
                        t.$sticky && t.$sticky.remove(), i >= 0 && (t.$sticky = r(t.ui.stickies[i]).clone(), t.$sticky.data("id", i), t.$el.append(t.$sticky), TweenLite.set(t.$sticky, {
                            autoAlpha: 1
                        })), TweenLite.set(this, {
                            autoAlpha: 1
                        }), this.state = !1
                    }
                }), this.projects && this.projects.each(function(e) {
                    e.get("yPos") < n && e.get("yPosBottom") >= n ? (t.projectActive = e, e.set({
                        inView: !0
                    })) : e.set({
                        inView: !1
                    })
                }), this.projectActive && this.projectActive.set({
                    scrollProgress: (n - this.projectActive.get("yPos")) / this.projectActive.get("height")
                });
                if (this.loadingNewProject) return !0;
                this.projectActive && !this.projectActive.get("passedLoadNewY") && this.projectActive.get("loadNewY") != -1 && this.projectActive.get("loadNewY") < n && (this.projectActive.set({
                    passedLoadNewY: !0
                }), this.loadNewProject())
            },
            loadNewProject: function() {
                this.loadingNewProject = !0;
                var e = this.projects.findWhere({
                    loaded: !1
                });
                if (e) {
                    var t = new u({
                        element: ".project"
                    });
                    t.fetch({
                        url: e.get("url"),
                        dataType: "html",
                        success: i.bind(this.newProjectLoaded, this, t, e),
                        error: function(e, t) {}
                    })
                }
            },
            newProjectLoaded: function(e, t) {
                var n = e.get("viewData");
                n.appendTo(this.ui.projectsHolder), t.set({
                    element: n,
                    loaded: !0
                }), this.loadingNewProject = !1, this.bindUIElements(), this.bounding = this.ui.section[0].getBoundingClientRect().height, this.resize()
            },
            projectInViewChange: function(t) {
                var n = t.get("slug"),
                    r = this.ui.projectsHolder.find('.project[data-postname="' + n + '"]');
                if (t.get("inView")) {
                    r.addClass("active");
                    var i = window.location.protocol + "//" + window.location.host,
                        s = t.get("url");
                    e.history.navigate(s.replace(i, ""), {
                        trigger: !1,
                        replace: !0
                    })
                } else r.removeClass("active")
            },
            initGallery: function(e) {
                var t = this,
                    n = e.currentTarget,
                    i = r(window).scrollTop();
                this.slideshow = new l({
                    target: n,
                    position: i
                }), this.$el.append(this.slideshow.render().el)
            },
            ajaxNewsletter: function(e) {
                var t = r(e.currentTarget).parent(),
                    n = t.parent(),
                    i = r('input[type="email"]', t),
                    s = r("p", n),
                    u = r("span", n),
                    a = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
                    f = a.test(i[0].value);
                return f ? r.post(o.get("ajaxUrl"), {
                    nonce: o.get("ajaxNonce"),
                    action: "newsletter_subscribe",
                    email: i[0].value
                }).done(function(e) {
                    e.success && i.hasClass("error") && i.removeClass("error"), TweenLite.to(s, 1, {
                        autoAlpha: 0
                    }), u.html(e.message).addClass("show")
                }) : (i.addClass("error"), TweenLite.to(s, 1, {
                    autoAlpha: 0
                }), u.html("Email is not valid").addClass("show")), !1
            },
            onDestroy: function() {
                a.prototype.onDestroy.apply(this, arguments), this.slideshow && this.slideshow.destroy()
            }
        });
        return c
    }),
    function(e) {
        typeof define == "function" && define.amd ? define("mousewheel", ["jquery"], e) : typeof exports == "object" ? module.exports = e : e(jQuery)
    }(function(e) {
        function a(t) {
            var n = t || window.event,
                o = r.call(arguments, 1),
                a = 0,
                c = 0,
                h = 0,
                p = 0,
                d = 0,
                v = 0;
            t = e.event.fix(n), t.type = "mousewheel", "detail" in n && (h = n.detail * -1), "wheelDelta" in n && (h = n.wheelDelta), "wheelDeltaY" in n && (h = n.wheelDeltaY), "wheelDeltaX" in n && (c = n.wheelDeltaX * -1), "axis" in n && n.axis === n.HORIZONTAL_AXIS && (c = h * -1, h = 0), a = h === 0 ? c : h, "deltaY" in n && (h = n.deltaY * -1, a = h), "deltaX" in n && (c = n.deltaX, h === 0 && (a = c * -1));
            if (h === 0 && c === 0) return;
            if (n.deltaMode === 1) {
                var m = e.data(this, "mousewheel-line-height");
                a *= m, h *= m, c *= m
            } else if (n.deltaMode === 2) {
                var g = e.data(this, "mousewheel-page-height");
                a *= g, h *= g, c *= g
            }
            p = Math.max(Math.abs(h), Math.abs(c));
            if (!s || p < s) s = p, l(n, p) && (s /= 40);
            l(n, p) && (a /= 40, c /= 40, h /= 40), a = Math[a >= 1 ? "floor" : "ceil"](a / s), c = Math[c >= 1 ? "floor" : "ceil"](c / s), h = Math[h >= 1 ? "floor" : "ceil"](h / s);
            if (u.settings.normalizeOffset && this.getBoundingClientRect) {
                var y = this.getBoundingClientRect();
                d = t.clientX - y.left, v = t.clientY - y.top
            }
            return t.deltaX = c, t.deltaY = h, t.deltaFactor = s, t.offsetX = d, t.offsetY = v, t.deltaMode = 0, o.unshift(t, a, c, h), i && clearTimeout(i), i = setTimeout(f, 200), (e.event.dispatch || e.event.handle).apply(this, o)
        }

        function f() {
            s = null
        }

        function l(e, t) {
            return u.settings.adjustOldDeltas && e.type === "mousewheel" && t % 120 === 0
        }
        var t = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
            n = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
            r = Array.prototype.slice,
            i, s;
        if (e.event.fixHooks)
            for (var o = t.length; o;) e.event.fixHooks[t[--o]] = e.event.mouseHooks;
        var u = e.event.special.mousewheel = {
            version: "3.1.12",
            setup: function() {
                if (this.addEventListener)
                    for (var t = n.length; t;) this.addEventListener(n[--t], a, !1);
                else this.onmousewheel = a;
                e.data(this, "mousewheel-line-height", u.getLineHeight(this)), e.data(this, "mousewheel-page-height", u.getPageHeight(this))
            },
            teardown: function() {
                if (this.removeEventListener)
                    for (var t = n.length; t;) this.removeEventListener(n[--t], a, !1);
                else this.onmousewheel = null;
                e.removeData(this, "mousewheel-line-height"), e.removeData(this, "mousewheel-page-height")
            },
            getLineHeight: function(t) {
                var n = e(t),
                    r = n["offsetParent" in e.fn ? "offsetParent" : "parent"]();
                return r.length || (r = e("body")), parseInt(r.css("fontSize"), 10) || parseInt(n.css("fontSize"), 10) || 16
            },
            getPageHeight: function(t) {
                return e(t).height()
            },
            settings: {
                adjustOldDeltas: !0,
                normalizeOffset: !0
            }
        };
        e.fn.extend({
            mousewheel: function(e) {
                return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
            },
            unmousewheel: function(e) {
                return this.unbind("mousewheel", e)
            }
        })
    }), 
	
	
	define("views/About", ["backbone", "marionette", "vent", "jquery", "underscore", "isMobile", "models/App", "models/PageData", "views/OnePager", "tweenmax", "mousewheel"], function(e, t, n, r, i, s, o, u, a, f, l) {
        var c = a.extend({
            initialize: function(e) {
                e.minimumHeight = !0, a.prototype.initialize.apply(this, arguments)
            },
            ui: i.extend({
                intro: ".intro",
                introBg: ".intro .intro-background",
                introText: ".intro-text",
                introTextRight: ".intro-text-right",
                introHolder: ".intro-holder",
                contactLink: ".js-contact-scroll",
                cursor: ".cursor"
            }, a.prototype.ui),
            events: i.extend({
                "click @ui.intro": "hideIntro",
                "click @ui.contactLink": "gotoContact"
            }, a.prototype.events),
            initialize: function(e) {
                e.isOverlayView = !1, a.prototype.initialize.apply(this, arguments)
            },
            onViewDataAdded: function() {
				
                var e = this;
                this.state = !0, this.isMobile = s.any(), this.isMobile && (this.isTablet = r("html").hasClass("tablet") ? !0 : !1), r("body").hasClass("is-ready") && r("body").removeClass("is-ready"), r(".site-logo").click(function() {
                    window.scrollTo(0, 0)
                }), r("body").addClass("no-sidebar"), r("#container").addClass("show-intro"), r('.nav-sidebar nav ul li[data-postname="about-contact"]').find("a").addClass("no-hijack"), r(".js-contact-scroll").addClass("no-hijack"), this.ui.intro.on("mousewheel", this.wheelHandler.bind(this)), this.ui.intro.on("mousemove", this.mouseHandler.bind(this)), document.body.onkeydown = this.keyHandler.bind(this), this.isMobile && (document.ontouchmove = this.touchMoveHandler.bind(this)), n.trigger("sidebar:close");
                var t = new TimelineMax({
                    paused: !0,
                    onComplete: function() {
                        e.state = !1
                    }
                });
                t.from(this.ui.introBg, 2, {
                    scale: 1.5,
                    ease: Expo.easeOut
                }), t.to(this.ui.introText, 1.5, {
                    scale: 1,
                    autoAlpha: 1,
                    ease: Expo.easeOut
                }, "-=1.2"), t.restart(), a.prototype.onViewDataAdded.apply(this, arguments)
            },
            mouseHandler: function(e) {
				
                TweenLite.set(this.ui.cursor, {
                    x: e.clientX,
                    y: e.clientY
                })
            },
            wheelHandler: function(e) {
                (e.deltaY >= 2 || e.deltaY <= -2) && this.hideIntro()
            },
            keyHandler: function(e) {
                (e.keyCode == 32 || e.keyCode == 40) && this.hideIntro()
            },
            touchMoveHandler: function(e) {
                e.preventDefault(), this.hideIntro()
            },
            hideIntro: function() {
				
                var e = this;
                if (this.state == 1) return;
                r("body").addClass("intro-done"), TweenLite.to(this.ui.introText, .6, {
                    autoAlpha: 0
                }), f.delayedCall(.2, function() {
                    r("#container").removeClass("show-intro"), !e.isMobile || e.isTablet && window.innerWidth >= 1025 ? (n.trigger("sidebar:open"), TweenLite.to(r(".site-logo"), .7, {
                        autoAlpha: 1,
                        clearProps: "all",
                        delay: .7,
                        ease: Expo.easeOut
                    })) : n.trigger("sidebar:close")
                }), f.delayedCall(1.7, function() {
                    r("body").addClass("is-ready"), r("body").removeClass("no-sidebar"), !r("html").hasClass("mobile") && !r("html").hasClass("tablet") && e.fixScrollbar()
                }), document.body.onkeydown = null, this.isMobile && (document.ontouchmove = null), this.ui.intro.off("mousewheel", this.wheelHandler.bind(this)), this.ui.intro.off("mousemove", this.mouseHandler.bind(this)), (!this.isMobile || this.isTablet) && TweenLite.to(r(".site-logo"), .4, {
                    autoAlpha: 0,
                    ease: Expo.easeOut
                }), this.state = !0
            },
            fixScrollbar: function() {
                var e = window.innerWidth - (window.innerWidth >= 1025 ? 280 : 0);
                r("body").css({
                    width: window.innerWidth + window.sw
                }), r(".view-article.about").css({
                    width: e
                })
            },
            gotoContact: function(e) {
                e.preventDefault();
                var t = document.querySelector(".is-about-ref"),
                    n = r(window).scrollTop() + t.getBoundingClientRect().top;
                window.scrollTo(0, n)
            },
            onResize: function() {
                TweenLite.set(this.ui.introHolder, {
                    clearProps: "scale,opacity"
                }), this.ui.intro.css({
                    height: o.get("windowHeight")
                }), this.ui.introHolder.css({
                    "min-height": o.get("windowHeight")
                }), this.ui.introTextRight.css({
                    "min-height": o.get("windowHeight") - 200
                }), this.introBottomY = this.ui.introHolder.position().top + this.ui.introHolder.outerHeight(), a.prototype.onResize.apply(this, arguments)
            },
            onScroll: function(e) {
                var t = o.get("windowHeight"),
                    n = e + t,
                    i = r('ul.nav-main li[data-postname="about-contact"]'),
                    s = e / (this.introBottomY - t);
                n >= this.introBottomY ? i.removeClass("active") : (i.addClass("active"), i.find(".progress-bar").css({
                    height: Math.max(0, Math.min(100, 100 * s)) + "%"
                })), a.prototype.onScroll.apply(this, arguments)
            }
        });
        return c
    }), define("views/Project", ["backbone", "marionette", "vent", "jquery", "underscore", "isMobile", "models/App", "models/PageData", "views/OnePager"], function(e, t, n, r, i, s, o, u, a) {
        var f = a.extend({
            initialize: function(e) {
                e.minimumHeight = !0, a.prototype.initialize.apply(this, arguments)
            },
            ui: i.extend({}, a.prototype.ui),
            events: i.extend({}, a.prototype.events),
            initialize: function(e) {
                e.isOverlayView = !1, a.prototype.initialize.apply(this, arguments)
            },
            onViewDataAdded: function() {
                var e = this;
                this.bindUIElements(), r("body").addClass("is-ready"), r("body").removeClass("no-sidebar"), r('.nav-sidebar nav ul li[data-postname="about-contact"]').removeClass("active").find("a").removeClass("no-hijack"), n.trigger(s.any() ? "sidebar:close" : "sidebar:open"), a.prototype.onViewDataAdded.apply(this, arguments)
            }
        });
        return f
    }), define("views/Publications", ["backbone", "marionette", "vent", "jquery", "underscore", "isMobile", "models/App", "models/PageData", "views/Page"], function(e, t, n, r, i, s, o, u, a) {
        var f = a.extend({
            ui: i.extend({}, a.prototype.ui),
            events: {},
            initialize: function(e) {
                e.isOverlayView = !0, this.isMobile = s.any(), a.prototype.initialize.apply(this, arguments)
            },
            onViewDataAdded: function() {
                var e = this;
                r("body").addClass("is-ready"), r("#container").addClass("app-started"), this.listenTo(n, "overlay:open", this.enableScroll), this.listenTo(n, "overlay:close", this.hideOverlay), n.trigger("sidebar:close"), this.showOverlay()
            },
            showOverlay: function() {
                var e = this;
                n.trigger("overlay:open"), TweenLite.to(this.$el.parent(), 1, {
                    autoAlpha: 1,
                    delay: 1,
                    onComplete: function() {
                        n.trigger("overlay:opened")
                    }
                })
            },
            hideOverlay: function() {
                var e = this;
                TweenLite.to(this.$el.parent(), 1, {
                    autoAlpha: 0,
                    onComplete: this.destroy.bind(this)
                })
            },
            onDestroy: function() {
                n.trigger("overlay:closed"), !this.isMobile && !r("body").hasClass("no-sidebar") && n.trigger("sidebar:open"), r("body").hasClass("intro-done") || r("body").removeClass("is-ready"), r("body").removeClass("is-overlay"), a.prototype.onDestroy.apply(this, arguments)
            }
        });
        return f
    }), define("controllers/Controller", ["app", "backbone", "underscore", "marionette", "vent", "views/App", "models/App", "controllers/Preloader"], function(e, t, n, r, i, s, o, u) {
        return t.Marionette.Controller.extend({
            containerElement: "#container",
            mainViewElement: "#main-view",
            initialMainViewElement: "#main-view .views",
            preloaderViewElement: "#preloader",
            isInitialView: window.history && window.history.pushState ? !0 : !1,
            isInitialOverlayView: window.history && window.history.pushState ? !0 : !1,
            initialize: function(e) {
                o.set({
                    ajaxUrl: BIA.AJAX_URL,
                    ajaxNonce: BIA.AJAX_NONCE,
                    siteUrl: BIA.SITE_URL,
                    themeUrl: BIA.THEME_URL
                });
                var n = window.history && window.history.pushState && window.location.hash && this.isRootURL();
                n && (this.isInitialView = !1, this.isInitialOverlayView = !1), this.isInitialView || this.forceReload(), this.appView = (new s({
                    containerElement: this.containerElement,
                    mainViewElement: this.mainViewElement,
                    preloaderViewElement: this.preloaderViewElement
                })).render(), t.history.start({
                    pushState: !0,
                    root: "/",
                    silent: !0
                }), this.preload()
            },
            isRootURL: function() {
                return window.location.pathname.replace(/[^\/]$/, "$&/") === "/"
            },
            forceReload: function() {
                this.isInitialView = !1, $(this.mainViewElement).html("")
            },
            forceOverlayReload: function() {
                this.isInitialOverlayView && (this.isInitialOverlayView = !1, $("#overlay").html(""))
            },
            preload: function() {
                this.preloader = new u, this.appView.showPreloader(function() {
                    e.start()
                })
            },
            page: function(e) {
                var n = this;
                require(["views/Page"], function(e) {
                    n.addViewToRegion(t.history.getFragment(), e), n.forceOverlayReload()
                })
            },
            about: function(e) {
                var n = this;
                require(["circlevisions_agent/contact"], function(e) {
                    n.addViewToRegion(t.history.getFragment(), e), n.forceOverlayReload()
                })
            },
            project: function(e) {
                var n = this;
                require(["views/Project"], function(e) {
                    n.addViewToRegion(t.history.getFragment(), e), n.forceOverlayReload()
                })
            },
            publication: function(e) {
                var n = this;
                this.isInitialView && this.forceReload(), require(["views/About"], function(e) {
                    n.addViewToRegion("/", e, {
                        overlayOpened: !0
                    }), require(["views/Publications"], function(e) {
                        n.appView.overlayRegion.show(new e({
                            el: n.isInitialOverlayView ? "#overlay .views" : "",
                            isInitialView: n.isInitialOverlayView,
                            elementSelector: "#overlay .views",
                            url: t.history.getFragment()
                        })), n.isInitialOverlayView = !1
                    })
                })
            },
            addViewToRegion: function(e, t, r) {
                i.trigger("overlay:close"), e ? e.indexOf("/") !== 0 && (e = "/" + e) : e = "/";
                var s = this.appView.mainViewRegion.currentView;
                if (r && r.overlayOpened || s && s.originalUrl == e && s.originalUrl != "/" && e != "/") return this;
                $(this.initialMainViewElement).length == 0 && (this.isInitialView = !1), this.appView.mainViewRegion.show(new t(n.extend({
                    el: this.isInitialView ? this.initialMainViewElement : "",
                    isInitialView: this.isInitialView,
                    elementSelector: this.initialMainViewElement,
                    url: e
                }, r))), this.isInitialView = !1
            }
        })
    }), define("consoleMonkeyPatch", [], function() {
        return function(e) {
            var t, n = function() {},
                r = ["assert", "clear", "count", "debug", "dir", "dirxml", "error", "exception", "group", "groupCollapsed", "groupEnd", "info", "log", "markTimeline", "profile", "profileEnd", "table", "time", "timeEnd", "timeStamp", "trace", "warn"],
                i = r.length,
                s = window.console = window.console || {};
            while (i--) {
                t = r[i];
                if (!s[t] || !e) s[t] = n
            }
        }
    });
var BIA = window.BIA || {};
require.config({
    paths: {
        jquery: "vendor/jquery/jquery-2.1.3.min",
        underscore: "vendor/underscore/underscore",
        backbone: "vendor/backbone/backbone",
        marionette: "vendor/backbone-marionette/backbone.marionette",
        "backbone.babysitter": "vendor/backbone-marionette/backbone.babysitter",
        "backbone.wreqr": "vendor/backbone-marionette/backbone.wreqr",
        text: "vendor/requirejs/text",
        async: "vendor/requirejs/async",
        imagesloaded: "vendor/imagesloaded/imagesloaded.pkgd.min",
        rAF: "vendor/polyfills/rAF",
        mousewheel: "vendor/jquery-mousewheel/jquery.mousewheel",
        swiper: "vendor/swiper/dist/js/swiper.jquery",
        isMobile: "vendor/ismobile/detectmobilebrowser",
        modernizr: "vendor/modernizr/modernizr",
        "bootstrap.tabs": "vendor/bootstrap/tab",
        "bootstrap.collapse": "vendor/bootstrap/collapse",
        "bootstrap.transition": "vendor/bootstrap/transition",
        tweenmax: "vendor/tweenmax/TweenMax.min",
        tweenmaxscrollto: "vendor/tweenmax/ScrollToPlugin.min",
        matchMedia: "vendor/polyfills/matchMedia",
        matchMediaListener: "vendor/polyfills/matchMedia.addListener",
        consoleMonkeyPatch: "vendor/console-monkey-patch"
    },
    shim: {
        underscore: {
            exports: "_"
        },
        mousewheel: {
            deps: ["jquery"]
        },
        tweenmax: {
            exports: "TweenMax",
            deps: ["jquery"]
        },
        timelinemax: {
            exports: "TimelineMax",
            deps: ["tweenmax"]
        },
        "bootstrap.transition": {
            deps: ["jquery"]
        },
        "bootstrap.tabs": {
            deps: ["bootstrap.transition"]
        },
        "bootstrap.collapse": {
            deps: ["bootstrap.transition"]
        },
        matchMediaListener: {
            deps: ["matchMedia"]
        },
        modernizr: {
            exports: "Modernizr"
        }
    },
    deps: ["modernizr", "jquery", "underscore", "backbone", "marionette", "bootstrap.transition", "matchMedia", "matchMediaListener", "rAF"]
}), require(["app", "routers/Router", "controllers/Controller", "consoleMonkeyPatch"], function(e, t, n, r) {
    r(BIA.IS_DEV), e.appRouter = new t({
        controller: new n
    })
}), define("main", function() {});


