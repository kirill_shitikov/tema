//jQuery(function($){
	/*jQuery(document).ready(function(){
		if ( jQuery(window).width() > 959 ) {
			jQuery('#posts-carousel-wrap .bxslider').bxSlider({
				responsive: false,
				minSlides: 3,
				maxSlides: 3,
				moveSlides: 3,
				slideWidth: 300,
				slideMargin: 20,
				pager: false,
				auto: true,
				adaptiveHeight: false,
				onSliderLoad: function(){
					alert("pass");
				  jQuery('#posts-carousel-wrap').removeClass('bx-loading');
				  jQuery('#posts-carousel-wrap li').equalHeights();
				}
			});
		} else {
			jQuery('#posts-carousel-wrap').hide();
		}

	});*/
//});
jQuery(function($){
	$(document).ready(function(){
		//alert($(window).width());
		if ( $(window).width() > 1025 ) {
			$('#posts-carousel-wrap .bxslider').bxSlider({
				responsive: true,
				minSlides: 3,
				maxSlides: 3,
				moveSlides: 3,
				slideWidth: 400,
				slideMargin: 20,
				pager: false,
				auto: true,
				adaptiveHeight: false,
				onSliderLoad: function(){					
				  $('#posts-carousel-wrap').removeClass('bx-loading');
				  $('#posts-carousel-wrap li').equalHeights();
				  
				}
			});
		} else if ( $(window).width() < 481 ) {
			
			$('#posts-carousel-wrap .bxslider').bxSlider({
				responsive: true,
				minSlides: 1,
				maxSlides: 1,
				moveSlides: 1,
				slideWidth: 400,
				//autoWidth:true,
				slideMargin: 20,
				pager: false,
				auto: true,
				adaptiveHeight: true,
				onSliderLoad: function(){
				  $('#posts-carousel-wrap').removeClass('bx-loading');
				  $('#posts-carousel-wrap li').equalHeights();
				}
			});
		}else if ( $(window).width() < 1024 ) {
			
			$('#posts-carousel-wrap .bxslider').bxSlider({
				responsive: true,
				minSlides: 1,
				maxSlides: 1,
				moveSlides: 1,
				slideWidth: 400,
				//autoWidth:true,
				slideMargin: 20,
				pager: false,
				auto: true,
				adaptiveHeight: true,
				onSliderLoad: function(){
				  $('#posts-carousel-wrap').removeClass('bx-loading');
				  $('#posts-carousel-wrap li').equalHeights();
				}
			});
		}
		
		else {
			$('#posts-carousel-wrap').hide();
		}
		

	});
});

jQuery(function($){
	$(document).ready(function(){
		
		if ( $(window).width() > 1025 ) {
			$('#posts-carousel-wrap2 .bxslider').bxSlider({
				responsive: false,
				minSlides: 1,
				maxSlides: 1,
				moveSlides: 1,
				slideWidth: 1280,
				slideMargin: 20,
				pager: false,
				auto: true,
				adaptiveHeight: false,
				onSliderLoad: function(){
				  $('#posts-carousel-wrap2').removeClass('bx-loading');
				  $('#posts-carousel-wrap2 li').equalHeights();
				}
			});
			
		}else if ( $(window).width() < 481 ) {
			
			$('#posts-carousel-wrap2 .bxslider').bxSlider({
				responsive: true,
				minSlides: 1,
				maxSlides: 1,
				moveSlides: 1,
				slideWidth: 1280,
				//autoWidth:true,
				slideMargin: 20,
				pager: false,
				auto: true,
				adaptiveHeight: true,
				onSliderLoad: function(){
				  $('#posts-carousel-wrap2').removeClass('bx-loading');
				  $('#posts-carousel-wrap2 li').equalHeights();
				}
			});
		}else if ( $(window).width() < 1024 ) {
			
			$('#posts-carousel-wrap2 .bxslider').bxSlider({
				responsive: true,
				minSlides: 1,
				maxSlides: 1,
				moveSlides: 1,
				slideWidth: 1280,
				//autoWidth:true,
				slideMargin: 20,
				pager: false,
				auto: true,
				adaptiveHeight: true,
				onSliderLoad: function(){
				  $('#posts-carousel-wrap2').removeClass('bx-loading');
				  $('#posts-carousel-wrap2 li').equalHeights();
				}
			});
		}
		
		else {
			$('#posts-carousel-wrap2').hide();
		}

	});
});