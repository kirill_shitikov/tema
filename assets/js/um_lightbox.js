jQuery(document).ready(function($){
	"use strict";
	
	$('body').on('click','.um_expand_lightbox',function(e){
		e.preventDefault();
		
		$('.um_shadow').addClass('active');	
		
		var postId = $(this).data('postid');
		
		var ajaxData = {
			"action" : "um_lightbox",
			"post_id" : postId
		}; 
		$.post(ajax_url, ajaxData, function(data){
			if(data){
				$('body').append(data);
				
				$(".roomsWideSlider").royalSlider({
			        keyboardNavEnabled: true, 
			        imageScaleMode: 'fill', 
			        controlNavigation: 'none', 
			        arrowsNavAutoHide: false, 
			        navigateByClick : false,
			        transitionType: "fade"
			    });	
			    um_videos(false); 
			    
			    $('.um_shadow').removeClass('active');	
			}
		});

	});
	
	$('.closeLightbox').live('click',function(e){	
		var slider = $(".roomsWideSlider").data('royalSlider');
		slider.destroy();
		$('.um_ex_lightbox').remove();
		
	});
});
