// Rules for acf 
jQuery(document).live('acf/setup_fields', function(e, div){
	var $ = jQuery;
	
	(function(){
		
		$('#acf-slider tr.row').each(function(i){
			
			if($(this).find("[data-field_name='type']").find('li:first').find('input').attr('checked')){
				$(this).find("[data-field_name='image']").closest('tr').show();
				$(this).find("[data-field_name='image_overlay']").closest('tr').show();
				$(this).find("[data-field_name='overlay_percentage']").closest('tr').show();
				
				$(this).find("[data-field_name='video_type']").closest('tr').hide();
				$(this).find("[data-field_name='video']").closest('tr').hide();
				$(this).find("[data-field_name='video_poster']").closest('tr').hide();
				$(this).find("[data-field_name='video_url']").closest('tr').hide();
			}
			else{
				
				$(this).find("[data-field_name='image']").closest('tr').hide();
				$(this).find("[data-field_name='image_overlay']").closest('tr').hide();
				$(this).find("[data-field_name='overlay_percentage']").closest('tr').hide();
				
				$(this).find("[data-field_name='video_type']").closest('tr').show();
				$(this).find("[data-field_name='video_poster']").closest('tr').show();
				if($(this).find("[data-field_name='video_type']").find('li:first').find('input').attr('checked')){
					$(this).find("[data-field_name='video_url']").closest('tr').hide();
					$(this).find("[data-field_name='video']").closest('tr').show();
				}
				else{
					$(this).find("[data-field_name='video_url']").closest('tr').show();
					$(this).find("[data-field_name='video']").closest('tr').hide();
				}
			}
		});
		
		$('#acf-um_fields tr.row').each(function(i){
			
			$(this).find("[data-field_name='unique_id']").closest('tr').hide();
				
				var selectValue = $(this).find("[data-field_name='field_type']").find('select').val();
				if( selectValue == 'drop' || selectValue == 'radio' || selectValue == 'check'){
					
					$(this).find("[data-field_name='mendatory']").closest('tr').hide();
					
					$(this).find("[data-field_name='values']").closest('tr').show();
				}
				else{
					$(this).find("[data-field_name='values']").closest('tr').hide();
					$(this).find("[data-field_name='mendatory']").closest('tr').show();
				}
				
				if(selectValue == 'email' || selectValue == 'number' ){
					
					$(this).find("[data-field_name='unique_id']").closest('tr').show();
				}
		});

	})();

});

jQuery(document).ready(function($){
	
	
	//on Click change ruls for acf
	
	$("[data-field_name='type']").find('li').find('input').live('click',function(){
		if($(this).attr('value') == 'image'){
	
			$(this).closest('table').find("[data-field_name='image']").closest('tr').show();
			$(this).closest('table').find("[data-field_name='image_overlay']").closest('tr').show();
			$(this).closest('table').find("[data-field_name='overlay_percentage']").closest('tr').show();
		
			$(this).closest('table').find("[data-field_name='video_type']").closest('tr').hide();
			$(this).closest('table').find("[data-field_name='video']").closest('tr').hide();
			$(this).closest('table').find("[data-field_name='video_url']").closest('tr').hide();
			$(this).closest('table').find("[data-field_name='video_poster']").closest('tr').hide();
		}
		else{
			
			$(this).closest('table').find("[data-field_name='image']").closest('tr').hide();
			$(this).closest('table').find("[data-field_name='image_overlay']").closest('tr').hide();
			$(this).closest('table').find("[data-field_name='overlay_percentage']").closest('tr').hide();
			
			$(this).closest('table').find("[data-field_name='video_type']").closest('tr').show();
			$(this).closest('table').find("[data-field_name='video_poster']").closest('tr').show();
			if($(this).closest('table').find("[data-field_name='video_type']").find('li:first').find('input').attr('checked')){
				$(this).closest('table').find("[data-field_name='video']").closest('tr').show();
				$(this).closest('table').find("[data-field_name='video_url']").closest('tr').hide();
			}else{
				$(this).closest('table').find("[data-field_name='video']").closest('tr').hide();
				$(this).closest('table').find("[data-field_name='video_url']").closest('tr').show();
			}	
		}
		
		
		$("[data-field_name='video_type']").find('li').find('input').live('click',function(){
			
			if($(this).attr('value') == 'selfhost'){
				$(this).closest('table').find("[data-field_name='video']").closest('tr').show();
				$(this).closest('table').find("[data-field_name='video_url']").closest('tr').hide();
			}
			else{
				$(this).closest('table').find("[data-field_name='video']").closest('tr').hide();
				$(this).closest('table').find("[data-field_name='video_url']").closest('tr').show();
			}
		});	
	});
	
	
	$("[data-field_name='field_type']").find('select').live('change',function(){
		
		$(this).closest('table').find("[data-field_name='unique_id']").closest('tr').hide();
		
		var selectedValue = $(this).val();
		
		if( selectedValue == 'drop' || selectedValue == 'radio' || selectedValue == 'check'){
				$(this).closest('table').find("[data-field_name='values']").closest('tr').show();
				$(this).closest('table').find("[data-field_name='mendatory']").closest('tr').hide();
		}else{
			$(this).closest('table').find("[data-field_name='mendatory']").closest('tr').show();
			$(this).closest('table').find("[data-field_name='values']").closest('tr').hide();
		}
		if(selectedValue == 'email' || selectedValue == 'number' ){
			$(this).closest('table').find("[data-field_name='unique_id']").closest('tr').show();
		}
	});
	
	
	
});