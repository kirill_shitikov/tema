
jQuery(window).load(function(){
	jQuery('.um_shadow').removeClass('active');	
});   

jQuery(document).ready(function($) {
	"use strict";
	
   /*WooCommerce Cart Count*/
  	$("body").on("added_to_cart",function(){
		/*Refresh Cart Totals*/
		$.post(ajax_url,{ action : "um_cart_total" } , function(data){
			$("span.cart_totl").text(data);
		});
		/*Refresh Cart Totals*/
	});  
     
/* RoyalSlider*/   

	/* Home page and room wide slider */
	
	function royalAutoPlay(){
			 $(".homeSlider, .roomsWideSlider").royalSlider({
		        keyboardNavEnabled: true, 
		        imageScaleMode: 'fill', 
		        controlNavigation: 'none', 
		        arrowsNavAutoHide: false,
		        navigateByClick : false, 
		        transitionType: "fade",
		        autoPlay: {
					enabled: true,
					pauseOnHover: true,
					delay :	9000
				}
		    });
	}
	function royalNone(){
		
		$(".homeSlider, .roomsWideSlider").royalSlider({
	        keyboardNavEnabled: true, 
	        imageScaleMode: 'fill', 
	        controlNavigation: 'none', 
	        arrowsNavAutoHide: false, 
	        navigateByClick : false,
	        transitionType: "fade" /*, 
			autoScaleSlider: true, 
			autoScaleSliderWidth: 800, 
			autoScaleSliderHeight:400 */
	    });	
	}
	
	if(typeof autoPlayHomeSlider === "undefined"){
		royalNone();
	}
	else{
		if(autoPlayHomeSlider){
			royalAutoPlay();
		}
		else{
			royalNone();
		}
		
	}
	

		
		// homePage slider custom arrows
		var homeSlider = jQuery(".homeSlider").data('royalSlider');
		
		$('#umLeft').on('click', function(){
			homeSlider.prev();
		});
		$('#umRight').on('click', function(){
			homeSlider.next();
		});
	    
	    // check slider custom arrows if they need extra classes
		
		if(($('.homeSlider').length > 0) && ($('.um_cusotmArrows').length > 0)) {
			homeSlider.ev.on('rsAfterSlideChange', function(event) {
				addArrowClasses();
			});
				
			homeSlider.ev.on('rsAfterContentSet', function(e, slideObject) {
	  			addArrowClasses();
			});
		}
			
	   function addArrowClasses(){
			var lclass = $('.rsArrowLeft').attr('class');
			var rclass = $('.rsArrowRight').attr('class');
			
			$('.um_cusotmArrows .rsArrow').removeClass('rsArrowDisabled');
			
			$('.um_cusotmArrows #umLeft').addClass(lclass);
			$('.um_cusotmArrows #umRight').addClass(rclass);
	   }
	    
	    
    /* Home page and room wide slider END */
   
   /* single room */
  
  			
  		 $(".roomsSlider").royalSlider({
	        fullscreen: {
		      enabled: true,
		      nativeFS: false
		    },
		    
		    imageScaleMode: 'fill', 
		    controlNavigation: 'thumbnails',
		    autoScaleSlider: true, 
		    autoScaleSliderWidth: 740,     
		    autoScaleSliderHeight: 510,
		    loop: false,
		    transitionType: "fade",
		    navigateByClick: true,
		    arrowsNav:true,
		    arrowsNavAutoHide: true,
		    arrowsNavHideOnTouch: true,
		    keyboardNavEnabled: true,
		    fadeinLoadedSlide: true,
		    globalCaption: false,
		    globalCaptionInside: false,
		    thumbs: {
		      appendSpan: true,
		      firstMargin: true,
		      paddingTop: 5, 
		      spacing: 10
		    }
	    });
	    
	    
	    if ($(".roomsSlider").length >= 1) { 	    
			var slider = $(".roomsSlider").data('royalSlider');
  			slider.ev.on('rsEnterFullscreen', function() {
				$(".roomsSlider").hide();
			    setTimeout(function(){
						$(".roomsSlider").show();
				}, 200);
			});
			
		    slider.ev.on('rsEnterFullscreen', function() {
			    $('header#siteHeader, footer#siteFooter').css('z-index', '-1');
			});
			
			slider.ev.on('rsExitFullscreen', function() {
			     $('header#siteHeader, footer#siteFooter').css('z-index', '3');
			});
			
		}
  
   /* single room END */
   
   
/* Royal Slider END*/


	if($(".homeSlider, .roomsWideSlider, .roomsSlider").length > 0){  
		if(typeof autoPlayVideo === "undefined"){
			um_videos(false);    
		}
		else{
			um_videos(autoPlayVideo);
		}
	}


/* Parallax */

    jQuery('.um_parallax').scrolly({
        bgParallax: true
    });

/* Parallax END */
    
	

 /* Rooms Filter */
    $('.selectpicker').selectpicker();
 /* Rooms Filter END */    
     
  
/* initialize Isotope */   
    
    
  		$('.footerMasonry.um_masonry').isotope({
			   itemSelector: '.um_masonryItem',
                 masonry: {
				    columnWidth: '.um_masonryItem'
				  }
		});
     
     
     function loadIsotope($container, $colmnWidth){
     		     	
     		 // initialize Isotope
     		var $_container = $container.isotope({
			   itemSelector: '.um_masonryItem',
                 masonry: {
				    columnWidth: $colmnWidth
				  }
		});
     	
 			// layout Isotope again after all images have loaded
				$_container.imagesLoaded( function() {
	  	 		$_container.isotope('layout');
	  	
			
				if($('.roomsWidgetSlider').length >= 1){
							
					$('.roomsWidgetSlider').owlCarousel({ 
						items: 1, 
						navigation: true,
						singleItem:true,
						navigationText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
					});
				}
		
		});	
     }
      
     loadIsotope($('.articles.um_masonry'), '.um_masonryItem');  			//Specials
     loadIsotope($('.masontyContainer'), 1);					 			//Attractions
     loadIsotope($('.blogPage .masontyContainer'), '.um_masonryItem');		//Blog
     loadIsotope($('.menuContent.um_masonry'), '.um_masonryItem'); 			//Menu
     loadIsotope($('.aboutContent.um_masonry'), '.um_masonryItem'); 		//About Us
     loadIsotope($('.homeWidget_masonry.um_masonry'), '.um_masonryItem'); 	//Home Widgets

/*initialize Isotope  END*/        
 
 

/* Home Owl carousel */
	$('.roomsHomeSlider').owlCarousel({
		items : 3,
		itemsCustom: [ [993, 3], [768, 2], [0, 1] ],
		navigation: true,
		navigationText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
	});
	 	  
 /* Home Owl carousel END */ 
 
 /*Isotope Menu Filter*/

	$('.um_menu_filter').on('click',function(e){
		$containerMenu = $('.menuContent.um_masonry');
		e.preventDefault();
		var um_menu_class = '';
		if(!$(this).hasClass('all')){	
			um_menu_class = '.' + $(this).text();
		}
		$containerMenu.isotope({ filter: um_menu_class })
	});
	
 /*Isotope Menu Filter END*/
          


/*Load More Blog*/


	$('.loadMoreBlog').on('click',function(e){
		
		$containerBlog = $('.blogPage .masontyContainer');
		e.preventDefault();
		
		$(this).addClass('isLoading');
		pageBlog++;
		var ajax_data = {
				paged : pageBlog,
				um_page : true
			
		};
		
		$.post(document.URL, ajax_data, function(data){
			if(data){
			
				$(".loadMoreBlog").removeClass('isLoading');
	
					$(data).imagesLoaded(function(){
						$containerBlog.isotope( 'insert', $(data) );
		  				$containerBlog.isotope('layout');
							if(lastPageBlog == pageBlog ){
								$(".loadMoreBlog").hide();
							}
					});
				}
			}).error(function(){
                $(".loadMore").hide();
        	});
		});

/*Load More Blog END*/


             
/* Ajax Contact Form*/  
 
  $("body").on("submit","form.contactForm",function(e){
		 
		   e.preventDefault();
		   
			$('.error').removeClass('error');
			
            var name = $(this).find("#full_name");
            var email = $(this).find("#email");
            var message = $(this).find("#comment");
          
            var url = document.URL;

            var return_state = true;
            var form = $(this);

            if(name.val() == ""){
                name.addClass("error");
                return_state = false;
            }
            if(email.val() == "" || !validateEmail(email.val())){
                email.addClass("error");
                return_state = false;
            }
            if(message.val() == ""){
                message.addClass("error");
                return_state = false;
            }

            
			if(return_state){
					 var data = {
						 um_name : name.val(),
						 um_email : email.val(),
						 um_message : message.val()
					}
				
					$.post(url,data,function(data){

						form.fadeOut("normal",function(){
							$('.um_message h1').html(data).parent().fadeIn("normal");
						});
					});
			}
			return false;
		});
		
        
/* Ajax Contact Form END*/         
});



//Adds support for HTML5 Videos
//Adds support for auto-play videos 
function um_videos(autoPlayVideo){	
	var $ = jQuery;
	var slider = $(".homeSlider, .roomsWideSlider, .roomsSlider").data('royalSlider');
	function autoPlay(){
			if(autoPlayVideo){
				if(slider.currSlide.videoURL != "undefined" ){
					setTimeout(function(){
						slider.playVideo();
					}, 1000);
				}
				if(slider.currSlide.images == null){			
					setTimeout(function(){
							$(slider.currSlide.content).find('video').get(0).play();
							$('.selfPlay').addClass('um_hidden');
						},1000);
				}
				$('.noCover').find('video').removeAttr('poster');	
			}
			$("video").each(function(){
			if(!$(this).get(0).paused)
				{
					$(this).get(0).pause();
					$('.selfPlay').removeClass('um_hidden');
				}
			});	
	}
	autoPlay();
	slider.ev.on('rsAfterSlideChange', function(event) {
		autoPlay();
	});
	
	$('.selfPlay').live('click',function(){
		var video =	$(this).parent().find('video');
			if($(video).get(0).paused){
				$(video).get(0).play();
				$(this).addClass('um_hidden');
			}
	});
	
	$('video').live('click',function(){
		if(!$(this).get(0).paused)	
		{
			$(this).get(0).pause();
			$('.selfPlay').removeClass('um_hidden');
		}
	});
	
	$("video").each(function(){
		$(this).get(0).onended = function(e) { 
			$('.selfPlay').removeClass('um_hidden');
		}
	});
}


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function validatePhone(phone){
	var re = /^[+#*\(\)\[\]]*([0-9][ ext+-pw#*\(\)\[\]]*){6,45}$/;
	 return re.test(phone);
}

function validateNumbers(num){
	
	var re = /^(0|[1-9][0-9]*)$/;
	return re.test(num);
}
