$(document).ready(function()
{ 
	$('body').js();
	
});

$.fn.js = function () 
{ 
	$(this).find('[data-js]').add(this).each(function()
	{
		var $e = $(this);
		if ($e.attr('data-js'))
		$.each($e.attr('data-js').split(' '), function(i, item)
		{
			if (typeof $.fn[item] == 'function') 
				$e[item](); 
			else 
				if (console) 
					console.log('JS: $.fn.'+item+'() not found');
		});
	}); 
	return this;
}
//

$.fn.jsFooter = function () 
{
	this.each(function()
	{
		$(this).find('[class=col] small').on('click', function()
		{
			$(this).toggleClass('opened');
			$(this).find('~div').slideToggle(300);
		})
	});
	return this;
}

$.fn.jsCarousel = function(item_selector)
{
	var item_selector = item_selector || '>*:not(span)';
	this.each(function()
	{
		var $this = $(this);
		var $items = $this.find(item_selector);
		var $first = $items.eq(0);
		var style = $first.attr('style');
		$first.attr('style', '');
		var width = $first.outerWidth(true)*($this.attr('data-count')?parseInt($this.attr('data-count')):1);
		var width_all = width*$items.length;
		var margin = parseInt($first.css('margin-left'));
		$first.attr('style', style);
		var slide = function()
		{
			if ($items.eq(0).is(':animated')) return;
			var diff = $(this).hasClass('left')?-1:1;
			var next = Math.ceil(-(parseInt($first.css('margin-left'))+margin)/width) + diff;
			if (next==-1) return $first.animate({'margin-left': parseInt($first.css('margin-left'))+30}, 50 , function() { $(this).animate({'margin-left': parseInt($first.css('margin-left'))-30}, 100);});
			if (next>$items.length-Math.floor($this.width()/width)) return $first.animate({'margin-left': parseInt($first.css('margin-left'))-30}, 50 , function() { $(this).animate({'margin-left': parseInt($first.css('margin-left'))+30}, 100);});;
			$first.stop().animate({'margin-left': -Math.min(next*width-margin, width_all - $this.width())}, Math.min(width*1.5, 300));
			$(window).on('resize', function() { console.log($this.width()); $first.css('margin-left', -Math.min(-parseInt($first.css('margin-left')),  width_all - $this.width()));});
		}
		$this.parent().on('click','.left, .right', slide).on('mousedown', false).on('mouseup', false);
	});
	return this;
}

$.fn.jsSlider = function ()
{
	this.each(function()
	{
		var $e = $(this);
		var $left = $e.find('.left');
		var $right = $e.find('.right');
		var $slides = $e.find('>article');
		var $dots = $e.find('.dots span');
		var count = $slides.length;
		var current = 0;
		var moving = false;
		var timer = false;
		var move = function (diff)
		{
			if (moving==true) return false;
			clearTimeout(timer);
			var prev = current;
			current = (current+diff+count)%count;
			moving = true;
			$slides.eq(current).css({'left': (diff==1)?$e.outerWidth():-$e.outerWidth(), 'display': 'block'}).animate({'left': 0}, 300);
			$slides.eq(prev).animate({'left': (diff==1)?-$e.outerWidth():$e.outerWidth()} , 300, function() { $(this).hide(); moving = false;});
			//
			$slides.eq(current).find('header').css({'left': (diff==1)?$e.outerWidth()/4:-$e.outerWidth()/4}).animate({'left': 0}, 1000, 'easeOutBack');
			$dots.removeClass('current').eq(current).addClass('current');
			timer = setTimeout(function() { move(1);}, 8000);
		}
		$left.click(function() { move(-1); return false;});
		$right.click(function() { move(1); return false;});
		setTimeout(function() { move(1);}, 8000);
	})
	return this;
}

$.fn.jsMenu = function ()
{
	this.each(function()
	{
		var $e = $(this);
		$e.find('button').on('click', function()
		{
			 $e.find('nav').slideToggle(300);
		});
		$e.find('a').on('click', function() { $e.find('nav').slideUp(300);});
		$('body>*:not(header)').on('touchstart', function() { $e.find('nav').slideUp(300);});
	})
	return this;
}

$.fn.jsSubscribe = function()
{
	this.each(function() 
	{ 
		var $form = $(this);
		$form.on('focus', '.error', function() { $(this).removeClass('error');});
		var $button = $form.find('button');
		$form.ajaxForm(
		{
			beforeSubmit: function()
			{
				$form.find('input, textarea').filter(function(){ return $(this).val() == '';}).addClass('error');
				if ($form.find('.error').length) return false;
				$button.attr('disabled', true);
			},
			success: function(data)
			{
				$form.resetForm();
				$button.attr('disabled', false);
				alert('Subscribed');
			}
		});
	});
	return this;
}
 