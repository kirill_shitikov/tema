(function($) {
   
   
    $.fn.twitterfeed = function(options) {
        var startPoint = options;

        options = $.extend({}, $.fn.twitterfeed.defaultOptions, options);
        var main_element = $(this);
        var tweet_sample = $(this).find("div.tweet_sample");

    	var livetweeter = function(container){
            var twitter_search = $(container).attr("twitter_search");

            var counter = startPoint;
            var container = $(container);

            container.children('div.tweet').eq(counter).css('display','block');
            container.children('div.tweet').eq(counter + 1).css('display','block');
            
            slide();

            function slide()
            { 	
            	counter += 2;
            	
            	 if(counter > container.find("div.tweet").length){
            		counter = 0; 	
            	 }
            		 
                var next_tweet = container.find("div.tweet").eq(counter);
                if(next_tweet[0])
                {
                    window.setTimeout(function(){
                        container.find("div.tweet:visible").fadeOut("normal",function(){
                            next_tweet.fadeIn("normal");
                            
                          				var next_tweet2 = container.find("div.tweet").eq(counter - 1);
                           				if(next_tweet2[0]){
			                            	next_tweet2.fadeIn("normal");			                           
	                        			}		
	                       });
                         slide();
	                         
                    },6000);
                }
                else
                {
                    window.setTimeout(function(){
                        slide();
                    },6000);
                }
            }
        };

        main_element.each(function(){
            var twitter_search = $(this).attr("twitter_search");
            var twitterfedd = new livetweeter($(this));
        });
    };


})(jQuery);




