jQuery(document).ready(function(){
	///MENU 
	jQuery(window).bind('scroll', function () {
		//jQuery('.sectionPaddingTopBottom').css('padding-top', '0px');
		if (jQuery('.topsimpleheader2').hasClass('showSimpleTopHeader')){
			jQuery('.sectionPaddingTopBottom').removeClass('sectionPaddingTopBottomResponsive');
		}else{
			jQuery('.sectionPaddingTopBottom').addClass('sectionPaddingTopBottomResponsive');
		}
		
		if (jQuery(window).scrollTop() > 50) {
			jQuery('.topclientheader2').addClass('hideTopHeader');
			jQuery('.topsimpleheader2').removeClass('hideSimpleTopHeader');
			jQuery('.topsimpleheader2').addClass('showSimpleTopHeader');
			jQuery('.sectionPaddingTopBottom').css('padding-top', '0px');
		}else{
			jQuery('.topclientheader2').removeClass('hideTopHeader');
			jQuery('.topsimpleheader2').addClass('hideSimpleTopHeader');
			jQuery('.topsimpleheader2').removeClass('showSimpleTopHeader');
			jQuery('.sectionPaddingTopBottom').css('padding-top', '0px');
			
		}
		
	});
	
	
	
	//Back To Top
	jQuery('#backTop').backTop({
		'position' : 200, //1600
		'speed' : 500,
		'color' : 'black',
	});
	
	//Add name to IDX Quick Search
	//jQuery('#ihf-area-input').attr("name", "location");
	
	jsMap();
	//jQuery.scrollSpeed(0, 800);
	//alert("here");
	//STARTING MENU ACTIVE CONFIG
	//Make Sure Link is
	var full = window.location.hostname;
	var fUrl = window.location.pathname.split("/");
	var chkurl2 = fUrl[1];
	var chkurl3 = fUrl[2];
	
	//var chkurl2 = fUrl[2];//localhost
	//var chkurl3 = fUrl[3];//localhost
	//alert(chkurl3);
	var win = window.location.href;
	
	var urlElement = jQuery('#urlElement').val(); //URL ELEMENT
	var site = jQuery('#siteUrl').val();//add / at the end
	var fSite = site+"/";
	
	
	//alert(fUrl[1]);
	if(fSite == win){
		if(urlElement == full){//Admin (Localhost & Live)
			var finalUrl = "http://"+full+"/"+fUrl[1]+"home/"; 
			
		}else if(urlElement == chkurl2){//Agent (Live)
			var finalUrl = "http://"+full+"/"+fUrl[1]+"/home/";
			
		}else if(urlElement == fUrl[2]){//Agent (Localhost)
			var finalUrl = "http://"+full+"/"+fUrl[1]+"/"+fUrl[2]+"/home/"; 			
		}
		
		/*setTimeout(function(){
			jQuery("body").removeClass("sidebar-open"),
			jQuery("#container").addClass("show-intro app-started"),
			jQuery(".intro-text").removeAttr("style"),
			jQuery(".intro-text").css("opacity", "1"),			
			jQuery(".intro-scroll").removeAttr("style"),
			jQuery(".intro-scroll").css("opacity", "1"),
			
			jQuery(".nav-sidebar").addClass("remove-sidebar");
		}, 4000);*/
		
	}else if(fUrl[1] == 'homes-for-sale-results' || fUrl[1] == 'homes-for-sale-search-advanced'){//Admin (Live)
		var finalUrl = "http://"+full+"/property-search/";
		
	}
	else if(fUrl[2] == 'homes-for-sale-results' || fUrl[2] == 'homes-for-sale-search-advanced'){//Admin (Localhost) & Agent (Live)
		var finalUrl = "http://"+full+"/"+fUrl[1]+"/property-search/";
	
	}else if(fUrl[3] == 'homes-for-sale-results' || fUrl[3] == 'homes-for-sale-search-advanced'){//Agent (Localhost)
		var finalUrl = "http://"+full+"/"+fUrl[1]+"/"+fUrl[2]+"/property-search/";
	}
	else{
		//alert(urlElement);
		//alert(fUrl[2]);
		if(urlElement == full){//Admin (Localhost & Live)
			var finalUrl = "http://"+full+"/"+fUrl[1]+"/"+fUrl[2];	
					
		}else if(urlElement == chkurl2){//Admin (Localhost)
			var finalUrl = "http://"+full+"/"+fUrl[1]+"/"+fUrl[2]+"/";
			
		}else if(urlElement == fUrl[2]){//Agent (Localhost)
			var finalUrl = "http://"+full+"/"+fUrl[1]+"/"+fUrl[2]+"/"+fUrl[3]+"/";			
		}

	}
	
	var urlname = location.pathname;
	var spliturl = urlname.split('/');
	var chkurl = spliturl[1];
	var chkurl = spliturl[2]; //localhost
	
	//if(chkurl == chkurl2){
	//alert(finalUrl);
	jQuery("a[href='" + finalUrl + "']").addClass('menuactive');
	//}
	
	//ENDING OF MENU ACTIVE CONFIG
		if ( jQuery(window).width() > 1024) {
			jQuery("#navSiteBar").addClass('menuDisplay');
		} else {
			jQuery("#navSiteBar").addClass('menuDisplay nav-sidebar hideMenuCustom');
			jQuery('.site-logo').addClass('hideLogoCustom');
		}

	//jQuery(".el").addClass('elimg');
	//jQuery(".elimg").css('height', '65%');
	jQuery(".content-page").css({ opacity: 1});
	
	//jQuery('a[data-postname=Contact]').attr('id', 'goMail');
	
	//Scrolling to Div
	jQuery('.goMail, #learnMore').click(function() {
		
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		
	     var target = jQuery(this.hash);
		 
	      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
	       if (target.length) {
	        jQuery('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	jQuery("#movetomail, #movetophone, #movetomailsmall, #movetophonesmall").click(
		function(e){
			if(device.mobile()){
				console.log('mobile');
			}else{
				console.log('other');
				jQuery("body").scrollTo("#mailSections", 1000);
				console.log(e.currentTarget);
				e.preventDefault();
			}
		}
	);
	  
	 //Properties
	 jQuery(".col-xs-10 a, .back-to-results a, .col-xs-5 a, .ihf-results-grid-photo a, .ihf-grid-result-address a").click(function(e){
		var pageurl = jQuery(this).attr('href');
		//alert(pageurl);
		var pagename = window.location.pathname.split("/");
		var pname = pagename[2]; //Localhost
		//var pname = pagename[1]; //Live
		
		//alert('pname: '+pname);
		
		var paUrl = pageurl.split("/");
		var purl = paUrl[4]; //Localhost
		//var purl = paUrl[3]; //Live
		
		//alert('PURL: '+purl);
		
		
		if(pname == 'about-me'){
			//jQuery('#soldl').load(pageurl);
			jQuery('#soldl').load(pageurl, function(){
				jQuery('#loadingFunction').addClass('loadingHide');
				jQuery('#loadingFunction').removeClass('loadingShow');
			});
		}
		if(pname == 'homes-for-sale-details'){
			//jQuery('#featContent').load(pageurl);
			jQuery('#featContent').load(pageurl, function(){
				jQuery('#loadingFunction').addClass('loadingHide');
				jQuery('#loadingFunction').removeClass('loadingShow');
				jQuery('.homefeature').addClass('hideFeatureListing');
				
			});
		}
		if(pname == 'featured-listings'){
			//jQuery('#featContent').load(pageurl);
			jQuery('#featContent').load(pageurl, function(){				
				jQuery('#loadingFunction').addClass('loadingHide');
				jQuery('#loadingFunction').removeClass('loadingShow');
			});
		}
		
		
		if(pname == 'homes-for-sale-results' && purl == 'homes-for-sale-details'){
			jQuery('.normalProSearch').css('display', 'none');
			jQuery('.resultSearch').css('display', 'block');
			//jQuery('#prosContent').load(pageurl);
			jQuery('#prosContent').load(pageurl, function(){
				jQuery('#loadingFunction').addClass('loadingHide');
				jQuery('#loadingFunction').removeClass('loadingShow');
				
			});
		}
		
		if(pname == 'home' && purl == 'homes-for-sale-details'){
			//jQuery('#soldl').load(pageurl);
			jQuery('#featContentHome').load(pageurl, function(){
				jQuery('#loadingFunction').addClass('loadingHide');
				jQuery('#loadingFunction').removeClass('loadingShow');
				jQuery('.homefeature').removeClass('hideFeatureListing');
				jQuery('.featuredFrame').addClass('hideFeatureListing');
				jQuery(".quickSection").load(location.href + " .quickBg");
			});
		}
		if(pname == 'home' && purl == 'featured-listings-details'){
			jQuery('#featContentHome').load(pageurl, function(){
				jQuery('#loadingFunction').addClass('loadingHide');
				jQuery('#loadingFunction').removeClass('loadingShow');
				jQuery('.homefeature').removeClass('hideFeatureListing');
				jQuery('.featuredFrame').addClass('hideFeatureListing');
				jQuery(".quickSection").load(location.href + " .quickBg");
			});
		}
		
		if(pname == 'homes-for-sale-results' && purl == 'property-search'){
			var pageurl = jQuery(this).attr('href');
			jQuery.ajax({url:pageurl, success: function(data){
			jQuery(location).attr('href', pageurl);
			jQuery(".nav-sidebar nav ul li a").removeClass('menuactive');
			jQuery("a[href='" + pageurl + "']").addClass('menuactive');
				
				return false;
				
			}});
			

			//to change the browser URL to 'pageurl'
			if(pageurl!=window.location){
				window.history.pushState({path:pageurl},'',pageurl);	
				return false;
			}

			return false; 
		}
		
		
		
		
		return false;  
	});
	
	//Menu Change URL Name
	//jQuery("a[rel='tab'], .col-xs-10 a, .back-to-results a, .col-xs-5 a, .ihf-results-grid-photo a").click(function(e){
	jQuery("a[rel='tab']").click(function(e){
		//get the link location that was clicked
		var pageurl = jQuery(this).attr('href');
		
			//to get the ajax content and display in div with id 'content'
			jQuery.ajax({url:pageurl+'?rel=tab',success: function(data){
				jQuery(location).attr('href', pageurl);
				jQuery(".nav-sidebar nav ul li a").removeClass('menuactive');
				jQuery("a[href='" + pageurl + "']").addClass('menuactive');
				
				return false;
				
			}});
			

			//to change the browser URL to 'pageurl'
			if(pageurl!=window.location){
				window.history.pushState({path:pageurl},'',pageurl);	
				return false;
			}

		return false;  
	});
	
	jQuery("a[rel='comment'], #paginationLink").click(function(e) {
        //Do stuff when clicked
		var gourl = jQuery(this).attr('href');
		window.location = gourl;
		return false; 
    });
	
	
	if (window.history && window.history.pushState) {
		jQuery(window).bind('popstate', function() {
			
			jQuery.ajax({url:location.pathname+'?rel=tab',success: function(data){
				jQuery('body').html(data);	
				return false;
			}});
			
		});
	}
	
	
	
	//CALCULATOR
	jQuery(".mortgageContainer input[type=text]").keyup(function(){	
			var principal = jQuery('#sellP').val();
			var downpay	  = jQuery('#downP').val();
			var interest  = jQuery('#intR').val();
			var noy 	  = jQuery('#yearP').val();
			
			if (principal == '') { principal = '0'; }
			//if (downpay == '') { downpay = '0'; }
			if (interest == '')  { interest = '0'; }
			if (noy == '') 		 { noy = '0'; }
			
			if (principal != "0"  && interest != "0" && noy != "0" ){//&& downpay != "0"
				var str = principal;
				var a = (str.replace(/,/, ""));
				
				var down = downpay;
				var b = (down.replace(/,/, ""));
				
				if(downpay != "0"){
					var a = +a - +b;
				}
				
				var t_years = eval(noy*12); 
				var t_interest = eval(interest/1200); 
				var t = eval(1.0 /(Math.pow((1+t_interest),t_years)));
				
				if(t < 1){ 
					var payment = eval((a*t_interest)/(1-t)); 
				} else { 
					var payment = eval(amt/$t_years); 
				}
				
				var total = payment.toFixed(2); 
				jQuery('#monthP').val(total);
			}
			//payment.toFixed(2); 
			//alert("pass");
	
	});
	
	//Toggle Menu
	/*jQuery('.mobile-menu-close').click(function () {
		jQuery('body').removeClass('sidebar-open');
		jQuery('.nav-sidebar').removeClass('showMenuCustom');
		jQuery('.site-logo').removeClass('showLogoCustom');
		jQuery('.mobile-menu').removeClass('mobile-menuclose');
		jQuery('.content-view').removeClass('sidebar-closecustom');
		
		jQuery('.nav-sidebar').addClass('hideMenuCustom');
		jQuery('.site-logo').addClass('hideLogoCustom');
		jQuery('.mobile-menu-close').css('display', 'none');
		jQuery('.mobile-menu').addClass('mobile-menuopen');
		jQuery('.content-view').addClass('sidebar-opencustom');
	
		jQuery('#formTopHeader').css('display', 'block');
		jQuery('#mailForm').css('display', 'block');
		jQuery('#main-view').css('opacity', '1');
		jQuery('.topsimpleagentname').css('opacity', '1');
		
	});*/
	
	jQuery('.mobile-menu-closeleft').click(function () {
		jQuery('body').removeClass('sidebar-open');
		jQuery('.nav-sidebar').removeClass('showMenuCustom');
		jQuery('.site-logo').removeClass('showLogoCustom');
		jQuery('.mobile-menu').removeClass('mobile-menuclose');
		jQuery('.content-view').removeClass('sidebar-closecustom');
		
		jQuery('.nav-sidebar').addClass('hideMenuCustom');
		jQuery('.site-logo').addClass('hideLogoCustom');
		jQuery('.mobile-menu-close').css('display', 'none');
		jQuery('.mobile-menu').addClass('mobile-menuopen');
		jQuery('.content-view').addClass('sidebar-opencustom');
	
		jQuery('#formTopHeader').css('display', 'block');
		jQuery('#mailForm').css('display', 'block');
		jQuery('#main-view').css('opacity', '1');
		jQuery('.topsimpleagentname').css('opacity', '1');
		jQuery('.mobile-leftclose').css('display', 'none');
	});
	
	jQuery('.mobile-menu').click(function () {
		jQuery('body').addClass('sidebar-open');
		jQuery('.nav-sidebar').removeClass('hideMenuCustom');
		jQuery('.site-logo').removeClass('hideLogoCustom');
		jQuery('.mobile-menu').removeClass('mobile-menuopen');
		jQuery('.content-view').removeClass('sidebar-opencustom');
		
		jQuery('.nav-sidebar').addClass('showMenuCustom');
		jQuery('.site-logo').addClass('showLogoCustom');
		jQuery('.mobile-menu-close').css('display', 'block');
		jQuery('.mobile-menu').addClass('mobile-menuclose');
		jQuery('.content-viewform').addClass('sidebar-closecustom');
		
		jQuery('#formTopHeader').css('display', 'none');
		jQuery('#mailForm').css('display', 'none');
		jQuery('#main-view').css('opacity', '1');
		jQuery('.topsimpleagentname').css('opacity', '0');
		jQuery('.mobile-leftclose').css('display', 'block');
			
	});
	
	//main Page Scroll detect
	jQuery('#bgscroll').bind('mousewheel DOMMouseScroll MozMousePixelScroll', function(e){
		//alert('here');
        if(e.originalEvent.wheelDelta /120 > 0) {
			//alert('here1');
			jQuery("#intro-scroll").addClass("hidescroll");
			jQuery('body').removeClass('no-siderbar');
			jQuery('body').addClass('intro-done sidebar-open is-ready');
			jQuery('#bgscroll').remove();		
			
			jQuery('#container').removeClass('show-intro');
			jQuery('#navSiteBar').removeClass('remove-sidebar');			
			jQuery('#intro-text-bottom').addClass('intro-text textintro-bottom');
			jQuery('#main-view').addClass('mainviewScroll');
			
			window.setTimeout(function(){
				window.location.replace(fSite+"home/");
			}, 1000);
        }
        else{
			//alert('here2');
			jQuery("#intro-scroll").addClass("hidescroll");
            jQuery('body').removeClass('no-siderbar');
			jQuery('body').addClass('intro-done sidebar-open is-ready');
			jQuery('#bgscroll').remove();
			
			jQuery('#container').removeClass('show-intro');
			jQuery('#navSiteBar').removeClass('remove-sidebar');			
			jQuery('#intro-text-bottom').addClass('intro-text textintro-bottom');
			jQuery('#main-view').addClass('mainviewScroll');
			
			
			window.setTimeout(function(){
				window.location.replace(fSite+"home/");
			}, 1000);
        }
    });
	
	//Intro Page onClick
	jQuery('#bgscroll').bind('click', function(e){
		//alert('here');
			jQuery('#bgscroll').remove();
			jQuery("#intro-scroll").addClass("hidescroll");
			jQuery('body').removeClass('no-siderbar');
			jQuery('body').addClass('intro-done sidebar-open is-ready');
			
			jQuery('#container').removeClass('show-intro');
			jQuery('#navSiteBar').removeClass('remove-sidebar');			
			jQuery('#intro-text-bottom').addClass('intro-text textintro-bottom');
			jQuery('#main-view').addClass('mainviewScroll');
			
			
			window.setTimeout(function(){
				window.location.replace(fSite+"home/");
			}, 1000);

    });
	
	jQuery(window).resize(function() {
		

		if ( jQuery(this).width() > 1024 ) {
			
			jQuery('body').addClass();
			jQuery('body').addClass('intro-done is-ready');
			jQuery('#navSiteBar').addClass()
			jQuery('#navSiteBar').addClass('nav-sidebar menuDisplay showMenuCustom')
			// jQuery('#navSiteBar').removeClass('remove-sidebar hideMenuCustom');
			jQuery('.site-logo').removeClass('hideLogoCustom');
			jQuery('.mobile-menu-close').removeAttr('style');
			jQuery('#main-view').removeAttr('style');
			jQuery('.mobile-menu').removeClass('mobile-menuopen');
			// jQuery('#navSiteBar').addClass('showMenuCustom');
		}
	});
	
	//Hide Show Agent name
	jQuery(window).resize(function() {
		if ( jQuery(this).width() < 1025 ) {
			jQuery('.topsimpleagentname').css('opacity', '1');
			jQuery('#navSiteBar').addClass();
			jQuery('#navSiteBar').addClass('nav-sidebar menuDisplay hideMenuCustom');
			jQuery('#navSiteBar').removeClass('showMenuCustom');
			jQuery('.site-logo').addClass('hideLogoCustom');
		}
	});
	
	//IDX Plugin Custom
	jQuery('.ihf-grid-result').addClass('col-md-4');
	
	
	//Home Header
	var homeAnimation = jQuery('.topclientheader, .aboutSection, .buysellSection, .featuredSection, .recentBlog, .featuredListSection, .prosearchSection, .buyingPageSection, .sellingPageSection, .investPageSection, .communityPageSection, .aboutmePageSection, .testimonialSection, .soldproPageSection, .skillsetPageSection, .blogPageSection');
	var windows = jQuery(window);
	function homeAnimate() {
		var window_height = windows.height();
		var window_top_position = windows.scrollTop();
		var window_bottom_position = window_top_position + window_height;
		jQuery.each(homeAnimation, function () {
			var element = jQuery(this);
			var element_height = element.outerHeight();
			var element_top_position = element.offset().top;
			var element_bottom_position = element_top_position + element_height;
			if (element_bottom_position >= window_top_position && element_top_position <= window_bottom_position) {
				//element.addClass('in-view');
				element.addClass('content-visible');
				
			} /*else {
				element.removeClass('in-view');
				element.removeClass('content-visible');
			}*/
		});
	}
	windows.on('scroll resize', homeAnimate);
	windows.trigger('scroll');
	
	//Bottom Email
	var mailAnimation = jQuery('.emailBottom, .emailBottomDivider');
	var windows = jQuery(window);
	function mailAnimate() {
		var window_height = windows.height();
		var window_top_position = windows.scrollTop();
		var window_bottom_position = window_top_position + window_height;
		jQuery.each(mailAnimation, function () {
			var element = jQuery(this);
			var element_height = element.outerHeight();
			var element_top_position = element.offset().top;
			var element_bottom_position = element_top_position + element_height;
			if (element_bottom_position >= window_top_position && element_top_position <= window_bottom_position) {
				//element.addClass('in-view');
				element.addClass('content-visible');
				
			} else {
				element.removeClass('content-visible');
			}
		});
	}
	windows.on('scroll resize', mailAnimate);
	windows.trigger('scroll');
	 	


});

function jsMap()
{
	
	var that = jQuery('.gmap[data-js="jsMap"]');
	that.each(function()
	{
		var e = jQuery(that);
		var c = new google.maps.LatLng(parseFloat(e.attr('data-lat')),parseFloat(e.attr('data-lng')));
		jQuery(window).resize( function() { e.height(e.prev().outerHeight()+20); if (map) { google.maps.event.trigger(map, 'resize'); map.panTo(c);}}).trigger('resize');
		/*var styles = [
			{ 
				featureType: 'all', 
				elementType: 'all', stylers: [ { hue: '#e5e6e9' }, { saturation: -70 }, { "lightness": 50 }] 
			}
		];*/
		var map = new google.maps.Map(e[0], 
		{
			scrollwheel: false,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.LARGE,
				position: google.maps.ControlPosition.RIGHT_BOTTOM
			},
			/*Added on 3 July 2015*/
			streetViewControlOptions: {
				position: google.maps.ControlPosition.RIGHT_TOP
			},
			
			

			mapTypeId: google.maps.MapTypeId.ROADMAP,
			//mapTypeId: google.maps.MapTypeId.HYBRID,
			disableDefaultUI: false, 
			zoom: parseInt(e.attr('data-zoom')), 
			center: c,						
			draggable: true, //true,
			//styles: styles
		});
		var marker = new google.maps.Marker({ position: new google.maps.LatLng(e.attr('data-lat'), e.attr('data-lng')), icon: "../../wp-content/themes/Circlevisionsagent/images/marker.png"});
		var titleName = jQuery(".comContent .title").attr('title');
		var infowindow = new google.maps.InfoWindow();
			google.maps.event.addListener(marker, 'click', function() {
			infowindow.setContent(titleName);
			infowindow.open(map, this);
		  });

		
		marker.setMap(map);
	});
	return this;
};



//Controlling iFrame
window.onload = function() {
	var ischrome = /chrome/.test(navigator.userAgent.toLowerCase());
	var site = jQuery('#siteUrl').val();//add / at the end
	var fSite = site+"/";
	
	//alert(fSite);
	
	if (ischrome) {
		var height = document.body.scrollHeight;//Chrome
		//document.getElementById("neighboriFrame").style.height = parseInt(height) + 10 + 'px';
		jQuery('#neighboriFrame').css('height', parseInt(height) + 10 + 'px');
		
		//jQuery("#soldListings").attr({"scrolling": "yes"})
		//var divHeight = document.getElementById('soldproPageContainer').scrollHeight;
		//alert(divHeight);
		//jQuery('#soldListings').css('height', parseInt(divHeight) + 10 + 'px');
		
	}
	
	if( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ){
		var height2 = document.documentElement.scrollHeight; //Firefox
		document.getElementById('neighboriFrame').style.height = parseInt(height2) + 10 + 'px';
	}
	
	var pagename = window.location.pathname.split("/");
	var pname = pagename[2];//Localhost
	//var pname = pagename[1];//Live
	
	if(pname == 'about-me'){
		jQuery('#soldl').load(fSite+"sold-featured-listing/", function(){
			jQuery('#loadingFunction').addClass('loadingHide');
			jQuery('#loadingFunction').removeClass('loadingShow');
		});
	}
	//.showFeatureListing{display: block;}
   // .hideFeatureListing{display: none;}
	if(pname == 'featured-listings'){
		jQuery('#featContent').load(fSite+"featured-listings-details/", function(){
			
			jQuery('#loadingFunction').addClass('loadingHide');
			jQuery('#loadingFunction').removeClass('loadingShow');
			//jQuery('.homefeature').addClass('hideFeatureListing');
			
		});

	}
	
	if(pname == 'home'){
		jQuery('#featContentHome').load(fSite+"featured-listings-details/", function(){
			jQuery('#loadingFunction').addClass('loadingHide');
			jQuery('#loadingFunction').removeClass('loadingShow');
			jQuery('.homefeature').removeClass('hideFeatureListing');
			jQuery('.featuredFrame').addClass('hideFeatureListing');
			jQuery(".quickSection").load(location.href + " .quickBg");
		});

	}
}; 	 