<?php
get_header();
wp_reset_postdata();
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;


$featurepost =  array(
	//'s'				  =>get_search_query(),
	//'ID'			  => $post->ID,
	'showposts'		  => 1,
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'post_type'       => 'blog',
	'meta_query' => array(
		array(
			'key' => 'blog_featured',
			'value' => 1,
			'compare' => 'LIKE'
		),
	),
	//'post_status'	  => 'publish', //draft, publish, future, pending, private
	'paged'			  => $paged
);

$recentpost =  array(
	//'s'				  =>get_search_query(),
	'showposts'		  => 5,
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'post_type'       => 'blog',
	'meta_query' => array(
		array(
			'key' => 'blog_recent',
			'value' => 1,
			'compare' => 'LIKE'
		),
	),
	'paged'			  => $paged
);
$recent_posts = wp_get_recent_posts( $recentpost, ARRAY_A );
?>
<div id="blogSection" class="content-hidden">
	<!--<header>
        <h1><span>< ?=get_field('blog_divider_title','options');?></span></h1>
    </header>   -->
    <div class="leftContainer">
    <div class="leftTitle">Featured Posts</div>
        <div class="leftblog_list">

            <?php
               query_posts($featurepost);

                 if (have_posts())  {
                    while (have_posts()) : the_post();
              			$content = get_the_content();
						$blogImg = get_field('blog_post_image');
						//$image = wp_get_attachment_image_src(($blogImg),'full');

            ?>
                <div class="leftContent">
                        <div class="title"><a href="<?php echo get_permalink(); ?>" rel="tab"><?php the_title(); ?></a></div>
                        <div class="clearfix"></div>
                        <div class="subtitle"><?= get_post_time('F j, Y'); ?> by <?php the_author();?> - <a href="<?php the_permalink(); ?>" rel="tab">Leave a Comment</a> </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="description">
                         <?=truncate($content, $length = 300); ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="button_wrapper">
                            <a class="button" href="<?php the_permalink(); ?>" rel="tab">Read More</a>
                        </div>
                        <div class="clearfix"></div>
                </div>

            <?php endwhile; ?>
                <?php } ?>

        </div> <!-- End of Left Blog List -->

           <!-- <div class="pagination">
                <div class="alignlefts">
                    < ?php previous_posts_link( __( '&larr; Newer entries', 'circlevisions_agent' )); ?>
                </div>
                <div class="alignrights">
                    < ?php next_posts_link( __( 'Older entries &rarr;', 'circlevisions_agent')); ?>
                </div>
            </div>--><!--end pagination-->

   </div>  <!-- End of Left Container -->


   <!-- Right Recent Blog Posts -->
   <div class="rightContainer">
		<div class="rightTitle">Recents Posts</div>
        <div class="rightblog_list">
        	<?php
               query_posts($recentpost);
                 if (have_posts())  {
                    while (have_posts()) : the_post();
              			$content = get_the_content();

            ?>
           	 <div class="rightContent">
                        <div class="title"><a href="<?php echo get_permalink(); ?>" rel="tab"><?php the_title(); ?></a></div>
                        <div class="clearfix"></div>
                        <div class="subtitle"><?= get_post_time('F j, Y'); ?> by <?php the_author();?> - <a href="<?php the_permalink(); ?>" rel="tab">Leave a Comment</a> </div>
                        <div class="clearfix"></div>
                        <div class="description">
                         <?=truncate($content, $length = 60); ?> <a class="button" href="<?php the_permalink(); ?>" rel="tab">Read More</a>
                        </div>
                        <!--<div class="clearfix"></div>
                        <div class="button_wrapper">
                            <a class="button" href="< ?php the_permalink(); ?>" rel="tab">Read More</a>
                        </div>-->
                        <div class="clearfix"></div>
                </div>


             <?php endwhile; ?>
                <?php } ?>


        </div><!-- End of Right Blog List -->

   </div>



</div><!-- End of Blog Section-->

</body>
</html>