<?php 
$posts = get_posts( array(
	'numberposts'     => -1,
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'post_type'       => 'blog',
) );
//Search by categories
$arr_tags = array();
foreach($posts as $post){ setup_postdata($post);
	$post_id = $post->ID; 
	$terms = wp_get_post_terms($post_id, 'blog_categories');
	foreach ($terms as $t) {
			if(!in_array($t->slug, $arr_tags)){
				$arr_tags[] = $t->slug;
			}
	}
}

//Search by archives
$args = array(
    'post_type'    => 'blog',
    'type'         => 'monthly',
    'format'       => 'option',
	'show_post_count' => false,
	'echo' => 0
);
?>
<!-- Search form -->

<div id="searchBlog" class="container">
	<div class="container">
    	 <form method="get" action="<?=home_url('/blog/');?>">
       	<form role="search" method="get" action="<?=home_url('/blog/');?>">
           <div class="col-md-12 col-xs-12 col-sm-12">
                	<div class="col-md-8 col-xs-6 col-sm-6">
                		<input class="searchBlogText" type="search" placeholder="Search" value="<?=get_search_query();?>" name="s" autocomplete="off"/>
                    </div>
                    <div class="col-md-4 col-xs-6 col-sm-6" id="searchRow">
                		<a href="<?=site_url();?>/blog/" class="btn_back_bloghome" rel="tab">BACK TO BLOG HOME</a>
                    </div>
           </div>
       </form> 
    </div>
</div>