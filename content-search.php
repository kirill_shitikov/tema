<?php 
get_header(); 
wp_reset_postdata(); 

$blogTitle = get_field('blog_header_title', 'option');
?>
<div id="blogHeader">
	<div class="headerTitle"><?=$blogTitle;?></div>
</div>

<?php get_search_form(); ?>


	<div id="blogSearchContent">
		<div class="fblog_list center">
        	<?php	
				$posts = get_posts( array(
					's'				  =>get_search_query(),
					'numberposts'     => -1,
					'order'           => 'asc',
					'post_type'       => 'blog',
				) );
				foreach($posts as $post) { 
					$content = get_the_content();
					
					
					
				?>
        
			<div class="blogSearchDivider">
				<div class="title"><a href="<?php echo get_permalink(); ?>" rel="tab"><?php the_title(); ?></a></div>
				<div class="clearfix"></div>
				<div class="subtitle"><?= get_post_time('F j, Y'); ?> by <?php the_author();?></div>
				<div class="clearfix"></div>
				<div class="description">
                	<?=truncate($content, $length = 300); ?>
				</div>
				<div class="clearfix"></div>
 				<div class="button_wrapper">
                    <a class="button" href="<?php the_permalink(); ?>" rel="tab">Read More</a>                    
                </div>
			</div>
            <?php
				}
			?>
            <div class="pagination index">
            <div class="alignleft">
                    <?php previous_posts_link( __( '&larr; Newer entries', 'Circlevisions_agent' )); ?>
                </div>
                <div class="alignright">
                    <?php next_posts_link( __( 'Older entries &rarr;', 'Circlevisions_agent' )); ?>
                </div>
            </div><!--end pagination-->
            
		</div>	
	</div>	
<?php get_template_part('blogfooter'); ?>
