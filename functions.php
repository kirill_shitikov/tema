<?php
$theme_name = "Circlevisionsagent";
$theme_version = "1.0.1";
global $theme_name;
global $theme_version;


define( 'ACF_LITE' , true);
include_once('includes/advanced-custom-fields/acf.php' );

include 'includes/custom-fields.php';
include 'includes/post-types.php';
include 'includes/social-share.php';
include 'includes/comment.php';
include 'includes/ajax-filters.php';
include 'includes/ajax_lightbox.php';
include 'includes/shortcodes/shortcodes.php';

//ADD Roles
/*$result = add_role( 
	'client', __('Client' ),
		array(
			'read' => true, // true allows this capability
			'edit_posts' => true, // Allows user to edit their own posts
			'edit_pages' => true, // Allows user to edit pages
			'edit_others_posts' => false, // Allows user to edit others posts not just their own
			'create_posts' => true, // Allows user to create new posts
			'manage_categories' => true, // Allows user to manage post categories
			'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
			'edit_themes' => false, // false denies this capability. User can’t edit your theme
			'install_plugins' => false, // User cant add new plugins
			'update_plugin' => false, // User can’t update any plugins
			'update_core' => false // user cant perform core updates
	 )
 
);*/

/*function wpse28782_remove_menu_items() {
    if( !current_user_can( 'administrator' ) ):
        remove_menu_page( 'edit.php?post_type=testimonials' );
    endif;
}
add_action( 'admin_menu', 'wpse28782_remove_menu_items' );
*/

/*function wpse28782_remove_menu_items() {
	$user = wp_get_current_user();
	//if(!current_user_can( 'admnistrator' ) ):
    //if(!current_user_can( 'client' ) ):
	if ( in_array( 'client', (array) $user->roles ) ) :
        remove_menu_page( 'edit.php?post_type=testimonials' );
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'edit.php' );
		remove_menu_page( 'options-general.php' );
    endif;
}
add_action( 'admin_menu', 'wpse28782_remove_menu_items' );*/


//Подключение стилей и скриптов для загрузчика
function my_add_upload_scripts() {  
    wp_enqueue_style('thickbox');
    wp_enqueue_script('thickbox');
}  

//Remove Plugin Editor
/*function my_remove_menu_elements(){
	remove_submenu_page( 'plugins.php', 'plugin-editor.php' );
}
add_action('admin_init', 'my_remove_menu_elements');
 */ 
  
// Запускаем функцию подключения загрузчика  
if( is_admin() )  
add_action('admin_print_scripts', 'my_add_upload_scripts');  

/*ACF*/
// Fields
    add_action('acf/register_fields', 'my_register_fields');

    function my_register_fields()
    {
        include_once('includes/add-ons/acf-repeater/repeater.php');
        include_once('includes/add-ons/acf-gallery/gallery.php');
        include_once('includes/add-ons/acf-flexible-content/flexible-content.php');
        include_once('includes/add-ons/advanced-custom-fields-limiter-field/limiter-v4.php');
		
		
	   	
        include_once('includes/registered-fields/presets/acf-presets.php');
        include_once('includes/registered-fields/google-font/acf-googlefonts.php');
        //include_once('includes/registered-fields/googlemap/acf-googlemap.php');
		
    }
	
//font awesome picker
	include_once('includes/add-ons/advanced-custom-fields-font-awesome/acf-font-awesome.php');
	

	
// Options Page
    include_once( 'includes/add-ons/acf-options-page/acf-options-page.php' );


add_filter('acf/options_page/settings','register_options_page');

    function register_options_page($options)
    {
       $options['title'] = __('Circlevisions','um_lang');
       $options['pages'] = array(
			__('Theme Setting','um_lang'),
			__('Home Setting','um_lang'),
			__('Featured Listings Setting','um_lang'),
			__('Properties Search Setting','um_lang'),
			__('Buying Sell Setting','um_lang'),
			__('Community Setting','um_lang'),
			__('About Me Setting','um_lang'),
			//__('Testimonial Setting', 'um_lang'),
			//__('Quick Search Setting','um_lang'),
			//__('Featured Setting','um_lang'),
			__('Blog Setting','um_lang'),		
			__('Email Form Setting','um_lang'),
			
        );
       return $options;
    }
   

    add_action( 'acf/input/admin_enqueue_scripts', 'acf_collapse_fields' );
    function acf_collapse_fields(){
        wp_enqueue_script('collapse-fields_js', get_template_directory_uri().'/includes/collapse-fields/acf_collapse.js', array(),'',TRUE);
        wp_enqueue_style('collapse-fields_css',  get_template_directory_uri().'/includes/collapse-fields/acf_collapse.css' , false, "1.0");
    }

/*ACF*/


if ( ! isset( $content_width ) ) $content_width = 1170;


/*Image Sizes*/

	add_image_size('a_box', 405, 315, true);
	add_image_size('a_landscape', 855, 315, true);
	add_image_size('a_landscapeB', 1305, 315, true);
	add_image_size('a_portrait', 405, 765, true);
	add_image_size('blogMasonry', 720, 410, true);
	add_image_size('service_icon', 208, 208, true);
	add_image_size('shop_products', 450, 450, true);
	
/*Image Sizes*/


/*Support Language Translation*/
    add_action('after_setup_theme', 'um_theme_setup');
    function um_theme_setup(){
        load_theme_textdomain('um_lang', get_template_directory().'/lang');
    }
/*Support Language Translation*/



/*Add Menu Support*/ 
//Will appear in Admin Appearance (Menu)

    add_action( 'after_setup_theme', 'register_theme_menus' );
    function register_theme_menus() {
		register_nav_menus(
			array(
					/*'topLeftMenu' => __( 'Top Left Menu',"um_lang" ),
					'topRightMenu' => __('Top Right Menu', "um_lang"),*/
					'menu' => __('Menu', "um_lang")
				)
			);
	}
	
	function my_wp_nav_menu_args( $args = '' ) {
		$args['container'] = false;
		return $args;
	}
	add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );
	
	function add_menuclass($ulclass) {
	   return preg_replace('/<a /', '<span class="progress-bar"></span><a class="vertical-center"', $ulclass);
	}
	add_filter('wp_nav_menu','add_menuclass');
	
	add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
	add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
	add_filter('page_css_class', 'my_css_attributes_filter', 100, 1);
	function my_css_attributes_filter($var) {
	  return is_array($var) ? array() : '';
	}
	
	
	/*add_filter( 'posts_where', 'wpse18703_posts_where', 10, 2 );
	function wpse18703_posts_where( $where, &$wp_query )
	{
		global $wpdb;
		if ( $wpse18703_title = $wp_query->get( 'wpse18703_title' ) ) {
			$where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'' . esc_sql( $wpdb->esc_like( $wpse18703_title ) ) . '%\'';
		}
		return $where;
	}*/
	add_action( 'pre_get_posts', function( $q )
{
    if( $title = $q->get( '_meta_or_title' ) )
    {
        add_filter( 'get_meta_sql', function( $sql ) use ( $title )
        {
            global $wpdb;

            // Only run once:
            static $nr = 0; 
            if( 0 != $nr++ ) return $sql;

            // Modify WHERE part:
            $sql['where'] = sprintf(
                " AND ( %s OR %s ) ",
                $wpdb->prepare( "{$wpdb->posts}.post_title = '%s'", $title ),
                mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
            );
            return $sql;
        });
    }
});
/*Add Menu Support*/


/*Excerpt more*/


	function custom_excerpt_length( $length ) {
		return 30;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
	
	function new_excerpt_more($more )
    {
        return '...';
    }
    add_filter('excerpt_more', 'new_excerpt_more');
	
	//Custom Excerpt for blog
	function excerpt($limit){	
     
		 $excerpt = explode(' ', get_the_excerpt(), $limit);
	     if (count($excerpt) >= $limit){
	        	array_pop($excerpt);
	        	$excerpt = implode(" ",$excerpt).'...';
	      }
		  else{
	       	 $excerpt = implode(" ",$excerpt);
	      } 
	      	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	      	return strip_tags($excerpt);
    }
	
/*Excerpt more*/


	add_theme_support( 'woocommerce' );
	add_theme_support( 'automatic-feed-links' );
  	add_theme_support( 'post-thumbnails' );
  	add_theme_support( 'post-formats', array('image','video','audio'));
	add_post_type_support('post','page-attributes');


function remove_admin_submenu_items() {
	remove_submenu_page( 'themes.php', 'widgets.php' );
}
 
add_action( 'admin_menu', 'remove_admin_submenu_items');


/*Hide Editor Specific Page*/

add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
	// Get the Post ID.
 $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
 if( !isset( $post_id ) ) return;
   /*// Hide the editor on the page titled 'Homepage'
  $homepgname = get_the_title($post_id);
  if($homepgname == 'Home'){ 
    remove_post_type_support('page', 'editor');
  }*/
  
  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  $homePage = get_post_meta($post_id, '_wp_page_template', true);
  if($homePage == 'template/template-homepage.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
  
  $featuredListingPage = get_post_meta($post_id, '_wp_page_template', true);
  if($featuredListingPage == 'template/template-featured-listings.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
  
  $propertiesSearchPage = get_post_meta($post_id, '_wp_page_template', true);
  if($propertiesSearchPage == 'template/template-properties-search.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
  
  $buyingPage = get_post_meta($post_id, '_wp_page_template', true);
  if($buyingPage == 'template/template-buying.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
  
  $sellingPage = get_post_meta($post_id, '_wp_page_template', true);
  if($sellingPage == 'template/template-selling.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
  
  $investPage = get_post_meta($post_id, '_wp_page_template', true);
  if($investPage == 'template/template-investing.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
  
  $communitiesPage = get_post_meta($post_id, '_wp_page_template', true);
  if($communitiesPage == 'template/template-community.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
  
  $aboutmePage = get_post_meta($post_id, '_wp_page_template', true);
  if($aboutmePage == 'template/template-about-me.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
}


add_filter( 'nav_menu_link_attributes', 'wpse121123_contact_menu_atts', 10, 3 );
function wpse121123_contact_menu_atts( $atts, $item, $args )
{
  // The ID of the target menu item
  //$menu_target = 123;

  // inspect $item
  //if ($item->ID == $menu_target) {
    $atts['data-postname'] = $item->title;
 // }
  return $atts;
}


/*Add theme css and js*/

    add_action( 'wp_enqueue_scripts', 'add_external_stylesheets' );
    

    function add_external_stylesheets()
    {
    	
		//responsive-min.css
        	wp_enqueue_style("responsive-min", get_template_directory_uri().'/assets/css/responsive.min.css' , false, "1.0");	
		//plugins.css
        	wp_enqueue_style("plugins", get_template_directory_uri().'/assets/css/plugins.css' , false, "1.0");	
		//style.css
        	wp_enqueue_style("style", get_stylesheet_uri() , false, "1.0");	
		
    }
	
	

	
	add_action( 'wp_enqueue_scripts', 'add_external_js' );
    function add_external_js()
    {
		//echo content_url();
        //jQyery
            wp_enqueue_script('jquery');
			//wp_enqueue_script('jqueryscrollspeed', get_template_directory_uri()."/assets/js/jQuery.scrollSpeed.js" , array(),'', FALSE);
			wp_enqueue_script('script', get_template_directory_uri()."/assets/js/script.js" , array(),'', FALSE);
			//wp_enqueue_script('script');
			
		
			
        //easing
			wp_enqueue_script("easing", get_template_directory_uri()."/assets/royalSlider/royalslider/jquery.easing-1.3.js" , array(),'',TRUE);   
		// Royal Slider
			wp_enqueue_script("royalSlider", get_template_directory_uri()."/assets/royalSlider/royalslider/jquery.royalslider.min.js" , array(),'',TRUE);   
		// Owl Carousel
			wp_enqueue_script("owlCarousel", get_template_directory_uri()."/assets/owl-carousel/owl.carousel.js" , array(),'',TRUE);   
		//Scrolly (parallax)
			wp_enqueue_script("scrolly", get_template_directory_uri()."/assets/js/jquery.scrolly.js" , array(),'',TRUE);   
		// Smooth Scroll
			//wp_enqueue_script("smoothscroll", get_template_directory_uri()."/assets/js/jquery.smoothscroll.js" , array(),'',TRUE);   
			wp_enqueue_script("SmoothScroll", get_template_directory_uri()."/assets/js/SmoothScroll.js" , array(),'', FALSE); 
		//Bootstrap
			wp_enqueue_script("bootstrap", get_template_directory_uri()."/assets/bootstrap/js/bootstrap.min.js" , array(),'', FALSE);
	 	//BootstrapSelect
			wp_enqueue_script("BootstrapSelectJS", get_template_directory_uri()."/assets/js/bootstrap-select.js" , array(),'', FALSE);   
	 	//isotope
			 wp_enqueue_script("isotopeJS", get_template_directory_uri()."/assets/js/isotope.js" , array(),'',TRUE);   	
			 wp_enqueue_script("imgLoaded", get_template_directory_uri()."/assets/js/imagesloaded.pkgd.min.js" , array(),'',TRUE);  
		//date time picker
			wp_enqueue_script("dateTimePicker", get_template_directory_uri()."/assets/js/jquery.datetimepicker.js" , array(),'',TRUE);   	  	
	 	//mCustomScroll
			wp_enqueue_script("mCustomScroll", get_template_directory_uri()."/assets/js/jquery.mCustomScrollbar.min.js" , array(),'',TRUE);   	  	
	 	//twitter
			wp_enqueue_script("widgetTwitter", get_template_directory_uri()."/assets/js/jquery.twitterfeed.js" , array(),'',TRUE);
	 	//GoogleMap
			wp_enqueue_script("google_map","https://maps.googleapis.com/maps/api/js?sensor=true",'','',TRUE);	
	 	
	 	//themeScripts
			wp_enqueue_script("tabAccTog", get_template_directory_uri()."/assets/js/tabAccTog.js" , array(),'',TRUE); 
			wp_enqueue_script("mainScript", get_template_directory_uri()."/assets/js/main_script.js" , array(),'', TRUE);  
			wp_enqueue_script("um_lightbox", get_template_directory_uri()."/assets/js/um_lightbox.js" , array(),'',TRUE);  
			
			wp_enqueue_script("jquery_jcarousel_min", get_template_directory_uri()."/assets/js/jquery.jcarousel.min.js" , array(),'',TRUE);   
			wp_enqueue_script("jquery_mfs_min", get_template_directory_uri()."/assets/js/jquery.mfs.min.js" , array(),'',TRUE);   
			  
		
		//Top Menu Js
			wp_enqueue_script("topmenu", get_template_directory_uri()."/assets/js/topmenu.js" , array(),'',TRUE);  	
			
		//FrontPage
		 	//wp_enqueue_script('jquery_min', get_template_directory_uri()."/assets/js/jquery.min.js" , array(),'',TRUE);
			//wp_enqueue_script('jquery_min', get_template_directory_uri()."/assets/js/jquery.min.js" , array(),'',TRUE);
			wp_enqueue_script("global", get_template_directory_uri()."/assets/js/global.js" , array(),'',TRUE); 
			//wp_enqueue_script("jquery_easing_1_3", get_template_directory_uri()."/assets/js/jquery.easing.1.3.js" , array(),'',TRUE); 
			//wp_enqueue_script("jquery_form", get_template_directory_uri()."/assets/js/jquery.form.js" , array(),'',TRUE); 	
		
		//Social JS
		wp_enqueue_script("socialpopup", get_template_directory_uri()."/assets/js/socialpopup.js" , array(),'',TRUE);
		
		//Touch Screen Devices
		wp_enqueue_script("doubletap", get_template_directory_uri()."/assets/js/doubletap.js" , array(),'',TRUE); 
			
		//Comment Replay
	   if ( is_singular() ){
		   //echo site_url();
			wp_enqueue_script('comment-reply', site_url()."/wp-includes/js/comment-reply.js" , array(),'',FALSE);
			//wp_enqueue_script('comment-reply','', array(),'', FALSE );
		}
			
		//Popup Display
		wp_enqueue_script("html5lightbox", get_template_directory_uri()."/assets/mediapopup/html5lightbox.js" , array(),'',TRUE); 
		
		//Back To Top
		wp_enqueue_script("backTop", get_template_directory_uri()."/assets/js/jquery.backTop.min.js" , array(),'', FALSE); 
		
		//Preload
		//wp_enqueue_script("main-build", get_template_directory_uri()."/assets/preload/main-build.js" , array(),'',TRUE); 
		
		
    }

 
 
 	function um_html5_shiv() {
        ?>
        <!-- IE Fix for HTML5 Tags -->
        <!--[if lt IE 9]>
    	 	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
	}
	//add scripts for IE 9
    add_action('wp_head','um_html5_shiv');
	
 //*Add theme css and js*/


/*Add admin css and js*/
    add_action('admin_enqueue_scripts', 'load_custom_admin_js');
    add_action( 'admin_enqueue_scripts', 'load_custom_admin_css' );

    function load_custom_admin_js()
    {
         wp_enqueue_script("um_adminJS", get_template_directory_uri()."/assets/js/admin_js.js" , array(),'',TRUE);
    }
    function load_custom_admin_css()
    {
	//iCONS
         wp_enqueue_style("font-awesome", get_template_directory_uri().'/assets/fonts/FontAwesome/css/font-awesome.min.css' , false, "1.0");
    //admin Styles
        wp_enqueue_style("um_adminStyle", get_template_directory_uri()."/assets/css/admin_style.css" , false, "1.0");
    }
 //*Add admin css and js*/



 
 
 function pagination($query, $wp_rewrite){
    ?>
	
    <div class="isCentered col-md-12">
        <div class="pagination mainBgColor type2">
            <?php
            $format = $wp_rewrite->using_permalinks() ? "/page/%#%" : '?paged=%#%';
            $big = 999999999; // need an unlikely integer
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => $format,
                'current' => max( 1, get_query_var('paged') ),
                'total' => $query->max_num_pages,
                'prev_next'    => true,
                'show_all'     => False,
            ) );
            ?>
        </div>
    </div>
<?php
}
/*pagination*/

/*Live Pagination setting*/
add_filter('next_posts_link_attributes', 'get_next_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'get_previous_posts_link_attributes');

if (!function_exists('get_next_posts_link_attributes')){
	function get_next_posts_link_attributes($attr){
		$attr = 'rel="tab" title="Order entries" id="paginationLink"';
		return $attr;
	}
}
if (!function_exists('get_previous_posts_link_attributes')){
	function get_previous_posts_link_attributes($attr){
		$attr = 'rel="tab" title="Next entries" id="paginationLink"';
		return $attr;
	}
}

/*function get_home_pagination() {
	global $paged, $wp_query, $wp;
	$args = wp_parse_args($wp->matched_query);
	if ( !empty ( $args['paged'] ) && 0 == $paged ) {
		$wp_query->set('paged', $args['paged']);
	  	$paged = $args['paged'];
	}
}*/


//Добавляем загрузку картинок к категориям Start
add_action('admin_head', 'wpds_admin_head');
add_action('edit_term', 'wpds_save_tax_pic');
add_action('create_term', 'wpds_save_tax_pic');
function wpds_admin_head() {
    $taxonomies = get_taxonomies();
    //$taxonomies = array('category'); // раскомментируйте и укажите определенные таксономии, в которые хотите добавить изображение
    if (is_array($taxonomies)) {
        foreach ($taxonomies as $z_taxonomy) {
            add_action($z_taxonomy . '_add_form_fields', 'wpds_tax_field');
            add_action($z_taxonomy . '_edit_form_fields', 'wpds_tax_field');
        }
    }
}


// добавляем поле картинки в форме
function wpds_tax_field($taxonomy) {
    wp_enqueue_style('thickbox');
    wp_enqueue_script('thickbox');
    if(empty($taxonomy)) {
        echo '<div class="form-field">
                <label for="wpds_tax_pic">Image</label>
                <input type="text" name="wpds_tax_pic" id="wpds_tax_pic" value="" />
            </div>';
    }
    else{
        $wpds_tax_pic_url = get_option('wpds_tax_pic' . $taxonomy->term_id);
        echo '<tr class="form-field">
		<th scope="row" valign="top"><label for="wpds_tax_pic">Image</label></th>
		<td><input type="text" name="wpds_tax_pic" id="wpds_tax_pic" value="' . $wpds_tax_pic_url . '" /><br />';
        if(!empty($wpds_tax_pic_url))
            echo '<img src="'.$wpds_tax_pic_url.'" style="max-width:200px;border: 1px solid #ccc;padding: 5px;box-shadow: 5px 5px 10px #ccc;margin-top: 10px;" >';
        echo '</td></tr>';        
    }
    echo '<script type="text/javascript">
	    jQuery(document).ready(function() {
                jQuery("#wpds_tax_pic").click(function() {
                    tb_show("", "media-upload.php?type=image&amp;TB_iframe=true");
                    return false;
                });
                window.send_to_editor = function(html) {
                    jQuery("#wpds_tax_pic").val( jQuery("img",html).attr("src") );
                    tb_remove();
                }
	    });
	</script>';
}


// сохраняем наше изображение таксономии при редактировании
function wpds_save_tax_pic($term_id) {
    if (isset($_POST['wpds_tax_pic']))
        update_option('wpds_tax_pic' . $term_id, $_POST['wpds_tax_pic']);
}

// выводим ссылку на изображение таксономии для данного term_id (NULL по-умолчанию)
function wpds_tax_pic_url($term_id = NULL) {
    if ($term_id) 
        return get_option('wpds_tax_pic' . $term_id);
    elseif (is_category())
        return get_option('wpds_tax_pic' . get_query_var('cat')) ;
    elseif (is_tax()) {
        $current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
        return get_option('wpds_tax_pic' . $current_term->term_id);
    }
}
//Добавляем загрузку картинок к категориям End


//Category Sorting ORder
add_action('admin_head', 'wpds_admin_head_sorting');
add_action('edit_term', 'wpds_save_sorting');
add_action('create_term', 'wpds_save_sorting');
function wpds_admin_head_sorting() {
    $taxonomies = get_taxonomies();
    //$taxonomies = array('category'); // раскомментируйте и укажите определенные таксономии, в которые хотите добавить изображение
    if (is_array($taxonomies)) {
        foreach ($taxonomies as $z_taxonomy) {
            add_action($z_taxonomy . '_add_form_fields', 'wpds_tax_field_sort');
            add_action($z_taxonomy . '_edit_form_fields', 'wpds_tax_field_sort');
        }
    }
}

// добавляем поле картинки в форме
function wpds_tax_field_sort($taxonomy) {
    wp_enqueue_style('thickbox');
    wp_enqueue_script('thickbox');
    if(empty($taxonomy)) {
		echo '<div class="form-field">
                <label for="wpds_save_cat_order">Sorting Order</label>
                <input type="text" name="wpds_save_cat_order" id="cat_order" value="" />
            </div>';
    }
    else{
		$wpds_save_cat_order = get_option('wpds_save_cat_order' . $taxonomy->term_id);
		echo '<tr class="form-field">
			<th scope="row" valign="top"><label for="wpds_save_cat_order">Sorting Order</label></th>
			<td><input type="text" name="wpds_save_cat_order" id="wpds_save_cat_order" value="' . $wpds_save_cat_order . '" /><br />';
        echo '</td></tr>'; 
    }
}

function wpds_save_sorting($term_id) {
    if (isset($_POST['wpds_save_cat_order']))
        update_option('wpds_save_cat_order' . $term_id, $_POST['wpds_save_cat_order']);
}

///FEATURE AND RECENT POST
/*function register_post_assets(){
    add_meta_box('featured-post', __('Featured Post'), 'add_featured_meta_box', 'blog', 'advanced', 'high');
}
add_action('admin_init', 'register_post_assets', 1);

function add_featured_meta_box($post){
    $featured = get_post_meta($post->ID, '_featured-post', true);
    echo "<label for='_featured-post'>".__('Feature this post ?', 'circlevisions')."</label>";
    echo "<input type='checkbox' name='featured-post' id='featured-post' value='1' ".checked(1, $featured)." />";
}*/


//Category Banner Title
add_action('admin_head', 'wpds_admin_head_bannerTitle');
add_action('edit_term', 'wpds_save_bannerTitle');
add_action('create_term', 'wpds_save_bannerTitle');
function wpds_admin_head_bannerTitle() {
    $taxonomies = get_taxonomies();
    //$taxonomies = array('category'); // раскомментируйте и укажите определенные таксономии, в которые хотите добавить изображение
    if (is_array($taxonomies)) {
        foreach ($taxonomies as $z_taxonomy) {
            add_action($z_taxonomy . '_add_form_fields', 'wpds_tax_field_title');
            add_action($z_taxonomy . '_edit_form_fields', 'wpds_tax_field_title');
        }
    }
}

// добавляем поле картинки в форме
function wpds_tax_field_title($taxonomy) {
    wp_enqueue_style('thickbox');
    wp_enqueue_script('thickbox');
    if(empty($taxonomy)) {
		echo '<div class="form-field">
                <label for="wpds_save_cat_bannertitle">Banner Title</label>
                <input type="text" name="wpds_save_cat_bannertitle" id="wpds_save_cat_bannertitle" value="" />
            </div>';
    }
    else{
		$wpds_save_cat_bannerTitle = get_option('wpds_save_cat_bannertitle' . $taxonomy->term_id);
		echo '<tr class="form-field">
			<th scope="row" valign="top"><label for="wpds_save_cat_bannertitle">Banner Title</label></th>
			<td><input type="text" name="wpds_save_cat_bannertitle" id="wpds_save_cat_bannertitle" value="' . $wpds_save_cat_bannerTitle . '" /><br />';
        echo '</td></tr>'; 
    }
}

function wpds_save_bannerTitle($term_id) {
    if (isset($_POST['wpds_save_cat_bannertitle']))
        update_option('wpds_save_cat_bannertitle' . $term_id, $_POST['wpds_save_cat_bannertitle']);
}

function wpds_save_cat_bannerTitle($term_id = NULL) {
    if ($term_id) 
        return get_option('wpds_save_cat_bannertitle' . $term_id);
    elseif (is_category())
        return get_option('wpds_save_cat_bannertitle' . get_query_var('cat')) ;
    elseif (is_tax()) {
        $current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
        return get_option('wpds_save_cat_bannertitle' . $current_term->term_id);
    }
}



//Category Banner Description
add_action('admin_head', 'wpds_admin_head_bannerDesc');
add_action('edit_term', 'wpds_save_bannerDesc');
add_action('create_term', 'wpds_save_bannerDesc');
function wpds_admin_head_bannerDesc() {
    $taxonomies = get_taxonomies();
    //$taxonomies = array('category'); // раскомментируйте и укажите определенные таксономии, в которые хотите добавить изображение
    if (is_array($taxonomies)) {
        foreach ($taxonomies as $z_taxonomy) {
            add_action($z_taxonomy . '_add_form_fields', 'wpds_tax_field_bannerdesc');
            add_action($z_taxonomy . '_edit_form_fields', 'wpds_tax_field_bannerdesc');
        }
    }
}

// добавляем поле картинки в форме
function wpds_tax_field_bannerdesc($taxonomy) {
    wp_enqueue_style('thickbox');
    wp_enqueue_script('thickbox');
    if(empty($taxonomy)) {
		echo '<div class="form-field">
                <label for="wpds_save_cat_bannerdesc">Banner Description</label>
				<textarea name="wpds_save_cat_bannerdesc" id="wpds_save_cat_bannerdesc" rows="5" cols="50" class="large-text"></textarea>
            </div>';
    }
    else{
		$wpds_save_cat_bannerDesc = get_option('wpds_save_cat_bannerdesc' . $taxonomy->term_id);
		echo '<tr class="form-field">
			<th scope="row" valign="top"><label for="wpds_save_cat_bannerdesc">Banner Description</label></th>
			<td><textarea name="wpds_save_cat_bannerdesc" id="wpds_save_cat_bannerdesc" rows="5" cols="50" class="large-text">' . $wpds_save_cat_bannerDesc . '</textarea><br />';
        echo '</td></tr>'; 
    }
}

function wpds_save_bannerDesc($term_id) {
    if (isset($_POST['wpds_save_cat_bannerdesc']))
        update_option('wpds_save_cat_bannerdesc' . $term_id, $_POST['wpds_save_cat_bannerdesc']);
}
function wpds_save_cat_bannerDesc($term_id = NULL) {
    if ($term_id) 
        return get_option('wpds_save_cat_bannerdesc' . $term_id);
    elseif (is_category())
        return get_option('wpds_save_cat_bannerdesc' . get_query_var('cat')) ;
    elseif (is_tax()) {
        $current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
        return get_option('wpds_save_cat_bannerdesc' . $current_term->term_id);
    }
}



//Добавляем метки(tags) к типу постам toolbox
add_action( 'init', 'post_tag_for_pages' );
function post_tag_for_pages(){
	register_taxonomy_for_object_type( 'post_tag', 'toolbox');
}

function truncate($text,$length = 100) {
	$str = strip_tags(htmlspecialchars_decode($text));
	if(mb_strlen($str, 'UTF-8') > $length) {
		$str = mb_substr($str, 0, $length, 'UTF-8');
		$str .= '...';
	}
	return $str;
}


//Customize by Terrance
//Add tags to the type of Testimonial post
//Terrance 14 July 2015
/*add_action( 'init', 'post_tag_for_testimonial' );
function post_tag_for_testimonial(){
	register_taxonomy_for_object_type( 'post_tag', 'testimonials');
}*/


//Add Student Saying
//Terrance 14 July 2015
/*add_action( 'init', 'post_tag_for_studentsay' );
function post_tag_for_studentsay(){
	register_taxonomy_for_object_type( 'post_tag', 'Student Saying');
}*/


//Customize Post Title Placeholder Start
//Testimonial
function testimonial_placeholder($label, $post){

    if($post->post_type == 'testimonials')
        $label= __('Enter customer name....');

    return$label;
}
add_filter('enter_title_here', 'testimonial_placeholder', 2, 2);

//HEADER TOP TESTIMONIAL TABLE LABEL
add_filter( 'manage_edit-testimonials_columns', 'testimonial_columns', 10, 1 );
function testimonial_columns( $columns ) {
	
	//$column_thumbnail = array( 'thumbnail' => 'Thumbnail' );
	$column_customer = array( 'title' => 'Customer Name' );
	
	//$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
	$columns = array_slice( $columns, 0, 1, true ) + $column_customer + array_slice( $columns, 1, NULL, true );
	return $columns;
}

function template_testimonial($template){    
	global $wp_query;  
	$post_type = get_query_var('post_type');   
	if($post_type == 'testimonials'){
		return locate_template('testimonials.php');  //  redirect to archive-search.php
	}   
  	return $template;   
}
add_filter('template_include', 'template_testimonial');


//ADD NEW TESTIMONIAL COLUMNS
/*add_action( 'manage_posts_custom_column', 'testimonial_page', 10, 1 );
function testimonial_page( $column ) {
	global $post;
	$customer = get_the_title();
	switch ( $column ) {
		case 'titles':
			if($customer[0]){
				echo "sss";	
				break;
			}else{
				echo " - ";	
				break;
			}
	}
	
}*/
//Customize Post Title Placeholder End



//Customize Blog Template
function template_blog($template){    
	global $wp_query;  
	$post_type = get_query_var('post_type');   
	//echo $post_type;
	echo $wp_query->is_paged();
	if($wp_query->is_single && $post_type == 'blog'){
		//echo "success single";
		//return locate_template('content-search.php');  //  redirect to archive-search.php
		return locate_template('single-blog.php');

	}else if($post_type == 'blog'){
		//echo "success page";
		return locate_template('blog.php');
	}
  	return $template;   
}
add_filter('template_include', 'template_blog');

/*if(  && $post_type == 'toolbox' )   
  {
    return locate_template('content-toolbox_search.php');
*/

function stateToAbb($input){ 
    //reset found 
    $found = 0; 
    //list states 
    $states = "Alaska,Alabama,Arkansas,Arizona,California,Colorado,Connecticut,Delaware,Florida,Georgia,Hawaii,Iowa,Idaho,Illinois,Indiana,Kansas,Kentucky,Louisiana,Massachusetts,Maryland,Maine,Michigan,Minnesota,Mi  ssouri,Mississippi,Montana,North Carolina,North Dakota,Nebraska,New Hampshire,New Jersey,New Mexico,Nevada,New York,Ohio,Oklahoma,Oregon,Pennsylvania,Rhode Island,South Carolina,South Dakota,Tennessee,Texas,Utah,Virginia,Vermont,Washington,Wisconsin,West Virginia,Wyoming"; 
    //list abbreviations 
    $abb = "AK,AL,AR,AZ,CA,CO,CT,DE,FL,GA,HI,IA,ID,IL,IN,KS,KY,LA,MA,MD,ME,MI,MN,MO,MS,MT,NC,ND,NE,NH,NJ,NM,NV,N  Y,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VA,VT,WA,WI,WV,WY"; 
    //create arrays 
    $states_array = explode(",", $states); 
    $abb_array = explode(",", $abb); 
     
    //run test 
    for ($i = 0; $i < count($states_array); $i++){ 
        if (strtolower($input) == strtolower($states_array[$i])){ 
            $found = 1; 
            $output = $abb_array[$i]; 
            return $output; 
        } 
    } 
    if ($found != 1){ 
        $output = $input; 
        return $output; 
    } 
    return $output; 
}  
// TODO: Do something with the results
// Class for passing the APIToken
class AuthHeader
{
	var $APIToken;

	function __construct()
	{

	}
}

function get_post_by_title($page_title) {
    global $wpdb;
        $id = $wpdb->get_results($wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type='post'", $page_title )); //you can change the post type here to just look in CPTs or pages. Default is regular posts.
        if ($id)
            return $id[0]->ID;
        else 
            return null;
}


/*Trainer Filter*/
add_filter( 'manage_edit-trainers_post_columns', 'edittrainers_post_columns' ) ;

function edittrainers_post_columns( $columns ) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Trainer Name' ),
		'date' => __( 'Date' )
	);

	return $columns;
}


add_filter( 'manage_edit-trainers_post_columns', 'trainers_post_columns', 10, 1 );
function trainers_post_columns( $columns ) {
 	$column_thumbnail = array( 'thumbnail' => 'Thumbnail' );
	$column_position = array( 'office' => 'Position' );
	$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
	$columns = array_slice( $columns, 0, 3, true ) + $column_position + array_slice( $columns, 3, NULL, true );
	return $columns;
}



add_action( 'manage_posts_custom_column', 'my_column_action', 10, 1 );
function my_column_action( $column ) {
	global $post;
	
	//Custom list Trainer
	$model_pic = get_post_custom_values('trainer_0_photo');
	$image = wp_get_attachment_image_src(($model_pic[0]),'full');
	$trainer_office = get_post_custom_values('trainer_0_office');

	switch ( $column ) {
		case 'thumbnail':
			if ($model_pic[0]) {
				echo '<img src="' . $image[0] . '" width="95" height="95"/>';
				break;
			} else {
				echo '<img src="' .get_bloginfo('template_directory'). '/images/no_image.jpg" width="95" height="95"/>';
				break;
			}
		case 'office':
			if($trainer_office[0]){
				echo $trainer_office[0];	
				break;
			}else{
				echo " - ";	
				break;
			}
	}
	
}

// Register the column as sortable
function position_register_sortable( $columns ) {
    $columns['office'] = 'office';

    return $columns;
}
add_filter( 'manage_edit-trainers_post_sortable_columns', 'position_register_sortable' );


//Change Trainer Title Label Placeholder
function trainer_title_label($label, $post){

    if($post->post_type == 'trainers_post')
        $label= __('Enter trainer name....');

    return$label;
}
add_filter('enter_title_here', 'trainer_title_label', 2, 2);



//Home Page Company Logo
add_filter( 'manage_edit-home-company_columns', 'home_company_columns', 10, 1 );
function home_company_columns( $columns ) {
 	$column_thumbnail = array( 'company_logo' => 'Thumbnail' );
	$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
	return $columns;
}

add_action( 'manage_posts_custom_column', 'home_page_company_logo', 10, 1 );
function home_page_company_logo( $column ) {
	global $post;
	
	//Company Logo
	$model_pic = get_post_custom_values('home_company_block_0_home_company_imageone');
	$image = wp_get_attachment_image_src(($model_pic[0]),'full');
	switch ( $column ) {
		case 'company_logo':
		if ($model_pic) {
			echo '<img src="' . $image[0] . '" width="125" height="125"/>';
			break;
		}else {
			echo '<div style="width:123px;height:186px;background:rgba(0,0,0,.05);line-height:188px;text-align:center;border:1px solid rgba(0,0,0,.04);"><span style="padding:0px 15px;word-wrap: normal !important;">No Photo</span></div>';
		}
		
		
	}
	
}


//Add Custom Class To Edit Comment Link
/*function myclass_edit_comment_link( $output ) {
  $myclass = 'myclass';
  return preg_replace( '/comment-edit-link/', 'comment-edit-link', $output, 1 );
}
add_filter( 'edit_comment_link', 'myclass_edit_comment_link' );
*/
add_filter( 'edit_comment_link', 'addRelinEdit' );
function addRelinEdit( $link ) {
	$link = str_replace( '">', '" rel="comment">', $link);
	return $link;
}


function addRelToRemove($link){
  return str_replace('rel="nofollow"', "rel='comment'", $link);
  //$link = str_replace( "rel='nofollow'", '" rel="comment">', $link);
//	return $link;
}
add_filter('cancel_comment_reply_link', 'addRelToRemove', 420, 4);

//Add Custom Class To Reply Comment Link
function remove_nofollow($link, $args, $comment, $post){
  return str_replace("rel='nofollow'", "rel='comment'", $link);
}

add_filter('comment_reply_link', 'remove_nofollow', 420, 4);

//ADD BLOG AUTHOR
 /*function add_author_col($columns) {
   return array_merge($columns, 
		  array(
		  	'author' => __('Author'),
		)
	);
	return $columns;
}
add_filter('manage_blog_posts_columns' , 'add_author_col');*/

add_filter( 'manage_edit-blog_columns', 'blog_author', 10, 1 );
function blog_author( $columns ) {
 	$column_author = array( 'author' => 'Author' );;
	$columns = array_slice( $columns, 0, 2, true ) + $column_author + array_slice( $columns, 2, NULL, true );
	return $columns;
}


//Add ToolBox Resource Taxanomy
add_action( 'init', 'create_toolbox_resources_taxonomies', 0 );
function create_toolbox_resources_taxonomies() {
	$labels = array(
		'name'              => _x( 'ToolBox Resource', 'taxonomy general name' ),
		'singular_name'     => _x( 'ToolBox Resources', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Resources' ),
		'all_items'         => __( 'All Resources' ),
		'parent_item'       => __( 'Parent Resources' ),
		'parent_item_colon' => __( 'Parent Resources:' ),
		'edit_item'         => __( 'Edit Resources' ),
		'update_item'       => __( 'Update Resources' ),
		'add_new_item'      => __( 'Add New Resources' ),
		'new_item_name'     => __( 'New Resources Name' ),
		'menu_name'         => __( 'ToolBox Resources' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'toolbox_resources' ),
	);

	register_taxonomy( 'toolbox_resources', array( 'toolbox' ), $args );
	
}


//Change Join Us Post Title Label Placeholder
function career_title_label($label, $post){

    if($post->post_type == 'careers')
        $label= __('Enter career name....');

    return$label;
}
add_filter('enter_title_here', 'career_title_label', 2, 2);

//Change Join Us Post Title Label Placeholder
function partner_title_label($label, $post){

    if($post->post_type == 'partners')
        $label= __('Enter partner name....');

    return$label;
}
add_filter('enter_title_here', 'partner_title_label', 2, 2);

//Change Policies Title Label Placeholder
function policies_title_label($label, $post){

    if($post->post_type == 'policies')
        $label= __('Enter policy title name....');

    return$label;
}
add_filter('enter_title_here', 'policies_title_label', 2, 2);


//Add Ranking Column to Bios
add_filter( 'manage_edit-trainers_post_columns', 'bios_columns', 10, 1 );
function bios_columns( $columns ) {
 	$column_rankingcore = array( 'rankingcore' => 'Core Team Ranking' );
	$column_rankingcon = array( 'rankingcon' => 'Consultant Ranking' );
	$column_rankingtrainer = array( 'rankingtrain' => 'Trainer Ranking' );
	
	$column_category = array( 'category' => 'Team Categories' );
	$columns = array_slice( $columns, 0, 4, true ) + $column_rankingcore + array_slice( $columns, 4, NULL, true );
	$columns = array_slice( $columns, 0, 5, true ) + $column_rankingcon + array_slice( $columns, 5, NULL, true );
	$columns = array_slice( $columns, 0, 6, true ) + $column_rankingtrainer + array_slice( $columns, 6, NULL, true );
	$columns = array_slice( $columns, 0, 7, true ) + $column_category + array_slice( $columns, 7, NULL, true );
	return $columns;
}

add_action( 'manage_posts_custom_column', 'bios_ranking_column', 10, 1 );
function bios_ranking_column( $column ) {
	global $post;
	
	//Ranking
	$rankingcore = get_field('bios_coreteam_ranking');
	$rankingcon = get_field('bios_consultant_ranking');
	$rankingtrainer = get_field('bios_trainer_ranking');
	
	$category = get_field('bios_team_categories');
	
	switch ( $column ) {
		case 'rankingcore':
		if ($rankingcore) {
			echo $rankingcore;
			break;
		}else {
			echo '-';
			break;
		}
		
		case 'rankingcon':
		if ($rankingcon) {
			echo $rankingcon;
			break;
		}else {
			echo '-';
			break;
		}
		
		case 'rankingtrain':
		if ($rankingtrainer) {
			echo $rankingtrainer;
			break;
		}else {
			echo '-';
			break;
		}
		
		
		case 'category':
		if ($category) {
			$prefix = '';
			foreach($category as $value){
				//$vals .= $prefix . '' . $value . '';
    			
				
				if($value == 1){ 
					echo $prefix . "Our Core Team";
				}
				if($value == 2){ 
					echo $prefix . "Our Consultants";
				}
				if($value == 3){ 
					echo $prefix . "Our Trainers";
				}
				$prefix = '<br> ';
			}
			
			break;
		}else {
			echo '<font style="color: #ff0000; font-weight: Bold;"> -  </font>';	
			break;
		}
		
		
	}
	
}


//Search filter
/*function toolbox_archive_where($where, $args){  
	$post_type  = isset($args['post_type'])  ? $args['post_type']  : 'toobox';  
	$where = "WHERE post_type = '$post_type' AND post_status = 'publish'";
	return $where;  
}
add_filter( 'getarchives_where','toolbox_archive_where',10,2);
*/
// Show posts of 'post', and 'news' post types on archive page
/*add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

function add_my_post_types_to_query( $query ) {
    if ( is_archive() && $query->is_main_query() )
        $query->set( 'post_type', array('toolbox' ) );
    return $query;
}*/

function template_toolbox($template)   
{    
  global $wp_query;   
  $post_type = get_query_var('post_type');   
  if( $wp_query->is_search && $post_type == 'toolbox' )   
  {
    return locate_template('content-toolbox_search.php');  //  redirect to archive-search.php
  }   
  return $template;   
}
add_filter('template_include', 'template_toolbox');








//Filter search
/*function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type', array('post','toolbox', 'blog'));
    }

	return $query;
}

add_filter('pre_get_posts','searchfilter');*/

function toolbox_page( $query ) {
    if ( $query->is_archive('toolbox') ) {
        set_query_var('posts_per_page', 10);
    }
}
add_action( 'pre_get_posts', 'toolbox_page' );


//SOCIAL SHARE CUSTOMIZE BY TERRANCE
/** Get tweet count from Twitter API (v1.1) */

function ds_post_tweet_count( $post_id ) {
 
  // Check for transient
  if ( ! ( $count = get_transient( 'ds_post_tweet_count' . $post_id ) ) ) {
 
    // Do API call
    $response = wp_remote_retrieve_body( wp_remote_get( 'https://cdn.api.twitter.com/1/urls/count.json?url=' . urlencode( get_permalink( $post_id ) ) ) );
 
    // If error in API call, stop and don't store transient
    if ( is_wp_error( $response ) )
      return 'error';
 
    // Decode JSON
    $json = json_decode( $response );
 
    // Set total count
    $count = absint( $json->count );
 
    // Set transient to expire every 30 minutes
    set_transient( 'ds_post_tweet_count' . $post_id, absint( $count ), 30 * MINUTE_IN_SECONDS );
 
  }
 
 return absint( $count );
 
}  /** Twitter End */
 
 
/** Get like count from Facebook FQL  */

function ds_post_like_count( $post_id ) {
 
  // Check for transient
  if ( ! ( $count = get_transient( 'ds_post_like_count' . $post_id ) ) ) {
 
    // Setup query arguments based on post permalink
    $fql  = "SELECT url, ";
    //$fql .= "share_count, "; // total shares
    //$fql .= "like_count, "; // total likes
    //$fql .= "comment_count, "; // total comments
    $fql .= "total_count "; // summed total of shares, likes, and comments (fastest query)
    $fql .= "FROM link_stat WHERE url = '" . get_permalink( $post_id ) . "'";
 
    // Do API call
    $response = wp_remote_retrieve_body( wp_remote_get( 'https://api.facebook.com/method/fql.query?format=json&query=' . urlencode( $fql ) ) );
 
    // If error in API call, stop and don't store transient
    if ( is_wp_error( $response ) )
      return 'error';
 
    // Decode JSON
    $json = json_decode( $response );
 
    // Set total count
   // $count = absint( $json[0]->total_count );
     $count = absint( $json->count );
 
    // Set transient to expire every 30 minutes
    set_transient( 'ds_post_like_count' . $post_id, absint( $count ), 30 * MINUTE_IN_SECONDS );
 
  }
 
 return absint( $count );
 
} /** Facebook End  */


/** Get share count from Google Plus */

function ds_post_plusone_count($post_id) {

	// Check for transient
	if ( ! ( $count = get_transient( 'ds_post_plus_count' . $post_id ) ) ) {
     
	    $args = array(
	            'method' => 'POST',
	            'headers' => array(
	                // setup content type to JSON 
	                'Content-Type' => 'application/json'
	            ),
	            // setup POST options to Google API
	            'body' => json_encode(array(
	                'method' => 'pos.plusones.get',
	                'id' => 'p',
	                'method' => 'pos.plusones.get',
	                'jsonrpc' => '2.0',
	                'key' => 'p',
	                'apiVersion' => 'v1',
	                'params' => array(
	                    'nolog'=>true,
	                    'id'=> get_permalink( $post_id ),
	                    'source'=>'widget',
	                    'userId'=>'@viewer',
	                    'groupId'=>'@self'
	                ) 
	             )),
	             // disable checking SSL sertificates               
	            'sslverify'=>false
	        );
	     
	    // retrieves JSON with HTTP POST method for current URL  
	    $json_string = wp_remote_post("https://clients6.google.com/rpc", $args);
	     
	    if (is_wp_error($json_string)){
	        // return zero if response is error                             
	        return "0";             
	    } else {        
	        $json = json_decode($json_string['body'], true);                    
	        // return count of Google +1 for requsted URL
	        $count = intval( $json['result']['metadata']['globalCounts']['count'] ); 
	    }
	    
	    // Set transient to expire every 30 minutes
		set_transient( 'ds_post_plus_count' . $post_id, absint( $count ), 30 * MINUTE_IN_SECONDS );
	    
	}
 
	return absint( $count );    
} /** Google Plus End */


/** Get Flattr count */

//function ds_post_flattr_count( $post_id ) {
// 
//  // Check for transient
//  if ( ! ( $count = get_transient( 'ds_post_flattr_count' . $post_id ) ) ) {
// 
//    // Check if URL exists
//    $response = wp_remote_retrieve_body( wp_remote_get( 'https://api.flattr.com/rest/v2/things/lookup/?url=' . urlencode( get_permalink( $post_id ) ) ) );
// 
//    // Decode JSON
//    $json = json_decode( $response );
// 
//	// Get URL ID
//	$message = $json->message;
//	
//	if ($message == "not_found") {
//      return 0;
//	}
//	
//	else {
//		$location = $json->location;
//		$flattrs = $json->flattrs;
//		$count = $flattrs;
//	}
//	
//	// Set transient to expire every 30 minutes
//	set_transient( 'ds_post_flattr_count' . $post_id, absint( $count ), 30 * MINUTE_IN_SECONDS );
// 
//  }
// 
// return absint( $count );
// 
//} /** Flattr End */


/** Get Count from LinkedIn */

function ds_post_linkedin_count( $post_id ) {
 
  // Check for transient
  if ( ! ( $count = get_transient( 'ds_post_linkedin_count' . $post_id ) ) ) {
 
    // Do API call
    $response = wp_remote_retrieve_body( wp_remote_get( 'https://www.linkedin.com/countserv/count/share?url=' . urlencode( get_permalink( $post_id ) ) . '&format=json' ) );
 
    // If error in API call, stop and don't store transient
    if ( is_wp_error( $response ) )
      return 'error';
 
    // Decode JSON
    $json = json_decode( $response );
 
    // Set total count
    $count = absint( $json->count );
 
    // Set transient to expire every 30 minutes
    set_transient( 'ds_post_linkedin_count' . $post_id, absint( $count ), 30 * MINUTE_IN_SECONDS );
 
  }
 
 return absint( $count );

}  /** LinkedIn End */

 
/** Markup for Social Media Icons */

function ds_social_media_icons() {
	
  // Get the post ID
  $post_id = get_the_ID(); ?>
 
  <div class="social-icons-wrap">
	<ul class="social-icons">
		<!-- Facebook Button-->
		<li class="social-icon facebook">
			<a onclick="javascript:popupCenter('https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&amp;appId=','Facebook Share', '540', '400');return false;" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&amp;appId=LitheSpeed" target="blank"><i class="fa fa-facebook"></i>&nbsp;&nbsp;&nbsp;&nbsp;</a><span class="share-count"><?php echo ds_post_like_count( $post_id ); ?></span>
		</li>
		<!-- Twitter Button -->
		<li class="social-icon twitter">
			<a onclick="javascript:popupCenter('https://twitter.com/share?&amp;url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&amp;via=litheSpeed','Tweet', '540', '400');return false;" href="https://twitter.com/share?&amp;url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&amp;via=litheSpeed" target="blank"><i class="fa fa-twitter"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><span class="share-count"><?php echo ds_post_tweet_count( $post_id ); ?></span>
		</li>
		<!-- Google + Button-->
		<li class="social-icon google-plus">
			<a onclick="javascript:popupCenter('https://plus.google.com/share?url=<?php the_permalink(); ?>','Share on Google+', '600', '600');return false;" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="blank"><i class="fa fa-google-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;</a><span class="share-count"><?php echo ds_post_plusone_count( $post_id ); ?></span>
		</li>
		<!-- Flattr Button-->
		<!--<li class="social-icon flattr">
			<a onclick="javascript:popupCenter('https://flattr.com/submit/auto?user_id=XXX_YOUR_FLATTR_ID&url=< ?php echo urlencode(get_permalink($post->ID)); ?>&title=< ?php echo rawurlencode(strip_tags(get_the_title())) ?>&language=de_DE&&category=text','Support with Flattr', '680', '400');return false;" href="https://flattr.com/submit/auto?user_id=XXX_YOUR_FLATTR_ID&url=< ?php echo urlencode(get_permalink($post->ID)); ?>&title=< ?php echo rawurlencode(strip_tags(get_the_title())) ?>&language=de_DE&&category=text" target="blank" rel="nofollow">Flattr</a><span class="share-count">< ?php echo ds_post_flattr_count( $post_id ); ?></span>
		</li>-->
		<!-- LinkedIn Button -->
		<li class="social-icon linkedin">
			<a onclick="javascript:popupCenter('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>&amp;source=<?php site_url(); ?>','Share on LinkedIn', '520', '570');return false;" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>&amp;source=<?php site_url(); ?>" target="blank"><i class="fa fa-linkedin"></i>&nbsp;&nbsp;&nbsp;&nbsp;</a><span class="share-count"><?php echo ds_post_linkedin_count( $post_id ); ?></span>
		</li>
	</ul>
  </div><!-- .social-icons-wrap -->
 
<?php 

}

?>


