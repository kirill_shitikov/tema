<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
    <script>
        var template_directory = "<?php echo get_template_directory_uri(); ?>";
        var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
    </script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?php if(get_field("site_favicon","options")): ?>
        <link rel="icon" type="image/png" href="<?php the_field("site_favicon","options"); ?>" />
    <?php endif; ?>

    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    <!--<script type="text/javascript" src="< ?php echo site_url();?>/wp-includes/js/comment-reply.js">-->

    <?php wp_head(); ?>
    <?php
    $url = site_url();
    $tokens = explode('/', $url);
    ?>
    <? //=$tokens[sizeof($tokens)-1]?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js"></script>
    <script src="<?php echo get_template_directory_uri().'/assets/js/device.min.js' ?>"></script>
    <!--<script type="text/javascript" src="< ?=get_template_directory_uri()?>/assets/js/script.js"></script>-->
    <!-- recaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head> <!--class="no-sidebar" class="intro-done sidebar-open is-ready" style="width: 1926px;"-->
<input type="hidden" name="siteUrl" id="siteUrl" value="<?=$url;?>">
<input type="hidden" name="urlElement" id="urlElement" value="<?=$tokens[sizeof($tokens)-1]?>">
<!--<body class="no-sidebar" style="width: 1926px;">
<div id="container" class="app-started">-->
