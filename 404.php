<?php get_header(); ?>
<?php wp_reset_postdata(); ?>
<?php $featuredImageType = get_field('background_image_type', 'options'); 
?>


<main class="content defaultPage <?php echo $featuredImageType; ?>"> 
		<section class="pageImage">
			<?php if($featuredImageType == "bgImg"): ?>	
				<div class="fullBg"></div>
			<?php elseif($featuredImageType == "headerImg"): ?>
				<div class="um_parallax" data-velocity="-.1"></div>
			<?php endif; ?>
		</section>
	
        <div class="container">
            <div class="row">
                <section class="pageTitle text-center">
                    <div class="um_helper">
                    	<div class="um_middle">
                    		<h3 class="title pageTitle titleUp"></h3>
                    	</div>
                    </div>
                </section>
				
				<section class="fourOfour mainColor text-center">
					<div class="um_error">
						<h3>ERROR</h3>
						<h1>404</h1>
					</div>
				
					<div class="um_error_contnet">
						<?php the_field('404_content','options'); ?>
						
					</div>	
					
					<?php if(get_field('back_button','options')): ?>
					<a href="<?php echo home_url(); ?>" class="btn white"><?php the_field('back_button_text','options'); ?></a>
					<?php endif; ?>
				</section>
		
            </div>
            <!--row-->
        </div>
    </main>

    <?php get_footer(); ?>