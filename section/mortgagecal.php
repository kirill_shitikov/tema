<div class="mortgageContainer">
	<div class="calTitle">Mortgage Calculator</div>
    <div class="sellPrice">
    	<div class="pLabel">Selling Price</div>
        <input name="sellprice" type="text" value="" id="sellP">
    </div>
    <div class="downPayment">
    	<div class="downLabel">Down Payment</div>
        <input name="downpayment" type="text" value="" id="downP">
    </div>
    <div class="interestRate">
    	<div class="interestLabel">Interest Rate</div>
        <input name="interest" type="text" value="" id="intR" maxlength="6">
    </div>
    <div class="year">
    	<div class="downLabel">Years</div>
        <input name="year" type="text" value="" id="yearP" maxlength="2">
    </div>
    <div class="monthPayment">
    	<div class="monthLabel">Monthly Payment</div>
        <input name="month" type="text" value="" id="monthP" disabled>
    </div>
</div>
<?php
/*$Path= $_SERVER['REQUEST_URI'];
$parts = explode("/", $Path);

echo $parts[2];*/
?>