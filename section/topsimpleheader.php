<?php get_header(); ?>
<?php
$topHeaderBg = wp_get_attachment_image_src((get_field('agent_topheaderbg', 'options')),'full');
$agentLogo = wp_get_attachment_image_src((get_field('agent_logo', 'options')),'full');
$agentImg = wp_get_attachment_image_src((get_field('agent_image', 'options')),'full');
$agentName = get_field('agent_name', 'options');
$compContact = get_field('mail_form_contact', 'options');
$compMail = get_field('mail_form_email', 'options');
?>
<?php
$contArr = array();
preg_match_all('/[0-9]+/',get_field('mail_form_contact', 'options'),$contArr);
$compContact2 = implode($contArr[0]);
?>

<div class="container-fluid topsimpleheader">
	<div class="container smallScreen">
		<div class="row">
			<div class="col-md-10 col-xs-8 topsimpleagentname"><?=$agentName;?></div>
			<div class="col-md-2 col-xs-4 topsimplerighticon">
				<a id = "movetophone" href="tel:<?=$compContact2?>" style="cursor: default; text-decoration: none">
					<!--<span class="glyphicon roundBorder"><img src="< ?php bloginfo('template_url')?>/images/phonesymbol.png" /></span>-->
                    <span class="glyphicon"><img src="<?php bloginfo('template_url')?>/images/phoneicon.png" width="40" height="34"/></span>
				</a>
				<a id = "movetomail" href="mailto:<?=$compMail?>" style="cursor: default; text-decoration: none">
					<span class="glyphicon"><img src="<?php bloginfo('template_url')?>/images/mail2.png"  width="40" height="32"/></span>
				</a>
			</div>
		</div>
	</div>
</div>      