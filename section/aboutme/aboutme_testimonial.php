<?php get_header(); ?>
<?php
$sectionBg = wp_get_attachment_image_src((get_field('aboutme_testsectionbg', 'options')),'full');
$tesTitle = get_field('aboutme_testitle', 'options');


wp_reset_postdata(); 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$testimonialPost =  array(
	'showposts'		  => -1,
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'post_type'       => 'testimonials',
	'paged'			  => $paged
);
?>
<div class="container-fluid testimonialSection content-hidden" id="main-view">
    <div class="testimonialBg" style="background-image: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('<?=$sectionBg[0];?>')"></div>
	<div class="container testimonialContainer smallScreen">
    	<div class="row">
            <div class="col-md-12 testimonialTitle"><?=$tesTitle;?></div>
        </div>
        
        <div class="row">
        	<div id="posts-carousel-wrap2">
                <ul id="posts-carousel2" class="bxslider col-md-4">
    		<?php
               query_posts($testimonialPost);
			   
                 if (have_posts())  {
                    while (have_posts()) : the_post();
              			$content = get_the_content();
						$custname = get_post_custom_values('custest_custname');
            ?>
            
            	<li class="bx-clone col-md-12">
                    <article class="posts-carousel-article2">
                    	<?=truncate($content, $length = 300); ?>
                    </article>
                    <div class="col-md-12 testCustName">
						<?=$custname[0];?>
                    </div>
                </li>
                
                
				
             
            <?php 
				endwhile; 
				}
			?>
            </ul>   
            <div class="bx-controls bx-has-controls-direction"></div>         
            </div>          
         </div>
         
        
        </div>
        
       
    </div>
</div>   
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/assets/flexslider/bxslider.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/assets/flexslider/equal-heights.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/assets/flexslider/posts-carousel-init.js"></script>