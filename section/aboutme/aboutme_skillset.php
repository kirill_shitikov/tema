<?php 
get_header(); 
$title = get_field('aboutme_skilltitle', 'options');
$desc = get_field('aboutme_skilldesc', 'options');
$imgUrl = get_field('aboutme_skillimgurl', 'options');
$image = wp_get_attachment_image_src((get_field('aboutme_skillimg', 'options')),'full');
?>
<div class="container-fluid skillsetPageSection content-hidden" id="main-view">
	<div class="container skillsetPageContainer smallScreen">
    	<div class="row bottomPadding">
        	<div class="col-md-12 col-xs-12 skillsettitle"><?=$title;?></div>
        </div>
        <div class="row">
        	<div class="col-md-12 col-xs-s skillsetdesc"><?=$desc;?></div>
        </div>
        <div class="row">
        	<div class="col-md-6 col-xs-12 skillsetlist">
            	<ul>
				<?php
					$repeater = get_field('aboutme_skillrepeater','options');
					foreach($repeater as $skill) { 
						$listing = $skill['aboutme_skilllist'];
				?>  
                	<li><?=$listing;?></li>
                	
                <?php  } ?> 
                </ul>         
            </div>
            <div class="col-md-6 col-xs-12 skillsetimg">
				<a href="<?=$imgUrl;?>" title="<?=$imgUrl;?>"><img src="<?=$image[0];?>" /></a>                
            </div>
        </div>
    </div>
</div>    