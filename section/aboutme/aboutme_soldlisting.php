<?php 
get_header(); 
$title = get_field('aboutme_soldtitle', 'options');
?>
<div class="container-fluid soldproPageSection content-hidden" id="main-view">
	<div class="container soldproPageContainer smallScreen" id="soldproPageContainer">
    	<div class="row">
        	<div class="col-md-12 soldtitle"><?=$title;?></div>
            <div class="col-md-12" id="soldl">
            	<div id="loadingFunction" class="col-md-12 loadingShow">
                  <img id="loading-image" src="<?php bloginfo('template_url')?>/images/ajax-loader.gif" alt="Loading..."/>
                </div>
            </div>
    		<!--<iframe  id="soldListings" class="noScrolling" src="< ?php echo site_url();?>/sold-featured-listing/" scrolling="yes" frameborder="0" width="100%" height="0"></iframe>-->
        </div>    
    </div>
</div>    