<?php 
get_header(); 
$agentName = get_field('aboutme_agentname', 'options');
$agentAdd = get_field('aboutme_agentadd', 'options');
$agentContact = get_field('aboutme_agentcontact', 'options');
$agentEmail = get_field('aboutme_agentemail', 'options');
$aboutme_agentinfo = get_field('aboutme_agentinfo', 'options');

$agentImage = wp_get_attachment_image_src((get_field('aboutme_agentimg', 'options')),'full');
$agentBadge = wp_get_attachment_image_src((get_field('aboutme_agentbadge', 'options')),'full');

$display = get_field('aboutme_imgvideo', 'options');
$youtubeId = get_field('aboutme_agentyoutube', 'options');
$vimeoId = get_field('aboutme_agentvimeo', 'options');

$btnNewName = get_field('button_name_aboutmetab', 'options');
$btnNewUrl = get_field('button_url_aboutmetab', 'options');

$socialMedia = get_field('aboutme_social_media','options');

?>
<div class="container-fluid aboutmePageSection content-hidden" id="main-view">
	<div class="container aboutmePageContainer smallScreen">
    	<div class="row topbottomPadding">
        	<div class="col-md-8">
				<?php
					if($display == 2 && $youtubeId !='' && $youtubeId != '#'){ //Youtube Video
					
				?>
                	<div class="col-md-12 agentVideo">
						<iframe src="https://www.youtube.com/embed/<?=$youtubeId?>" width="100%" height="350" frameborder="0"  allowfullscreen></iframe>
					</div>
				<?php }else if($display == 3 && $vimeoId !='' && $vimeoId != '#'){ //Vimeo Video?>
					<div class="col-md-12 agentVideo">
						<iframe src="https://player.vimeo.com/video/<?=$vimeoId?>" width="100%" height="350" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
				<?php }else if($display == 1 && $agentImage !=''){?>
						<div class="col-md-12 agentImg">
							<img src="<?=$agentImage[0];?>" />
						</div>
				<?php }?>
            </div>
            <div class="col-md-4">
				<div class="col-md-12 col-xs-12 agentName" style="font-weight: bold;"><?=$agentName;?></div>
                <div class="col-md-12 col-xs-12">
                	<?php
					$i = 0;
					foreach($socialMedia as $icon) {
						$image_icon = wp_get_attachment_image_src(($icon['aboutme_socialmedia_img']),'full');
						$image_icon_hover = wp_get_attachment_image_src(($icon['aboutme_socialmedia_hoverimg']),'full');
						$activateSM = $icon['aboutme_activate_socialmedia'];
					?>
                    <?php if($activateSM == 1){ ?>
						<a class="soc <?php echo 'soc'.$i; ?>" target="_blank" href="<?= $icon['aboutme_social_mediaurl'] ?>"></a>
                    <?php } ?>
					<style>
						.<?php echo'soc'.$i;?>{
							background: url(<?= $image_icon[0] ?>) center center no-repeat; transition: background 0.3s ease; -webkit-transition: background 0.3s ease; -o-transition: background 0.3s ease; -moz-transition: background 0.3s ease;;
						}
						.<?php echo'soc'.$i;?>:hover{
							background: url(<?= $image_icon_hover[0] ?>) center center no-repeat; transition: background 0.3s ease; -webkit-transition: background 0.3s ease; -o-transition: background 0.3s ease; -moz-transition: background 0.3s ease;
							
						}
					</style>
					<?php $i++; } ?>
                </div>
                <div class="col-md-12 col-xs-12 agentAdd"><?=$agentAdd;?></div>
                <div class="col-md-12 col-xs-12 col-sm-4 agentContact"><span class="glyphicon roundBorderBlack"><img src="<?php bloginfo('template_url')?>/images/phonesymbol_black.png" /></span> <?=$agentContact;?></div>
                <div class="col-md-12 col-xs-12 col-sm-8 agentEmail"><span class="glyphicon"><img src="<?php bloginfo('template_url')?>/images/blackmail.png" width="40" height="32"/></span> <?=$agentEmail;?></div>
                <div class="col-md-12 agentBadge"><img src="<?=$agentBadge[0];?>" /></div>
            </div>
        </div>
        
        <div class="row topbottomPadding">
        	<div class="col-md-12 agentDesc"><?=$aboutme_agentinfo;?></div>
        </div>

		<style>
			#imagebutton{
				background-color: #d9d9d4;
				border-radius: 5px;
				color: #000;
				display: inline-block;
				font-family: "Futura W02";
				font-size: 18px;
				font-weight: 400;
				height: 50px;
				line-height: 50px;
				text-align: center;
				text-decoration: none;
				text-transform: uppercase;
				transition: all 0.3s ease 0s;
				padding-left: 10px;
				padding-right: 10px;
				-webkit-transition: width 2s, height 4s; /* For Safari 3.1 to 6.0 */
				transition: width 2s, height 4s;
			}
			#imagebutton:hover{
				text-decoration: none;
				background-color: #000;
				color: #fff;
				-webkit-transition: width 2s, height 4s; /* For Safari 3.1 to 6.0 */
				transition: width 2s, height 4s;
			}
		</style>

		<div class="row rowBtnMore" style="text-align: center;">
			<div class="col-md-12 btnMoreList" >
				<a href="<?=$btnNewUrl;?>" id="imagebutton" >
					<?=$btnNewName;?>
				</a>
			</div>
		</div>
    </div>
</div>    