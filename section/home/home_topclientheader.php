<?php get_header(); ?>
<?php
$topHeaderBg = wp_get_attachment_image_src((get_field('agent_topheaderbg', 'options')),'full');
$agentLogo = wp_get_attachment_image_src((get_field('agent_logo', 'options')),'full');
$agentImg = wp_get_attachment_image_src((get_field('agent_image', 'options')),'full');
$agentName = get_field('agent_name', 'options');
$compContact = get_field('mail_form_contact', 'options');
$compMail = get_field('mail_form_email', 'options');
?>
<?php
$contArr = array();
preg_match_all('/[0-9]+/',get_field('mail_form_contact', 'options'),$contArr);
$compContact2 = implode($contArr[0]);
?>
<div class="container-fluid topclientheader content-hidden" id="main-view" xmlns="http://www.w3.org/1999/html">
    <div class="topclientheaderbg" style="background-image: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('<?=$topHeaderBg[0];?>')"></div>
    <div class="container smallScreen">
        <div class="row topHeaderContent">
            <div class="col-md-2 col-xs-4 topAgentPic">
                <img src="<?=$agentImg[0];?>" class="agentPic"/>
            </div>
            <div class="col-md-10 col-xs-8 topHeaderInfo">
                <div class="row topAgentName"><?=$agentName;?></div>
                <div class="row topAgentContact">
                    <a id = "movetophonesmall" href="tel:<?=$compContact2?>" style="text-decoration: none"><span class="glyphicon roundBorder"><img src="<?php bloginfo('template_url')?>/images/phonesymbol.png" /></span>
                    </a>
                    <?=$compContact;?></div>
                <div class="row topAgentEmail">
                    <a id = "movetomailsmall" href="mailto:<?=$compMail?>" style="text-decoration: none">
                        <span class="glyphicon"><img src="<?php bloginfo('template_url')?>/images/mail2.png" width="24" height="20"/></span>
                    </a>
                    <?=$compMail;?></div>

            </div>
        </div>
    </div>
</div>  