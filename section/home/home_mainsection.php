<?php get_header(); ?>
<?php
$verifyLogo = wp_get_attachment_image_src((get_field('about_verified_img', 'options')),'full');
$title = get_field('about_header_title', 'options');
$desc = get_field('about_description', 'options');
$btnMLabel = get_field('about_more_btnlbl', 'options');
$btnMURL = get_field('about_more_btnurl', 'options');
$btnCLabel = get_field('about_contact_btnlbl', 'options');
$btnCURL = get_field('about_contact_btnurl', 'options');
$quickSearchShortCode = get_field('about_quick_shortcode', 'options');


$btnNewName = get_field('new_button_name', 'options');
$btnNewUrl = get_field('new_button_url', 'options');
$btnBackground = wp_get_attachment_image_src((get_field('new_button_background', 'options')),'full');

?>
<div class="container-fluid aboutSection content-hidden" id="main-view">
	<div class="container sectionPaddingTopBottom smallScreen">
    	<!-- Starting Columns -->
        <div class="col-md-8 colPaddingAll">
        	<div class="row rowPaddingTopBottom">
            	<div class="col-md-2 col-xs-12 col-sm-2 aboutVerifyLogo">
        			<img src="<?=$verifyLogo[0];?>"/>
                </div>
                <div class="col-md-10  col-sm-10 aboutTitle">
                	<?=$title;?>
                </div>
            </div>

            <div class="row rowPaddingTopBottom" style="padding-bottom: 0px;">
                <div class="col-md-12 aboutDesc">
                    <?=$desc;?>
                </div>
            </div>

            <div class="row rowPaddingTopBottom" style="padding-top: 0px;">
                <div class="col-md-4 col-xs-12 col-sm-6 knowmoreDiv paddingRight">
                    <a href="<?=$btnMURL;?>" id="knowmoreBtn"><?=$btnMLabel;?></a>
                </div>
                <div class="col-md-8 col-xs-12 col-sm-6 contactDiv paddingRight">
                    <a href="<?=$btnCURL;?>" id="contactBtn" class="goMail"><?=$btnCLabel;?></a>
                </div>
            </div>


        </div>
        
        <!-- Quick Search Section -->
        <div class="col-md-4 colPaddingAll quickSection">
        	<div class="quickBg colPaddingAll">
            	<div class="quickTitle">I’m looking for…</div>
        		<?=$quickSearchShortCode;?>
            </div>
        </div>
        
        <!-- End of Columns -->
        <style>
            #imagebutton{
                background-image: url(<?=$btnBackground[0]?>);
                background-repeat:no-repeat;
                background-attachment:fixed;
                background-position:center;
                background-color: #d9d9d4;
                border-radius: 5px;
                color: #000;
                display: inline-block;
                font-family: "Futura W02";
                font-size: 18px;
                font-weight: 400;
                height: 50px;
                line-height: 50px;
                text-align: center;
                text-decoration: none;
                text-transform: uppercase;
                transition: all 0.3s ease 0s;
				padding-left: 10px;
				padding-right: 10px;
                /*width: 170px;*/
            }
            #imagebutton:hover{
                text-decoration: none;
                background-color: #000;
                color: #fff;
                -webkit-transition: all 0.3s;
                -moz-transition: all 0.3s;
                transition: all 0.3s;
            }
        </style>

        <div class="row rowPaddingTopBottom rowBtnMore" style="text-align: center;">
            <div class="col-md-12 btnMoreList" >
                <a href="<?=get_site_url(); ?>/<?=$btnNewUrl;?>" id="imagebutton" >
                    <?=$btnNewName;?>
                </a>
            </div>
        </div>
        
    </div>
</div>   
