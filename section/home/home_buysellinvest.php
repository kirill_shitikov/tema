<?php get_header(); ?>
<?php
$sectionBg = wp_get_attachment_image_src((get_field('bsi_bg', 'options')),'full');
$bsiDesc = get_field('bsi_description', 'options');

$bsiBuyLbl = get_field('bsi_buybtnlbl', 'options');
$bsiBuyUrl = get_field('bsi_buybtnurl', 'options');

$bsiSellLbl = get_field('bsi_sellbtnlbl', 'options');
$bsiSellUrl = get_field('bsi_sellbtnurl', 'options');

$bsiInvestLbl = get_field('bsi_investbtnlbl', 'options');
$bsiInvestUrl = get_field('bsi_investbtnurl', 'options');
?>
<div class="container-fluid buysellSection content-hidden" id="main-view">
    <div class="bsiBg" style="background-image: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('<?=$sectionBg[0];?>')"></div>
	<div class="container buySellContainer smallScreen">
    	<div class="row">
            <div class="col-md-12 bsiDesc">
                <?=$bsiDesc;?>
            </div>
        </div>
        
        <div class="row rowPaddingTopBottom">
            <div class="col-md-4 col-xs-12 col-sm-4 buyBtnSection">
                 <a href="<?=$bsiBuyUrl;?>" id="buyBtn"><?=$bsiBuyLbl;?></a>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-4 sellBtnSection">
                <a href="<?=$bsiSellUrl;?>" id="sellBtn"><?=$bsiSellLbl;?></a>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-4 investBtnSection">
                <a href="<?=$bsiInvestUrl;?>" id="sellBtn"><?=$bsiInvestLbl;?></a>
            </div>
        </div>
    </div>
</div>    