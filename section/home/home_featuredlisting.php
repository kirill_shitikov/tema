<?php 
get_header(); ?>
<?php
$title = get_field('homefea_title', 'options');
//$feaShortCode = get_field('homefea_shortcode', 'options');

$feaBtnLbl = get_field('homefea_btnlbl', 'options');
$feaBtnUrl = get_field('homefea_btnurl', 'options');
?>
<div class="container-fluid featuredSection content-hidden" id="main-view">
	<div class="container featuredContainer smallScreen">
    	<div class="row">
        	<div class="col-md-12 featuredTitle"><?=$title;?></div>
        </div>
        <div class="row">
            <div class="col-md-12" id="featContentHome">
            	<div id="loadingFunction" class="col-md-12 loadingShow">
                  <img id="loading-image" src="<?php bloginfo('template_url')?>/images/ajax-loader.gif" alt="Loading..."/>
                </div>
            </div>
        </div>
        <div class="row rowPaddingTopBottom rowBtnMore">
        	<div class="col-md-12 btnMoreList"><a href="<?=get_site_url(); ?>/<?=$feaBtnUrl;?>" id="morelistbtn"><?=$feaBtnLbl;?></a></div>
        </div>
    </div>
</div>    