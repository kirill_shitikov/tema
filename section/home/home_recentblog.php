<?php 
get_header(); 
wp_reset_postdata(); 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$featurepost =  array(
	'showposts'		  => -1,
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'post_type'       => 'blog',
/*	'meta_query' => array(
		array(
			'key' => 'blog_featured',
			'value' => 1,
			'compare' => 'LIKE'
		),
	),*/
	'paged'			  => $paged
);

$recentpost =  array(
	'showposts'		  => 5,
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'post_type'       => 'blog',
/*	'meta_query' => array(
		array(
			'key' => 'blog_recent',
			'value' => 1,
			'compare' => 'LIKE'
		),
	),*/
	'paged'			  => $paged
);
$recent_posts = wp_get_recent_posts( $recentpost, ARRAY_A );

$blogTitle = get_field('rblog_title', 'options');

$rblogBtnLbl = get_field('rblog_btnlbl', 'options');
$rblogBtnUrl = get_field('rblog_btnurl', 'options');
?>
<div class="container-fluid recentBlog content-hidden" id="main-view">
	<div class="container recentBlogContainer smallScreen">
    	<div class="row">
        	<div class="col-md-12 recentPostTitle"><?=$blogTitle;?></div>
        
        	<div id="posts-carousel-wrap">
                <ul id="posts-carousel" class="bxslider col-md-4 blogWrapper">
    		<?php
               query_posts($featurepost);
			   
                 if (have_posts())  {
                    while (have_posts()) : the_post();
              			$content = get_the_content();
						$blogImg = get_field('blog_post_image');
						$comments_count = wp_count_comments($post->ID);
						
						if($comments_count->approved > 1){
							$labelcomments = 'Comments';
						}else{
							$labelcomments = 'Comment';
						}
            ?>
            
            	<li class="bx-clones col-md-12 blogContent">
					<a href="<?php echo get_permalink(); ?>" rel="tab" class="posts-carousel-item">
                    	<div class="row">
                                <div class="col-md-12  recentBlogImage">
                                    <?php
                                        if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                            the_post_thumbnail();
                                        }
                                    ?>
                                </div>
                            </div>
                        <i class="overlay-plus"></i>
                    </a>
                    <article class="posts-carousel-article">
                    	 <div class="row rowBTitle">
                         	<div class="col-md-12 recentBlogTitle"><h2><a href="<?php echo get_permalink(); ?>" rel="tab" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2></div>
                            <div class="col-md-12 recentBlogDate"><?= get_post_time('F j, Y'); ?> by <?php the_author();?> | <?=$comments_count->approved;?> <?=$labelcomments;?></div>
                         </div>

                        <div class="row rowDTitle">
                            <div class="col-md-12 recentBlogDesc"><?=truncate($content, $length = 300); ?></div>
                        </div>
                    </article>
                </li>
				
             
            <?php 
				endwhile; 
				}
			?>
            	<!--<li class="col-md-12 blogContent">
                
                </li>-->
            </ul>
            <div class="bx-controls bx-has-controls-direction"></div>         
            </div>          
         </div>
         
         <div class="row rowPaddingTopBottom rowBtnBlog">
        	<div class="col-md-12 btnMoreBlog"><a href="<?=get_site_url(); ?>/<?=$rblogBtnUrl;?>" id="moreBlogbtn"><?=$rblogBtnLbl;?></a></div>
        </div>
    </div>
</div>   

<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/assets/flexslider/bxslider.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/assets/flexslider/equal-heights.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/assets/flexslider/posts-carousel-init.js"></script>

