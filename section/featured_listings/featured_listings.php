<?php 
get_header(); ?>
<?php
$title = get_field('fealist_title', 'options');
$fealistShortCode = get_field('fealist_shortcode', 'options');
?>
<div class="container-fluid featuredListSection content-hidden" id="main-view">
	<div class="container featuredListContainer smallScreen">
       
    	<div class="row rowPaddingTopBottom">
        	<div class="col-md-12 featuredTitle"><?=$title;?></div>
        </div>
        <div class="row">
        	<div class="col-md-12" id="featContent">
            	<div id="loadingFunction" class="col-md-12 loadingShow">
                  <img id="loading-image" src="<?php bloginfo('template_url')?>/images/ajax-loader.gif" alt="Loading..."/>
                </div>
            </div>
          <!--  <div class="col-md-12 featureFrame">
				< ?=$fealistShortCode;?>
            </div>-->
        </div>
    </div>
</div>    