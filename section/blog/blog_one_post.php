<?php get_header();
wp_reset_postdata();
?>

    <div id="blogContentContainer" style="margin-top: 90px">
        <div class="fblog_list center" style="width: 860px">
            <div class="center-block">
                <div class="title"><?php the_title(); ?></div>
                <div class="clearfix"></div>
                <div class="subtitle"><?= get_post_time('F j, Y'); ?> by <?php the_author();?></div>
                <div class="clearfix"></div>
                <div class="description">
                    <?php the_content(); ?>
                </div>
                <div class="clearfix"></div>
                <section class="singleComments mainBgColor">
                    <?php comments_template(); ?>
                </section>
                <?php
                $args = array(
                    'comment_notes_after' => '',
                );
                ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<?php get_template_part('blogfooter'); ?>