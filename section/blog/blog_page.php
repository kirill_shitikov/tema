<?php
wp_reset_postdata();

$blogTitle = get_field('blog_header_title', 'option');
$titlecolor = get_field('blog_header_titlecolor', 'option');
$titleshadowcolor = get_field('blog_header_titleshadowcolor', 'option');
?>
<?php
get_header();
$neighborUrl = get_field('neighbor_url', 'options');

?>
<style type="text/css">
<?php  if($titlecolor !=''){?>
#blogHeader .headerTitle{color: <?=$titlecolor;?> !important;} 
<?php } ?>

<?php  if($titleshadowcolor !=''){?>
#blogHeader .headerTitle{text-shadow: 0 4px 4px <?=$titleshadowcolor;?> !important;
}
<?php } ?>

</style>
<div class="row">
    <div>
        <div id="blogHeader">
            <div class="headerTitle"><?=$blogTitle;?></div>
        </div>
    </div>
</div>
<?php get_search_form(); ?>