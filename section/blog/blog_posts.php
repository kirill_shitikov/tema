<?php
wp_reset_postdata();

$blogTitle = get_field('blog_header_title', 'option');
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$posts =  array(
    's'				  =>get_search_query(),
    'showposts'		  => 10,
    'orderby'         => 'post_date',
    'order'           => 'DESC',
    'post_type'       => 'blog',
    'paged'			  => $paged
);
?>

<div id="blogSearchContent">
    <div class="fblog_list center">
        <?php
        query_posts($posts);

        if (have_posts())  {
            while (have_posts()) : the_post();

                $content = get_the_content();
                ?>

                <div class="blogSearchDivider">
                    <div class="title"><a href="<?php echo get_permalink(); ?>" rel="tab"><?php the_title(); ?></a></div>
                    <div class="clearfix"></div>
                    <div class="subtitle"><?= get_post_time('F j, Y'); ?> by <?php the_author();?></div>
                    <div class="clearfix"></div>
                    <div class="description">
                        <?=truncate($content, $length = 300); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="button_wrapper">
                        <a class="button" href="<?php the_permalink(); ?>" rel="tab">Read More</a>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php } ?>


        <div class="pagination">
            <div class="alignlefts">
                <?php previous_posts_link( __( '&larr; Newer entries', 'Circlevision_agent' )); ?>
            </div>
            <div class="alignrights">
                <?php next_posts_link( __( 'Older entries &rarr;', 'Circlevision_agent')); ?>
            </div>
        </div><!--end pagination-->

    </div>
</div>


<?php get_template_part('section/blog/blogfooter'); ?>
