<?php 
get_header(); 
$mailTitle = get_field('mail_title', 'options');
$compName = get_field('mail_form_company_name', 'options');
$compContact = get_field('mail_form_contact', 'options');
$compMail = get_field('mail_form_email', 'options');
$compAdd = get_field('mail_form_company_address', 'options');

$arrow = wp_get_attachment_image_src((get_field('mail_arrow', 'options')),'full');		
?>
<div class="container-fluid emailBottom content-hidden" id="main-view">
	<div class="container emailBottomContainer smallScreen" id="mailSections">
    	<div class="row rowemailTitle">
        	<div class="col-md-12 mailTitle"><?=$mailTitle;?></div>
        </div>
        <div class="row rowMailContent">
        	<div class="col-md-6">
            	<div class="col-md-12 emailCompName"><?=$compName;?></div>
                <div class="col-md-12 compAdd"><span class="glyphicon roundBorder"><img src="<?php bloginfo('template_url')?>/images/locationsymbol.png" /></span> <?=$compAdd;?></div>
                <div class="col-md-12 compContact"><span class="glyphicon roundBorder"><img src="<?php bloginfo('template_url')?>/images/phonesymbol.png" /></span> <?=$compContact;?></div>
                <div class="col-md-12 compeMail"><span class="glyphicon"><img src="<?php bloginfo('template_url')?>/images/mail2.png" width="24" height="20"/></span> <?=$compMail;?></div>
            </div>
            <div class="col-md-6">
            	 <div class="formMail">
                    <div id="success" class="message_send d_none">thank you <br /> message has been sent</div>
                    <div id="check" class="message_send d_none"><span style="color:red">Check <br /> the captcha, please</span></div>
                     <form action="" enctype="text/plain" method="post" id="_form">
                        <input type="text" name="name" class="name" placeholder="NAME" required="">
                        <input type="text" name="phone" class="phone" placeholder="PHONE NUMBER" required="">
                        <input type="text" name="email" class="email" placeholder="E-MAIL" required="">
                        <textarea placeholder="MESSAGE" name="message" class="message" required=""></textarea>
						<div class="g-recaptcha" data-sitekey="6LchBhQTAAAAAD8XhK5EVTqUPYq45g3euNYDDmAm"></div>
                        <input type="submit" name="send_message" class="send_message" value="SEND MESSAGE">
                    </form>
                    <?php// echo do_shortcode('[contact-form-7 id="105" title="Contact form 1"]')?>
                </div>
            </div>
        </div>
    </div>
</div>  
<?php
?>
<script>
jQuery(document).ready(function() {
	jQuery('.send_message').click(function() {
		
		var name = jQuery('.name').val();
		var reg_ex_n = (/^[^-\s0-9`~!@#№$%^&*()_=+\\|\[\]{};:',.<>\/?]+$/g);
		
		var phone = jQuery('.phone').val();
		
		var email = jQuery('.email').val();
		var reg_ex_e = (/.+@.+\..+/g);
		
		var validation = jQuery('.g-recaptcha-response').val();

		var mails = [];
		jQuery('.con_block .contacts_email').each(function() {
			var mail = jQuery(this).find('a').text();
			mails.push(mail);
		})
		
		var message = jQuery('.message').val();
		if(name.length>=3){
			if(reg_ex_e.test(email) && email.length>=3){
				if(phone.length>=3){
					if(message.length>=3){
						// jQuery.post('/wp-content/themes/Circlevisionsagent/mail.php',{name: name,phone: phone,email: email,message: message,mails: mails,capcha: validation}, function(data) {
						// 	jQuery('.message_send').removeClass('d_none');
						// 	jQuery('#_form').addClass('d_none');
						// 	jQuery( ".result" ).html( data );
						// 	//alert('sss');
						// });

						jQuery.ajax({
						  type: 'POST',
						  url: '/wp-content/themes/Circlevisionsagent/mail.php',
						  data: 'name='+name+'&phone='+phone+'&email='+email+'&message='+message+'&mails='+mails+'&capcha='+validation+'',
						  success: function(data){
						       var json_x = jQuery.parseJSON(data);
						       console.log(json_x.success);
						       if(json_x.success === true) {
									jQuery('#success').removeClass('d_none');
									jQuery('#check').addClass('d_none');
									jQuery('form#_form').remove();
							    }
						       if(json_x.success === false) {
									jQuery('#check').removeClass('d_none');
									// jQuery('#success').remove('form#_form');

							    }

						    // jQuery('.result').html(data);
						  }
						});
					} else {
						jQuery('input').removeClass('border_error');
						jQuery('.message').addClass('border_error');
					}
				} else {
					jQuery('input').removeClass('border_error');
					jQuery('.phone').addClass('border_error');
				}
			}else {	
				jQuery('input').removeClass('border_error');
				jQuery('.email').addClass('border_error');
			}	
		}else {
			jQuery('input').removeClass('border_error');
			jQuery('.name').addClass('border_error');
		}
			return false;
	})	
})
</script>