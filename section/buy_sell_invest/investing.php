<?php 
get_header(); 

$headerTitle = get_field('invest_header_title', 'option');
$shortContent = get_field('invest_short_description', 'option');
$fullContent = get_field('invest_full_description', 'option');
?>
<div class="container-fluid investPageSection content-hidden" id="main-view">
	<div class="container investPageContainer smallScreen">
    	<div class="row rowPaddingTopBottom">
        	<div class="col-md-12 investTitle"><?=$headerTitle;?></div>
        </div>
        <div class="row">
            <div class="col-md-12 investSubTitle"><?=$shortContent;?></div>
        </div>
         <div class="row">
            <div class="col-md-12 investFullDesc"><?=$fullContent;?></div>
        </div>
    </div>
</div>    