<?php 
get_header(); 

$headerTitle = get_field('sell_header_title', 'option');
$shortContent = get_field('sell_short_description', 'option');
$rightContent = get_field('sell_description', 'option');
$fullContent = get_field('sell_full_description', 'option');
?>
<div class="container-fluid sellingPageSection content-hidden" id="main-view">
	<div class="container sellingPageContainer smallScreen">
    	<div class="row rowPaddingTopBottom">
        	<div class="col-md-12 sellingTitle"><?=$headerTitle;?></div>
        </div>
        <div class="row">
            <div class="col-md-12 sellingSubTitle"><?=$shortContent;?></div>
        </div>
        <div class="row">
            <div class="col-md-12 sellingRighDesc"><?=$rightContent;?></div>
        </div>
         <div class="row">
            <div class="col-md-12 sellingFullDesc"><?=$fullContent;?></div>
        </div>
    </div>
</div>    