<?php 
get_header(); 

$headerTitle = get_field('buy_header_title', 'option');
$shortContent = get_field('buy_short_description', 'option');
$rightContent = get_field('buy_description', 'option');
$fullContent = get_field('buy_full_description', 'option');
?>
<div class="container-fluid buyingPageSection content-hidden" id="main-view">
	<div class="container buyingPageContainer smallScreen">
    	<div class="row rowPaddingTopBottom">
        	<div class="col-md-12 buyingTitle"><?=$headerTitle;?></div>
        </div>
        <div class="row">
            <div class="col-md-12 buyingSubTitle"><?=$shortContent;?></div>
        </div>
        <div class="row">
            <div class="col-md-6 buyingCal"><div class="calculator"><?php get_template_part('section/mortgagecal'); ?></div></div>
            <div class="col-md-6 buyingRighDesc"><?=$rightContent;?></div>
        </div>
         <div class="row topbottomPadding">
            <div class="col-md-12 buyingFullDesc"><?=$fullContent;?></div>
        </div>
    </div>
</div>    