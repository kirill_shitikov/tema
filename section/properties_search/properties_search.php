<?php 
get_header(); ?>
<?php
$title = get_field('pros_title', 'options');
?>
<div class="container-fluid prosearchSection content-hidden" id="main-view">
	<div class="container prosearchContainer smallScreen">
    	<div class="row rowPaddingTopBottom">
        	<div class="col-md-12 prosearchTitle"><?=$title;?></div>
        </div>
        <div class="row">
            <div class="col-md-12 normalProSearch">
			<?php 
				if (have_posts()) : while (have_posts()) : the_post(); 
			
			?>                                                 		
				  <div class="entry-single"><?php the_content(); ?></div>
			<?php endwhile; endif; //} ?>
            </div>
            <div class="col-md-12 resultSearch">
            	<div class="col-md-12" id="prosContent">
                	<div id="loadingFunction" class="col-md-12 loadingShow">
                      <img id="loading-image" src="<?php bloginfo('template_url')?>/images/ajax-loader.gif" alt="Loading..."/>
                    </div>
                </div>	
            </div>
        </div>
    </div>
</div>    