<?php 
get_header(); 

$neighborUrl = get_field('neighbor_url', 'options');
?>
<div class="container-fluid communityPageSection content-hidden" id="main-view">
	<div class="container communityPageContainer">
    	<div class="row">
        	<div class="col-md-12">
				<iframe id="neighboriFrame" src="<?=$neighborUrl;?>" scrolling="true" frameborder="0" style="width:101%;" height="0"></iframe>
            </div>
        </div>
    </div>
</div>    