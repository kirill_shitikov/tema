 <?php
	$agentLogo = wp_get_attachment_image_src((get_field('agent_logo', 'options')),'full');
	$agentImg = wp_get_attachment_image_src((get_field('agent_image', 'options')),'full');
	$agentName = get_field('agent_name', 'options');
    $agentUrl = get_field('agent_url', 'options');
 ?>
<header class="header-top">
        <div class="mobile-menu">
            <div></div>
            <div></div>
            <div></div>
        </div>
        <!--<div class="mobile-menu-close"></div>-->
    <a href="<?php echo home_url(); ?>/<?=$agentUrl?>" >
        <div class="site-logo col-md-12">
            <div class="agentLogo">
            	<img src="<?=$agentLogo[0];?>" />
            </div>
        </div>
    </a>
       <div class="mobile-leftclose mobile-menu-closeleft">
        	<img src="<?php bloginfo('template_url')?>/images/quit.png"/>
        </div>
	</header>
    
    <!-- style="transform: matrix(1, 0, 0, 1, 0, 0); visibility: inherit; opacity: 1;"-->
    <aside class="nav-sidebar" id="navSiteBar" style="transform: matrix(1, 0, 0, 1, 0, 0); visibility: inherit; opacity: 1; overflow: hidden;">
        <div class="nav-thumb" style="visibility: inherit; opacity: 0; transform: matrix(1, 0, 0, 1, 169, 292);"></div>
        <nav style="">
        	<ul class="nav-main">
        
        	<?php
				/*$menu = array(
					'theme_location'  => 'menu',
					'container' => false,
					'menu_class' => 'nav-main',
					
					'depth' => 0
				);
				wp_nav_menu( $menu ); */

			?>
        <?php wp_nav_menu( array( 'container' => '', 'menu_class' => 'nav-main', 'menu_id' => 'nav-main', 'items_wrap' => '%3$s', 'link_before' => '<div class="vertical-menu"><span>', 'link_after' => '</span></div>',) ); ?>
        
            
            
        </ul>       
        </nav>
        
    </aside>