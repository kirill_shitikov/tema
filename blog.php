<?php
/*
	Template Name: Template - Blog
*/
get_header();
?>
<body class="intro-done is-ready">
<div id="container" class="app-started">
    <!-- Menu -->
    <?php get_template_part('menu'); ?>
    <!-- Head -->
    <?php get_template_part('section/topsimpleheader'); ?>

    <div class="container-fluid blogPageSection content-hidden" id="main-view">
        <div class="container blogPageContainer">
            <!--Content Page-->
            <?php get_template_part('section/blog/blog_page'); ?>

            <?php get_template_part('section/blog/blog_posts'); ?>
        </div>
    </div>
</div>
<a id='backTop'>Back To Top</a>
</body>
</html>
