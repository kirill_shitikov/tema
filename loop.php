<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="post-header">
				<h2 class="post-title">
					<a href="<?php the_permalink(); ?>" rel="bookmark">
					  <?php the_title(); ?>
					</a>
				</h2>
				<div class="post-meta">
					posted by
					<?php the_author_posts_link(); ?> 
					on
					<?php the_date(); ?>
				</div>

				<?php if ( has_post_thumbnail() ) { ?>
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail(); ?>
					</a>
				<?php } ?>
			</div><!--end post-header-->
			<div class="entry mg">
				<?php //the_content('Continue Reading', true); ?>
				<?php the_content('Continue reading ' . the_title('"', '" &raquo;', false)); ?>
				<?php //the_content( __( 'Read more', 'lithespeed' )); ?>
				<?php edit_post_link( __( 'Edit this', 'Circlevision_agent' ), '<p>', '</p>' ); ?>
			</div><!--end entry-->
			<div class="post-footer">
				<p><?php comments_popup_link( __( 'Leave a comment', 'Circlevision_agent' ), __( '1 Comment', 'Circlevision_agent' ), __( '% Comments', 'Circlevision_agent' ) ); ?></p>
				<div class="tags">
					<?php the_tags( __( 'Tags: ', 'Circlevision_agent' ), ', ', '' ); ?>
				</div>
			</div><!--end post-footer-->
		</div><!--end post-->
	<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
		<div class="pagination index">
			<div class="alignleft">
				<?php previous_posts_link( __( '&larr; Newer entries', 'Circlevision_agent' )); ?>
			</div>
			<div class="alignright">
				<?php next_posts_link( __( 'Older entries &rarr;', 'Circlevision_agent' )); ?>
			</div>
		</div><!--end pagination-->
	<?php else : ?>
<?php endif; ?>